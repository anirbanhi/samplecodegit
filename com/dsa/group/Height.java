package com.dsa.group;


public class Height {
    public static void main(String[] args)  {
        Node r = new Node(1);
        r.left = new Node(2);
        r.right = new Node(3);
        r.right.right = new Node(4);
        r.right.right.left = new Node(5);

        Height h = new Height();
        System.out.println("Height = "+h.findHeight(r));
    }

    public int findHeight(Node h)    {
        if(h == null)
            return 0;
        int l = findHeight(h.left);
        int r = findHeight(h.right);

        return Math.max(l,r) + 1;
    }


}
