package com.shopping.site.main;

import com.shopping.site.account.Seller;
import com.shopping.site.enums.OrderStatus;
import com.shopping.site.enums.ProductType;
import com.shopping.site.enums.ShipmentStatus;
import com.shopping.site.order.Order;
import com.shopping.site.order.Shipment;
import com.shopping.site.product.Product;
import com.shopping.site.product.SellerProduct;

import java.util.List;
import java.util.Map;

public class ShoppingSite {
    Map<ProductType, List<Product>> productTypeProductListMap;
    Map<ProductType, List<SellerProduct>> productTypeSellerProductListMap;
    Map<OrderStatus, List<Order>> orderStatusListMap;
    Map<ShipmentStatus, List<Shipment>> shipmentStatusListMap;
}
