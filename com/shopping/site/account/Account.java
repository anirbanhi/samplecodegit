package com.shopping.site.account;

public interface Account {
    void browseProductTypes();
    void browseProducts();
}
