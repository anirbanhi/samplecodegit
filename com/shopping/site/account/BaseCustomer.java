package com.shopping.site.account;

import com.shopping.site.order.ShoppingCart;
import com.shopping.site.product.SellerProduct;

public abstract class BaseCustomer extends  BaseAccount implements Account   {
    ShoppingCart shoppingCart = new ShoppingCart();

    void addOrUpdateProductsInShoppingCart(SellerProduct sp, int quantity)    {
        shoppingCart.addOrUpdateProductToCart(sp,quantity);
    }

    void viewShoppingCart() {
        shoppingCart.showItems();
    }
}
