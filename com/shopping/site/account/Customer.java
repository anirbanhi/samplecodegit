package com.shopping.site.account;

import com.shopping.site.enums.OrderStatus;
import com.shopping.site.main.ShoppingSite;
import com.shopping.site.order.Order;
import com.shopping.site.order.ShoppingCart;
import com.shopping.site.payment.Payment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.ObjDoubleConsumer;

public class Customer extends BaseCustomer {
    List<Payment> paymentModes = new ArrayList<>();
    List<Order> orderList = new ArrayList<>();

    void placeOrder()   {
        Order order = new Order();
        orderList.add(order);
        Random rnd = new Random(paymentModes.size());
        shoppingCart.checkOut(paymentModes.get(rnd.nextInt()));
        order.setOrderItemSet(shoppingCart.getItemSet());
        order.setOrderStatus(OrderStatus.OrderPlaced);
    }

    void changePassword()   {

    }

    void updateDetails()    {

    }
}
