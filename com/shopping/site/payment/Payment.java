package com.shopping.site.payment;

public interface Payment {
    boolean makePayment(double amnt);
}
