package com.shopping.site.payment;

public class Upi implements Payment {

    @Override
    public boolean makePayment(double amnt) {
        System.out.println("Paid "+amnt+" via "+" Upi with upiid as ");
        return false;
    }
}
