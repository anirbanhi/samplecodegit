package com.shopping.site.product;

import com.shopping.site.account.Seller;
import com.shopping.site.enums.ProductType;

public class Product {
    String productId;
    ProductType productType;
    int quantity;
    boolean isAvailable;
    String productName;
}
