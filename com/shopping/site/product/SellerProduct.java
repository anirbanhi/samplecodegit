package com.shopping.site.product;

import com.shopping.site.account.Seller;

import java.util.Date;

public class SellerProduct extends Product {
    String sellerProductId;
    float price;
    int availableQuantity;
    Seller seller;
    Date itemListedOn;
    ProductReview productReview;

    public void reduceAvailableQuanitity(int q)  {
        if(q > availableQuantity)
            throw new RuntimeException("Can't reduce more from available");
        else
            availableQuantity -= q;
    }

    public float getPrice() {
        return price;
    }

    public String getSellerProductId() {
        return sellerProductId;
    }

    public int getAvailableQuantity() {
        return availableQuantity;
    }
}
