package com.shopping.site.enums;

public enum ProductType {
    food,
    vegetables,
    apparel,
    footwear,
    electronics,
    electrical,
}
