package com.shopping.site.enums;

public enum ShipmentStatus {
    Shipped,
    OutForDelivery,
    Delivered,
}
