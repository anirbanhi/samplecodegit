package com.shopping.site.enums;

public enum OrderStatus {
    Initiated,
    Payed,
    OrderPlaced,
    Cancelled,
}
