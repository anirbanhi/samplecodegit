package com.shopping.site.notification;

public interface Notification {
    void sendNotification(String msg);
}
