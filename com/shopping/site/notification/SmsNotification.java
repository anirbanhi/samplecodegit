package com.shopping.site.notification;

public class SmsNotification implements Notification    {
    private static class SmsNotificationHelper {
        private static final
        SmsNotification smsNotification = new SmsNotification();
    }

    private SmsNotification()   {}

    public static SmsNotification getInstance()    {
        return SmsNotificationHelper.smsNotification;
    }

    @Override
    public void sendNotification(String msg) {
        System.out.println("Sms ---- "+msg);
    }
}
