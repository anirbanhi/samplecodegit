package com.shopping.site.notification;


public class EmailNotification implements Notification  {
    @Override
    public void sendNotification(String msg) {
        System.out.println("Email ---- "+msg);
    }
    private EmailNotification() {}

    private static class EmailNotificationHelper    {
        private static final EmailNotification
         emailNotification = new EmailNotification();
    }

    public static EmailNotification getInstance()   {
        return EmailNotificationHelper.emailNotification;
    }
}
