package com.shopping.site.order;

import com.shopping.site.account.Account;
import com.shopping.site.payment.Payment;
import com.shopping.site.product.SellerProduct;

import java.util.*;
import java.util.stream.Collectors;

public class ShoppingCart {
    Account account;
    Set<Item> itemSet = new HashSet<>();

    public void checkOut(Payment p) {
        showItems();
        validateAndUpdateShoppingCart();
        showItems();
        double amount = getTotalAmount();
        System.out.println("Total amount is "+amount);
        boolean paymentStatus = p.makePayment(amount);
        if(paymentStatus)
            reduceSellerProducts();
    }

    public boolean reduceSellerProducts()   {
        itemSet.stream().forEach(item ->
                item.sellerProduct.reduceAvailableQuanitity(item.quantity));
        return true;
    }

    public void addOrUpdateProductToCart(SellerProduct sp, int quantity)  {
        if(sp.getAvailableQuantity() < quantity)
            throw new RuntimeException("Quantity not available");
        Optional<Item> isProdExist = itemSet.stream().
                findAny().filter(item -> item.productId == sp.getSellerProductId());
        if(quantity > 0) {
            if (isProdExist.isPresent())
                isProdExist.get().quantity = quantity;
            else
                itemSet.add(new Item(sp, quantity));
        }
        else    {
            if (isProdExist.isPresent())
                itemSet.remove(isProdExist.get());
        }
    }

    public void showItems() {
        System.out.println("------ displaying shopping cart ------");
        itemSet.stream().forEach(item -> System.out.println(item));
        System.out.println("---------------------------------");
    }

    public void validateAndUpdateShoppingCart() {
        Set<Item> items = itemSet.stream().
                filter(item -> item.isThisItemAvailable()).
                collect(Collectors.toSet());
        this.itemSet = items;
    }

    public double getTotalAmount()   {
        double total = 0.0d;
        total = itemSet.stream().
                mapToDouble(value -> value.price * value.quantity).
                sum();
        return total;
    }
    public Account getAccount() {
        return account;
    }

    public Set<Item> getItemSet() {
        return itemSet;
    }
}
