package com.shopping.site.order;

import com.shopping.site.enums.OrderStatus;
import com.shopping.site.notification.EmailNotification;
import com.shopping.site.notification.Notification;

import java.util.HashSet;
import java.util.Set;

public class Order {
    Set<Notification> notifications = new HashSet<>();
    OrderStatus orderStatus;
    Set<Item> orderItemSet = new HashSet<>();

    public Order()  {
        this.orderStatus = OrderStatus.Initiated;
        addNotification(EmailNotification.getInstance());
    }

    public void addNotification(Notification n) {
        notifications.add(n);
    }

    public void setOrderItemSet(Set<Item> orderItemSet) {
        this.orderItemSet = orderItemSet;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
        int orderid = this.hashCode();
        String text = "Your Order "+orderid+" is at status "+orderStatus;
        notifications.forEach(
                notification -> notification.sendNotification(text));
    }
}
