package com.shopping.site.order;

import com.shopping.site.product.SellerProduct;

public class Item {
    String productId;
    int quantity;
    float price;
    SellerProduct sellerProduct;

    public Item(SellerProduct sp, int quantity)   {
        this.productId = sp.getSellerProductId();
        this.quantity = quantity;
        this.price = sp.getPrice();
        this.sellerProduct = sp;
    }

    public boolean isThisItemAvailable()    {
        return this.quantity < this.sellerProduct.getAvailableQuantity() ?
                true : false;
    }
    @Override
    public String toString() {
        return "Item{" +
                "productId='" + productId + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
