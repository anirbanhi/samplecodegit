package com.apal.stack;

import java.util.Stack;

public class BalancedParenthesis {
    public static void main(String[] args)  {
        String exp = "{[({})]}]";
        char[] exp2 = exp.toCharArray();
        Stack<Character> stack = new Stack<Character>();
        for(int i=0;i<exp2.length;i++){
            char ch = exp2[i];
            if(ch=='(' || ch=='{' || ch=='[')
                stack.push(ch);
            else
            {
                if(ch==')' || ch=='}' || ch==']'){
                    if(ch==')' && !stack.isEmpty() && stack.peek()=='(')
                    {
                        stack.pop();
                        continue;
                    }
                    if(ch=='}' && !stack.isEmpty() && stack.peek()=='{')
                    {
                        stack.pop();
                        continue;
                    }
                    if(ch==']' && !stack.isEmpty() && stack.peek()=='[')
                    {
                        stack.pop();
                        continue;
                    }
                    System.out.println("Unmatched");
                    return;
                }
            }
        }
        if(stack.isEmpty()) {
            System.out.println("Parenthesis are balanced");
        }   else
            System.out.println("Parenthesis are not balanced");

    }
}
