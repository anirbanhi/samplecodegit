package com.apal.dynamicprogramming;

import java.util.*;

/*
https://www.geeksforgeeks.org/subset-sum-problem-dp-25/
*/

public class SubsetSum {
    public static void main(String[] args) {
        List<Integer> l = new ArrayList<>();
        l.add(3);l.add(4);l.add(5);l.add(2);
        int[] visited = new int[l.size()];
        int target = 9;
        System.out.println(isSum(l,target,0,visited));
        System.out.println(isSumDP(l,l.size()-1,target));
        int[][] dp = new int[l.size()][target+1];
        for(int[] a : dp)
        Arrays.fill(a,-1);
        System.out.println(isSumDP2(l,0,target,dp) == 1 ? true : false);
    }

    public static boolean isSum(List<Integer> list, int target,
                         int indx, int[] visited)  {
        if(indx >= visited.length)
            return false;
        if(visited[indx] == 1)  return false;
        if(target == list.get(indx))    return true;
        if(target < list.get(indx)) {
            visited[indx] = 1;
            return isSum(list,target,indx+1,visited);
        }
        int[] v = Arrays.copyOf(visited, visited.length);
        v[indx] = 1;
        boolean takeit = isSum(list,target,indx+1,v);
        boolean donttakeit =
                isSum(list,target-list.get(indx),indx+1,v);
        return takeit || donttakeit;
    }

    public static boolean isSumDP(List<Integer> l, int n, int target)  {
        if(n < 0)   return false;
        if(target < 0)  return false;
        if(l.get(n) == target)  return true;
        boolean takeit = isSumDP(l,n-1,target);
        boolean donttakeit = isSumDP(l,n-1,target-l.get(n));
        return takeit || donttakeit;
    }

    public static int isSumDP2(List<Integer> l, int n,
                                   int target, int[][] dp)  {
        if(n >= l.size())   return 0;
        if(target < 0)  return 0;
        if(l.get(n) == target)  return 1;
        if(dp[n][target] == -1) {
            boolean takeit = isSumDP2(l, n + 1, target, dp)
                    == 1 ? true : false;
            boolean donttakeit = isSumDP2(l, n + 1, target - l.get(n), dp)
                    == 1 ? true : false;
            dp[n][target] =  (takeit || donttakeit) == true ? 1 : 0;
        }
        return dp[n][target];
    }
}