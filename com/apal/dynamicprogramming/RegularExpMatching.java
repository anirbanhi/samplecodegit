package com.apal.dynamicprogramming;

/*
    https://leetcode.com/problems/regular-expression-matching/
*/

import java.util.HashMap;
import java.util.Map;

public class RegularExpMatching {

    public static void main(String[] args) {
        new RegularExpMatching().driver();
    }

    private void driver()   {
        System.out.println(" true "+isMatch("aa","a*"));
    }

    public boolean isMatch(String s, String p) {
        Map<String,Boolean> dp = new HashMap();
        return matchRegularExp(s,p,0,0,dp);
    }

    private boolean matchRegularExp(String s, String p, int i, int j,Map<String,Boolean> dp)   {
        //base case, when regular exp has matched.
        if(i>=s.length() && j>=p.length())
            return true;

        //regular expression has finished
        if(j>=p.length())
            return false;
        String key = i+"-"+j;
        if(dp.containsKey(key))
            return dp.get(key);

        boolean match = i<s.length() &&( s.charAt(i) == p.charAt(j) || p.charAt(j) == '.');

        //if next char in regular exp is a *
        if((j+1 < p.length()) && (p.charAt(j+1) == '*')) {
            boolean takeStar = false;
            boolean skipStar = false;
            //take 2 decision. either take current char or skip it
            if(match)
                takeStar = matchRegularExp(s, p, i + 1, j,dp);
            skipStar = matchRegularExp(s, p, i, j + 2,dp);
            boolean res = takeStar || skipStar;
            dp.put(key,res);
            return res;
        }

        //single character matching
        if(match) {
            dp.put(key, matchRegularExp(s, p, i + 1, j + 1,dp));
            return dp.get(key);
        }

        dp.put(key, false);
        return false;
    }



}