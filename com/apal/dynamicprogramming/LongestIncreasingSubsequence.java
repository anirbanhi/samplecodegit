package com.apal.dynamicprogramming;

public class LongestIncreasingSubsequence {
    public static void main(String[] args)  {
        LongestIncreasingSubsequence liss = new LongestIncreasingSubsequence();
        int[] num = new int[]{4,1,2,6,10,1,12};
        System.out.println("Longest increasing subsequence is " +
                liss.longestIncreasingSubsequence(num,-1,0));
        System.out.println("Maximum Sum increasing subsequence is " +
                liss.maximumSumIncreasingSubsequence(num,-1,0));
    }

    public int longestIncreasingSubsequence(int[] num, int last, int cur)   {
        if(cur == num.length)
            return 0;
        int c1 = 0;
        if(last == -1 || num[last] < num[cur] )
        {
            System.out.println(cur);
            c1 = 1 + longestIncreasingSubsequence(num,cur,cur+1);
        }
        int c2 = longestIncreasingSubsequence(num,last,cur+1);
        if(c1 > c2)
            System.out.println("--"+cur);
        return Math.max(c1,c2);
    }

    public int maximumSumIncreasingSubsequence(int[] num, int last, int cur)   {
        if(cur == num.length)
            return 0;
        int c1 = 0;
        if(last == -1 || num[last] < num[cur] )
            c1 = num[cur] + maximumSumIncreasingSubsequence(num,cur,cur+1);
        int c2 = maximumSumIncreasingSubsequence(num,last,cur+1);
        return Math.max(c1,c2);
    }

}
