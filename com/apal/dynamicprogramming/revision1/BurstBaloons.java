package com.apal.dynamicprogramming.revision1;

/*
https://leetcode.com/problems/burst-balloons/
 */

public class BurstBaloons {

    public static void main(String[] args) {
        new BurstBaloons().driver();
    }

    public void driver()    {
        int[] nums = {3,1,5,8};
        
    }

    public int maxCoins(int[] nums) {
        int[] numsnew = new int[nums.length+2];
        numsnew[0] = numsnew[numsnew.length-1] = 1;
        for(int i=0;i<nums.length;i++)
            numsnew[i+1] = nums[i];

        int max=Integer.MIN_VALUE;
        for(int i=1;i<=nums.length;i++)  {
            int cnt = maxCoinRec(numsnew, 1,i,nums.length);
            if(cnt > max)
                max = cnt;
        }
        return max;
    }

    public int maxCoinRec(int[] nums, int st, int mid, int end)   {

        int cost = nums[mid] * nums[mid-1] * nums[mid+1];
        return 1;
    }

}