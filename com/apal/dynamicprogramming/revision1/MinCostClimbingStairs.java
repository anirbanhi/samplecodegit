package com.apal.dynamicprogramming.revision1;

/*
https://leetcode.com/problems/min-cost-climbing-stairs/
 */

import java.util.Arrays;

public class MinCostClimbingStairs {

    public static void main(String[] args) {
        new MinCostClimbingStairs().driver();
    }

    public void driver()    {
        int[] cost = {10, 15, 20};
        System.out.println("mincost 15 == "+minCostClimbingStairs(cost));
        int[] cost1 = {1, 100, 1, 1, 1, 100, 1, 1, 100, 1};
        System.out.println("mincost is 6 == "+minCostClimbingStairs(cost1));
    }

    public int minCostClimbingStairs(int[] cost) {
        int[] dp = new int[cost.length];
        Arrays.fill(dp, -1);
        minCostRec(0, cost, dp);
        return Math.min(dp[0], dp[1]);
    }

    public int minCostRec(int n, int[] cost, int[] dp)    {
        if(n == cost.length)  return 0;
        if(n > cost.length)    return Integer.MAX_VALUE;

        if(dp[n] == -1) {
            int p1 = Integer.MAX_VALUE, p2 = Integer.MAX_VALUE;
            p1 = minCostRec(n + 1, cost, dp);
            if (p1 != Integer.MAX_VALUE)
                p1 = p1 + cost[n];
            p2 = minCostRec(n + 2, cost, dp);
            if (p2 != Integer.MAX_VALUE)
                p2 = p2 + cost[n];
            dp[n] = Math.min(p1, p2);
        }
        return dp[n];
    }
}