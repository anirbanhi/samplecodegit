package com.apal.dynamicprogramming.revision1;

import java.util.*;

public class KnapSack {

    public static void main(String[] args) {
        new KnapSack().driver();
    }

    private void driver()    {
        int[] w = {2,3,1,4};
        int[] p = {4,5,3,7};
        int cap = 5;
        Map<Key, Integer> dp = new HashMap<>();
        System.out.println(" " + knapsack(w,p,5,0,0));
        System.out.println(" " + knapsackDP(w,p,5,0,0, dp));
        System.out.println("bottom up " + knapsackBottomUp(w, p, 5));
    }

    //this is just to get max cost of bounded knapsack, naive
    private int knapsack(int[] w, int[] p, int cap, int cur, int cost)  {
        if(cur >= w.length) return cost;
        if(cap == 0) return cost;
        int p1 = -1;
        if(cap >= w[cur])
            p1 = knapsack(w,p, cap - w[cur], cur+1, cost + p[cur]);
        int p2 = knapsack(w,p, cap , cur+1, cost );
        return Math.max(p1, p2);
    }

    //this is just to get max cost of bounded knapsack, dp
    private int knapsackDP(int[] w, int[] p, int cap, int cur,
                           int cost, Map<Key, Integer> dp)  {
        if(cur >= w.length) return cost;
        if(cap == 0) return cost;
        Key key = new Key(cur, cost);
        if(!dp.containsKey(key))    {
            int p1 = -1;
            if (cap >= w[cur])
                p1 = knapsack(w, p, cap - w[cur], cur + 1, cost + p[cur]);
            int p2 = knapsack(w, p, cap, cur + 1, cost);
            dp.put(key,Math.max(p1, p2));
        }
        return dp.get(key);
    }

    class Key   {
        int cur;
        int cost;
        public Key(int cur, int cost) {
            this.cur = cur;
            this.cost = cost;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return cur == key.cur &&
                    cost == key.cost;
        }
        @Override
        public int hashCode() {
            return Objects.hash(cur, cost);
        }
    }

    private int knapsackBottomUp(int[] w, int[] p, int cap) {
        int[][] dp = new int[w.length+1][cap+1];
        for(int i=0;i<=w.length;i++)
            Arrays.fill(dp[i], 0);

        //populate the table
        for(int r=1;r<=w.length;r++) {
            for(int c=1;c<=cap;c++) {
                if(c >= w[r-1])
                    dp[r][c] = Math.max(p[r-1]+dp[r-1][c-w[r-1]], dp[r-1][c]);
                else
                    dp[r][c] = dp[r-1][c];
            }
        }

        System.out.println();
        for(int r=0;r<=w.length;r++) {
            for(int c=0;c<=cap;c++) {
                System.out.print(dp[r][c]+" ");
            }
            System.out.println();
        }
        int profit = dp[w.length][cap];
        int capRet = cap;
        List<Integer> op = new ArrayList<>();
        for(int i=w.length-1;i>0;i--)   {
            if(profit != dp[i-1][cap])   {
                op.add(w[i]);
                profit = profit - p[i];
                cap = cap - w[i];
            }
        }
        for (int i : op)
            System.out.println(i+" == ");
        return dp[w.length][capRet];
    }

}