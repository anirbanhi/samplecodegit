package com.apal.dynamicprogramming.revision1;

import java.util.Arrays;

/*
https://leetcode.com/problems/predict-the-winner/
 */
public class PredictWinner {
    public static void main(String[] args) {
        new PredictWinner().driver();
    }
    public void driver()    {
        int[] nums = {1, 5, 2};
        System.out.println("False == "+PredictTheWinner(nums));

        int[] nums1  = {1, 5, 233, 7};
        System.out.println("True == "+PredictTheWinner(nums1));

        int[] nums2 = {0};
        System.out.println("True == "+PredictTheWinner(nums2));

        int[] nums3 = {3,7,2,3};
        System.out.println("True == "+PredictTheWinner(nums3));

    }

    public boolean PredictTheWinner(int[] nums) {
        int[][] dp = new int[nums.length][nums.length];
        for(int i=0;i<dp.length;i++)
            Arrays.fill(dp[i],-1);
        //return predict(nums,0,nums.length-1,0,0,true,dp);
        return predict(nums,0,nums.length-1,0,0,true);
    }

    public boolean predict(int[] nums, int l, int r, int ac, int bc, boolean isa)   {
        if(l>r) return ac>=bc ? true:false;
            if (isa) {
                return (predict(nums, l + 1, r, ac + nums[l], bc, false) ||
                        predict(nums, l, r - 1, ac + nums[r], bc, false));
            }
            else {
                return (predict(nums, l + 1, r, ac, bc + nums[l], true) &&
                        predict(nums, l, r - 1, ac, bc + nums[r], true));
            }
        }

    public boolean predict(int[] nums, int l, int r, int ac, int bc, boolean isa,int[][]dp)   {
        if(l>r) return ac>=bc ? true:false;

        int res = -1;
        if(dp[l][r]==-1)   {
            if (isa) {
                res = (predict(nums, l + 1, r, ac + nums[l], bc, false, dp) ||
                            predict(nums, l, r - 1, ac + nums[r], bc, false, dp)) == true ? 1 : 0;
                }
             else {
                 res = (predict(nums, l + 1, r, ac, bc + nums[l], true, dp) &&
                            predict(nums, l, r - 1, ac, bc + nums[r], true, dp)) == true ? 1 : 0;
                }
            }
                dp[l][r] = res;
                return dp[l][r]==1?true:false;
            }
}
