package com.apal.dynamicprogramming.revision1;

import java.util.Arrays;

/*
Count ways to reach the nth stair using step 1 or 2
Count ways to form 5 using 1 length tile or 2 length tile
 */
public class TotalWaysToFill {
    public static void main(String[] args) {
        new TotalWaysToFill().driver();
    }
    public void driver()    {
        int[] items = new int[]{1,2};
        int cap = 5;

        System.out.println(" total = "+noofways(cap));

        int[] dp = new int[cap+1];
        Arrays.fill(dp, -1);
        System.out.println(" total = "+noofways(cap,dp));
    }

    private int noofways(int cap)   {
        if(cap ==1 || cap ==0)
            return 1;
        if(cap == 2)
            return 2;

        return noofways(cap-1) + noofways(cap-2);
    }

    private int noofways(int cap, int[] dp)   {
        if(cap ==1 || cap ==0)
            return 1;
        if(cap == 2)
            return 2;

        if(dp[cap]!=-1)
            return dp[cap];
        else
            dp[cap] = noofways(cap-1) + noofways(cap-2);

        return dp[cap];
    }

}
