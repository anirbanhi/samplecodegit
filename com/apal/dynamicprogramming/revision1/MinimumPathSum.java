package com.apal.dynamicprogramming.revision1;

import java.util.Arrays;

/*
https://leetcode.com/problems/minimum-path-sum/
 */
public class MinimumPathSum {
    public static void main(String[] args) {
        new MinimumPathSum().driver();
    }

    public void driver()    {
        int[][] grid = new int[][]{{1,3,1},{1,5,1}, {4,2,1}};
        int[][] grid1 = new int[][]{{1,2,3}, {4,5,6}};

        System.out.println("grid sum 7 == " + minPathSum(grid));
        System.out.println("grid sum 12 == " + minPathSum(grid1));
    }

    private int minPathSum(int[][] grid)    {
        int xmax = grid.length;
        int ymax = grid[0].length;
        int[][] dp = new int[xmax][ymax];
        for(int i=0;i<xmax;i++)
            Arrays.fill(dp[i], -1);

        return minPath(grid, 0,0, dp);
    }

    private int minPath(int[][] grid, int x, int y, int[][] dp) {
        int xmax = grid.length;
        int ymax = grid[0].length;
        if(x >= xmax || y >= ymax)
            return Integer.MAX_VALUE;
        if(x == xmax-1 && y == ymax-1)
            return grid[x][y];

        if(dp[x][y] == -1) {
            int m1 = Integer.MAX_VALUE, m2 = Integer.MAX_VALUE;

            m1 = minPath(grid, x + 1, y, dp);
            m2 = minPath(grid, x, y + 1, dp);

            if (m1 != Integer.MAX_VALUE)
                m1 = m1 + grid[x][y];

            if (m2 != Integer.MAX_VALUE)
                m2 = m2 + grid[x][y];

            dp[x][y] = Math.min(m1, m2);
        }
        return dp[x][y];
    }

}