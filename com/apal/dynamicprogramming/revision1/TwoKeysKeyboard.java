package com.apal.dynamicprogramming.revision1;

/*

https://leetcode.com/problems/2-keys-keyboard/

 */

public class TwoKeysKeyboard {
    public static void main(String[] args) {
        new TwoKeysKeyboard().driver();
    }

    public void driver()    {

    }

    public int minSteps(int n) {
        return 1;
    }

    public int minStepRec(int n)    {
        if(n==1)
            return 1;
        if(n==0)
            return 0;
        if(n==2)
            return 1;
        return 1;
    }
}
