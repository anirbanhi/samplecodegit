package com.apal.dynamicprogramming.revision1;
/*
https://leetcode.com/problems/minimum-cost-for-tickets/
 */
public class MinimumCostForTickets {
    public static void main(String[] args) {
        new MinimumCostForTickets().driver();
    }

    public void driver()    {
        int[] days = {1,2,3,4,5,6,7,8,9,10,30,31};
        int[] costs = {2,7,15};
        System.out.println("17 == " + mincostTickets(days, costs));
    }

    public int mincostTickets(int[] days, int[] costs) {
        return 1;
    }

    public int mincostTicketsRec(int[] days, int[] costs, int[] left,
                                 int cur) {

        if(left[0]>0 && cur == days.length-1)
            return 0;
        if(left[1]>0 && cur == days.length-1)
            return 0;
        if(left[2]>0 && cur == days.length-1)
            return 0;

        if(left[0]-1 > 0 && cur < days.length-1)    {
            mincostTicketsRec(days, costs, left, cur);
        }
        return 1;
    }
}
