package com.apal.dynamicprogramming.revision1;
/*
https://leetcode.com/problems/minimum-number-of-refueling-stops/
 */
public class MinimumNumberofRefuelingStops {
    public static void main(String[] args) {
        new MinimumNumberofRefuelingStops().driver();
    }

    public void driver()    {

    }

    public int minRefuelStops(int target, int startFuel, int[][] stations) {
        return 1;
    }

    public int minRefuelStops(int target, int fuel, int[][] stations, int cur, int[][] dp, int st) {

        if(cur == target)   return 0;
        if(cur > target)    return Integer.MAX_VALUE;
        
        for(int i=1;i<=fuel;i++)    {

            minRefuelStops(target, fuel-i, stations,cur+i,dp, st);
        }
        return 1;
    }
}
