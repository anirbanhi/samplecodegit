package com.apal.dynamicprogramming.revision1;

/*
https://leetcode.com/problems/dungeon-game/
 */

public class DungeonGame {

    public static void main(String[] args) {
        new DungeonGame().driver();
    }

    public void driver()    {
        int[][] dungeon = new int[][]{{-2,-3,3},{-5,-10,1},{10,30,-5}};
        System.out.println(" 7 = "+calculateMinimumHP(dungeon));
    }

    public int calculateMinimumHP(int[][] dungeon) {
        int xmax = dungeon.length-1;
        int ymax = dungeon[0].length-1;
        int[][] dp = new int[xmax+1][ymax+1];

        if(dungeon[xmax][ymax] == 0)
            dp[xmax][ymax] = 1;
        else if(dungeon[xmax][ymax] < 0)
            dp[xmax][ymax] = 1 - dungeon[xmax][ymax];
        else dp[xmax][ymax] = 1;

        dungeon[xmax-1][ymax] = 5;
        dungeon[xmax][ymax-1] = 1;

        for(int r=xmax;r>=0;r--)    {
            for(int c=ymax;c>=0;c--)    {
                if(r == xmax && c == ymax)  continue;
                int hpd = Integer.MAX_VALUE;
                int hpr = Integer.MAX_VALUE;
                if(r+1 < xmax+1)
                    hpd = dp[r+1][c] - dungeon[r][c];
                if(c+1 < ymax+1)
                    hpr = dp[r][c+1] - dungeon[r][c];
                int hp = Math.min(hpd, hpr);
                if(hp <= 0)
                    dp[r][c] = 1;
                else dp[r][c] = hp;
            }
        }
        return dp[0][0];
    }

}