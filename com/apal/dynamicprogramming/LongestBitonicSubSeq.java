package com.apal.dynamicprogramming;

public class LongestBitonicSubSeq {
    public static void main(String[] args) {
        LongestBitonicSubSeq lbs = new LongestBitonicSubSeq();
        int[] a = {4,2,3,6,10,1,12};
        int[] a1 = {4,2,5,9,7,6,10,3,1};
        System.out.println("5 - "+(lbs.longestBitonic(a,0,1,true) - 1));
        System.out.println("7 - "+(lbs.longestBitonic(a1,0,1,true) - 1));
    }

    public int longestBitonic(int[] a, int prev,int indx, boolean asc ) {
        if(indx >= a.length || prev >= a.length)    return 0;
        int c1=0,c2=0,c3=0,c4=0;
        if(a[indx] > a[prev])
            c1 = 1 + longestBitonic(a,indx,indx+1,true);
        else
            c1 = 1 + longestBitonic(a,indx,indx+1,false);
        c2 = longestBitonic(a,prev,indx+1,true);
        c3 = longestBitonic(a,prev,indx+1,false);
        c4 = longestBitonic(a,prev+1,indx+1,true);
        return Math.max(Math.max(c1,c2),Math.max(c3,c4));
    }
}

