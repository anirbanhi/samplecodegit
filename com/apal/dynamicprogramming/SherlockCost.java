package com.apal.dynamicprogramming;

import java.util.HashMap;

public class SherlockCost {
    public static void main(String[] args)  {
        int[] b = {1,2,3};
        int[] a = {1,2,3};
        int[] b1 = {10, 2, 10, 2, 10};
        int[] a1 = {100, 2, 100, 2, 100};
        int[] a2 = {3, 15, 4, 12, 10};
        int[] a3 = {4,7,9};

        SherlockCost slc = new SherlockCost();
        //slc.findMax(a1,0);
        slc.findMaxCopy(a3);
        System.out.println("Max sum "+slc.max);
    }

    public void findMaxCopy(int[] B)   {
        String answer = "";
        int one = 0;
        int max = 0;
        for (int i = 1; i < B.length; i++) {
            int tempOne = Math.max(one, max + Math.abs(B[i - 1] - 1));
            max = Math.max(one + Math.abs(B[i] - 1), max + Math.abs(B[i] - B[i - 1]));
            one = tempOne;
        }
        answer += Math.max(max, one) + "\n";
        System.out.println(answer);
    }

    public void findMax(int[] a, int index)    {
        if(index >= a.length)
            return;
        int sum = findSum(a);
        max = Math.max(sum,max);

        findMax(a,index+1);
        int temp = a[index];

        a[index] = 1;
        findMax(a,index+1);
        a[index] = temp;
    }

    int max = 0;
    public void allCombo(int[] a, int index)   {
        if(index >= a.length)
            return;
        int sum = 0;
        sum = findSum(a);
        max = Math.max(max,sum);
        int l = a[index];
        for(int i=1;i<=l;i++)
        {
            a[index] = i;
            allCombo(a,index+1);
        }
    }

    public int findSum(int[] a) {
 /*       System.out.println();
        for(int i=0;i<a.length;i++)
            System.out.print(a[i]+" , ");
        System.out.print(" == ");
*/        int sum = 0;
        for(int i=1;i<a.length;i++) {
            int x = Math.abs(a[i] - a[i-1]);
            sum += x;
        }
        //System.out.print(sum);
        return sum;
    }

    public String getKey(int[] a, int indx)  {
        String s = "";
        for(int i=0;i<a.length;i++)
            s += a[i];
        s = s+"-"+indx;
        return s;
    }
}
