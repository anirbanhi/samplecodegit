package com.apal.dynamicprogramming;
/*
Problem Statement #

Given a set of positive numbers, find if we can partition it into two subsets such that the sum of elements in both the subsets is equal.
Example 1: #

Input: {1, 2, 3, 4}
Output: True
Explanation: The given set can be partitioned into two subsets with equal sum: {1, 4} & {2, 3}

Example 2: #

Input: {1, 1, 3, 4, 7}
Output: True
Explanation: The given set can be partitioned into two subsets with equal sum: {1, 3, 4} & {1, 7}

Example 3: #

Input: {2, 3, 4, 6}
Output: False
Explanation: The given set cannot be partitioned into two subsets with equal sum.
 */


import java.util.Arrays;

public class EqualSubsetSum {

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4};
        System.out.println("true - " + equalSubSetSum(a));

        int[] a1 = {1, 1, 3, 4, 7};
        System.out.println("true - " + equalSubSetSum(a1));

        int[] a2 = {2, 3, 4, 6};
        System.out.println("false - " + equalSubSetSum(a2));
    }

    public static boolean equalSubSetSum(int[] a)   {
        int totsum = 0;
        for(int i=0;i<a.length;i++)
            totsum += a[i];
        if(totsum %2 != 0)  return false;
        boolean[][] dp = new boolean[a.length][totsum/2 +1];
        for(boolean[] row : dp)
            Arrays.fill(row, false);
        for(int i=0;i<totsum/2 +1;i++) {
            if (a[0] >= i)
                dp[0][i] = true;
        }
        for(int i=1;i<a.length;i++) {
            for(int j=0;j<totsum/2 +1;j++)  {
                if(a[i] <= j)
                    dp[i][j] = dp[i-1][j] || dp[i][j-a[i]];
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
        return dp[a.length-1][totsum/2];
    }
}