package com.apal.dynamicprogramming;

public class MinimumPush {
    public static void main(String[] args)  {
        MinimumPush mp = new MinimumPush();
        int[] num = new int[]{1,1,3,6,9,3,0,1,3};
        int[] dp = new int[num.length];
        for(int i=0;i<dp.length;i++)
            dp[i] = -1;
        System.out.println("mp.minPush() "+mp.minPush(new int[]{2,1,1,1,4},0,dp));
        System.out.println("mp.minPush() "+mp.minPush(num,0,dp));
    }

    public int minPush(int[] num, int indx, int[] dp)    {
        if(indx > (num.length - 1))
            return Integer.MAX_VALUE;
        if(num[indx] == 0)
            return Integer.MAX_VALUE;
        if(indx == (num.length -1))
            return 0;
        if (dp[indx] > 0)
            return dp[indx];
        int push = num[indx];
        int minPush = Integer.MAX_VALUE;
        for(int i=1;i<=push;i++) {
            {
                int count = minPush(num, indx+i,dp);
                if(count != Integer.MAX_VALUE)
                    count += 1;
                if(count < minPush)
                    minPush = count;
            }
        }
        dp[indx] = minPush;
        return minPush;
    }
}
