package com.apal.dynamicprogramming;

public class StealFromHouse {
    public static void main(String[] args) {
        StealFromHouse sfh = new StealFromHouse();
        int[] num = new int[]{2, 5, 1, 3, 6, 2, 4};
        int[] num1 = new int[]{2, 10, 14, 8, 1};
        System.out.println("Steal "+ sfh.findMaxStealRecursive(num, 0));
        System.out.println("Steal "+ sfh.findMaxStealRecursive(num1, 0));
    }

    public int findMaxStealRecursive(int[] wealth, int currentIndex) {
        if( currentIndex >= wealth.length)
            return 0;

        // steal from current house and skip one to steal from the next house
        int stealCurrent = wealth[currentIndex] + findMaxStealRecursive(wealth, currentIndex + 2);
        // skip current house to steel from the adjacent house
        int skipCurrent = findMaxStealRecursive(wealth, currentIndex + 1);

        return Math.max(stealCurrent, skipCurrent);
    }
}
