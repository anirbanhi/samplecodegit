package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
https://www.geeksforgeeks.org/gold-mine-problem/
 */

public class GoldMine {
    public static void main(String[] args) {
        int[][] mat =
                {
                {1, 3, 3},
                {2, 1, 4},
                {0, 6, 4}
                };
        int rmax = mat.length;
        int cmax = mat[0].length;
        //System.out.println("rmax "+rmax+" cmax "+cmax);
         dp = new int[rmax][cmax];
        for (int[] row : dp)
            Arrays.fill(row, -1);
        int c = 0;
        int[][] v = new int[rmax][cmax];
        List<Path1> path = new ArrayList<>();
        //Arrays.fill(v, 0);
        for(int i=0;i<rmax;i++) {
            c = 0;
            v[i][0] = 1;
            path.add(new Path1(i,0));
            c = findpubProfit(mat, v, rmax,cmax,i,0,path);

        }
        System.out.println("Max cost "+max);
    }

    static class Path1 {
        int r;
        int c;
        public Path1(int r1, int c1)    {
            r = r1;
            c = c1;
        }

        @Override
        public String toString() {
            return "Path1{" +
                    "r=" + r +
                    ", c=" + c +
                    '}';
        }
    }
    static int[][] dp;
    static int max = 0;
    public static int findpubProfit(int[][] g, int[][] v,
                                     int rmax, int cmax,
                                     int r, int c, List<Path1> p) {

        int r1 = 0, c1 = 0;
        int p1 = 0, p2 =0, p3 =0;
        int cost = 0;
        if(dp[r][c] == -1) {
            r1 = r;
            c1 = c + 1;
            if (isSafe(g, v, r1, c1, rmax, cmax)) {
                v[r1][c1] = 1;
                p.add(new Path1(r1, c1));
                p1 = findpubProfit(g, v, rmax, cmax, r1, c1, p);
            }

            r1 = r + 1;
            c1 = c + 1;
            if (isSafe(g, v, r1, c1, rmax, cmax)) {
                v[r1][c1] = 1;
                p.add(new Path1(r1, c1));
                p2 = findpubProfit(g, v, rmax, cmax, r1, c1, p);
            }

            r1 = r - 1;
            c1 = c + 1;
            if (isSafe(g, v, r1, c1, rmax, cmax)) {
                v[r1][c1] = 1;
                p.add(new Path1(r1, c1));
                p3 = findpubProfit(g, v, rmax, cmax, r1, c1, p);
            }

            cost = Math.max(Math.max(p1, p2), p3) + g[r][c];
            if (cost > max) {
                max = cost;
                //System.out.println("cost ["+r+"]["+c+"] =  "+cost);
                //System.out.println("path "+p.toString());
            }
            p.remove(p.size() - 1);
            v[r][c] = 0;
            dp[r][c] = cost;
        }
        return dp[r][c];
    }

    public static boolean isSafe(int[][]g, int[][] v, int r, int c,
                                 int rmax, int cmax)  {
        if(r < 0 || c < 0 || r >= rmax || c >= cmax)
            return false;
        if(v[r][c] == 1)    return false;
        return true;
    }
}
