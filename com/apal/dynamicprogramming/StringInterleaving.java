package com.apal.dynamicprogramming;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/*
Problem Statement #

Give three strings ‘m’, ‘n’, and ‘p’, write a method to find out if ‘p’ has been formed by interleaving ‘m’ and ‘n’. ‘p’ would be considered interleaving ‘m’ and ‘n’ if it contains all the letters from ‘m’ and ‘n’ and the order of letters is preserved too.

Example 1:

Input: m="abd", n="cef", p="abcdef"
Output: true
Explanation: 'p' contains all the letters from 'm' and 'n' and preserves their order too.

Example 2:

Input: m="abd", n="cef", p="adcbef"
Output: false
Explanation: 'p' contains all the letters from 'm' and 'n' but does not preserve the order.
 */
public class StringInterleaving {
    public static void main(String[] args) {
        StringInterleaving sil = new StringInterleaving();
        System.out.println(sil.interleave("abd","cef","abcdef"));
        System.out.println(sil.interleave("abd","cef","adcbef"));
    }
    public boolean interleave(String s1, String s2, String comb)    {
        if(comb.length() != (s1.length()+s2.length()))
            return false;

        return isPatSubSeq(comb,s1,0,0,new HashMap<Key,Boolean>()) &&
                isPatSubSeq(comb,s2,0,0,new HashMap<Key,Boolean>());
    }
    class Key {
        int si;
        int pi;
        public Key(int s, int p)    {
            si = s;
            pi = p;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return si == key.si &&
                    pi == key.pi;
        }

        @Override
        public int hashCode() {
            return Objects.hash(si, pi);
        }
    }
    public boolean isPatSubSeq(String s, String p, int si, int pi,
                               Map<Key, Boolean> map)    {

        if(pi == p.length())    return true;
        if(si>=s.length() || pi >= p.length())  return false;
        Key k = new Key(si,pi);
        if(Objects.isNull(map.get(k))) {
            boolean b1=false, b2=false;
            if (s.charAt(si) == p.charAt(pi)) {
                b1 = isPatSubSeq(s, p, si + 1, pi + 1, map);
            }
            b2 = isPatSubSeq(s, p, si + 1, pi, map);
            map.put(k,b1||b2);
        }
        return map.get(k);
    }
}
