package com.apal.dynamicprogramming;
/*
https://www.geeksforgeeks.org/friends-pairing-problem/
 */
public class FriendsPairing {
    public static void main(String[] args) {
        System.out.println(pairing(4));
        System.out.println(pairing(3));
    }

    public static int pairing(int n)    {
        if(n==1 || n==0)    return 1;
        if(n==2)    return 2;

        return pairing(n-1) + (n-1) * pairing(n-2);
    }
}
