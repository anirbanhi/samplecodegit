package com.apal.dynamicprogramming;

import java.util.Arrays;

public class PalindromePartitioning {
    public static void main(String[] args)  {
        partition("abcbm");
    }

    public static void partition(String str) {
        char[] chars = str.toCharArray();
        int n = chars.length;
        int T[][] = new int[n][n];
        for(int i=0;i<n;i++)
            Arrays.fill(T[i], Integer.MAX_VALUE);
        for(int row=0;row<n;row++)  {
            for(int col=0;col<n;col++)    {
                int l = col + row;
                if(l < n)
                {
                    System.out.print("<["+col+"]["+l+"]>, ");
                    if(col == l)
                        T[col][l] = 0;
                    else{
                        if(isPal(str, col, l))
                        {
                            T[col][l] = 0;
                        }
                        else{
                            for(int k=col;k<=l;k++){
                                System.out.print("["+col+"]["+k+"], ");
                            }
                            System.out.println();
                        }
                    }
                }
            }
            System.out.println();
        }
    }

    public static boolean isPal(String str, int r, int t)   {
        while(r < t) {
            if (str.charAt(r) != str.charAt(t)) {
                return false;
            }
            r++;
            t--;
        }
        return true;
    }
}
