package com.apal.dynamicprogramming;
/*
Problem Statement #

Given a set of positive numbers, find the total number of subsets whose sum is equal to a given number ‘S’.
Example 1: #

Input: {1, 1, 2, 3}, S=4
Output: 3
The given set has '3' subsets whose sum is '4': {1, 1, 2}, {1, 3}, {1, 3}
Note that we have two similar sets {1, 3}, because we have two '1' in our input.

Example 2: #

Input: {1, 2, 7, 1, 5}, S=9
Output: 3
The given set has '3' subsets whose sum is '9': {2, 7}, {1, 7, 1}, {1, 2, 1, 5}
 */
public class CountOfSubsetSum {
    public static void main(String[] args) {
        int[] a1 = {1, 1, 2, 3};
        System.out.println(" 3 - " +findCountOfSubsetSum(a1,4));

        int[] a2 = {1, 2, 7, 1, 5};
        System.out.println(" 3 - " +findCountOfSubsetSum(a2,9));
    }
    public static int findCountOfSubsetSum(int[] a, int sum)   {
        int cnt = 0;
        boolean[][] dp = new boolean[a.length][sum+1];
        for(int i=0;i<sum+1;i++)    {
            if(a[0] >= i )
                dp[0][i] = true;
        }
        for(int i=1;i<a.length;i++) {
            for(int j=1;j<sum+1;j++)    {
                if(a[i] <= j)   {
                    dp[i][j] = dp[i-1][j] || dp[i][j-a[i]];
                    if(j==sum && dp[i][j])  {
                        cnt++;
                        dp[i][j] = false;
                    }
                }   else    {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return cnt;
    }
}