package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/*
Backtracking 0-1 knapsack
 */
public class Knapsack01 {
    public static void main(String[] args) {
      /*int[] w = {2,3,1,4};
        int[] p = {4,5,3,7};
        int cap = 5;
       */
        int[] w = {10,20,30};
        int[] p = {60,100,120};
        int cap = 50;

        int[] v = new int[w.length];
        Arrays.fill(v, 0);
        List<Integer> l = new ArrayList<>();
        int min = Integer.MAX_VALUE;
        for(int i:w)
            min = Math.min(min, i);
        dp = new int[cap][w.length];
        comb(w,p,v,-1,0,cap,min,l,0);
        System.out.println("********* Max "+max);
        System.out.println("Callingcount "+callingcount);
    }
    static int [][] dp;
    static int max = 0;
    static int callingcount = 0;
    public static void comb(int[] w, int[] p, int[] v,
                            int indx, int wt, int cap,
                            int min, List<Integer> wts,
                            int pft)   {
            callingcount++;
            int n = w.length;
            if (cap - wt < min) {
                System.out.println("Weight " + wt + "  " +
                        String.join(" + ", wts.toString()) +
                        " Profit " + pft);
                if (pft > max) max = pft;
                return;
            }
            for (int i = indx + 1; i < n; i++) {
                if (isSafe(w, v, i, cap, wt)) {
                    v[i] = 1;
                    wts.add(w[i]);
                    comb(w, p, v, i, wt + w[i], cap, min, wts, pft + p[i]);
                    wts.remove(wts.size() - 1);
                    v[i] = 0;
                }
            }
    }

    public static boolean isSafe(int[] w, int[] v, int x,
                                 int cap, int wt)  {
        if(x<0 && x >= w.length)    return false;
        if(v[x] == 1)   return false;
        if(wt + w[x]  <= cap)   return true;
        return false;
    }
}