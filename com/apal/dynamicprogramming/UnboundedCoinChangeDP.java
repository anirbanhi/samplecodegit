package com.apal.dynamicprogramming;

public class UnboundedCoinChangeDP {

    public static void main(String[] args) {
        new UnboundedCoinChangeDP().driver();
    }

    public void driver()    {
        int[] den = {1,2,3};
        int tot = 5;
        System.out.println(noOfCoinChange(den,tot,0));
        System.out.println(cnt);
    }
    int cnt = 0;
    int noOfCoinChange(int[] d, int tot, int cur)    {
        if(tot == 0) {
            cnt++;
            return 1;
        }
        if(tot < 0 || cur >= d.length)
            return 0;
        if(tot>0 && cur>=d.length)
            return 0;

           return noOfCoinChange(d,tot-d[cur],cur+1)
                   + noOfCoinChange(d,tot,cur+1);

    }
}