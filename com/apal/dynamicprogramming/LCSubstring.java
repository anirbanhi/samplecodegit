package com.apal.dynamicprogramming;

public class LCSubstring {
    public static void main(String[] args)  {
        String s11 = "abcda";
        String s22 = "cbda";

        System.out.println("LCS DP = " + findLCS(s11,s22));
        //String s1 = "passport";
        //String s2 = "ppspt";

        String s1 = "anirbanpal";
        String s2 = "adrijapal";

        LCSubstring lcs = new LCSubstring();
        Integer[][][] dp = new Integer[s1.length()][s2.length()][Math.max(s1.length(), s2.length())];
        //int l1 = lcs.getLCSLength(s1,s2,0,0,0, dp);
        //System.out.println("getLCS Length " + l1);

        int l2 = lcs.getLCSubsequenceLength(s1,s2,0,0,0,dp);
        System.out.println("getLCSubsequence Length " + l2);

        System.out.println("\"abdca\",\"cbda\" SUBSEQ => "+findLCSubseq("abdca","cbda"));
    }

    public int getLCSLength(String s1, String s2, int i1, int i2, int count, Integer[][][] dp)   {
        if( (i1 == s1.length()) || (i2 == s2.length()) )
            return count;
        int totalcount = count;
        if(dp[i1][i2][count] == null) {
            if (s1.charAt(i1) == s2.charAt(i2)) {
                totalcount = getLCSLength(s1, s2, i1 + 1, i2 + 1, count+1, dp);
            }

            int c1 = getLCSLength(s1, s2, i1 + 1, i2, 0,dp);
            int c2 = getLCSLength(s1, s2, i1, i2 + 1, 0,dp);
            dp[i1][i2][count] = Math.max(totalcount, Math.max(c1,c2));
        }
        return dp[i1][i2][count];
    }

    public int getLCSubsequenceLength(String s1, String s2, int i1, int i2, int count, Integer[][][] dp)   {
        if( (i1 == s1.length()) || (i2 == s2.length()) )
            return count;
        if(dp[i1][i2][count] == null) {
            int c1 = 0;
            int c2 = 0;
            int totalcount = count;
            if (s1.charAt(i1) == s2.charAt(i2)) {
                totalcount = getLCSubsequenceLength(s1, s2, i1 + 1, i2 + 1, count+1,dp);
            } else {
                c1 = getLCSubsequenceLength(s1, s2, i1 + 1, i2, totalcount,dp);
                c2 = getLCSubsequenceLength(s1, s2, i1, i2 + 1, totalcount,dp);
            }
            dp[i1][i2][count] = Math.max(totalcount, Math.max(c1, c2));
        }
        return dp[i1][i2][count];
    }

    public static int findLCS(String s1, String s2) {
        int[][] dp = new int[s1.length()+1][s2.length()+1];
        for(int j=0;j<s2.length();j++) {
            if (s1.charAt(1) == s2.charAt(j))
                dp[1][j+1] = Math.max(1, dp[1][j+1-1]);
        }
        int max = 0;
        for(int i=0;i<s1.length();i++)    {
            for(int j=0;j<s2.length();j++)    {
                if(s1.charAt(i) == s2.charAt(j))    {
                    dp[i+1][j+1] = 1 + dp[i+1-1][j+1-1];
                    max = Math.max(dp[i+1][j+1], max);
                } else
                    dp[i+1][j+1] = 0;
            }
        }
        return max;
    }

    public static int findLCSubseq(String s1, String s2) {
        int[][] dp = new int[s1.length()+1][s2.length()+1];
        int max = 0;
        for(int i=1;i<s1.length();i++)    {
            for(int j=1;j<s2.length();j++)    {
                if(s1.charAt(i) == s2.charAt(j))    {
                    dp[i+1][j+1] = 1 + dp[i][j];
                    max = Math.max(dp[i+1][j+1], max);
                } else
                    dp[i+1][j+1] = Math.max(dp[i][j+1],dp[i+1][j]);
            }
        }
        return max;
    }
}