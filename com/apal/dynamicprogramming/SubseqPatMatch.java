package com.apal.dynamicprogramming;

public class SubseqPatMatch {
    public static void main(String[] args) {
        SubseqPatMatch sp = new SubseqPatMatch();
        sp.patMatch("baxmx","ax",0,0,0);
        System.out.println("2 - "+" "+sp.cnt);
        sp.cnt=0;
        sp.patMatch("tomorrow","tor",0,0,0);
        System.out.println("4 - "+" "+sp.cnt);
    }
    int cnt = 0;
    public void patMatch(String s, String p, int si, int pi, int m)   {
        if(m == p.length())
            cnt++;
        if(si >= s.length() || pi >= p.length())    return;

        int c1=0,c2=0,c3=0,c4=0;
        if(s.charAt(si) == p.charAt(pi)) {
            patMatch(s, p, si + 1, pi + 1,m+1);
        }
        patMatch(s,p,si+1,pi,m);
    }
}
