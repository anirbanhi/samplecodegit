package com.apal.dynamicprogramming;

/*

DP solution using memoization
count of calling function has come down from 24 to 6
Significant improvement.

Instead of dp[n][n] we could have used a hashmap
where key is "indx"|"indx+1" and value would be cnt

 */

import java.util.Arrays;

public class LongestIncreasingSubSeq {

    public static void main(String[] args) {
        int[] a = {4,2,3,6,10,1,12};
        int[] dp = new int[a.length];
        Arrays.fill(dp, -1);
        int res = findInc(a, 0,dp) + 1;
        System.out.println("Find Inc Sub Seq "+ res +" count = "+cnt);

        int[] a1 = {-4,10,3,7,15};
        dp = new int[a1.length];
        Arrays.fill(dp, -1);
        cnt = 0;
        res = findInc(a1, 0,dp) + 1;
        System.out.println("Find Inc Sub Seq "+ res +" count = "+cnt);
    }

    static int cnt = 0;
    public static int findInc(int[] a, int indx, int[] dp)    {
        if(indx == a.length-1)  return 0;
        if(dp[indx] == -1) {
            cnt++;
            int rec1 = 0, rec2 = 0;
            if (a[indx + 1] > a[indx])
                rec1 = findInc(a, indx + 1, dp) + 1;

            rec2 = findInc(a, indx + 1, dp);
            dp[indx] = Math.max(rec1, rec2);
        }
        return dp[indx];
    }
}