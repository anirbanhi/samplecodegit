package com.apal.dynamicprogramming;

public class LongestAlternatingSubSeq {
    public static void main(String[] args) {
        LongestAlternatingSubSeq la = new LongestAlternatingSubSeq();
        //need to add 1 fro mreturned value as our logic doesn't count
        //first number in the sequence
        int[] a = {1,2,3,4};
        System.out.println("2 - "+(la.longestAltDriver(a) + 1)+" "+la.cnt);
        int[] a1 = {3,2,1,4};la.cnt=0;
        System.out.println("3 - "+(la.longestAltDriver(a1) + 1)+" "+la.cnt);
        int[] a2 = {1,3,2,4};la.cnt=0;
        System.out.println("4 - "+(la.longestAltDriver(a2) + 1)+" "+la.cnt);
    }

    public int longestAltDriver(int[] a )   {
        if(a[1] > a[0])
            return longestAlt(a,1,0,true);
        else
            return longestAlt(a,1,0,false);
    }
    int cnt = 0;
    public int longestAlt(int[] a, int indx, int prev, boolean inc) {
        if(indx >= a.length || prev >= a.length)    return 0;
        cnt++;
        int c1=0,c2=0;
        if(inc) {
            if(a[indx] > a[prev])
                c1 = 1 + longestAlt(a,indx+1,indx,!inc);
            c2 = longestAlt(a,indx+1,prev,inc);
        } else  {
            if(a[indx] <= a[prev])
                c1 = 1 + longestAlt(a,indx+1,indx,!inc);
            c2 = longestAlt(a,indx+1,prev,inc);
        }
        return Math.max(c1,c2);
    }
}
