package com.apal.dynamicprogramming;

/*
https://www.geeksforgeeks.org/cutting-a-rod-dp-13/
 */

public class CuttingRodGFG {
    public static void main(String[] args) {
        int[] rod =   {1, 2, 3, 4, 5, 6, 7, 8};
        int[] price = {1, 5, 8, 9, 10, 17, 17, 20};
        int size = 8;
        int[] price2 = {3, 5, 8, 9, 10, 17, 17, 20};
        int[][] dp = new int[rod.length][size+1];
        cutRod(rod,price2,dp,size);
        System.out.println("Max profit = "+dp[rod.length-1][size]);
    }

    public static void cutRod(int[] rod, int[] p, int[][] dp, int size)    {
        dp[0][0] = 0;
        for(int c=1;c<=size;c++)    {
            dp[0][c] = p[0] + dp[0][c-1];
        }

        for(int i=1;i<rod.length;i++)    {
            for(int j=0;j<=size;j++)   {
                if(j >= rod[i]) {
                    dp[i][j] = Math.max(dp[i-1][j],p[i]+dp[i][j-rod[i]]);
                } else
                    dp[i][j] = dp[i-1][j];
            }
        }
    }
}