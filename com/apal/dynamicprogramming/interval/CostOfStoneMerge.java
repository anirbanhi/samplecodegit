package com.apal.dynamicprogramming.interval;

/*
https://leetcode.com/problems/minimum-cost-to-merge-stones/
 */

public class CostOfStoneMerge {
    public static void main(String[] args) {
        new CostOfStoneMerge().driver();
    }

    public void driver()    {
        int[] stones = {3,2,4,1};
        int k = 2;
        System.out.println("20 == "+mergeStones(stones,k));
    }

    public int mergeStones(int[] stones, int k) {
        return cost(stones,k,0,stones.length-1);
    }

    public int cost(int[] st, int k, int l, int r)    {
        if(l+k>r)   return Integer.MAX_VALUE;

        int min = Integer.MAX_VALUE;
        for(int i=l;i<=r-k;i=i+k)   {
            int cost = 0;
            for(int j=i;j<=i+k;j++)  {
                cost += st[j];
            }
            cost = cost + cost(st,k,l,i-1) + cost(st,k,i+k+1,r);
            System.out.printf("cost = [%d, %d] == %d \n",l,r,cost);
            if(cost < min)
                min = cost;
        }
        return min;
    }

}