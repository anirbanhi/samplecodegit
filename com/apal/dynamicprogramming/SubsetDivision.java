package com.apal.dynamicprogramming;
/*
https://www.geeksforgeeks.org/subset-sum-divisible-m/
 */
public class SubsetDivision {
    public static void main(String[] args) {
        //int[] arr = {3, 1, 7, 5};
        //int m = 10;
        int[] arr = {1, 7};
        int m = 5;
        int sum1 = 0;
        for(int i : arr)
            sum1 += i;
        Boolean[][] dp = new Boolean[arr.length+1][sum1+1];
        System.out.println("IsDiv by "+m+" "+isDiv(arr,0,m,0)
        +" count = "+cnt);
        cnt = 0;
        System.out.println("IsDiv by "+m+" "+
                isDivDP(arr,0,m,0,dp)+" count = "+cnt);
    }

    static int cnt = 0;

    public static boolean isDiv(int[] a, int indx, int div, int sum) {
        if( sum >= div && sum % div == 0)  return true;
        if(indx >= a.length)    return false;
        cnt++;
        boolean taken = isDiv(a, indx+1, div, sum+a[indx]);
        boolean ntTaken = isDiv(a, indx+1, div, sum);
        return taken || ntTaken;
    }

    public static boolean isDivDP(int[] a, int indx, int div,
                                  int sum, Boolean[][] dp) {
        if(dp[indx][sum] == null) {
            if (sum >= div && sum % div == 0) return true;
            if (indx >= a.length) return false;
            cnt++;
            boolean taken = isDivDP(a, indx + 1, div, sum + a[indx],dp);
            boolean ntTaken = isDivDP(a, indx + 1, div, sum,dp);
            dp[indx][sum] = taken || ntTaken;
        }
        return dp[indx][sum];
    }
}