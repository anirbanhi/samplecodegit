package com.apal.dynamicprogramming;

//https://www.youtube.com/watch?v=IRwVmTmN6go
//https://github.com/mission-peace/interview/blob/master/src/com/interview/dynamic/CuttingRod.java

public class CuttingRod {
    public static void main(String[] args)  {

        System.out.println(
                cuttingRod(5,
                        new int[]{1,2,3,4},
                        new int[]{2,5,7,8}));
    }

    public static int cuttingRod(int rodLength, int[] length, int[] profit) {
        int row = length.length;
        int col = rodLength + 1;

        int pT[][] = new int[row][col];
        pT[0][0] = 0;
        for(int c=1;c<col;c++)    {
            pT[0][c] = profit[0] + pT[0][c-1];
        }

        for(int i=1;i<row;i++){   //i = row
             for(int j=1;j<col;j++){    //j = column
                if(length[i] > j){
                    pT[i][j] = pT[i-1][j];  //you cant take length[i] into solution.
                }else   {
                    pT[i][j] =  Math.max(pT[i-1][j],profit[i] + pT[i][j-length[i]]);
                }
            }
        }
        print(pT, row, col);
        return pT[row-1][col-1];
    }

    public static void print(int[][] pt, int row, int col)    {
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                System.out.print(pt[i][j]+" ");
            }
            System.out.println();
        }
    }
}
