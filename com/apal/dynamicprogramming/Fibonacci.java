package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fibonacci {
    public static void main(String[] args)  {
        Fibonacci fib = new Fibonacci();
        Map<Integer, Integer> nthFib = new HashMap<>();
        long start = System.currentTimeMillis();
        System.out.println("40th Fib "+fib.nthFibonacci(40, nthFib));
        long end = System.currentTimeMillis();
        System.out.println("Time taken "+(end - start));

        long start1 = System.currentTimeMillis();
        System.out.println("40th Fib "+fib.nthFibonacciNormal(40));
        long end1 = System.currentTimeMillis();
        System.out.println("Time taken "+(end1 - start1));

        long start2 = System.currentTimeMillis();
        System.out.println("40th Fib "+fib.nthFibBottomUp(40));
        long end2 = System.currentTimeMillis();
        System.out.println("Time taken "+(end2 - start2));

    }

    public int nthFibonacci(int n, Map<Integer,Integer> nthFib)  {
        if(n==0)
            return 0;
        if(n==1)
            return 1;
        if(nthFib.containsKey(n))
            return nthFib.get(n);
        else
        {
            int nthFibComputed = nthFibonacci(n-1, nthFib) + nthFibonacci(n-2, nthFib);
            nthFib.put(n, nthFibComputed);
            return nthFibComputed;
        }
    }

    public int nthFibonacciNormal(int n)  {
        if(n==0)
            return 0;
        if(n==1)
            return 1;
        else
        {
            int nthFibComputed = nthFibonacciNormal(n-1) + nthFibonacciNormal(n-2);
            return nthFibComputed;
        }
    }

    public int nthFibBottomUp(int n)    {
        int n1, n2;
        n1 = 0; n2=1;
        int temp = 0;
        for(int i=0;i<n;i++){
            temp = n1 + n2;
            n1 = n2;
            n2 = temp;
        }
        return temp;
    }
}
