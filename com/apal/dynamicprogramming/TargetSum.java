package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.List;

/*
Problem Statement #

Given a set of positive numbers (non zero) and a target sum ‘S’. Each number should be assigned either a ‘+’ or ‘-’ sign. We need to find out total ways to assign symbols to make the sum of numbers equal to target ‘S’.
Example 1: #

Input: {1, 1, 2, 3}, S=1
Output: 3
Explanation: The given set has '3' ways to make a sum of '1': {+1-1-2+3} & {-1+1-2+3} & {+1+1+2-3}

Example 2: #

Input: {1, 2, 7, 1}, S=9
Output: 2
Explanation: The given set has '2' ways to make a sum of '9': {+1+2+7-1} & {-1+2+7+1}
 */

public class TargetSum {
    public static void main(String[] args) {
        int[] a1 = {1, 1, 2, 3};
        int t1 = 1;
        findSum(a1,t1,0,0,new ArrayList<>());
        System.out.println(" no of subset "+cnt);

        int[] a2 = {1, 2, 7, 1};
        int t2 = 9;
        cnt = 0;
        findSum(a2,t2,0,0,new ArrayList<>());
        System.out.println(" no of subset "+cnt);
    }

    static int cnt = 0;
    public static boolean findSum(int[] a, int target, int sum, int indx,
                                  List<Integer> l)  {
        if(indx == a.length && sum == target)   {
            System.out.println(l.toString());
            cnt++;
            return true;
        }
        if(indx >= a.length)    return false;
        List<Integer> l1 = new ArrayList<>(l);
        l1.add(a[indx]);
        boolean positive = findSum(a,target,sum+a[indx],indx+1,l1);
        List<Integer> l2 = new ArrayList<>(l);
        l2.add(-a[indx]);
        boolean negative = findSum(a,target,sum-a[indx],indx+1,l2);
        return positive || negative;
    }
}