package com.apal.dynamicprogramming;
/*
https://www.geeksforgeeks.org/coin-change-dp-7/
 */
public class CoinChangeDP {
    public static void main(String[] args) {
        int[] coins = {1,2,3};
        coin(5,coins,0);
        System.out.println("Total no of combinations "+cnt+" Called "+c1);

    }

    static int cnt = 0;
    static int c1 = 0;
    public static void coin(int n, int[] c, int indx)   {
        c1++;
        if(n == 0)    {
            cnt++;
            return;
        }
        if(n < 0 || indx >= c.length) return;
        for(int i = indx ; i < c.length;i++)   {
            if(c[i] <= n)
                coin(n-c[i],c,i);
            coin(n,c,i+1);
        }
    }
}
