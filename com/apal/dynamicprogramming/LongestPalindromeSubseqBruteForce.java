package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.List;

/*
Problem Statement #

Given a sequence, find the length of its Longest Palindromic Subsequence (LPS). In a palindromic subsequence, elements read the same backward and forward.

A subsequence is a sequence that can be derived from another sequence by deleting some or no elements without changing the order of the remaining elements.

Example 1:

Input: "abdbca"
Output: 5
Explanation: LPS is "abdba".

Example 2:

Input: = "cddpd"
Output: 3
Explanation: LPS is "ddd".

Example 3:

Input: = "pqr"
Output: 1
Explanation: LPS could be "p", "q" or "r".
 */
public class LongestPalindromeSubseqBruteForce {
    public static void main(String[] args) {
        new LongestPalindromeSubseqBruteForce().driver();
    }

    public void driver()    {
        maxlen = 0;
        findLPS("abdbca",0,new ArrayList<>());
        System.out.println("5 - "+maxlen);
        findLPSDP("abdbca");

        maxlen = 0;
        findLPS("cddpd",0,new ArrayList<>());
        System.out.println("3 - "+maxlen);
        findLPSDP("cddpd");

        maxlen = 0;
        findLPS("pqr",0,new ArrayList<>());
        System.out.println("1 - "+maxlen);
        findLPSDP("pqr");
    }

    int maxlen = 0;
    public void findLPS(String s, int si, List<Character> l)   {
        isPalindrome(l);
        if(si >= s.length())
            return;

        List<Character> l1 = new ArrayList<>(l);
        l1.add(s.charAt(si));
        findLPS(s,si+1,l1);

        List<Character> l2 = new ArrayList<>(l);
        findLPS(s,si+1,l2);
    }

    public void isPalindrome(List<Character> l) {
        //System.out.println(l.toString());
        int cnt = 0;
        for(int i=0,j=l.size()-1;i<j;i++,j--) {
            if(l.get(i) == l.get(j))
                cnt += 2;
            else
                return;
        }
        if(l.size() % 2 == 1)
            cnt = cnt + 1;
        if(cnt > maxlen)
            maxlen = cnt;
    }

    public void findLPSDP(String s) {
        int l = s.length();
        int[][] dp = new int[l][l];
        for(int i=0;i<l;i++)
            dp[i][i] = 1;

        System.out.println(s+" - "+findLPSHelper(s,dp));
    }

    public int findLPSHelper(String s, int[][] dp) {
        int l = s.length();
        for (int st = l-1; st >= 0 ; st--) {
            for (int end = st+1; end < l; end++) {
                if (s.charAt(st) == s.charAt(end))
                    dp[st][end] = 2 + dp[st + 1][end - 1];
                else
                    dp[st][end] = Math.max(dp[st + 1][end], dp[st][end - 1]);
            }
        }
        return dp[0][l-1];
    }
}