package com.apal.dynamicprogramming;

public class MinimumFee {
    public static void main(String[] args)  {
        int[] fee = new int[]{1,2,5,2,1,2};
        int[] dp = new int[fee.length];
        MinimumFee mf = new MinimumFee();
        long l1 = System.currentTimeMillis();
        System.out.println("minimum fee "+ mf.minimumFee(fee, 0));
        System.out.println("time taken "+(System.currentTimeMillis() -l1));

        long l2 = System.currentTimeMillis();
        System.out.println("minimum fee dp "+ mf.minimumFeeDP(fee, 0,dp));
        System.out.println("time taken "+(System.currentTimeMillis() -l2));
    }
    public int minimumFee(int[] fee, int indx)  {
        if(indx > (fee.length-1))
            return Integer.MAX_VALUE;
        if(indx == (fee.length-1))
            return 0;

        int c1 = minimumFee(fee,indx+1);
        int c2 = minimumFee(fee,indx+2);
        int c3 = minimumFee(fee,indx+3);

        int min = Math.min(c1,Math.min(c2,c3));
        if(min != Integer.MAX_VALUE)
            return min + fee[indx];
        else
            return Integer.MAX_VALUE;
    }

    public int minimumFeeDP(int[] fee, int indx, int[] dp)  {
        if(indx > (fee.length-1))
            return Integer.MAX_VALUE;
        if(indx == (fee.length-1))
            return 0;

        if(dp[indx] != 0)
            return dp[indx];
        int c1 = minimumFee(fee,indx+1);
        int c2 = minimumFee(fee,indx+2);
        int c3 = minimumFee(fee,indx+3);

        int min = Math.min(c1,Math.min(c2,c3));
        int ret = 0;
        if(min != Integer.MAX_VALUE)
            ret = min + fee[indx];
        else
            ret = Integer.MAX_VALUE;
        dp[indx] = ret;
        return ret;
    }

}
