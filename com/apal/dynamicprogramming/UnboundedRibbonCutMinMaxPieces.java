package com.apal.dynamicprogramming;

public class UnboundedRibbonCutMinMaxPieces {

    public static void main(String[] args) {
        new UnboundedRibbonCutMinMaxPieces().driver();
    }

    public void driver()    {
        int[] p = {2,3,5};
        int cap = 5;

        int[] p1 = {2,3};
        int cap1 = 7;

        ribbonCut(p1,cap1,0,0);
        System.out.println("Min cut = "+min);
        System.out.println("Max cut = "+max);


        System.out.println("Max cut dp = "+ribbonCutDP(p,cap,0));
    }

    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;

    public void ribbonCut(int[] p, int cap, int cur, int cnt)   {
        if(cap == 0) {
            min = Math.min(min, cnt);
            max = Math.max(max, cnt);
        }
        if(cur >= p.length)
            return;
        //take current element if its allowed
        if(cap >= p[cur])
            ribbonCut(p, cap-p[cur],cur,cnt+1);

        //do not take current element
        ribbonCut(p, cap,cur+1,cnt);
    }

    public Integer ribbonCutDP(int[] p, int cap, int cur)   {
        if(cap == 0) {
            return 0;
        }
        if(cur >= p.length)
            return null;
        Integer c1 = 0;
        Integer c2 = 0;
        //take current element if its allowed
        if(cap >= p[cur]) {
            c1 = ribbonCutDP(p, cap - p[cur], cur);
            if(c1 != null)
               c1 = c1 + 1;
            else
                c1 = 0;
        }

        //do not take current element
        c2 = ribbonCutDP(p, cap,cur+1);
        if(c2 == null)
            c2 = 0;
        return Math.max(c1 , c2);
    }
}