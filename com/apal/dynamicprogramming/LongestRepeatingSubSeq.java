package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LongestRepeatingSubSeq {
    public static void main(String[] args) {
        String s = "tomorrow";
        LongestRepeatingSubSeq lr = new LongestRepeatingSubSeq();
        System.out.println(2+" - "+lr.longRepSubSeq(s,1,0));
        System.out.println(3+" - "+lr.longRepSubSeq("aabdbcec",1,0));
        System.out.println(2+" - "+lr.longRepSubSeq("fmff",1,0));

    }

    public int longRepSubSeq(String s, int indx, int prev)  {
        if(indx >= s.length() || prev >= s.length()) return 0;
        int c1=0,c2=0,c3=0,c4=0;
        if(s.charAt(indx) == s.charAt(prev) && indx != prev)
            c1 = 1+longRepSubSeq(s,indx+1,prev+1);

        c2 = longRepSubSeq(s,indx,prev+1);
        c4 = longRepSubSeq(s,indx+1,prev);
        c3 = longRepSubSeq(s,indx+1,prev+1);

        return Math.max(Math.max(c2,c1),Math.max(c3,c4));
    }

}
