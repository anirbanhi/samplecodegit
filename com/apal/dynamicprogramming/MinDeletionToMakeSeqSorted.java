package com.apal.dynamicprogramming;

import java.util.Arrays;

/*
Problem Statement #

Given a number sequence, find the minimum number of elements that should be deleted to make the remaining sequence sorted.

Example 1:

Input: {4,2,3,6,10,1,12}
Output: 2
Explanation: We need to delete {4,1} to make the remaing sequence sorted {2,3,6,10,12}.
 */

public class MinDeletionToMakeSeqSorted {
    public static void main(String[] args) {
        int[] a = {4,2,3,6,10,1,12};
        int[][] dp = new int[a.length][a.length];
        for(int[] x : dp)
            Arrays.fill(x,-1);
        System.out.println(findSortedSeqCount(a,0,0,dp));
    }

    public static int findSortedSeqCount(int[] a, int indx, int previndx,
                                         int[][] dp) {
     if (indx == a.length) return 0;
     //if(dp[indx][previndx] == -1)   {
        int c1 = 0;
        int c2 = 0;
        int c3 = 0;
        if (a[indx] > a[previndx]) {
            c1 = 1 + findSortedSeqCount(a, indx + 1, indx, dp);
        }
        c3 = findSortedSeqCount(a, indx + 1, previndx, dp);
        dp[indx][previndx] = Math.max(c1, c2);
    //}
      //  return dp[indx][previndx,c3];
        System.out.println("c1 "+c1+" c2 "+c2+ " indx "+indx+" prevIndx "+previndx);
       return Math.max(Math.max(c1, c2),c3);
    }
}