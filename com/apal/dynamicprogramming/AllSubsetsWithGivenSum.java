package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.List;

/*
https://www.geeksforgeeks.org/perfect-sum-problem-print-subsets-given-sum/
 */
public class AllSubsetsWithGivenSum {

    public static void main(String[] args) {
        int[]  arr = {2, 3, 5, 6, 8, 10};
        int sum = 10;
        int[]  arr1 = {1, 2, 3, 4, 5};
        int sum1 = 10;
        List<List<Integer>> l = new ArrayList<>();
        allSubsetWithGivensum(arr,sum,0,0,l,
                new ArrayList<Integer>());
        System.out.println("sum count "+found+" steps "+cnt);
        for(List<Integer> l1 : l)
            System.out.println(l1.toString());
    }

    static int found = 0;
    static int cnt = 0;
    public static void allSubsetWithGivensum(int[] a, int sum,
                                             int indx, int target,
                                             List<List<Integer>> l,
                                             List<Integer> p)  {
        if(sum == target)   {
            found++;
            l.add(new ArrayList<>(p));
            return;
        }
        if(indx >= a.length)    return;
        if(target > sum )  return;
        cnt++;
        List<Integer> p1 = new ArrayList<>(p);
        p1.add(a[indx]);
        allSubsetWithGivensum(a,sum,indx+1,target+a[indx],l,p1);
        allSubsetWithGivensum(a,sum,indx+1,target,l,p);
    }

    public static void allSubsetWithGivensumDP()  {
        //TODO
    }

}
