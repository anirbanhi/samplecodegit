package com.apal.dynamicprogramming.lineardp;

import java.util.Arrays;

public class NumSquares {
    public static void main(String[] args) {
        new NumSquares().driver();
    }

    public void driver()    {
        System.out.println("12  3 == "+numSquares(12));
        System.out.println("13  2 == "+numSquares(13));
    }

    public int numSquares(int n) {
        int[] dp = new int[n+1];
        Arrays.fill(dp, -1);
        return num(n,dp);
    }

    public int num(int n, int[] dp)  {
        if(n==0)    return 0;
        if(n<0) return Integer.MAX_VALUE;

        if(dp[n] == -1) {
            int min = Integer.MAX_VALUE;
            Double d = Math.sqrt(n);
            int max = d.intValue();
            for (int i = 1; i <= max; i++) {
                int cnt = num(n - i * i, dp);
                if (cnt != Integer.MAX_VALUE)
                    cnt = cnt + 1;
                if (cnt < min)
                    min = cnt;
            }
            dp[n] = min;
        }
        return dp[n];
    }
}
