package com.apal.dynamicprogramming.lineardp;

/*
https://leetcode.com/problems/decode-ways/
 */

import java.util.Arrays;

public class DecodeWays {
    public static void main(String[] args) {
        new DecodeWays().driver();
    }

    public void driver()    {
        String s = "226";
        System.out.println(s + " 3 == "+numDecodings(s));
        s = "12";
        System.out.println(s + " 2 == "+numDecodings(s));
        s = "0";
        System.out.println(s + " 0 == "+numDecodings(s));
        s = "09";
        System.out.println(s + " 0 == "+numDecodings(s));
    }

    public int numDecodings(String s) {
        if(s==null || s.isEmpty())  return 0;
        int[] dp = new int[s.length()];
        Arrays.fill(dp, -1);
        return noOfDP(s,0,dp);
    }

    public int noOf(String s, int cur)   {
        if(cur == s.length()) return 1;
        if(cur > s.length())   return 0;

        int p1 = 0, p2 = 0;
        if(s.charAt(cur)>='1' && s.charAt(cur)<='9') {
            p1 = noOf(s, cur + 1);
        }
        if(cur+1<s.length())    {
            Character tens = s.charAt(cur);
            Character ones = s.charAt(cur+1);
            int tensint = Integer.parseInt(tens.toString());
            int onesint = Integer.parseInt(ones.toString());
            int no = tensint * 10 + onesint;
            if(tensint!=0 && no <= 26)
                p2 = noOf(s,cur+2);
        }
        return p1 + p2;
    }

    public int noOfDP(String s, int cur, int[] dp)   {
        if(cur == s.length()) return 1;
        if(cur > s.length())   return 0;

        if(dp[cur] == -1) {
            int p1 = 0, p2 = 0;
            if (s.charAt(cur) >= '1' && s.charAt(cur) <= '9') {
                p1 = noOf(s, cur + 1);
            }
            if (cur + 1 < s.length()) {
                Character tens = s.charAt(cur);
                Character ones = s.charAt(cur + 1);
                int tensint = Integer.parseInt(tens.toString());
                int onesint = Integer.parseInt(ones.toString());
                int no = tensint * 10 + onesint;
                if (tensint != 0 && no <= 26)
                    p2 = noOf(s, cur + 2);
            }
            dp[cur] = p1 + p2;
        }

        return dp[cur];
    }

}