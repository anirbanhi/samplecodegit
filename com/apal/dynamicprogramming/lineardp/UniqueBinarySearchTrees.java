package com.apal.dynamicprogramming.lineardp;

import java.util.Arrays;

/*
https://leetcode.com/problems/unique-binary-search-trees/
 */
public class UniqueBinarySearchTrees {
    public static void main(String[] args) {
        new UniqueBinarySearchTrees().driver();
    }

    public void driver() {
        System.out.println("3 -- 5 == "+numTrees(3));
    }

    public int numTrees(int n) {
        int[][] dp = new int[n+1][n+1];
        for(int i=0;i<n;i++)
            Arrays.fill(dp[i],-1);
        return numTreesDP(1,n,dp);
    }

    public int numTreesDP(int st, int end, int[][] dp)   {
        if(st==end) return 1;
        if(st>end) return 1;

        if(dp[st][end] == -1) {
            int total = 0;
            for (int i = st; i <= end; i++) {
                int cost = numTreesDP(st, i - 1,dp) * numTreesDP(i + 1, end,dp);
                total = total + cost;
            }
            dp[st][end] = total;
        }
        return dp[st][end];
    }
}