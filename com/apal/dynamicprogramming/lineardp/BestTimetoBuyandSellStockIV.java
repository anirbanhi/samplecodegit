package com.apal.dynamicprogramming.lineardp;

/*
https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iv/
 */

import java.util.Arrays;

public class BestTimetoBuyandSellStockIV {
    public static void main(String[] args) {
        new BestTimetoBuyandSellStockIV().driver();
    }

    public void driver()    {
        int[] p = new int[]{2,4,1};
        int k = 2;
        System.out.println(Arrays.toString(p) +"2 == "+maxProfit(k,p));

        int[] p1 = new int[]{3,2,6,5,0,3};
        int k1 = 2;
        System.out.println(Arrays.toString(p1) +"7 == "+maxProfit(k1,p1));
    }

    public int maxProfit(int k, int[] p) {
        if(p.length < 2 || k <= 0)
            return 0;

        int[] buy = new int[k];
        int[] sell = new int[k];
        Arrays.fill(buy, Integer.MIN_VALUE);

        for (int i = 0; i < p.length; i++) {
            for(int j=0;j<k;j++) {
                if(j==0)    {
                    buy[j] = Math.max(buy[j], -p[i]);
                    sell[j] = Math.max(sell[j], buy[j]+p[i]);
                }   else {
                        buy[j] = Math.max(buy[j], sell[j-1]-p[i]);
                        sell[j] = Math.max(sell[j], buy[j]+p[i]);
                }
            }
        }


        return sell[k-1];
    }

}
