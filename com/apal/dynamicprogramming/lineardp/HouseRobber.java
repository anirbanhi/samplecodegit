package com.apal.dynamicprogramming.lineardp;

import java.util.Arrays;

/*
https://leetcode.com/problems/house-robber/
 */
public class HouseRobber {
    public static void main(String[] args) {
        new HouseRobber().driver();
    }
    public void driver()    {
        int[] nums = new int[]{1,2,3,1};
        System.out.println("Robbing "+nums+" 4 == "+rob(nums));
        int[] nums1 = new int[]{2,7,9,3,1};
        System.out.println("Robbing "+nums+" 12 == "+rob(nums1));

    }
    public int rob(int[] nums) {
        int[] dp = new int[nums.length];
        Arrays.fill(dp,-1);
        return rob(nums,0,dp);
    }

    public int rob(int[] nums, int cur,int[] dp) {
        if(cur>=nums.length)    return 0;
        if(dp[cur] == -1) {
            int p1, p2 = 0;
            //rob current house
            p1 = rob(nums, cur + 2,dp);
            p1 = p1 + nums[cur];

            //dont rob current house
            p2 = rob(nums, cur + 1,dp);
            dp[cur] = Math.max(p1, p2);
        }
        return dp[cur];
    }
}
