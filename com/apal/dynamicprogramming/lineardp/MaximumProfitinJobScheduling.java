package com.apal.dynamicprogramming.lineardp;

/*
https://leetcode.com/problems/maximum-profit-in-job-scheduling/
 */

import java.util.Arrays;

public class MaximumProfitinJobScheduling {
    public static void main(String[] args) {
        new MaximumProfitinJobScheduling().driver();
    }

    public void driver()    {
        int[] startTime = new int[]{1,2,3,3};
        int[] endTime = new int[]{3,4,5,6};
        int[] profit = new int[]{50,10,40,70};

        System.out.println("120 == " + jobScheduling(startTime,endTime,profit));

        int[] startTime1 = new int[]{1,2,3,4,6};
        int[] endTime1 = new int[]{3,5,10,6,9};
        int[] profit1 = new int[]{20,20,100,70,60};

        System.out.println("150 == " + jobScheduling(startTime1,endTime1,profit1));

        int[] startTime2 = new int[]{1,1,1};
        int[] endTime2 = new int[]{2,3,4};
        int[] profit2 = new int[]{5,6,4};

        System.out.println("6 == " + jobScheduling(startTime2,endTime2,profit2));

    }

    public int jobScheduling(int[] startTime, int[] endTime, int[] profit) {
        sort(startTime,endTime,profit);
        int[] dp = new int[endTime.length+1];
        Arrays.fill(dp,-1);
        return job(startTime,endTime,profit,-1, dp);
    }

    public int job(int[] st, int[] end, int[] p, int cur, int[] dp)   {
        if(cur == end.length)   return 0;

        if(dp[cur+1] == -1) {
            int max = 0;
            for (int i = cur + 1; i < end.length; i++) {
                if (cur == -1) {
                    int cost = job(st, end, p, i, dp) + p[i];
                    max = Math.max(max, cost);
                } else if (st[i] >= end[cur]) {
                    int cost = job(st, end, p, i, dp) + p[i];
                    max = Math.max(max, cost);
                }
            }
            dp[cur+1] = max;
        }
        return dp[cur+1];
    }

    public void sort(int[] st, int[] end, int[] profit)  {
        int n = end.length;
        for(int i=0;i<n;i++)    {
            for(int j=i+1;j<n;j++)  {
                if(st[i]>st[j]) {
                    swap(end,i,j);
                    swap(st,i,j);
                    swap(profit,i,j);
                }
            }
        }
    }

    public void swap(int[] a, int i, int j)    {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
}