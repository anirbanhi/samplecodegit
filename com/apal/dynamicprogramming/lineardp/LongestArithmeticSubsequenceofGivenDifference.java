package com.apal.dynamicprogramming.lineardp;

import java.util.Arrays;

/*
https://leetcode.com/problems/longest-arithmetic-subsequence-of-given-difference/
 */
public class LongestArithmeticSubsequenceofGivenDifference {
    public static void main(String[] args) {
        new LongestArithmeticSubsequenceofGivenDifference().driver();
    }

    public void driver()    {
        int[] nums = new int[]{1,2,3,4};
        System.out.println(nums.toString()+" 4 == "+longestSubsequence(nums,1));

        int[] nums1 = new int[]{1,5,7,8,5,3,4,2,1};
        System.out.println(nums.toString()+" 4 == "+longestSubsequence(nums1,-2));

        int[] nums2 = new int[]{1,3,5,7};
        System.out.println(nums.toString()+" 1 == "+longestSubsequence(nums2,1));
    }

    public int longestSubsequence(int[] arr, int difference) {
        int n = arr.length;
        int[][] dp = new int[n+1][n+1];
        for(int i=0;i<n+1;i++)
            Arrays.fill(dp[i],-1);
        return longestdp(arr,difference,0,-1,dp);
    }

    public int longestdp(int[] arr, int diff, int cur, int prev, int[][] dp)  {
        if(cur == arr.length)   return 0;

        if(dp[cur+1][prev+1] == -1) {
            int len1 = 0, len3 = 0;
            if (prev == -1 || (arr[cur] - arr[prev] == diff)) {
                len1 = longestdp(arr, diff, cur + 1, cur, dp) + 1;
            }
            len3 = longestdp(arr, diff, cur + 1, prev, dp);

            dp[cur+1][prev+1] = Math.max(len1, len3);
        }
        return dp[cur+1][prev+1];
    }


}
