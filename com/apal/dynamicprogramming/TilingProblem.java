package com.apal.dynamicprogramming;
/*
https://www.geeksforgeeks.org/tiling-problem/
 */
public class TilingProblem {
    public static void main(String[] args) {
        System.out.println(findTiling(7));
    }

    public static int findTiling(int n)    {
        if(n < 0)   return 0;
        if(n == 1)  return 1;
        if(n == 2)  return 2;
        if(n == 3)  return 3;
        if(n == 4)  return 5;
        return findTiling(n-2) + findTiling(n-1);
    }
}
