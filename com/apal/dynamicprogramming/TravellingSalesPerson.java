package com.apal.dynamicprogramming;
/*
Travelling Sales Person Problem Brute Force
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TravellingSalesPerson {
    public static void main(String[] args) {
        int[][] g = {
                {0,16,11,6},
                {8,0,33,16},
                {4,7,0,9},
                {15,12,2,0}
        };

        int st = 0;
        List<Integer> l = new ArrayList<>();
        l.add(0);
        int[] visited = new int[g.length];
        visited[0] = 1;
        tsp(g,g.length-1,l,visited,0,0);
        System.out.println("Minimum cost = "+minCost+" sol "+res.toString()+" 0");
    }
    public static int minCost = Integer.MAX_VALUE;
    public static String res = "";
    public static void tsp(int[][] g, int left, List<Integer> l,
                            int[] v, int prev,int c)    {
        if(left==0) {
            int cost = findCost(l,g);
            c += g[l.get(l.size()-1)][0];
            System.out.println(l.toString() + " " + cost+" "+c);
            if(cost < minCost)  {
                minCost = cost;
                res = l.toString();
            }
            return;
        }
        for(int i=0;i<g.length;i++) {
            if(v[i] == 0 && g[prev][i] != 0 && prev != i)   {
                int[] v1 = Arrays.copyOf(v,v.length);
                List<Integer> l1 = new ArrayList<>(l);
                l1.add(i);
                v1[i] = 1;
                int c1 = g[prev][i] + c;
                tsp(g,left-1,l1,v1,i,c1);
            }
        }
    }

    public static int findCost(List<Integer> l, int[][] g)    {
        int cost = 0;
        for(int i=0;i<l.size()-1;i++) {
            cost += g[l.get(i)][l.get(i+1)];
        }
        cost += g[l.get(l.size()-1)][0];
        return cost;
    }
}
