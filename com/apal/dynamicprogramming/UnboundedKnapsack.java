package com.apal.dynamicprogramming;

public class UnboundedKnapsack {

    int cnt = 0;

    public static void main(String[] args) {
        new UnboundedKnapsack().driver();
    }

    public void driver()    {
        int[] wt = {1,2,3};
        int[] p = {15,20,50};
        int cap = 5;
        Integer[] dp = new Integer[cap+1];
        System.out.println("Max profit = "+unbKnapsack(wt,p,cap));
        System.out.println("cnt = "+cnt);
        cnt=0;
        System.out.println("Max profit = "+unbKnapsackDP(wt,p,cap,dp));
        System.out.println("cnt = "+cnt);
    }

    public int unbKnapsack(int[] wt, int[] p, int cap)  {
        if(cap <= 0)
            return 0;
        cnt++;
        int max = 0;
        int temp = 0;
        for(int i=0;i<wt.length;i++)    {
            if(cap >= wt[i])    {
                temp = p[i] + unbKnapsack(wt,p,cap-wt[i]);
                if(temp > max)
                    max = temp;
            }
        }
        return max;
    }

    public int unbKnapsackDP(int[] wt, int[] p, int cap, Integer[] dp)  {
        if(cap <= 0)
            return 0;
        if(dp[cap] == null) {
            cnt++;
            int max = 0;
            int temp = 0;

            for (int i = 0; i < wt.length; i++) {
                if (cap >= wt[i]) {
                    temp = p[i] + unbKnapsackDP(wt, p, cap - wt[i], dp);
                    if (temp > max)
                        max = temp;
                }
            }
            dp[cap] = max;
        }
        return dp[cap];
    }
}