package com.apal.dynamicprogramming.revision2;

/*
https://leetcode.com/problems/unique-paths/
 */

public class UniquePaths {
    public static void main(String[] args) {
        new UniquePaths().driver();
    }

    public void driver()    {
        System.out.println("28 == "+uniquePaths(3,7));
    }

    public int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];
        int x = path(m,n,dp,m-1,n-1);
        for(int i=0;i<m;i++)    {
            for(int j=0;j<n;j++){
                System.out.println(dp[i][j]+"  ");
            }
            System.out.println();
        }
        return x;
    }

    public int path(int m, int n, int[][] dp, int x, int y) {
        if(x==0 && y==0)    return 1;
        if(x<0 || y <=0) return Integer.MAX_VALUE;

        int p1=Integer.MAX_VALUE, p2=Integer.MAX_VALUE;

        p1 = path(m,n,dp,x-1,y);
        p2 = path(m,n,dp,x,y-1);

        if(p1==Integer.MAX_VALUE && p2==Integer.MAX_VALUE)
            return Integer.MAX_VALUE;
        if(p1!=Integer.MAX_VALUE && p2!=Integer.MAX_VALUE)
            return p1+p2;
        if(p1==Integer.MAX_VALUE)
            return p2;
        else
            return p1;

    }

}