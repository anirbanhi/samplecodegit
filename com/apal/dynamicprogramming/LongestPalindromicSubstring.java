package com.apal.dynamicprogramming;

public class LongestPalindromicSubstring {
    public static void main(String[] args) {
        new LongestPalindromicSubstring().driver();
    }

    public void driver()    {
        String s = "abdbca";
        System.out.println("3 - "+findLPString(s));

        String s1 = "cddpd";
        System.out.println("3 - "+findLPString(s1));

    }
    public int findLPString(String s)  {
        return findLPS(s,0,s.length()-1);
    }

    public int findLPS(String s, int st, int end)   {
        if(st > end)    return 0;
        if(st == end)    return 1;
        if(st >= s.length() || end < 0)
            return 0;
        int c1=0, c2=0, c3=0;
        if(s.charAt(st) == s.charAt(end))   {
            c1 = end - st - 1;
            if(c1 == findLPS(s,st+1,end-1))
                return c1+2;
        }
        c2 = findLPS(s,st+1,end);
        c3 = findLPS(s,st,end-1);
        return Math.max(c3,c2);
    }
}
