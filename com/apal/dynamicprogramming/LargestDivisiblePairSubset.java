package com.apal.dynamicprogramming;

/*
https://www.geeksforgeeks.org/largest-divisible-pairs-subset/
 */

public class LargestDivisiblePairSubset {
    public static void main(String[] args) {
        int[] a = {10, 5, 3, 15, 20};
        int[] a1 = {18, 1, 3, 6, 13, 17};
        System.out.println(findPair(a,0,-1,0)+" "+ex);
        ex=0;
        System.out.println(findPair(a1,0,-1,0)+" "+ex);
        ex=0;
        int[] dp = new int[a1.length+1];
        System.out.println(findPairDP(a1,0,-1,0,dp) - 1+" "+ex);
    }
    static int ex = 0;
    public static int findPair(int[] a, int indx, int hi, int cnt)   {
        if(indx >= a.length)    return cnt;
        ex++;
        int taken = 0;
        int nottaken = 0;
        if(hi == -1)
            taken = findPair(a,indx+1,a[indx],cnt+1);
        else if(a[indx] >= hi && a[indx] % hi == 0)
            taken = findPair(a,indx+1,a[indx],cnt+1);
            else if(a[indx] < hi && hi % a[indx] == 0)
            taken = findPair(a,indx+1,hi,cnt+1);

        nottaken = findPair(a,indx+1,hi,cnt);
        return Math.max(taken,nottaken);
    }

    public static int findPairDP(int[] a, int indx, int hi,
                                 int cnt, int[] dp)   {
        if(dp[indx] == 0) {
            if (indx >= a.length) return cnt;
            ex++;
            int taken = 0;
            int nottaken = 0;
            if (hi == -1)
                taken = findPairDP(a, indx + 1, a[indx], cnt + 1, dp);
            else if (a[indx] >= hi && a[indx] % hi == 0)
                taken = findPairDP(a, indx + 1, a[indx], cnt + 1, dp);
            else if (a[indx] < hi && hi % a[indx] == 0)
                taken = findPairDP(a, indx + 1, hi, cnt + 1, dp);

            nottaken = findPairDP(a, indx + 1, hi, cnt, dp);
            a[indx] = Math.max(taken, nottaken);
        }
        return a[indx];
    }
}