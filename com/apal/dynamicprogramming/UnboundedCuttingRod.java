package com.apal.dynamicprogramming;

public class UnboundedCuttingRod {
    public static void main(String[] args) {
        new UnboundedCuttingRod().driver();
    }

    public void driver()    {
        int[] len = {1, 2, 3, 4, 5};
        int[] p = {2, 6, 7, 10, 13};
        int cap = 5;
        Integer[] dp = new Integer[cap+1];
        System.out.println(cuttingRod(len,p,cap));
        System.out.println(cuttingRodDP(len,p,cap,dp));
    }

    public int cuttingRod(int[] l, int[] p, int cap) {
        if(cap<=0)
            return 0;

        int max = 0;
        int temp = 0;
        for(int i=0;i<l.length;i++) {
            if(cap >= l[i]) {
                temp = p[i] + cuttingRod(l,p,cap - l[i]);
                if(temp > max)
                    max = temp;
            }
        }
        return max;
    }

    public int cuttingRodDP(int[] l, int[] p, int cap,Integer[] dp) {
        if (cap <= 0)
            return 0;
        if (dp[cap] == null)    {
        int max = 0;
        int temp = 0;
        for (int i = 0; i < l.length; i++) {
            if (cap >= l[i]) {
                temp = p[i] + cuttingRod(l, p, cap - l[i]);
                if (temp > max)
                    max = temp;
            }
        }
        dp[cap] = max;
        }
        return dp[cap];
    }
}