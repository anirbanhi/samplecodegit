package com.apal.dynamicprogramming;
/*
0-1 knapsack DP
 */
public class Knapsack01DP {
    public static void main(String[] args) {
        int[] w = {2,3,1,4};
        int[] p = {4,5,3,7};
        int cap = 5;
        dp = new int[cap+1][w.length+1];
        int max = knapsack(w,p,cap,0,0);
        System.out.println("Max profit with DP "+max);
        System.out.println("Calling count with DP "+cnt);

        max = 0;
        cnt = 0;
        max = knapsackRec(w,p,cap,0,0);
        System.out.println("Max profit without DP "+max);
        System.out.println("Calling count without DP "+cnt);

    }
    static int [][] dp;
    static int cnt = 0;
    public static int knapsack(int[] w, int[] p, int cap,
                               int indx, int prof)    {
        if (cap <= 0 || indx >= p.length)
            return 0;
        if(dp[cap][indx] == 0) {
            cnt++;
            int p1 = 0, p2 = 0;
            if (w[indx] <= cap)
                p1 = p[indx] + knapsack(w, p, cap - w[indx], indx + 1, prof);
            p2 = knapsack(w, p, cap, indx + 1, prof);
            dp[cap][indx] = Math.max(p1,p2);
        }
        return dp[cap][indx];
    }

    public static int knapsackRec(int[] w, int[] p, int cap,
                               int indx, int prof)    {
        if (cap <= 0 || indx >= p.length)
            return 0;
            cnt++;
            int p1 = 0, p2 = 0;
            if (w[indx] <= cap)
                p1 = p[indx] + knapsackRec(w, p, cap - w[indx], indx + 1, prof);
            p2 = knapsackRec(w, p, cap, indx + 1, prof);
        return Math.max(p1,p2);
    }
}