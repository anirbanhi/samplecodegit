package com.apal.dynamicprogramming;

import java.util.ArrayList;
import java.util.List;

public class ZeroOneKnapsackNormal {
    public static void main(String[] args)  {
        ZeroOneKnapsackNormal z1 = new ZeroOneKnapsackNormal();
        List<List<Integer>> sublist = z1.knapsacknormal(new int[]{2,3,1,4},new int[]{4,5,3,7},7);
        z1.printList(sublist);
    }

    public List<List<Integer>> knapsacknormal(int[] items, int[] profit, int maxwt)    {
        List<List<Integer>> sublist = new ArrayList<>();
        sublist.add(new ArrayList<>());
        for(int i=0;i<items.length;i++) {
            if(items[i] > maxwt)
                continue;
            else{
                int size = sublist.size();
                for(int j=0;j<size;j++){
                    List<Integer> list = sublist.get(j);
                    List<Integer> newList = new ArrayList<>(list);
                    newList.add(items[i]);
                    if(totalSublistWt(newList) <= maxwt)
                        sublist.add(newList);
                }
            }
        }
        return sublist;
    }

    private int totalSublistWt(List<Integer> list)    {
        int sum = 0;
        for(int num:list)
            sum += num;
        return sum;
    }

    private void printList(List<List<Integer>> sublist) {
        for(List<Integer> list : sublist)   {
            System.out.println(list);
        }
    }
}
