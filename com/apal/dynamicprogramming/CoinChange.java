package com.apal.dynamicprogramming;

/*
https://www.hackerrank.com/challenges/coin-change/problem
Got 48 out of 60 using memoization
Initially got 28 without memoization
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CoinChange {
    public static void main(String[] args)  {
        int n = 4;
        List<Long> c = new ArrayList<>();
        c.add(new Long(1));
        c.add(new Long(2));
        c.add(new Long(3));
        dp = new int[n][c.size()];
        for(int[] ar : dp)
            Arrays.fill(ar, -1);

        int x = coinChange(c, new Long(n), 0, new ArrayList<>());
        System.out.println("No of cases possible "+x);
    }
    static int[][] dp ;
    public static int coinChange(List<Long> wt, Long capacity,
                                 int indx, List<Long> list) {
        //found solution
        if(capacity == 0)  {
            System.out.println(Arrays.asList(list));
            return 1;
        }
        if(capacity < 0)
            return 0;
        if(indx >= wt.size())
            return 0;
        if(dp[capacity.intValue()-1][indx] != -1)
            return dp[capacity.intValue()-1][indx];
        else {
            int size = wt.size();
            boolean foundSol = false;
            int sum = 0;
            for (int i = indx; i < size; i++) {
                List<Long> list1 = new ArrayList<>(list);
                list1.add(wt.get(i));
                sum += coinChange(wt, capacity - wt.get(i), i, list1);
            }
            dp[capacity.intValue()-1][indx] = sum;
            return dp[capacity.intValue()-1][indx];
        }
    }
}