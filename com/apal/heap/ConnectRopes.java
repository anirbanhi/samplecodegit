package com.apal.heap;

import java.util.PriorityQueue;

public class ConnectRopes {
    public static void main(String[] args){
        int[] arr = new int[]{4, 3, 2, 6};
        PriorityQueue<Integer> q = new PriorityQueue<>();
        for(int i=0;i<arr.length;i++){
            q.add(arr[i]);
        }

        while(q.size()>1){
            int n1 = q.remove();
            int n2 = q.remove();
            q.add(n1+n2);
        }
        System.out.println("total cost " + q.remove());
    }


}
