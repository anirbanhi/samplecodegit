package com.apal.heap;

import java.util.PriorityQueue;

public class KthSmallestInMatrix {
    public static void main(String[] args){
        int[][] mat = {
                {2,6,8},
                {3,7,10},
                {5,8,11}
        };
        int n = mat.length;
        int k = 5;
        PriorityQueue<Node1> pq = new PriorityQueue<Node1>
                ((n1,n2) -> mat[n1.row][n1.col] - mat[n2.row][n2.col]);

        for(int i=0;i<n && i<k;i++){
            pq.add(new Node1(i,0));
        }
        int count = 0;
        Node1 temp = null;
        while(!pq.isEmpty()){
            temp = pq.poll();
            count++;
            if(count == k)
                break;
            if(mat[temp.row].length > temp.col)
                pq.add(new Node1(temp.row,temp.col+1));
        }
        System.out.println(k+" th smallest " + mat[temp.row][temp.col]);
    }

    static class Node1{
        int row;
        int col;

        public Node1(int r, int c){
            this.row = r;
            this.col = c;
        }
    }
}
