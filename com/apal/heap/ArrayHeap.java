package com.apal.heap;

public class ArrayHeap {

    int[] heap = new int[100];
    int count = -1;
    boolean isMaxheap = true;

    public boolean addElement(int x)    {
        count++;
        heap[count] = x;
        heapify(count);
        return true;
    }

    private void heapify(int indx)  {
        if(indx == 0)
            return;
        int parentIndx = (indx - 1 )/2;
        if(isMaxheap) {
            if (heap[parentIndx] < heap[indx]) {
                swap(parentIndx, indx);
                heapify(parentIndx);
            }
        } else {
            if (heap[parentIndx] > heap[indx]) {
                swap(parentIndx, indx);
                heapify(parentIndx);
            }
        }
    }

    public int removeElement()  {
        if(count < 0)
        {
            //throw new RuntimeException("Empty Heap");
            System.out.println("Empty Heap");
            return -1;
        }
        int elem = heap[0];
        if(count == 0) {
            heap[count] = elem;
            count--;
            return elem;
        }
        else
        {
            heap[0] = heap[count];
            //heap[count] = elem;
            count--;
            removeHeapify(0);
        }
        return elem;
    }

    private void removeHeapify(int pindx)    {
        if(pindx >= count)
            return;
        int lChildIndx = pindx * 2 + 1 ;
        int rChildIndx = pindx * 2 + 2 ;
        int indx  = lChildIndx;

        if(lChildIndx > count)
            return;
        if(lChildIndx == count)
            indx = lChildIndx;
        else {
            if (rChildIndx <= count) {
                if (isMaxheap) {
                    if ((heap[rChildIndx] > heap[lChildIndx]))
                        indx = rChildIndx;
                } else {
                    if ((heap[rChildIndx] < heap[lChildIndx]))
                        indx = rChildIndx;
                }
            }
        }
        if(isMaxheap) {
            if (heap[pindx] < heap[indx]) {
                swap(pindx, indx);
                removeHeapify(indx);
            }
        } else {
            if (heap[pindx] > heap[indx]) {
                swap(pindx, indx);
                removeHeapify(indx);
            }
        }
    }

    private void swap(int parentIndx, int childIndx)    {
        int temp = heap[parentIndx];
        heap[parentIndx] = heap[childIndx];
        heap[childIndx] = temp;
    }

    public void printHeap()     {
        System.out.println();
        for (int i=0;i<=count;i++){
            if(heap[i] != 0)
                System.out.print(heap[i]+" , ");
        }
    }
}
