package com.apal.heap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

public class KPairWithLargestSum {
    public static void main(String[] args)  {

        //sorted array
        int[] arr1 = {9,8,2};
        int[] arr2 = {6,3,1};
        int k = 3;

        PriorityQueue<int[]> pq = new PriorityQueue<>(
                (n1,n2) -> (n1[0]+n1[1]) - (n2[1]+n2[0])
        );
        int n = arr1.length;
        for(int i=0;i<k && i<n;i++){
            for(int j=0;j<k && j<n;j++){
                if(pq.size() < k){
                    pq.add(new int[]{arr1[i],arr2[j]});
                } else
                {
                    int sum = arr1[i] + arr2[j];
                    int[] top = pq.peek();
                    if(sum > (top[0] + top[1]))
                    {
                        pq.poll();
                        pq.add(new int[]{arr1[i] , arr2[j]});
                    }
                }
            }

        }
        for(int[] list : pq)
        System.out.println(list[0] + " , " + list[1]);
    }

    class Node{
        int sum;
        int a1;
        int a2;

        public Node(int sum, int a1, int a2){
            this.sum = sum;
            this.a1 = a1;
            this.a2 = a2;
        }
    }

}
