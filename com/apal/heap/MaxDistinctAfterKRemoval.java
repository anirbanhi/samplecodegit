package com.apal.heap;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class MaxDistinctAfterKRemoval {
    public static void main(String[] args)  {

        //int arr[] = new int[]{5,7,5,5,1,2,2};
        int arr[] = new int[]{1, 2, 3, 4, 5, 6, 7};
        int  k = 5;
        PriorityQueue<Node> pqmax = new PriorityQueue<>(
                (n1, n2) -> n2.count - n1.count
        );

        Map<Integer, Integer> map = new HashMap<>();

        for(int i=0;i<arr.length;i++){
            map.put(arr[i], map.getOrDefault(arr[i],0)+1);
        }

        for(Map.Entry<Integer,Integer> entry : map.entrySet()){
                pqmax.add(new Node(entry.getKey(), entry.getValue()));
        }
        int count = 0;
        for(int i =0;i<k;i++){
            Node temp = pqmax.poll();
            if(temp.count > 1)
            {
                temp.count = temp.count - 1;
                pqmax.add(temp);
            }
        }

        System.out.println(pqmax.size());


    }

    static class Node{
        int elem;
        int count;

        public Node(int elem, int count){
            this.elem = elem;
            this.count = count;
        }
    }
}
