package com.apal.heap;

public class UseArrayHeap {
    public static void main(String[] args)  {
        new UseArrayHeap().useMinHeap();
        //new UseArrayHeap().useMaxHeap();
    }

    static void useMinHeap()    {
        ArrayHeap heap = new ArrayHeap();
        heap.isMaxheap = false;
        heap.addElement(10);
        heap.printHeap();
        heap.addElement(20);
        heap.printHeap();
        heap.addElement(30);
        heap.printHeap();
        heap.addElement(40);
        heap.printHeap();
        heap.addElement(50);
        heap.printHeap();
        heap.addElement(60);
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
    }

    static void useMaxHeap()    {
        ArrayHeap heap = new ArrayHeap();
        heap.isMaxheap = true;
        heap.addElement(10);
        heap.printHeap();
        heap.addElement(20);
        heap.printHeap();
        heap.addElement(30);
        heap.printHeap();
        heap.addElement(40);
        heap.printHeap();
        heap.addElement(50);
        heap.printHeap();
        heap.addElement(60);
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
        heap.printHeap();
        System.out.println("\n Remove Element " + heap.removeElement());
    }
}
