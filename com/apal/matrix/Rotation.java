package com.apal.matrix;

public class Rotation {
    public static void main(String[] args)  {
        int[][] mat = {
                        {1,2,3},
                        {4,5,6},
                        {7,8,9}
                      };
        printMatrix(mat);
        int n = mat.length;
        for(int i=0;i<n;i++)    {
            for(int j=i;j<n;j++)
            {
                int temp = mat[i][j];
                mat[i][j] = mat[j][i];
                mat[j][i] = temp;
            }
        }

        for(int i=0;i<n;i++){
            int k=0,l=n-1;
            while(k<=l){
                int temp = mat[i][k];
                mat[i][k] = mat[i][l];
                mat[i][l] = temp;
                k++;
                l--;
            }
        }

        printMatrix(mat);
    }

    public static void printMatrix(int[][] mat){
        int n = mat.length;
        for (int i=0;i<n;i++) {
            for (int j = 0; j < n; j++)
                System.out.print("  " + mat[i][j]);
            System.out.println();
        }
    }
}
