package com.apal.interviewbit.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

/*
Problem Description

Given an array of integers A, sort the array into a wave like array and return it, In other words, arrange the elements into a sequence such that

a1 >= a2 <= a3 >= a4 <= a5.....

NOTE : If there are multiple answers possible, return the one that's lexicographically smallest.



Problem Constraints

1 <= len(A) <= 106

1 <= A[i] <= 106


Input Format

First argument is an integer array A.


Output Format

Return an array arranged in the sequence as described.


Example Input

Input 1:

A = [1, 2, 3, 4]

Input 2:

A = [1, 2]



Example Output

Output 1:

[2, 1, 4, 3]

Output 2:

[2, 1]

 */

//https://www.interviewbit.com/problems/wave-array/

public class WaveArray {
    public static void main(String[] args) {
        //int[] a = {1, 2, 3, 4};
        int[] a = {1, 2, 3};
        ArrayList<Integer> al = new ArrayList<>();
        IntStream st = Arrays.stream(a);
        st.forEach(value -> al.add(value));
        ArrayList<Integer> a1 = new WaveArray().wave(al);
        System.out.println(a1);
    }

    public ArrayList<Integer> wave(ArrayList<Integer> a) {
        Collections.sort(a);
        for(int i=0;i<a.size();i=i+2) {
            int s1 = i;
            int s2 = i+1;
            swap(a,s1,s2);
        }
        return a;
    }

    public void swap(ArrayList<Integer> a, int i, int j)    {
        if(j>=a.size())
            return;
        int temp = a.get(i);
        a.set(i,a.get(j));
        a.set(j,temp);
    }


}
