package com.apal.interviewbit.arrays;

import java.util.*;

public class HotelBookingsPossible {

    public static void main(String[] args) {
        int[] a = {1, 3, 5};
        int[] b = {2, 6, 8};
        int c = 1;

        ArrayList<Integer> ar = new ArrayList<>();
        ArrayList<Integer> dp = new ArrayList<>();

        Arrays.stream(a).forEach(value -> ar.add(value));
        Arrays.stream(b).forEach(value -> dp.add(value));
        boolean ans = new HotelBookingsPossible().hotel(ar, dp, c);
        System.out.println("ans [false] "+ans);

        int[] a1 = { 1, 2, 3, 4 };
        int[] b1 = { 10, 2, 6, 14 };
        int c1 = 2;

        ArrayList<Integer> ar1 = new ArrayList<>();
        ArrayList<Integer> dp1 = new ArrayList<>();

        Arrays.stream(a1).forEach(value -> ar1.add(value));
        Arrays.stream(b1).forEach(value -> dp1.add(value));
        boolean ans1 = new HotelBookingsPossible().hotel(ar1, dp1, c1);
        System.out.println("ans [false] "+ans1);

        int[] a2 =  { 1, 2, 3 };
        int[] b2 = { 2, 3, 4 };
        int c2 = 1;
        ArrayList<Integer> ar2 = new ArrayList<>();
        ArrayList<Integer> dp2 = new ArrayList<>();

        Arrays.stream(a2).forEach(value -> ar2.add(value));
        Arrays.stream(b2).forEach(value -> dp2.add(value));
        boolean ans2 = new HotelBookingsPossible().hotel(ar2, dp2, c2);
        System.out.println("ans [true] "+ans2);

    }

    public boolean hotel(ArrayList<Integer> a, ArrayList<Integer> d, int k) {
        ArrayList<Pair> s = new ArrayList<>();
        a.stream().forEach(value -> s.add(new Pair(value,1)));
        d.stream().forEach(value -> s.add(new Pair(value,0)));

        Collections.sort(s,(Pair o1,Pair o2) ->
        {
            if(o1.time != o2.time)
                return o1.time - o2.time;
            else return -1;
        });

        System.out.println(s);
        int cur = 0;
        int max = 0;
        for(Pair elem : s)    {

            //departure case
            if(elem.type == 0)  {
                cur--;
            }

            //arrival case
            if(elem.type == 1)  {
                cur++;
                max = Math.max(cur,max);
                if(max>k)
                    return false;
            }
        }
        return true;
    }

    class Pair  {
        int time;
        int type;

        Pair(int time, int type)    {
            this.time = time;
            this.type = type;
        }

        @Override
        public String toString() {
            return "Pair{" +
                    "time=" + time +
                    ", type=" + type +
                    '}';
        }
    }
}
