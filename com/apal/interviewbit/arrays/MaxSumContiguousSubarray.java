package com.apal.interviewbit.arrays;

import java.util.List;

public class MaxSumContiguousSubarray {
    public static void main(String[] args) {
        int ans = new MaxSumContiguousSubarray().maxSubArray();

    }
    public int maxSubArray(final List<Integer> a) {
        int n = a.size();
        int max = 0;
        int curmax = 0;
        boolean positiveFound = false;
        int negativeMax = Integer.MIN_VALUE;
        for(int i=0;i<n;i++)    {
            if(a.get(i) >= 0)
                positiveFound = true;
            negativeMax = Math.max(a.get(i),negativeMax);
            curmax = curmax + a.get(i);
            max = Math.max(max,curmax);
            if(curmax < 0)
                curmax = 0;
        }
        if(!positiveFound)
            return negativeMax;
        return curmax;
    }
}
