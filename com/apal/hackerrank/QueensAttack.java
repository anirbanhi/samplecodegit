package com.apal.hackerrank;

public class QueensAttack {
    public static void main(String[] args)  {

        int n = 5;
        int k = 3;
        int rq = 4;
        int cq = 3;
        int[][] obs = {{5,5},{4,2},{2,3}};
        int count = queensAttack(n, k, rq, cq, obs);
        System.out.println("Count " +count);
    }

    // Complete the queensAttack function below.
    static int queensAttack(int n, int k, int rq, int cq, int[][] obs) {
        Integer[][]board = new Integer[n][n];
        for(int i =0;i<k;i++){
            int[] ip = obs[i];
            int ro = ip[0];
            int co = ip[1];
            ro=ro-1;
            co=co-1;
            ro=(n-1)-ro;
            board[ro][co] = -1;
        }

        int[][] dir = {
                {-1,-1},{-1,0},{-1,1},
                {0,-1},{0,1},
                {1,-1},{1,0},{1,1}
        };

        int rq1 = rq-1;
        int cq1 = cq-1;
        rq1 = (n-1)-rq1;
        int count = 0;
        for(int i=0;i<dir.length;i++){
            int[] edir = dir[i];
            int rd = edir[0];
            int cd = edir[1];
            count += findCount(rq1,cq1,rd,cd,board,n);
        }
        return count;
    }

    static int findCount(int r, int c, int r1, int c1, Integer[][] board, int n)   {
        int row = r + r1;
        int col = c + c1;
        int count=0;
        if(row >= 0 && col >= 0 && row <n && col<n){
            if(board[row][col] != null &&  board[row][col] == -1){
                return 0;
            }else {
                count++;
                return 1+findCount(row,col,r1,c1,board,n);
            }
        }else
            return 0;
    }

}
