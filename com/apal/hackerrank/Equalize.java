package com.apal.hackerrank;

import java.util.HashMap;
import java.util.Map;

public class Equalize {
    public static void main(String[] args)  {
     //int[] arr = {3, 3, 2, 1, 3};
     int[] arr = {1, 2, 2, 3};
     System.out.println(equalizeArray(arr));
    }

    static int equalizeArray(int[] arr) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0;i<arr.length;i++){
            map.put(arr[i],map.getOrDefault(arr[i],0)+1);
        }
        int max = 0;
        for(Map.Entry<Integer, Integer> e : map.entrySet()){
            if(e.getValue() > max)
                max = e.getValue();
        }
        int ret = arr.length - max;
        return ret;
    }
}
