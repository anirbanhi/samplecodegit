package com.apal.hackerrank;

import java.util.Stack;

public class ValleyCount {
    public static void main(String[] args)  {
        String s = "DDUUUUDD";
        String s1 = "UDDDUDUU";
        String s2 = "DDUUDDUDUUUD";

        System.out.println( countingValleys(8, s));
    }
    static int countingValleys(int n, String s) {
        if (n < 2)
            return 0;
        int v = 0;     // # of valleys
        int lvl = 0;   // current level
        for(char c : s.toCharArray()){
            if(c == 'U') ++lvl;
            if(c == 'D') --lvl;

            // if we just came UP to sea level
            if(lvl == 0 && c == 'U')
                ++v;
        }
        return v;
    }

}
