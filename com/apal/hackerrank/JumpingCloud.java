package com.apal.hackerrank;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class JumpingCloud {


    static int jumpingCount(int[] c, int cur, Integer[] dp) {
        if(cur >= c.length)
            return -1;
        if(cur == c.length - 1 && c[cur] == 0)
            return 0;
        if(c[cur] == 1)
            return -1;
        int retValue = 0;
        if(dp[cur] == null) {
            int l1 = jumpingCount(c, cur + 1, dp);
            int l2 = jumpingCount(c, cur + 2, dp);
            if (l1 >= 0 && l2 >= 0) {
                retValue = 1 + Math.min(l1, l2);
            } else if (l1 == -1) {
                retValue = l2 + 1;
            } else if (l2 == -1) {
                retValue = l1 + 1;
            } else {
                retValue = -1;
            }
            dp[cur] = retValue;
        }
        return dp[cur];
    }

    // Complete the jumpingOnClouds function below.
    static int jumpingOnClouds(int[] c) {
        Integer[] dp = new Integer[c.length];
        System.out.println(" COUNT " + jumpingCount(c,0,dp));
        return 0;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

  //      int n = scanner.nextInt();
    //    scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");



        //String[] cItems = scanner.nextLine().split(" ");
        String[] cItems = {"0","0","0","0","1","0"};
        //String[] cItems = {"0", "0", "1", "0", "0", "1", "0"};

        int[] c = new int[cItems.length];
        for (int i = 0; i < cItems.length; i++) {
            int cItem = Integer.parseInt(cItems[i]);
            c[i] = cItem;
        }

        int result = jumpingOnClouds(c);

    }
}
