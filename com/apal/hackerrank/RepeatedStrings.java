package com.apal.hackerrank;

public class RepeatedStrings {
    public static void main(String[] args)  {
        String s = "a";
        long n = 10;

        System.out.println("Count = " + repeatedString(s,n));

    }

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        int len = s.length();
        long count = n/len;
        long add = n - count * len;

        long ains = 0;
        for(int i=0;i<s.length();i++)
            if(s.charAt(i)=='a')
                ains++;
        long aadd = 0;
        for(int i=0;i<add;i++){
            if(s.charAt(i)=='a')
                aadd++;
        }
        long acount = 0;
        acount = ains * count + aadd;
        return acount;
    }


}
