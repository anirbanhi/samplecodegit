package com.apal.hackerrank;

import java.util.ArrayList;
import java.util.List;

public class Grading {
    public static void main(String[] args)  {
        List<Integer> grades = new ArrayList<>();
        grades.add(73);
        grades.add(67);
        grades.add(38);
        grades.add(33);
        System.out.println( gradingStudents(grades));

    }

    public static List<Integer> gradingStudents(List<Integer> grades) {
        // Write your code here
        List<Integer> res = new ArrayList<>();
        for(int n:grades)   {
            if(n>37){
                int r = findNextMultipleOf5(n);
                if( r - n < 3)
                {
                    res.add(r);
                }
                else
                    res.add(n);
            }
            else
                res.add(n);
        }
        return res;
    }

    private static int findNextMultipleOf5(int n){
        int r = 5 - n%5;
        return r+n;
    }
}
