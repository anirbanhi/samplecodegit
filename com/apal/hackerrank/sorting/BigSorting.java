package com.apal.hackerrank.sorting;


import java.util.*;

public class BigSorting {
    public static void main(String[] args)  {

        String[] unsorted = {
        "31415926535897932384626433832795",
        "1",
        "3",
        "10",
        "3",
        "5"
        };

        String[] unsorted1 = {
                "1",
                "2",
                "100",
                "12303479849857341718340192371",
                "3084193741082937",
                "3084193741082938",
                "111",
                "200"
        };
        for(String s : unsorted1)
        System.out.println(s);

        String[] sorted = bigSorting(unsorted1);
        System.out.println(sorted);
    }

    static String[] bigSorting(String[] unsorted) {
     //   System.out.println("-------------");
/*
        List<String> list = new ArrayList<>();

        for(int i=0; i<unsorted.length; i++){
            list.add(unsorted[i]);
        }
*/

        Comparator<String> comp = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.length() != o2.length())
                    return (o1.length() - o2.length());
                else {
                    for(int i=0;i<o1.length();i++){
                        int c1 = o1.charAt(i);
                        int c2 = o2.charAt(i);
                        if(c1 == c2)
                            continue;
                        else {
                            return c1 - c2;
                        }
                    }
                    return 0;
                }
                //return 0;
            }
        };
        Arrays.sort(unsorted,comp);
        /*Collections.sort(list,comp);
        int k = 0;
        for(String s : list)
        {
            unsorted[k++] = s;
        }*/

        return unsorted;
    }
}
