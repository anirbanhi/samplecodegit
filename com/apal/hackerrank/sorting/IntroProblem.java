package com.apal.hackerrank.sorting;

public class IntroProblem {
    public static void main(String[] args)  {
        int[] arr = {1, 4, 5, 7, 9, 12};
        int v = 12;

        System.out.println(introTutorial(v,arr));
    }

    static int introTutorial(int v, int[] arr) {
        int lo = 0;
        int hi = arr.length - 1;
        int mid = 0;

        while(lo <= hi){
            mid = (hi - lo) / 2 + lo;
            if(arr[mid] == v)
                return mid;
            else {
                if(v > arr[mid])
                    lo = mid + 1;
                else
                    hi = mid - 1;
            }
        }
        return -1;
    }
}
