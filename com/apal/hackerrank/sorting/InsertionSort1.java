package com.apal.hackerrank.sorting;

public class InsertionSort1 {
    public static void main(String[] args)  {
     //int[] arr = {2, 4, 6, 8, 3};
     int[] arr = {1, 4, 3, 5, 6, 2};
     int n = arr.length;
     insertionSort2(n,arr);

    }

    static void printArray(int[] arr)   {
        for(int i : arr)
            System.out.print(i+" ");
    }

    static void insertionSort2(int n, int[] arr) {
        if(n < 2)
            return;
        int x = 0;
        int i = 0;
        for(int j=1;j<n;j++) {
            x = arr[j];
            for (i = j-1; i > -1 && arr[i] > x; i--) {
                if (x < arr[i]) {
                    arr[i + 1] = arr[i];
                    //printArray(arr);
                    //System.out.println();
                }
            }
            arr[i + 1] = x;
            printArray(arr);
            System.out.println();
        }
    }

    static void insertionSort1(int n, int[] arr) {
        if(n < 2)
            return;
        int x = arr[n-1];
        int i = 0;
        for(i=n-2;i>-1;i--){
            if(x < arr[i])
            {
                arr[i+1] = arr[i];
                printArray(arr);
            }
        }
        arr[i+1] = x;
        printArray(arr);

    }
}
