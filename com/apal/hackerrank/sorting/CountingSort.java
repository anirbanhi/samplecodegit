package com.apal.hackerrank.sorting;

public class CountingSort {
    public static void main(String[] args)  {
        int[] arr = {1,1,3,2,1};
        int[] arro = countingSort(arr);
        for(int k : arro)
            System.out.print(k + " ");
    }

    static int[] countingSort(int[] arr) {

        int[] arro = new int[arr.length];
        for(int k =0;k<arro.length;k++){
            arro[k] = 0;
        }

        for(int k : arr){
            arro[k] = arro[k] + 1;
        }

        int x = arro.length - 1;
        while(arro[x] == 0)
            x--;
        int[] op = new int[x+1];
        for(int k = 0; k <=x; k++){
            op[k] = arro[k];
        }

        int[] op2 = new int[arr.length];
        int i = 0;
        for(int k=0; k<op.length; k++){
            int cnt = op[k];

            while(cnt >0)
            {
                op2[i++] = k;
                cnt--;
            }
        }
        return op2;
    }
}
