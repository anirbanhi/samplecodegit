package com.apal.recursion;

/*
https://practice.geeksforgeeks.org/problems/number-of-paths/0
 */

import java.util.Arrays;

public class PathCount {

    public static void main(String[] args) {
        int rm1 = 2;
        int cm1 = 8;
        int[][] g = new int[rm1][cm1];
        for(int[] r : g)
            Arrays.fill(r,1);
        int[][] v = new int[rm1][cm1];
        countPath(g,0,0,v);
        System.out.println("8 - "+cnt);
    }

    static int cnt = 0;
    public static void countPath(int[][] g, int r, int c, int[][] v)    {
        int rm = g.length;
        int cm = g[0].length;
        if(r==rm-1 && c==cm-1)  {
            cnt++;
            return;
        }
        int r1=r;
        int c1=c+1;
        if(isSafe(g,r1,c1,v))   {
            v[r1][c1] = 1;
            countPath(g,r1,c1,v);
            v[r1][c1]=0;
        }


        int r2=r+1;
        int c2=c;
        if(isSafe(g,r2,c2,v))   {
            v[r2][c2] = 1;
            countPath(g,r2,c2,v);
            v[r2][c2]=0;
        }

    }

    public static boolean isSafe(int[][] g, int r, int c, int[][] v)  {
        int rm = g.length;
        int cm = g[0].length;
        if(r<0 || c<0 || r>=rm || c>=cm)
            return false;
        return v[r][c]==1?false:true;
    }
}