package com.apal.recursion;

public class Permutation {
    public static void main(String[] args)  {
        String str = "ABC";
        perm(str.toCharArray(), 0);
    }

    public static void perm(char[] ch, int curIndex)    {
        if(curIndex == ch.length)
        {
            System.out.println(String.valueOf(ch));
        }
        for(int i=curIndex; i<ch.length; i++)   {
            swap(ch, curIndex,i);
            perm(ch, curIndex+1);
            swap(ch, curIndex,i);
        }
    }

    public static void swap(char[] ch, int i, int j) {
        char temp = ch[i];
        ch[i] = ch[j];
        ch[j] = temp;
    }
}
