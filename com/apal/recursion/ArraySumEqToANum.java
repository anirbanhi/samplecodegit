package com.apal.recursion;
/*
Solved without precomputation in 0.01; this problem has a pattern of actions which is going to lead to the highest score which is not so hard to see & prove formally. Basically it is A, A, A, [A,] S, C, P, P, P, S, C, P, P, P, ..., S, C, P, P[, P]. So the function I have now is:

int f(int n) {
if (n < 5) {
return n;
}
return max(4*f(n-5), 3*f(n-4));
}
 */
public class ArraySumEqToANum {
    public static void main(String[] args) {
        ArraySumEqToANum a = new ArraySumEqToANum();
        a.driver();
    }
    public void driver()    {

    }

    int max = 0;
    public void findCount(int[] a,int indx, int curop, int cnt, int[] prev) {
        if(indx >= a.length)    return;

        if(indx == a.length-1)  {

        }

        for(int i=1;i<=4;i++) {
            if(isSafe(a,indx+1,i,prev)) {
                prev[indx+1] = i;
                findCount(a, indx + 1, i, cnt, prev);
                prev[indx+1] = 0;
            }
        }
    }

    public int calculate(int[] prev)  {
        int p = prev[0];
        int cnt = 0;
        int copy = 0;
        int tot = 0;
        for(int i=0;i<prev.length;i++)  {
            int c = prev[i];

            switch (c)  {
                case 1: cnt++;  break;
                case 2: copy = cnt;
                case 3:
                case 4: tot += copy;
            }
        }
        return 0;
    }
    /*
        op  1   =   Print A
        op  2   =   Ctrl A
        op  3   =   Ctrl C
        op  4   =   Ctrl V
     */
    public boolean isSafe(int[] a, int indx, int curop, int[] prev)    {
        if(indx >= a.length)    return false;
        int prevop = prev[indx-1];
        if(prev[indx] != 0) return false;
        switch (curop)  {
            case 1: if(prevop == 1)
                        return true;
                    else
                        return false;
            case 2: if(prevop == 2 || prevop == 3)
                        return false;
                    else
                        return true;
            case 3: if(prevop == 2)
                        return true;
                    else
                        return false;
            case 4: if(prevop == 3 || prevop == 4)
                        return true;
                    else
                        return false;
        }
        return false;
    }
}