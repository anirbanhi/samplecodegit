package com.apal.recursion;

public class CountRec {
    public static void main(String[] args)  {
        System.out.print(findCount(10));
    }
    public static int findCount(int no)    {
        if(no == 0)
            return 0;
        if(no%2 == 0)
            return findCount(no/2)+1;
        else{
            return findCount(no-1)+1;
        }
    }
}
