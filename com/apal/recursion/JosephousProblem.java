package com.apal.recursion;

import com.apal.linkedlist.LLBasicOperations;
import com.apal.linkedlist.Node;

import java.util.HashMap;
import java.util.Map;

/*
https://practice.geeksforgeeks.org/problems/josephus-problem/1
 */
public class JosephousProblem {
    public static void main(String[] args) {
        new JosephousProblem().driver();
    }

    public void driver()    {
        System.out.println("n = 3 k = 2 -- 3 "+josephus(3,2));

        System.out.println("n = 5 k = 3 -- 4 "+josephus(5,3));
    }

    public int josephus(int n, int k)
    {
        //Your code here
        findPlace(n,k,new int[n+1]);
        return 0;
    }

    public void findPlace(int n, int k, int[] kl) {

        Map<Integer, Boolean> v = new HashMap<>();
        Node h = new Node(1);
        Node p = h;
        for(int i=2;i<=n;i++)    {
            v.putIfAbsent(i,false);
            kl[i] = 0;
            Node c = new Node(i);
            p.next = c;
            p = c;
        }
        p.next = h;
        LLBasicOperations.printCircularLinkList(h," tr");
        for(int i=0;i<n;i++)    {
            if(LLBasicOperations.countNodesInCircularLL(h) > 1) {
                int l = 0;
                Node pr = null;
                Node t = h;
                for (int j = 1; j < k; j++) {
                    pr = t;
                    t = t.next;
                }
            }
        }
    }
/*
    class Node{
        int indx;
        Node next;
        public Node(int x)  {
            indx = x;
            next = null;
        }
    }
    */


}
