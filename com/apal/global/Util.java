package com.apal.global;

public class Util {
    public static void print(int[] arr, String msg) {
        System.out.println(msg);
        for(int elem:arr)
            System.out.print(elem+" , ");
        System.out.println();
    }

    public static void swap(int[] arr, int a, int b)   {
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }
}
