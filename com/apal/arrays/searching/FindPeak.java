package com.apal.arrays.searching;
/*
https://www.geeksforgeeks.org/find-a-peak-in-a-given-array/
 */
public class FindPeak {

    public static void main(String[] args)  {
        int[] a = {10, 20, 15, 2, 23, 90, 67};
        new FindPeak().findPeak(a);
    }

    public void findPeak(int[] a)   {
        if(a.length == 1)
        {
            System.out.println("Peak "+a[0]);
            return;
        }
        for(int i=0;i<a.length;i++) {
            if(i==0)    {
                if(a[i] > a[i+1])
                    System.out.println("Peak "+a[i]);
                continue;
            }
            if(i==a.length-1)   {
                if(a[i] > a[i-1])
                    System.out.println("Peak "+a[i]);
                continue;
            }
            if(a[i] > a[i-1] && a[i] > a[i+1])
                System.out.println("Peak "+a[i]);
        }
    }
}
