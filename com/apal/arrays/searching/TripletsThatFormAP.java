package com.apal.arrays.searching;

public class TripletsThatFormAP {
    public static void main(String[] args)  {
        int[] a = { 2, 6, 9, 12, 17, 22, 31, 32, 35, 42 };
        int prev = 0;
        int cur = 0;
        prev = Math.abs(a[0] - a[1]);
        for(int i=2;i<a.length;i++) {
            cur = Math.abs(a[i] - a[i-1]);
            if(cur == prev)
                System.out.println("AP Triplet "+a[i-2]+" "+a[i-1]+" "+a[i]);
            prev = cur;
        }
    }

}
