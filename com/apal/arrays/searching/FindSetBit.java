package com.apal.arrays.searching;

public class FindSetBit {
    public static void main(String[] args)  {
        FindSetBit fsb = new FindSetBit();
        fsb.findRightMostSetBit(24);

    }

    public void findRightMostSetBit(int no)    {
        int setbitno = 0;
        setbitno = no & ~(no-1);
        System.out.println(no + " " + setbitno);
    }
}