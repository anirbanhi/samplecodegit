package com.apal.arrays.searching;

import java.util.Arrays;

/*
https://www.geeksforgeeks.org/find-four-numbers-with-sum-equal-to-given-sum/
 */
public class FindFourElemHavingASum {
    public static void main(String[] args)  {
        int[] a = {10, 2, 3, 4, 5, 9, 7, 8};
        int x = 23;
        FindFourElemHavingASum fes = new FindFourElemHavingASum();
        fes.findElements(a,x);
    }

    public void findElements(int[] a, int x)  {
        Arrays.sort(a);
        for (int i:a)
            System.out.print(i+" ");
        for(int i=0;i<a.length;i++) {
            int s = x - a[i];
            for(int j=i+1;j<a.length && s >-1 ;j++){
                s = x -a[i] - a[j];
                for(int k=j+1,l=a.length-1;k<l && s > -1; )   {
                    int temp = a[k]+a[l];
                    if( temp == s) {
                        System.out.println("Found " + a[i] + " + " + a[j] +
                                " + " + a[k] + " + " + a[l]);
                        k++;
                        l--;
                    }
                    else if(temp > s)
                        l--;
                    else k++;
                }
            }
        }
    }
}
