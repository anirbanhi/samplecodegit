package com.apal.arrays.searching;
/*
Find closest element in a sorted array
 */
public class MinimumDiffElem {
    public static void main(String[] args)  {
        int[] a = {10, 22, 28, 29, 30, 40};
        MinimumDiffElem mde = new MinimumDiffElem();
        System.out.println(mde.findMinDiffElem(a,32));
        System.out.println(mde.findMinDiffElem(a,29));
        System.out.println(mde.findMinDiffElem(a,45));
    }
    public int findMinDiffElem(int[] a, int x) {
        int lo = 0;
        int hi = a.length - 1;
        int mid = 0;
        int diff = Integer.MAX_VALUE;
        while (lo <= hi)    {
            mid = ( lo + hi )/2;
            if(x > a[mid])  {
                diff = Math.min(Math.abs(x-a[mid]),diff);
                lo = mid + 1;
            } else if(x < a[mid])   {
                diff = Math.min(Math.abs(x-a[mid]),diff);
                hi = mid - 1;
            } else  return x;
        }
        if(diff == Integer.MAX_VALUE)   {
            if(Math.abs(x - a[lo]) > Math.abs(x - a[hi]))
                return a[lo];
            else
                return a[hi];
        } else
            return Math.abs(x-diff);
    }
}
