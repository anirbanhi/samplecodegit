package com.apal.arrays.searching;
/*
https://www.geeksforgeeks.org/find-the-two-repeating-elements-in-a-given-array/
 */
public class Find2Repeating {
    public static void main(String[] args)  {
        int[] a = {4, 2, 4, 5, 2, 3, 1,1};
        int n = 5;

        for(int i=0;i<a.length;i++) {
            int elem = a[i];
            int absLoc = Math.abs(elem);
            if(a[absLoc] < 0)
                System.out.println("Duplicate "+absLoc);
            else {
                int val = a[absLoc];
                a[absLoc] = Math.negateExact(val);
            }
        }
    }
}
