package com.apal.arrays.searching;

public class FindMissingAndDuplicate {

    public static void main(String[] args)  {

        int[] arr1 = {4, 3, 6, 2, 1, 1};

        int[] arr2 = {3,1,3};

        new FindMissingAndDuplicate().findElem(arr1);
        new FindMissingAndDuplicate().findElemReplace(arr1);
    }

    public void findElemReplace(int[] arr)   {
            for(int i=0;i<arr.length;i++)   {
                int abs_val = Math.abs(arr[i]);
                if(arr[abs_val-1] > 0)
                    arr[abs_val-1] = -arr[abs_val-1];
                else
                    System.out.println("Duplicate value is " + abs_val);
            }

            for(int i=0;i<arr.length;i++)
                if(arr[i]>0)
                    System.out.println("Missing value is "+(i+1));
        System.out.println();
            for(int i : arr)
                System.out.print(i + ", ");
    }

    public void findElem(int[] arr){
        int sum = arr[0];
        int i = 0;
        for(i=1;i<arr.length;i++){
            sum = sum ^ arr[i];
        }
        System.out.println("Missing " + sum);
//        System.out.println("Duplicate " + arr[sum]);

    }
}
