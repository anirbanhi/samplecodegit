package com.apal.arrays.searching;
/*
https://www.geeksforgeeks.org/find-number-pairs-xy-yx/
 */
import java.util.HashSet;
import java.util.Set;

public class FindPairs {
    public static void main(String[] args)  {
        FindPairs fp = new FindPairs();
        int[] x = {2,1,6};
        int[] y = {1,5};
        //int x[] = {10, 19, 18};
        //int y[] = {11, 15, 9};
        fp.findPairs(x,y);
    }

    private void findPairs(int[] x, int[] y)    {
        //special cases
        int[][] sp = new int[5][5];
        for(int i=0;i<5;i++)    {
            for(int j=0;j<5;j++) {
                if (i < j)
                    sp[i][j] = 1;
                else
                    sp[i][j] = 0;
            }
        }
        sp[4][2] = 0;sp[4][3] = 0;sp[4][3] = 0;

        //sort y
        sort(y);
        print(y);

        //find index of greater than x[i] in y
        int ans = 0;
        Set<Integer> xsmall = new HashSet<>();
        Set<Integer> ysmall = new HashSet<>();
        for(int i=0;i<x.length;i++) {
            if(x[i] < 5)
            {
                xsmall.add(x[i]);
                continue;
            }
            int yindex = binarySearch(y,x[i]);
            if(yindex == -1)
                continue;
            ans += y.length - yindex;
        }
        for(int i=0;i<y.length;i++) {
            if (y[i] < 5)
                ysmall.add(y[i]);
        }

        for(int i=0;i<5;i++)    {
            for(int j=0;j<5;j++)    {
                if(xsmall.contains(i) && ysmall.contains(j))
                    ans++;
            }
        }
        System.out.println("Total Pair "+ans);
    }

    private int binarySearch(int[] y, int x)  {
        int lo = 0;
        int hi = y.length -1;
        int mid = 0;
        int last = -1;
        while(lo <= hi)  {
            mid = lo + (hi - lo)/2;
            if(y[mid] == x && (mid-1) > -1)
                return mid+1; ///
            if(x > y[mid]) {
                lo = mid + 1;
            }
            else {
                hi = mid - 1;
                last = mid;
            }
        }
        return last;
    }

    private void sort(int[] y)  {
        print(y);
        //shell sort
        int len = y.length;
        int k = len;
        for(k = k / 2; k > 0; k = k /2) {
            for(int i = len-1; i >- 1 ; i -= k)    {
                int iprev = i - k;
                if( iprev > -1 && y[i] < y[iprev]) {
                    //swap
                    int temp = y[i];
                    y[i] = y[iprev];
                    y[iprev] = temp;
                }
            }
        }
        print(y);
    }

    private void print(int[] y) {
        System.out.println();
        for(int i=0;i<y.length;i++)
            System.out.print(y[i]+" , ");
        System.out.println();
    }
}
