package com.apal.arrays.searching;
//https://www.geeksforgeeks.org/ceiling-in-a-sorted-array/
public class CeilingOfASortedArray {
    public static void main(String[] args)  {
        int[] a = {1, 2, 8, 10, 10, 12, 19};
        int x = 1;  //  0, 5, 20
        CeilingOfASortedArray co = new CeilingOfASortedArray();
        System.out.println("Ceiling of "+0+"  "+co.findCeiling(a,0));
        System.out.println("Ceiling of "+1+"  "+co.findCeiling(a,1));
        System.out.println("Ceiling of "+5+"  "+co.findCeiling(a,5));
        System.out.println("Ceiling of "+20+"  "+co.findCeiling(a,20));
    }
    public int findCeiling(int[] a, int x)   {
        int lo = 0;
        int hi = a.length - 1;
        int res = -1;
        while(lo <= hi)  {
            int mid = lo + (hi-lo)/2;
            if(x > a[mid]) {
                lo = mid +1;
            } else if (x <= a[mid])  {
                res = mid;
                hi = mid-1;
            }
        }
        if(res >-1 && res < a.length)
            return a[res];
        else
            return -1;
    }
}
