package com.apal.arrays.searching;

/*
https://www.youtube.com/watch?v=v_wj_mOAlig
https://www.hackerearth.com/practice/notes/binary-indexed-tree-or-fenwick-tree/
https://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
 */
public class BinaryIndexedTree {

    public static void main(String[] args)  {
        BinaryIndexedTree bit = new BinaryIndexedTree();
        int[] arr = { 1, 7, 3, 0, 5, 8, 3, 2, 6, 2, 1, 1, 4, 5 };
        bit.initializeBit(arr);
        System.out.println("first 5 " + bit.getSum(4));
        System.out.println("first 13 " + bit.getSum(12));
        bit.updateSum(2,5);
        System.out.println("first 5 " + bit.getSum(4));
        System.out.println("first 13 " + bit.getSum(12));
    }

    private int[] bit;

    public void initializeBit(int[] arr) {
        bit = new int[arr.length + 1];

        //initialize all elements with 0
        for(int i=0;i<bit.length;i++)
            bit[i] = 0;

        //copy elemets from input arr[0 - n-1] to [1 - n]
        for(int i=0;i<arr.length;i++)
            bit[i+1] = arr[i];

        printArray(bit);

        for(int i=1; i<bit.length; i++) {
            int nextIndex = i + (i & -i);
            System.out.println("i " + i + " nextIndex " + nextIndex);
            if(nextIndex < bit.length)
                bit[nextIndex] = bit[nextIndex] + bit[i];
        }

        printArray(bit);
    }

    public void printArray(int[] arr)    {
        System.out.println();
        for(int i : arr)
            System.out.print(i+" , ");
        System.out.println();
    }

    public int getSum(int indx) {
        if(indx > bit.length -1)
            return -1;
        indx++;
        int sum = 0;
        for(;indx > 0; indx = indx - (indx & -indx))    {
            sum = sum + bit[indx];
        }
        return sum;
    }

    public void updateSum(int indx, int val) {
        if(indx > bit.length - 1)
        {
            System.out.println("not possible");
            return;
        }
        indx++;
        for(;indx < bit.length;indx = indx + (indx & -indx))    {
            bit[indx] = bit[indx] + val;
        }
    }
}
