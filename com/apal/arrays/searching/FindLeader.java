package com.apal.arrays.searching;

public class FindLeader {
    public static void main(String[] args)  {
        int[] a = {16,17,4,3,5,2};
        int max = Integer.MIN_VALUE;
        for(int i=a.length-1;i>-1;i--)  {
            if(a[i] > max)
            {
                System.out.print(a[i]+", ");
                max = a[i];
            }
        }
    }
}
