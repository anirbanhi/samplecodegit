package com.apal.arrays.searching;

//https://www.geeksforgeeks.org/print-maximum-shortest-distance/

public class PairWithSumWithShortestDistance {
    public static void main(String[] args)  {
        int a[] = {2, 4, 3, 2, 1};
        int k = 5;
        PairWithSumWithShortestDistance p = new PairWithSumWithShortestDistance();
        int no = p.findPairSum(a,5,0,4);
        System.out.println(no);
    }

    private int findPairSum(int[] a, int k, int i, int j)  {
        if(i>=j)
            return -1;

        int c1 = -1;
        if(a[i]+a[j] == k)
        {
            c1 = Math.max( i,Math.abs(j-a.length-1));
            return c1;
        }
        int c2 = findPairSum(a,k,i+1,j);
        int c3 = findPairSum(a,k,i,j-1);
        return Math.max(c2,c3);
    }
}
