package com.apal.arrays.TwoPointer;

public class TwopointerExample {
    public static void main(String[] args)  {

        //findElemOfGivenSum(new int[]{1,2,3,4,6},10);
        //removeDuplicatesFromSortedArray(new int[]{3,3,3,4,5,5,6,7,8,9,9});
    }

    public static void removeDuplicatesFromSortedArray(int[] arr)    {
        int left = 1,right =1;
        for(left=1; left< arr.length; left++)  {
            if(arr[left] != arr[left-1])
            {
                arr[right] = arr[left];
                right++;
            }
        }
        for (int i=0;i<right;i++)
            System.out.print(arr[i]+",");
    }

    public static void findElemOfGivenSum(int[] arr, int sum) {
        int left, right = 0;
        int len = arr.length;
        int tempsum = 0;
        for(left=0,right=len-1; left<right; ){
            tempsum = arr[left] + arr[right];
            if(tempsum == sum)
                break;
            if(tempsum > sum)
                right--;
            if(tempsum < sum)
                left++;
        }
        if(arr[left]+arr[right] == sum)
            System.out.println("elements found "+arr[left]+" "+arr[right]);
        else
            System.out.println("not found");
    }
}
