package com.apal.arrays.rangequeries;

public class SparseTable {
    public static void main(String[] args){

        int[] arr = {1,7,2,9,3,5,8,3,7};

        int k = findLog(arr.length);
        int n = arr.length;
        System.out.println(" k " + k);
        int[][] sptable = new int[n][k];

        for(int j=0;j<n;j++){
            sptable[j][0]=1;
        }
    }

    private static int[] logs = new int[10];
    private static int findLog(int n){
        int j = 0;
        for(int i=1;i<=n;i=i*2,j++);
        return j-1;
    }
}
