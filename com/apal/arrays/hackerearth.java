package com.apal.arrays;



import java.util.*;

public class hackerearth {

    public static void main(String[] args)  {
        System.out.println(solve(6));
        System.out.println(solve(5));
        System.out.println(solve(28));

        String abc = "1010";
        System.out.println(abc.length());
        char[] abc1 = abc.toCharArray();
        for(int i=0;i<abc.length();i++)
            System.out.println(abc1[i]);


        abc = "1 1 0";
        String[] abc2 = abc.split (" ");
        System.out.println(" -- " + abc2[0]);
        System.out.println(" -- " + abc2[1]);
        System.out.println(" -- " + abc2[2]);

        Map<Integer,Integer> map = new HashMap<>();
        map.put(1,map.getOrDefault(1,0)+1);

        Comparator<Elem> c1 = new Comparator<Elem>() {
            @Override
            public int compare(Elem o1, Elem o2) {
                return 0;
            }
        };
        PriorityQueue<Elem> pq = new PriorityQueue<>(new Comparator<Elem>() {
            @Override
            public int compare(Elem o1, Elem o2) {
                return o1.count - o2.count;
            }
        });

        PriorityQueue<Elem> pq1 = new PriorityQueue<>(pq);
        pq.addAll(pq1);
        pq.remove();
    }

    public class Elem{
        int prop;
        int count;
    }
    public static String solve(int n){
        int sum=0;
        for(int i=1;i<n/2+1;i++){
            if(n%i == 0)
            {
                sum += i;
            }
        }
        if(sum == n)
            return "YES";
        else
            return "NO";

    }
}
