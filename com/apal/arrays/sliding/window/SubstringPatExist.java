package com.apal.arrays.sliding.window;

import java.util.HashMap;
import java.util.Map;

/*
Given a string and a pattern, find all anagrams of the pattern in the given string.
 */
public class SubstringPatExist {

    public static void main(String[] args) {
        new SubstringPatExist().driver();
    }

    public void driver()    {
        System.out.println("true "+substringPatternExists("oidbcaf", "abc"));
        System.out.println("false "+substringPatternExists("odicf","dc"));
        System.out.println("true "+substringPatternExists("bcdxabcdy","bcdyabcdx"));
        System.out.println("true "+substringPatternExists("aaacb","abc"));
    }

    public boolean substringPatternExists(String s, String p)   {

        Map<Character, Integer> map = resetPatMap(p);
        for(int we=0;we<s.length();we++)    {
            char ch = s.charAt(we);
            if(!map.containsKey(ch))    {
                map = resetPatMap(p);
            } else {
                map.put(ch,map.get(ch)-1);
                if(map.get(ch)==0)
                    map.remove(ch);
                if(map.size()==0)
                    return true;
            }
        }
        return false;
    }

    public Map<Character, Integer> resetPatMap(String p) {
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0;i<p.length();i++){
            char ch = p.charAt(i);
            map.put(ch, map.getOrDefault(ch,0)+1);
        }
        return map;
    }

}
