package com.apal.arrays.sliding.window;

import java.util.HashMap;
import java.util.Map;

//Longest Substring with K Distinct Characters (medium)
public class LongestSubstringWithKDistinctChars {

    public static void main(String[] args)  {
        System.out.println(longestSubstringWithDistinctCharacters("araaci", 2));
        System.out.println(longestSubstringWithDistinctCharacters("araaci", 1));
        System.out.println(longestSubstringWithDistinctCharacters("cbbebi", 3));
    }

    public static String longestSubstringWithDistinctCharacters(String str, int k) {
        int start=0;
        Map<Character, Integer> characterMap = new HashMap<>();
        String subarray = "";
        for(int end=0; end < str.length() ; end++)  {
            char ch = str.charAt(end);
            characterMap.put(ch,characterMap.getOrDefault(ch,0)+1);

            while(characterMap.size() > k ) {
                if((end-start+1) > subarray.length())
                    subarray = str.substring(start, end);
                char chr = str.charAt(start);
                characterMap.put(chr, characterMap.get(chr)-1);
                if(characterMap.get(chr) == 0)
                    characterMap.remove(chr);
                start++;
            }
        }
        return subarray;
    }
}
