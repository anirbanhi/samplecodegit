package com.apal.arrays.sliding.window;

import java.util.HashMap;

//smallest subarray with a given sum
public class SlindingWindowEducative {
    public static void main(String[] args)  {
//        System.out.println( longestSubarrayWithGivenSum(new int[]{2, 1, 5, 2, 3, 2}, 7));
//        System.out.println( longestSubarrayWithGivenSum(new int[]{2, 1, 5, 2, 8}, 7));
//        System.out.println( longestSubarrayWithGivenSum(new int[]{3, 4, 1, 1, 6}, 8));

        System.out.println("aabccbb "+noRepeatSubstring("aabccbb".toCharArray()));
        System.out.println("abbbb "+noRepeatSubstring("abbbb".toCharArray()));
        System.out.println("abccde "+noRepeatSubstring("abccde".toCharArray()));
    }

    public static int longestSubarrayWithGivenSum(int[] arr, int target) {
        int windowStart = 0;
        int windowEnd = 0;
        int sum = 0;
        int minlength = Integer.MAX_VALUE;
        int length = 0;
        for(windowEnd = 0; windowEnd < arr.length ; windowEnd++)    {
            sum += arr[windowEnd];
            while(sum >= target)    {
                length = windowEnd - windowStart + 1;
                minlength = Math.min(minlength, length);
                sum -= arr[windowStart];
                windowStart ++;
            }
        }
        return minlength;
    }

    public static int noRepeatSubstring(char[] str)   {
        int windowStart = 0;
        int windowEnd = 0;
        int maxlen = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for(windowEnd=0; windowEnd<str.length; windowEnd++)    {
            char ch = str[windowEnd];
            if(map.containsKey(ch)) {
                windowStart = Math.max(windowStart, map.get(ch)+1);
            }
            map.put(ch, windowEnd);
            maxlen = Math.max(maxlen, windowEnd-windowStart+1);
        }
        return maxlen;
    }


    public static int characterReplacement(char[] str)   {
        int windowStart = 0;
        int windowEnd = 0;
        int maxlen = 0;
        HashMap<Character, Integer> map = new HashMap<>();
        for(windowEnd=0; windowEnd<str.length; windowEnd++)    {
            char ch = str[windowEnd];
            if(map.containsKey(ch)) {
                windowStart = Math.max(windowStart, map.get(ch)+1);
            }
            map.put(ch, windowEnd);
            maxlen = Math.max(maxlen, windowEnd-windowStart+1);
        }
        return maxlen;
    }
}