package com.apal.arrays.sliding.window;

import java.util.HashMap;
import java.util.Map;

/*
Given a string, find the length of the longest substring in it with no more than K distinct characters.
 */
public class LongestSubstringWithK {

    public static void main(String[] args) {
        new LongestSubstringWithK().driver();
    }

    public void driver()    {
        System.out.println("4 == "+longestSubstring("araaci",2));
        System.out.println("2 == "+longestSubstring("araaci",1));
        System.out.println("5 == "+longestSubstring("cbbebi",3));
    }

    public int longestSubstring(String s, int k)    {
        Map<Character, Integer> map = new HashMap<>();
        int ws=0;
        int maxSize=0;
        for(int we=0;we<s.length();we++)   {
            char ch = s.charAt(we);
            if(map.containsKey(ch)) {
                map.put(ch, map.get(ch)+1);
            } else {
                map.put(ch,1);
            }
            while(map.size()>k) {
                char chgo = s.charAt(ws);
                map.put(chgo, map.get(chgo)-1);
                if(map.get(chgo)==0)
                    map.remove(chgo);
                ws++;
            }
            int size = we-ws+1;
            if(size > maxSize)
                maxSize = size;
        }
        return maxSize;
    }
}
