package com.apal.arrays.sliding.window;

/*
https://leetcode.com/problems/minimum-window-substring/
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinimumWindowSubstring {

    public static void main(String[] args) {
        new MinimumWindowSubstring().driver();
    }

    public void driver()    {
        String s = "oidbcaf", t = "abc";
        System.out.println("true = "+anagramMatch(s,t));

        String s1 = "oidbcdaf", t1 = "abc";
        System.out.println("false = "+anagramMatch(s1,t1));

        String s2 = "oidbcaaf", t2 = "abac";
        System.out.println("true = "+anagramMatch(s2,t2));

        String s3 = "oidbcaf", t3 = "abac";
        System.out.println("false = "+anagramMatch(s3,t3));

        String s4 = "ppqp", t4 = "pq";
        System.out.println(" "+anagramMatchAll(s4,t4));
    }

    public boolean anagramMatch(String s, String t) {
        Map<Character,Integer> patmap = new HashMap<>();
        for(int i=0;i<t.length();i++)   {
            char ch = t.charAt(i);
            patmap.put(ch, patmap.getOrDefault(ch, 0) + 1);
        }

        int matched = 0;
        for(int winend=0,winst=0;winend<s.length();winend++)   {
            char ch1 = s.charAt(winend);

            if(patmap.containsKey(ch1)) {
                patmap.put(ch1, patmap.get(ch1) - 1);
                if (patmap.get(ch1) == 0) {
                    matched++;
                }
            }
                if(matched == patmap.size())
                    return true;

                if(winend >= t.length()-1) {
                    char ch2 = s.charAt(winst);
                    winst++;
                    if(patmap.containsKey(ch2)) {
                        if(patmap.get(ch2) == 0)
                            matched--;
                        patmap.put(ch2, patmap.get(ch2) + 1);
                    }
                }
        }
        return false;
    }

    public List<Integer> anagramMatchAll(String s, String t) {
        Map<Character,Integer> patmap = new HashMap<>();
        for(int i=0;i<t.length();i++)   {
            char ch = t.charAt(i);
            patmap.put(ch, patmap.getOrDefault(ch, 0) + 1);
        }

        List<Integer> response = new ArrayList<>();
        int matched = 0;
        for(int winend=0,winst=0;winend<s.length();winend++)   {
            char ch1 = s.charAt(winend);

            if(patmap.containsKey(ch1)) {
                patmap.put(ch1, patmap.get(ch1) - 1);
                if (patmap.get(ch1) == 0) {
                    matched++;
                }
            }
            if(matched == patmap.size())
                response.add(winst);

            if(winend >= t.length()-1) {
                char ch2 = s.charAt(winst);
                winst++;
                if(patmap.containsKey(ch2)) {
                    if(patmap.get(ch2) == 0)
                        matched--;
                    patmap.put(ch2, patmap.get(ch2) + 1);
                }
            }
        }
        return response;
    }

    public String minLengthSubstring(String s, String t)  {
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0;i<t.length();i++) {
            char ch = t.charAt(i);
            map.put(ch, map.getOrDefault(ch,0)+1);
        }
        int matched = 0;
        int minlength = Integer.MAX_VALUE;
        for(int winend=0,winst=0;winend<s.length();winend++)    {
            char ch = s.charAt(winend);

            if(map.containsKey(ch)) {
                map.put(ch,map.get(ch)-1);
                if(map.get(ch) >= 0)
                    matched++;
            }
            while(matched == t.length())   {
                int newlength = winend - winst + 1;
                minlength = Math.min(newlength, minlength);
                
            }
        }
        return null;
    }

}