package com.apal.arrays.sliding.window;

public class SlidingWindowSumGE {
    public static void main(String[] args)  {

        SlidingWindowSumGE sw = new SlidingWindowSumGE();
        int[] arr = {1,2,3,4,5};
        int k = 2;
        int arrLength = arr.length;
        arrLength += k ;
        int[] modifiedArr = new int[arrLength];
        sw.print(arr,"Initial Array");

        sw.modifyArr(arr,modifiedArr,k);
        sw.print(modifiedArr,"Converted Array");

        sw.findSum(arr,modifiedArr);
        sw.print(modifiedArr,"Converted Array With Sum");
    }

    public void print(int[] arr, String msg)    {
        System.out.println(msg);
        for(int elem:arr)
            System.out.print(elem+" , ");
        System.out.println();
    }

    public void modifyArr(int[] arr, int[] modifiedArr,int k)   {
        //fill with int.max
        for(int j=0;j<modifiedArr.length;j++)
            modifiedArr[j] = Integer.MAX_VALUE;

        //right movement/when k is positive
        if(k > 0)
        {
           int i=0;
           for(i=0;i<arr.length;i++)
           {
               modifiedArr[i]=arr[i];
           }
           for(int j=0;j<k;j++,i++)
            modifiedArr[i] = arr[j];
        }
    }

    public void findSum(int[] arr, int[] modifiedArr)   {
        
    }
}
