package com.apal.arrays.sliding.window;

import java.util.HashMap;
import java.util.Map;

/*
Longest Substring with Distinct Characters (hard)
 */
public class NoRepeatSubstring {
    public static void main(String[] args) {
        new NoRepeatSubstring().driver();
    }
    public void driver()    {
        System.out.println(" 3 = "+findLongest("aabccbb"));
        System.out.println(" 2 = "+findLongest("abbbb"));
        System.out.println(" 3 = "+findLongest("abccde"));
    }

    public int findLongest(String s)    {

        Map<Character, Integer> map = new HashMap<>();
        int ws=0;
        int maxsize=0;
        for(int we=0;we<s.length();we++)    {
            char ch = s.charAt(we);
            if(!map.containsKey(ch)) {
                map.put(ch,we);
            } else {
                ws = Math.max(ws, map.get(ch)+1);
                map.put(ch,we);
            }
            maxsize = Math.max(maxsize, we-ws+1);
        }
        return maxsize;
    }
}
