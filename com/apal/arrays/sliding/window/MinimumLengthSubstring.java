package com.apal.arrays.sliding.window;

import java.util.HashMap;
import java.util.Map;

/*
https://leetcode.com/problems/minimum-window-substring/
 */
public class MinimumLengthSubstring {

    public static void main(String[] args) {
        new MinimumLengthSubstring().driver();
    }

    public void driver()    {
        System.out.println("abdec "+findMinimumWindow("aabdec","abc"));
        System.out.println("abc "+findMinimumWindow("abdabca","abc"));
    }

    public String findMinimumWindow(String s, String p) {
        Map<Character, Integer> map = resetmap(p);
        Map<Character, Integer> window = new HashMap<>();
        for(Character c : map.keySet())    {
            window.put(c,0);
        }
        int having = 0;
        int target = map.size();
        int ws=0;
        int[] op = new int[]{-1,-1};
        int minLen = Integer.MAX_VALUE;
        int we = 0;
        for(we=0;we<s.length();we++)    {
            char ch = s.charAt(we);
            if(map.containsKey(ch)) {
                    window.put(ch,window.get(ch)+1);
                    if(window.get(ch) == map.get(ch))
                        having++;
                    while(having==target)  {
                        if(we-ws+1 < minLen)    {
                            minLen = we-ws+1;
                            op[0] = ws;
                            op[1] = we;
                        }

                        char chleft = s.charAt(ws);
                        if(map.containsKey(chleft) ) {
                            window.put(chleft, window.get(chleft)-1);
                            if(window.get(chleft) < map.get(chleft))
                                having--;
                        }
                        ws++;
                    }
            }
        }
        return minLen != Integer.MAX_VALUE ? s.substring(op[0],op[1]+1) : "";
    }

    public Map<Character, Integer> resetmap(String p)   {
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0;i<p.length();i++) {
            char ch = p.charAt(i);
            map.put(ch, map.getOrDefault(ch,0)+1);
        }
        return map;
    }
}
