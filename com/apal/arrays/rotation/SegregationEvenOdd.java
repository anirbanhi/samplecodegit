package com.apal.arrays.rotation;
/*
https://www.geeksforgeeks.org/segregate-even-odd-numbers-set-3/
 */
public class SegregationEvenOdd {
    public static void main(String[] args)  {
        int[] arr1 = {3,4,2,6,7,8,9,1};
        int[] arr = {45,6,9,2,11,51,98,54,2,1};
        for (int i:arr)
            System.out.print(i+", ");
        segregate(arr);
        System.out.println();
        for (int i:arr)
            System.out.print(i+", ");
    }
    public static void segregate(int[] arr)  {
        int i = 0;
        int j = arr.length - 1;
        while(i <= j)    {
            while(i < arr.length && arr[i]%2 == 0)    {
                i++;
            }
            while(j > -1 && arr[j]%2 == 1)    {
                j--;
            }
            if(i < arr.length && j > -1 && i < j)    {
                int t = arr[i];
                arr[i] = arr[j];
                arr[j] = t;
                i++;
                j--;
            }
        }
    }
}