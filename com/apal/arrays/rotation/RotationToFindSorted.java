package com.apal.arrays.rotation;
/*
Find minimum in a sorted array with no duplicate
Find minimum in a sorted array with duplicate

https://leetcode.com/explore/challenge/card/july-leetcoding-challenge/547/week-4-july-22nd-july-28th/3401/
 */
public class RotationToFindSorted {
    public static void main(String[] args)  {
        int[] a1 = {4,5,1,0,2,3};
        int[] a2 = {1,2,3};
     //   findMin(a1);
     //   findMin(a2);
        int[] b1 = {2,2,2,0,1};
        int[] b2 = {3,1};
     //   System.out.println("min is "+findMinWithDuplicate(a1,0,a1.length-1));
     //   System.out.println("min is "+findMinWithDuplicate(a2,0,a2.length-1));
     //   System.out.println("min is "+findMinWithDuplicate(b1,0,b1.length-1));
        System.out.println("min is "+findMinWithDuplicate(b2,0,b2.length-1));
    }

    public static void findMin(int[] a) {
        int lo = 0;
        int hi = a.length - 1;
        int min = Integer.MAX_VALUE;

        while(lo < hi) {
            int mid = lo + (hi - lo)/2;
            if(a[mid] > a[mid+1])  {
                min = a[mid+1];
                break;
            }
            if(a[mid] < a[mid-1])  {
                min = a[mid];
                break;
            }
            if(a[lo] < a[mid])  {
                lo = mid +1;
            } else if(a[mid] < a[hi])   {
                hi = mid -1 ;
            }
        }
        if(min == Integer.MAX_VALUE)
            System.out.println("minimum element is "+a[0]);
        else
            System.out.println("minimum element is "+min);
    }

    public static int findMinWithDuplicate(int[] a, int l, int h)   {
        int mid = (l+h)/2;
        if(mid == h) return a[mid];
        if(h < 0)   return a[mid];
        if((mid < h && mid > l )&& (a[mid] < a[mid+1]) &&
                (a[mid-1] > a[mid]))
            return a[mid];
        if(a[mid] > a[h])   {
            if(a[mid] > a[mid+1])   return a[mid+1];
            else return findMinWithDuplicate(a,mid+1,h);
        }   else if(a[mid] < a[h])  {
            if(mid >0 && a[mid-1] > a[mid])   return a[mid];
            else return findMinWithDuplicate(a,l,mid-1);
        }   else   {
            if(mid == 0)    return findMinWithDuplicate(a,mid+1,h);
            else
            return Math.min(
                    findMinWithDuplicate(a,l,mid-1),
                    findMinWithDuplicate(a,mid+1,h)
            );
        }
    }
}
