package com.apal.arrays.rotation;

import com.apal.linkedlist.LLBasicOperations;
import com.apal.linkedlist.Node;

public class LLRotation {
    public static void main(String[] args)  {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);

        LLBasicOperations.printLinkList(head,"");

        Node head1 = reverseInRange(head,1,3);
        LLBasicOperations.printLinkList(head1,"");

        Node head2 = reverseInRange(head1,4,5);
        LLBasicOperations.printLinkList(head2,"");

        Node head3 = reverseInRange(head1,1,5);
        LLBasicOperations.printLinkList(head3,"");
    }

    public static Node reverseInRange(Node head, int st, int end)   {
        Node temp = head;
        Node cur = null;
        Node prev = null;
        int i =1;
        while(temp!=null && i < st) {
            temp = temp.next;
            i++;
        }
        if(temp != null && i == st)   {
            cur = temp;
        }
        i = st;

        while(temp!=null && i <= end) {
            temp = temp.next;
            i++;
        }
        if(temp!=null && --i == end)
        {
            prev = temp;
            i = st;
            while(cur!=null && i < end)    {
                Node t = cur.next;
                cur.next = prev;
                prev = cur;
                cur = t;
                i++;
            }
        }
        return prev;
    }
}
