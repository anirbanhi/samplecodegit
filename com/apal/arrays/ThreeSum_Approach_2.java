package com.apal.arrays;

/*
https://leetcode.com/problems/3sum/solution/
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum_Approach_2 {

    public static void main(String[] args) {
        new ThreeSum_Approach_2().driver();
    }

    public void driver()    {
        int[] num = {-1,0,1,2,-1,-4};
        threeSum(num);
    }

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ret = new ArrayList<>();
        Arrays.sort(nums);
        int n = nums.length;
        for(int i=0;i<n-2;i++)    {
            int first = nums[i];
            int[] resp = find2Sum(nums,i+1,n-1,-first);
            if(resp != null)    {
                List<Integer> list = new ArrayList<>();
                list.add(first);
                list.add(resp[0]);
                list.add(resp[1]);
            }
        }
        return null;
    }

    public int[] find2Sum(int[] nums, int st, int end, int target) {
        if(st>= nums.length || end >= nums.length)
            return null;
        for(int i=st,j=end;i<j;)    {
            if(nums[i]+nums[j]==target)
                return new int[]{nums[i],nums[j]};
            if(nums[i]+nums[j] > target)
                j--;
            else i++;
        }
        return null;
    }

}