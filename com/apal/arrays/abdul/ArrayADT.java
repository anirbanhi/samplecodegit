package com.apal.arrays.abdul;

public class ArrayADT {
    public static void main(String[] args)  {
        ArrayADT adt = new ArrayADT();
        int[] arr = new int[10];
        arr[0] = 3;
        arr[1] = 25;
        arr[2] = 5;
        arr[3] = 12;
        arr[4] = 9;

        adt.display(arr);
        adt.insertAtPos(arr, 2,100, 5);
        adt.display(arr);
    }

    private void insertAtPos(int[] arr, int index, int elem, int arrlen)  {
        int rt = arrlen - 1;
        if(index<0 || index>arrlen)
            return;

        for(;rt>=index;rt--){
            arr[rt+1] = arr[rt];
        }
       // if(rt==index)
            arr[index] = elem;

    }
    private void display(int[] arr)  {
        System.out.println();
        for (int i=0;i<arr.length;i++)
            System.out.print(arr[i]+" ");
    }
}
