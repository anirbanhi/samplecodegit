package com.apal.arrays.jump;

/*
    https://leetcode.com/problems/jump-game/

 */

public class JumpGame {

    public static void main(String[] args)  {
        new JumpGame().driver();

    }

    public void driver()    {
        int[] arr1 = {2,3,1,1,4};
        System.out.println("Result = "+jump(arr1,0));

        int[] arr = {3,2,1,0,4};
        System.out.println("Result = "+jump(arr,0));
    }

    public boolean jump(int[] arr, int cur) {
        if(cur == (arr.length - 1))
            return true;
        if(cur >= arr.length)
            return false;
        for(int i=1;i<=arr[cur];i++) {
            boolean res = jump(arr, cur+i);
            if(res) return true;
        }
        return false;
    }
}