package com.apal.arrays.mostcommon;

/*
https://practice.geeksforgeeks.org/problems/-rearrange-array-alternately-1587115620/1
 */

import java.util.Arrays;

public class RearrangeAlternatively {
    public static void main(String[] args) {
        new RearrangeAlternatively().driver();
    }

    private void driver()   {
        int[] a1 = new int[]{1,2,3,4,5,6,7,8,9};
        int[] a2 = new int[]{1,2,3,4,5,6,7,8};


        rearrange(a1);
        System.out.println(Arrays.toString(a1));

        rearrange(a2);
        System.out.println(Arrays.toString(a2));
    }

    private void rearrange(int[] a)    {
        int n = a.length - 1;
        int max = a.length - 1;
        int min = 0;
        int maxElem = a[max] + 1;
        for(int i=0;i<n;i++)    {
            //even pos -> for max
            if(i%2 == 0)    {
                a[i] = a[i] + (a[max] % maxElem) * maxElem;
                max --;
            } else {
                a[i] = a[i] + (a[min] % maxElem) * maxElem;
                min++;
            }
        }
        for(int i=0;i<n;i++)    {
            a[i] = a[i] / maxElem;
        }
    }
}
