package com.apal.arrays.mostcommon;

    /*
    https://practice.geeksforgeeks.org/problems/inversion-of-array-1587115620/1
 */

import java.util.Arrays;

public class CountInversions {
    public static void main(String[] args) {
        new CountInversions().driver();
    }

    private void driver()   {
        int[] a1 = new int[]{2, 4, 1, 3, 5};
        System.out.println(" a1 "+findConversionsWithSpace(a1));

        int[] a2 = new int[]{10, 10, 10};
        System.out.println(" a2 "+findConversionsWithSpace(a2));

        int[] a3 = new int[]{9,8,200,45,13,9,1,55};
        System.out.println(" a3 "+findConversionsWithSpace(a3));

        int[] a4 = new int[]{468, 335, 1, 170, 225, 479, 359, 463, 465, 206, 146, 282, 328, 462, 492, 496, 443, 328, 437, 392, 105, 403, 154, 293, 383, 422, 217, 219, 396, 448, 227, 272, 39,370, 413, 168, 300, 36, 395, 204, 312, 323};
        System.out.println(" a4 "+findConversionsWithSpace(a4));
    }

    private int findConversions(int[] a)  {
        int n = a.length;
        int[] a1 = new int[n];
        int cnt = 0;
        for(int i=0;i<n;i++)    {
            for(int j=i+1;j<n;j++)  {
                if(a[j] < a[i])
                    cnt++;
            }
        }
        return cnt;
    }

    private int findConversionsWithSpace(int[] a)  {
        int count = divide(a,0,a.length-1);
        System.out.println(" sorted "+ Arrays.toString(a));
        return count;
    }

    private int merge(int[] a, int l, int mid, int h)    {
        int i=l;
        int j=mid+1;
        int[] c = new int[a.length];
        int k = l;
        int invCount = 0;

        while(i<=mid && j<=h)    {
            if(a[i] < a[j]) {
                c[k] = a[i];
                k++;
                i++;
                invCount++;
            } else {
                c[k] = a[j];
                k++;
                j++;
            }
        }
        while(i<=mid)   {
            c[k]=a[i];
            i++;
            k++;
        }
        while(j<=h) {
            c[k]=a[j];
            k++;
            j++;
        }
        for( i=l;i<=h;i++)   {
            a[i]=c[i];
        }
        return invCount;
    }

    private int divide(int[] a, int l, int h)   {
        int totInvCount = 0;
        if(l < h) {
            int mid = (l + h) / 2;
            divide(a, l, mid);
            divide(a, mid + 1, h);
            totInvCount += merge(a, l, mid, h);
        }
        return totInvCount;
    }

}