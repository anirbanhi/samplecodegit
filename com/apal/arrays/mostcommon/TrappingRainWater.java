package com.apal.arrays.mostcommon;

/*
https://practice.geeksforgeeks.org/problems/trapping-rain-water-1587115621/1

https://leetcode.com/problems/trapping-rain-water/submissions/
*/

import java.util.Arrays;

public class TrappingRainWater {

    public static void main(String[] args) {
        new TrappingRainWater().driver();
    }

    public void driver()    {
        int[] a = new int[]{3,0,0,2,0,4};
        //System.out.println(" "+trappingWater(a,a.length));
        //System.out.println(" "+trap(a));


            Arrays.sort(a,0,4);
        System.out.println(Arrays.toString(a));

    }

    int trappingWater(int a[], int n) {
        int[] lt = new int[a.length];
        int sum = 0;
        int max = Integer.MIN_VALUE;
        int maxrt = Integer.MIN_VALUE;
        for(int i=0;i<n;i++)    {
            if(a[i] > max)
                max = a[i];
            lt[i] = max;
        }

        for(int i=n-1;i>-1;i--) {
            maxrt = Math.max(a[i],maxrt);
            sum += Math.min(maxrt, lt[i]) - a[i];
        }

        return sum;
    }

    /*
        This one is better solution.
        It has it has clear approach.
     */

    public int trap(int[] h) {
        int n = h.length;
        int maxL = 0;
        int[] ml = new int[n];
        for(int i=0;i<n;i++) {
            ml[i] = Math.max(maxL,h[i]);
            if(ml[i]>maxL)
                maxL = ml[i];
        }

        int maxR = 0;
        int[] rl = new int[n];
        for(int i=n-1;i>0;i--) {
            rl[i] = Math.max(maxR,h[i]);
            if(rl[i]>maxR)
                maxR = rl[i];
        }

        //checking this is not necessary.
        int area = 0;
        for(int i=0;i<n;i++)    {
            int a = Math.min(ml[i],rl[i])-h[i];
            int b = a > 0 ? a : 0;
            //System.out.print(b+",");
            area += b;
        }
        return area;
    }
}