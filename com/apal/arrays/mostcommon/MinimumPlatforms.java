package com.apal.arrays.mostcommon;

/*
    https://practice.geeksforgeeks.org/problems/minimum-platforms-1587115620/1
*/

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MinimumPlatforms {

    public static void main(String[] args) {
        new MinimumPlatforms().driver();
    }

    public void driver()    {
        int[] arr = new int[]{900, 940, 950, 1100, 1500, 1800};
        int[] dep = new int[]{910, 1200, 1120, 1130, 1900, 2000};
        int n = 6;
        int res = findPlatform(arr, dep, n);
        System.out.println(" No of platforms = " +  res);
    }

    int findPlatform(int arr[], int dep[], int n)
    {
        quickSortArray(dep,0,arr.length-1,arr);
        System.out.println(Arrays.toString(arr));
        System.out.println(Arrays.toString(dep));
        Queue<Integer> q = new LinkedList<>();
        int maxQ=0;

        for(int i=0;i<n;i++)    {
            if(q.size()>maxQ)
                maxQ=q.size();
            int deptime = dep[i];
            int arrtime = arr[i];
            while(q.size() > 0 && q.peek()<arrtime){
                q.remove();
            }
            q.add(deptime);
        }
        return maxQ;

    }

    public void quickSortArray(int[] a, int lo, int hi, int[] b)    {
        if(lo<hi)   {
            int p = partition(a, lo, hi, b);
            quickSortArray(a,lo,p-1, b);
            quickSortArray(a,p+1,hi, b);
        }
    }

    public int partition(int[] a, int lo, int hi, int[] b)   {
        int l = lo;
        int pivot = a[l];
        int i=l+1;
        int j=hi;
        while(i <= j) {
            while(i<=hi && a[i]<pivot)  i++;
            while ((j>=lo) && a[j]>pivot)   j--;
            if(i<j) {
                swap(a,i,j);
                swap(b,i,j);
                i++;
                j--;
            }
        }
        swap(a,l,j);
        swap(b,l,j);
        return j;
    }

    public void swap(int[] a, int i, int j){
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

}