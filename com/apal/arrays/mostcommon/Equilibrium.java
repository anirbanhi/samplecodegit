package com.apal.arrays.mostcommon;

/*
https://practice.geeksforgeeks.org/problems/equilibrium-point/0
 */
public class Equilibrium {
    public static void main(String[] args)  {
        int[] arr = {1,3,5,2,2};
        int[] arr1 = {1};
        System.out.println("Equilibriam "+findEq(arr));
    }

    public static int findEq(int[] a)    {
        int n = a.length;
        if(a == null )
            return -1;
        if(a.length == 1)
            return 1;

        long tot = 0;
        for(int i=0;i<n;i++)
            tot += a[i];

        long sum = 0;
        for(int i=0;i<n;i++)    {
            if(sum*2 + a[i] == tot)
                return i+1;
            else {
                sum = sum + a[i];
            }
        }
        return -1;
    }
}
