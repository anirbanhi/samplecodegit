package com.apal.arrays.mostcommon;

import java.util.Arrays;

/*
https://practice.geeksforgeeks.org/problems/sort-an-array-of-0s-1s-and-2s4231/1
 */
public class Sort0_1_2 {
    public static void main(String[] args) {
        new Sort0_1_2().driver();
    }

    public void driver()    {
        int[] a1 = new int[]{0, 2, 1, 2, 0};
        rearrange(a1);
    }

    public void rearrange(int[] a)  {
        int n = a.length;
        int zeros=0;
        int twos=n-1;

        for(int i=0;i<n;)   {
            if(a[i]==2) {
                while(twos >=0 && a[twos] == 2)
                    twos--;
                a[i] = a[twos];
                a[twos] = 2;
                twos--;
            } else if(a[i]==0)  {
                while(zeros <n && a[zeros] ==0)
                    zeros++;
                a[i] = a[zeros];
                a[zeros] = 0;
                zeros++;
            } else {
                i++;
            }
        }
        System.out.println(" " + Arrays.toString(a));
    }
}