package com.apal.arrays.mostcommon;

/*
https://leetcode.com/problems/container-with-most-water/
 */

public class ContainerWithMostWater {

    public static void main(String[] args) {
        int[] ip = {1,8,6,2,5,4,8,3,7};
        System.out.println(mostWater(ip));
        System.out.println(mostWaterFaster(ip));
    }

    public static int mostWaterFaster(int[] a)  {
        int max = Integer.MIN_VALUE;
        int prod = 0;
        for(int i=0,j=a.length-1 ; j > i; ) {
                prod = Math.min(a[i], a[j]) * Math.abs(j - i);
                max = Math.max(prod, max);
                if(a[i] < a[j])
                    i++;
                else
                    j--;
        }
        return max;
    }

    public static int mostWater(int[] a)   {
        if(a.length == 0)   return 0;
        int max = Integer.MIN_VALUE;
        int prod = 0;
        for(int i=0 ; i < a.length ; i++) {
            for(int j=a.length-1;j>i;j--) {
                prod = Math.min(a[i], a[j]) * Math.abs(j - i);
                max = Math.max(prod, max);
            }
        }
        return max;
    }

}