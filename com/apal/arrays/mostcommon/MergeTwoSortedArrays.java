package com.apal.arrays.mostcommon;

/*
https://practice.geeksforgeeks.org/problems/merge-two-sorted-arrays-1587115620/1
 */

import java.util.Arrays;

public class
MergeTwoSortedArrays {
    public static void main(String[] args) {
        new MergeTwoSortedArrays().driver();
    }

    private void driver()   {
        long ar1[] = {1, 5, 9, 10, 15, 20};
        long ar2[] = {2, 3, 8, 13};

        long a1[] = {1, 2, 3, 5, 8, 9};
        long a2[] = {10, 13, 15, 20};

        merge(ar1,ar2,ar1.length,ar2.length);
        System.out.println(Arrays.toString(ar1)+" "+Arrays.toString(ar2));

        merge(a1,a2,a1.length,a2.length);
        System.out.println(Arrays.toString(a1)+" "+Arrays.toString(a2));
    }

    private void merge(long  a1[], long  a2[], int m, int n)    {

    }
}
