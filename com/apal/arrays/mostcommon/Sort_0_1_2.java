package com.apal.arrays.mostcommon;

import java.util.Arrays;

/*
https://practice.geeksforgeeks.org/problems/sort-an-array-of-0s-1s-and-2s4231/1
 */
public class Sort_0_1_2 {
    public static void main(String[] args) {
        new Sort_0_1_2().driver();
    }

    public void driver()    {
        int[] a1 = {0, 2, 1, 2, 0};
        sort(a1);

    }

    public void sort(int[] a)   {
        int n = a.length-1;
        int low=0;
        int mid=0;
        int high=n;
        int temp = 0;
        for(;mid<= high;)    {
            switch(a[mid])  {
                case 0: {
                    temp = a[mid];
                    a[mid] = a[low];
                    a[low] = temp;
                    low++;
                    mid++;
                    break;
                }
                case 1: {
                    mid++;
                    break;
                }
                case 2: {
                    temp = a[mid];
                    a[mid] = a[high];
                    a[high] = temp;
                    high--;
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(a));
    }


}
