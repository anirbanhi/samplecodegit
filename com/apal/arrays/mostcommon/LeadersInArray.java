package com.apal.arrays.mostcommon;

/*
    https://practice.geeksforgeeks.org/problems/leaders-in-an-array-1587115620/1
*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

public class LeadersInArray {

    public static void main(String[] args) {
        new LeadersInArray().driver();
    }

    public void driver()    {
        int[] a = {16,17,4,3,5,2};
        ArrayList<Integer> list = leaders(a,a.length);
        System.out.println(Arrays.toString(list.toArray()));
    }

    ArrayList<Integer> leaders(int a[], int n)    {
        // Your code here
        LinkedList<Integer> ll = new LinkedList<>();
        int maxFromRt = Integer.MIN_VALUE;
        ArrayList<Integer> list = new ArrayList<>();
        for(int i=n-1;i>=0;i--) {
            int cur = a[i];
            if(cur >= maxFromRt) {
                maxFromRt = cur;
                ll.addFirst(cur);
            }
        }
        Iterator<Integer> it = ll.iterator();
        while(it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

}