package com.apal.arrays.mostcommon;

/*
    https://practice.geeksforgeeks.org/problems/unsorted-array4925/1
*/

import java.util.Arrays;

public class FindFirstSortedPos {
    public static void main(String[] args) {
        new FindFirstSortedPos().driver();
    }

    public void driver()    {
        int[] a = new int[]{4, 2, 5, 7};
        System.out.println(" 5 == "+findElement(a,4));

        int[] b = new int[]{11, 9, 12};
        System.out.println(" -1 == "+findElement(b,3));
    }

    public int findElement(int arr[], int n)    {
        int minFromRt = arr[n-1];
        int[] maxAr = new int[n];
        Arrays.fill(maxAr, Integer.MIN_VALUE);
        for(int i=n-2;i>-1;i--)     {
            if(arr[i] <= minFromRt)  {
                minFromRt = arr[i];
                maxAr[i] = arr[i];
            }
        }

        int maxFromLeft = arr[0];

        for(int i=1;i<n;i++)    {
            if(arr[i] >= maxFromLeft )   {
                if(maxAr[i] == arr[i])
                    return arr[i];
                else
                    maxFromLeft = arr[i];
            }
        }

        return -1;
    }
}