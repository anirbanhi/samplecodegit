package com.apal.arrays;

import java.util.*;

/*
https://leetcode.com/problems/3sum/
 */

public class ThreeSum {

    public static void main(String[] args) {
        new ThreeSum().driver();
    }

    public void driver()    {
        int[] num = {-1,0,1,2,-1,-4};
        threeSum(num);
    }

    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> ret = new ArrayList<>();
        Arrays.sort(nums);
        Set<String> opSet = new HashSet<>();
        int n = nums.length;
        for(int i=0;i<n-2;i++)    {
            int t1 = nums[i];
            for(int j=i+1;j<n-1;j++)    {
                int t2 = nums[j];
                int sum = t1 + t2;
                int resp = bt(nums,j+1,n-1,-sum);
                if(resp != -1)
                {
                    ArrayList<Integer> op = new ArrayList<>();
                    op.add(t1);
                    op.add(t2);
                    op.add(-sum);
                    String key = t1+"#"+t2+"#"+-sum;
                    if(!opSet.contains(key)) {
                        opSet.add(key);
                        ret.add(op);
                        System.out.println(op);
                    }
                }
            }
        }
        return ret;
    }

    private int bt(int[] a, int s, int e, int key)    {
        int n = a.length;
        if(s>=n || e>= n)
            return -1;
        int mid = 0;
        while(s<=e) {
            mid = s + (e-s)/2;
            if(a[mid]==key)
                return mid;
            if(a[mid]>key)
                e=mid-1;
            else
                s = mid+1;
        }
        return -1;
    }
}