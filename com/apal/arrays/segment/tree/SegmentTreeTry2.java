package com.apal.arrays.segment.tree;
/*
https://leetcode.com/problems/count-of-smaller-numbers-after-self/
This is not a solution to above problem but a prerequisite of the above
 */
public class SegmentTreeTry2 {

    public static void main(String[] args) {
        new SegmentTreeTry2().driver();
    }

    public void driver()    {
        int[] arr = new int[]{5,2,6,1};

        SegTree root = createSegmentTree(arr,0,arr.length-1);
        printSegTree(root);

        System.out.println("find range sums ===== ");
        System.out.println("[0,3] "+traverseSegTree(root,0,3));
        System.out.println("[2,2] "+traverseSegTree(root,2,2));
        System.out.println("[2,3] "+traverseSegTree(root,2,3));
        System.out.println("[3,3] "+traverseSegTree(root,3,3));
    }

    class SegTree {
        int l;
        int r;
        int sum;
        SegTree left;
        SegTree right;

        public SegTree(int l, int r)    {
            this.l = l;
            this.r = r;
        }

        public SegTree(int l, int r, int sum)   {
            this(l,r);
            this.sum = sum;
        }
    }

    public void printSegTree(SegTree root)  {
        if(root == null)
            return;

        System.out.println(root.sum + " - ["+root.l+","+root.r+"]");
        printSegTree(root.left);
        printSegTree(root.right);
    }

    //first form a height balanced binary search tree
    //like we make for mergesort.
    public SegTree createSegmentTree(int[] a, int low, int hi) {
        if(low > hi)
            return null;

        if(low == hi)
            return new SegTree(low,hi,a[low]);

        int mid = (low + hi) / 2;
        SegTree node = new SegTree(low,hi);
        SegTree lchild = createSegmentTree(a, low, mid);
        SegTree rchild = createSegmentTree(a, mid+1,hi);
        node.left = lchild;
        node.right = rchild;

        if(node.left != null)
            node.sum += node.left.sum;
        if(node.right != null)
            node.sum += node.right.sum;

        return node;
    }

    public int traverseSegTree(SegTree root, int lrange, int rrange)    {
        if(root == null)
            return 0;

        if(root.l >= lrange && root.r <= rrange)    {
            return root.sum;
        } else if(root.r < lrange || root.l > rrange)   {
            return 0;
        }
        //this is partial case. we need to recur to both subtrees.
        Integer lval = traverseSegTree(root.left,lrange,rrange);
        Integer rval = traverseSegTree(root.right,lrange,rrange);

/*
        if(lval != null && rval != null)
            return lval + rval;
        if(lval != null && rval == null)
            return lval;
        if(rval != null && lval == null)
            return rval;
        return 0;
 */
        return lval + rval;
    }
}