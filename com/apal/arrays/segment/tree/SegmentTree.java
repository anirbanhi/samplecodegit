package com.apal.arrays.segment.tree;

public class SegmentTree {
    public static void main(String[] args)  {
        int[] arr = {1,2,3,4,5};
        int segTreeLength = arr.length * 4;
        int[] segTree = new int[segTreeLength];
        for(int i=0;i<segTree.length;i++)
        {
            segTree[i] = Integer.MAX_VALUE;
        }
        SegmentTree st = new SegmentTree();
        st.buildSegmentTree(arr,0, 4,segTree,1);
        st.print(arr,"Original array");
        st.print(segTree,"Segment Tree array");
        int qs = 3; //if input range is 2 then pass 3 here.
        int qe = 5;
        int min = st.getMin(qs,qe,segTree,1,9,1);
        System.out.println("Min of range "+(qs-2)+" - "+(qe-1)+" is "+min);
    }

    public int getMin(int qs, int qe, int[] segTree, int segSt, int segEnd, int segIndex)    {
        if(qs <= segSt && qe >= segEnd)
            return segTree[segIndex];
        if(qs > segEnd || qe < segSt)
            return Integer.MAX_VALUE;
        int mid = (segSt + segEnd)/2;
        return Math.min(getMin(qs,qe,segTree,segSt,mid,segIndex*2),getMin(qs,qe,segTree,mid+1,segEnd,segIndex*2+1));
    }

    public void buildSegmentTree(int[] arr, int arSt, int arEnd , int[] segTree, int segIndex)  {
        if(arSt == arEnd)
        {
            segTree[segIndex] = arr[arSt]; //leaf
            return;
        }

        int mid = (arSt + arEnd) /2;
        buildSegmentTree(arr,arSt,mid,segTree,segIndex*2);
        buildSegmentTree(arr,mid+1,arEnd,segTree,segIndex*2+1);

        segTree[segIndex] = Math.min(segTree[segIndex * 2],segTree[segIndex*2 +1]); //node
    }

    public void print(int[] arr, String msg)    {
        System.out.println("This is cool");
        System.out.println(msg);
        for(int elem:arr)
            System.out.print(elem+" , ");
        System.out.println("segment");
    }
}
