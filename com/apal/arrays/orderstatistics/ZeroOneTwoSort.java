package com.apal.arrays.orderstatistics;

public class ZeroOneTwoSort {
    public static void main(String[] args)   {
        int[] arr = {0, 2, 1, 2, 0};
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i]+", ");
        System.out.println();
        sort(arr);
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i]+", ");
    }

    public static void sort(int[] arr)  {
        int l = 0;
        int r = arr.length -1;
        int m = 0;
        while(m <= r) {
            switch(arr[m])  {
                case 0: swap(arr,l,m);
                    l++;
                    m++;
                    break;
                case 1: m++;
                    break;
                case 2:swap(arr,m,r);
                    r--;
                    break;
            }
        }
    }

    public static void swap(int[] arr, int i, int j)   {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
