package com.apal.arrays.orderstatistics;

public class MaxRepeat {
    public static void main(String[] args)  {

        int[] arr = {3, 1, 2, 2, 1, 2, 3, 3};
        int count=1;
        int elem=arr[0];
        for(int i=1;i<arr.length;i++){
            if(elem != Integer.MAX_VALUE) {
                if (arr[i] == elem)
                    count++;
                else
                    count--;
            }else{
                elem = arr[i];
                count = 1;
            }
            if(count == 0)
            {
                elem = Integer.MAX_VALUE;
            }
        }
        System.out.println("Max = "+elem);
    }


}
