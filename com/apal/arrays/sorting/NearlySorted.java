package com.apal.arrays.sorting;

/*
https://www.geeksforgeeks.org/nearly-sorted-algorithm/
 */

public class NearlySorted {
    public static void main(String[] args)  {
        //int[] a = {6, 5, 3, 2, 8, 10, 9};
        //int k = 3;
        int[] a = {10, 9, 8, 7, 4, 70, 60, 50};
        int k = 4;
        for(int i : a) System.out.print(i+", ");
        sort(a,k);
        System.out.println();
        for(int i : a) System.out.print(i+", ");
    }
    public static void sort(int[] a, int kpassed)    {
        int k = kpassed;
        while(k >= 1) {
            for (int i = a.length - 1; i >= k; i--) {
                if (a[i] < a[i - k])
                    swap(a, i, i - k);
            }
            k = k/2;
        }
    }

    public static void swap(int[] a, int i, int j)  {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
