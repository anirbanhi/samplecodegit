package com.apal.arrays.sorting;

public class MergeSort {
    public static void main(String[] args) {
        new MergeSort().driver();
    }

    public void print(int[] a) {
        System.out.println("print ");
        for(int i=0;i<a.length;i++)
            System.out.print(a[i]+", ");
        System.out.println();
    }
    public void driver()    {
        int[] a = {2,10,18,20,23};
        int[] b = {4,9,19,25};
        int[] c = merge(a,b);
        print(a);
        print(b);
        print(c);
    }

    public int[] merge(int[] a, int[] b) {
        int[] c = new int[a.length+b.length];
        int m = a.length;
        int n = b.length;
        int i=0,j=0,k=0;
        while(i<m && j<n) {
            if (a[i] < b[j]) {
                c[k] = a[i];
                i++;
                k++;
            } else {
                c[k] = b[j];
                j++;
                k++;
            }
        }
        while(i<m){
            c[k]=a[i];
            k++;
            i++;
        }
        while(j<n)  {
            c[k]=b[j];
            k++;
            j++;
        }
        return c;
    }
}