package com.apal.arrays.sorting;

public class WaveFormGFG {
    public static void main(String[] args)  {
        int arr[] = {10, 5, 6, 3, 2, 20, 100, 80};

        for (int i=0;i<arr.length;i++)
            System.out.print(arr[i]+", ");

        System.out.println();

        for(int i=0;i<arr.length;i=i+2){

            if(i > 0 && arr[i] < arr[i-1])
                swap(arr,i, i-1);
            if(i < arr.length-1 && arr[i] < arr[i+1])
                swap(arr,i, i+1);
        }

        for (int i=0;i<arr.length;i++)
            System.out.print(arr[i]+", ");
    }

    public static void swap(int[] arr, int i, int j){
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
    }
}
