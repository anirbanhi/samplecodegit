package com.apal.arrays.sorting;

public class InsertionSort {
    public static void main(String[] args) {
        new InsertionSort().driver();
    }

    public void driver()    {
        int[] a = {10,20,60,40,50,30};
        print(a);
        insertionSort(a);
        print(a);
    }

    public void insertionSort(int[] a)  {
        int n = a.length;
        for(int i=n-1;i>0;i--)  {   //pass
            int j = i-1;
            int x = a[i];
            while(a[j]>x){
                    a[j+1] = a[j];
                    j--;
                }
            a[j+1]=x;
            }
        }

    public void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public void print(int[] a)  {
        System.out.println();
        for (int i=0;i<a.length;i++)
            System.out.print(a[i]+",");
        System.out.println();
    }
}
