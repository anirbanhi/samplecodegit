package com.apal.arrays.sorting;

import java.util.ArrayList;
import java.util.List;

public class Merge2SortedArrayWithConstExtraSpace {
    public static void main(String[] args)  {
        List<Integer> l1 = new ArrayList<>();
        l1.add(11);
        l1.add(2);
        l1.add(4);

        List<Integer> l2 = new ArrayList<>();
        l2.add(4);
        l2.add(5);
        l2.add(6);

        List<Integer> l3 = new ArrayList<>();
        l3.add(10);
        l3.add(8);
        l3.add(-12);

        List<List<Integer>> ll = new ArrayList<>();
        ll.add(l1);
        ll.add(l2);
        ll.add(l3);
        System.out.println(diagonalDifference(ll));
    }
    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        int aScore = 0;
        int bScore = 0;
        for(int i=0;i<a.size();i++)
        {
            if(a.get(i) > b.get(i))
                aScore++;
            else if(a.get(i) < b.get(i))
                bScore++;
        }
        List<Integer> ret = new ArrayList<>();
        ret.add(0,aScore);
        ret.add(1,bScore);
        return ret;

    }

    public static int diagonalDifference(List<List<Integer>> arr) {
        // Write your code here
        int len = 0;
        int sum1 = 0;
        int sum2 = 0;
        int row = 0;
        for(int j=0;j<arr.size();j++ )  {
            List<Integer> l = arr.get(j);
            len = l.size();
            for(int i=0;i<len;i++)
            {
                if(i == row)
                    sum1 += l.get(i);
                if(i == len - row - 1)
                    sum2 += l.get(i);
            }
            row++;
        }
        return Math.abs(sum1 - sum2);
    }

}
