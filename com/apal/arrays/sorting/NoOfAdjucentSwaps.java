package com.apal.arrays.sorting;
/*
https://www.geeksforgeeks.org/minimum-swaps-required-sort-binary-array/
 */
public class NoOfAdjucentSwaps {
    public static void main(String[] args)  {
        int[] a = {0, 0, 1, 0, 1, 0, 1, 1};
        Util.print(a);
        new NoOfAdjucentSwaps().swaps(a);
        Util.print(a);
    }

    public void swaps(int[] a)  {
        int c = 1;
        for(int i=0;i<a.length;i++) {
            for(int j=i+1;j<a.length;j++)   {
                if(a[i] > a[j]) {
                    Util.swap(a, i, j);
                    c++;
                }
            }
        }
        System.out.println("swap count "+c);
    }
}
