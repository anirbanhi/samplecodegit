package com.apal.arrays.sorting;

import java.util.Arrays;

public class MinLengthUnsortedArray {
    public static void main(String[] args)  {
        int[] arr2 = {10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60};
        int[] arr1 =  {0, 1, 15, 25, 6, 7, 30, 40, 50};

        int n = arr1.length;
        int[] sorted = new int[n];
        for(int i=0;i<n;i++)
            sorted[i] = arr1[i];

        Arrays.sort(sorted);
        for(int k:sorted)
            System.out.print(k+", ");
        System.out.println();
        int i = 0;
        int lb=0;
        int ub=n-1;
        while(i<n && sorted[i] == arr1[i])
                i++;
        lb = i;
        i = n-1;
        while(i>=0 && sorted[i] == arr1[i])
            i--;
        ub = i;

        System.out.println("lb "+lb + " ub "+ub);
    }
}
