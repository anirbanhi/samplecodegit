package com.apal.arrays.sorting;

public class SortByFreq {

    public static void main(String[] args)  {

    }

    class Elem  {
        int data;
        int freq;
        int firstOccurence;
    }

    static class Node{
        Elem elem;
        Node left;
        Node right;

        public Node(Elem e) {
            this.elem = e;
            left = null;
            right = null;
        }
    }

    public static void sortByFreq(int[] a) {
        for(int i:a)    {

        }
    }

    public static void addToTree(Elem e, Node root)    {
        if(root == null)
            return ;
        if(e.data > root.elem.data)  {
            if(root.right == null) {
                root.right = new Node(e);
            } else
                addToTree(e, root.right);
        }
        else if(e.data < root.elem.data)   {
            if(root.left == null)   {
                root.left = new Node(e);
            } else
                addToTree(e, root.left);
        } else if(e.data == root.elem.data) {
            root.elem.freq = root.elem.freq + 1;
        }
    }

}
