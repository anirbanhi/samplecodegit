package com.apal.arrays.sorting;

public class Util {
    public static void print(int[] a)   {
        System.out.println();
        for(int i:a)
            System.out.print(i+" ,");
        System.out.println();
    }

    public static void swap(int[] a, int i, int j)  {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}
