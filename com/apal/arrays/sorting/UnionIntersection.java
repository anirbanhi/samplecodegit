package com.apal.arrays.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
Important is the arrays should be in sorted order else this wont work
 */
public class UnionIntersection {
    public static void main(String[] args)  {
/*
        int[] arr1 = {1, 3, 4, 5, 7};
        int[] arr2 = {2, 3, 5, 6};
*/

        int[] arr1 = {2, 5, 6};
        int[] arr2 = {4, 6, 8, 10};

        UnionIntersection ui = new UnionIntersection();
        System.out.println(ui.union_intersection(arr1,arr2));
    }

    public List<List<Integer>> union_intersection(int[] arr1, int[] arr2)  {
        List<Integer> op = new ArrayList<>();
        List<Integer> op2 = new ArrayList<>();
        int i = 0;
        int j = 0;
        for(;i<arr1.length && j<arr2.length;){
            if(arr1[i]>arr2[j]){
                op.add(arr2[j]);
                j++;
            }else{
                if(arr1[i]<arr2[j]){
                    op.add(arr1[i]);
                    i++;
                }
                else{
                    op.add(arr1[i]);
                    op2.add(arr1[i]);
                    i++;
                    j++;
                }
            }
        }
        while(i<arr1.length)
        {
            op.add(arr1[i]);
            i++;
        }
        while(j<arr2.length)
        {
            op.add(arr2[j]);
            j++;
        }
        List<List<Integer>> al = new ArrayList<>();
        al.add(op);
        al.add(op2);
        return al;
    }

}

