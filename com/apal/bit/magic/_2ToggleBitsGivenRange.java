package com.apal.bit.magic;
/*
https://practice.geeksforgeeks.org/problems/toggle-bits-given-range/0
 */
public class _2ToggleBitsGivenRange {
    public static void main(String[] args) {
        System.out.println("23  "+toggle(17,3,2));
        System.out.println("44  "+toggle(50,5,2));
    }

    public static int toggle(int n, int r, int l) {
        int x = 0;
        int noofbits = r - l + 1;
        for(int i=0;i<noofbits;i++) {
            x = x << 1;
            x = x | 1;
        }
        System.out.println("x no of bits "+x);
        x = x << (l-1);
        System.out.println("x no to add "+x);
        int res = n ^ x;
        return res;
    }
}