package com.apal.bit.magic;
/*
https://practice.geeksforgeeks.org/problems/find-first-set-bit/0
 */
public class _1FindFirstSetBit {
    public static void main(String[] args) {
        System.out.println("0 " +findFirstSetBit(0));
        System.out.println("1 " +findFirstSetBit(1));
        System.out.println("2 " +findFirstSetBit(2));
        System.out.println("3 " +findFirstSetBit(3));
        System.out.println("4 " +findFirstSetBit(4));
        System.out.println("64 " +findFirstSetBit(64));
        System.out.println("128 " +findFirstSetBit(128));
        System.out.println("18 " +findFirstSetBit(18));
        System.out.println("12 " +findFirstSetBit(12));
    }

    public static int findFirstSetBit(int n) {
        for(int i=0;i<32;i++)   {
            int x = n >> i;
            if(isFirstBitSet(x))
                return i+1;
        }
        return -1;
    }

    public static boolean isFirstBitSet(int n)  {
        int res = n & 1;
        if(res == 1)
            return true;
        return false;
    }

}