package com.apal.greedy;
/*
https://www.geeksforgeeks.org/activity-selection-problem-greedy-algo-1/
 */
public class ActivitySelection {
    public static void main(String[] args) {
        //int[] st = {10,12,20};
        //int[] end = {20,25,30};
        int[] st =  {5, 8, 0, 1, 5, 3};
        int[] end = {7, 9, 6, 2, 9, 4};
        sortBothArraysBasedOnOne(st, end);
        int n = end.length;
        for(int i=0;i<n;i++) {
            System.out.print(st[i] + " ");
        }
        System.out.println();
        for(int i=0;i<n;i++) {
            System.out.print(end[i] + " ");
        }
        System.out.println();

        //sorted in order of finish
        //if not sorted, then we have to sort end and then accordingly
        //have to sort st as well.

        System.out.println("Job "+0+" th   "+st[0]+" - "+end[0]);
        int prev = end[0];
        for(int i=1;i<n;i++)  {
            if(st[i] >= prev)
            {
                System.out.println("Job "+i+"th  "+st[i]+" - "+end[i]);
                prev = end[i];
            }
        }
    }
    public static void sortBothArraysBasedOnOne(int[] st, int[] end)    {
        int n = end.length;
        for(int i=0;i<n;i++)    {
            for(int j=i+1;j<n;j++)  {
                if(end[i] > end[j]) {
                    swap(end,i,j);
                    swap(st,i,j);
                }
            }
        }
    }

    public static void swap(int[] a, int i, int j)  {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void printArray(int[] a)    {
        System.out.println();
        for(int i=0;i<a.length;i++) {
            System.out.printf("%4d",a[i]);
        }
        System.out.println();
    }
}
