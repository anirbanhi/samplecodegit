package com.apal.greedy;
/*
https://www.geeksforgeeks.org/job-selection-problem-loss-minimization-strategy-set-2/
 */
public class JobSeqLossMin {
    public static void main(String[] args) {
        //int[] vol = {1,2,4,12,15,52,151};
        int[] vol = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        double finalvol = 0.0d;
        int n = vol.length - 1;
        for(int i=0;i<=n;i++)   {
            finalvol += vol[i]*(Math.pow((0.9),(n-i)));
        }
        System.out.println("Final Volume "+finalvol);
    }
}
