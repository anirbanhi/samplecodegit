package com.apal.greedy;
/*
https://www.geeksforgeeks.org/job-sequencing-problem/
 */
public class JobSequencing {
    public static void main(String[] args) {
        int[] deadline = {4,1,1,1};
        int[] profit = {20,10,40,30};
        findMaxProfit(deadline, profit);

        int[] deadline1 = {2,1,2,1,3};
        int[] profit1 = {100,19,27,25,15};
        findMaxProfit(deadline1, profit1);

    }

    public static void findMaxProfit(int[] d, int[] p)   {
        ArrayUtils.sortBothArraysBasedOnOne(d,p);
        ArrayUtils.printArray(d);
        ArrayUtils.printArray(p);
        int maxDeadLine = 0;
        int minDeadLine = Integer.MAX_VALUE;
        for(int k:d)
        {
            maxDeadLine = Math.max(maxDeadLine, k);
            minDeadLine = Math.min(minDeadLine, k);
        }
        int[] sol = new int[maxDeadLine-minDeadLine+1];
        int[] jobseq = new int[sol.length];
        for(int i=0;i<sol.length;i++) {
            sol[i] = -1;
            jobseq[i] = -1;
        }
        for(int i=0;i<p.length;i++) {
                int j = Math.min(sol.length - 1,d[i]-1);
                for(;j>=0;j--){
                    if(sol[j]==-1)  {
                        sol[j] = 1;
                        jobseq[j] = i;
                        break;
                    }
                }
        }
        for(int i:jobseq)
        {
            if(i != -1)
                System.out.println(i+"th job included ");
        }
    }
}
