package com.apal.greedy;
/*
https://www.geeksforgeeks.org/fitting-shelves-problem/
 */
public class FittingShelves {
    public static void main(String[] args) {
        //int wall = 24;
        //int s1 = 3;
        //int s2 = 5;

        int wall = 29;
        int s1 = 3;
        int s2 = 9;

        if(s1 > s2)
            findCount(s1,s2,wall);
        else
            findCount(s2,s1,wall);
    }

    //s1 = smaller shelve size
    //s2 = larger shelve size
    public static void findCount(int s1, int s2, int wall)  {
        int s1x = wall / s1;
        int s2x = 0;
        int diff = 0;
        int minDiff = wall;
        int mins1x = 0;
        int mins2x = 0;
        while(s1x >= 0 && s2x >= 0)    {
            diff = wall - s1*s1x - s2*s2x;
            System.out.println("mindiff "+diff+" s1x "+s1x+" s2x "+s2x);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(diff == 0) {
                mins1x = s1x;
                mins2x = s2x;
                minDiff = diff;
                break;
            }
            else {
                if (diff < 0)
                    s1x--;
                else {
                    if(diff < minDiff)  {
                        mins1x = s1x;
                        mins2x = s2x;
                        minDiff = diff;
                    }
                    s2x++;
                }
            }
        }
        System.out.println("mindiff "+minDiff+" s1x "+mins1x+
                " mins2x "+mins2x);
    }
}
