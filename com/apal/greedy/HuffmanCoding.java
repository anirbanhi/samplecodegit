package com.apal.greedy;
/*
https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/
 */
import java.util.*;

public class HuffmanCoding {

    public static void main(String[] args) {
        char[] ch = {'a','b','c','d','e','f'};
        int[] data = {5,9,12,13,16,45};

        buildTree(data, ch);
    }

    static class Node   {
        Node left;
        Node right;
        int weight;
        char data;
        public Node(char d,int wt)   {
            data = d;
            left = null;
            right = null;
            weight = wt;
        }
    }

    public static void buildTree(int[] wt, char[] ch)   {
        Comparator<Node> com = new Comparator<Node>() {
            @Override
            public int compare(Node e1, Node e2) {
                return e1.weight - e2.weight;
            }
        };
        Queue<Node> pq = new PriorityQueue<>(com);
        ArrayUtils.sortBothArraysBasedOnOneAsc(ch,wt);
        //load pq with given input
        for(int i=0;i<wt.length;i++)
            pq.offer(new Node(ch[i],wt[i]));

        //start building tree with the help of pq
        while (!pq.isEmpty() && pq.size() > 1)   {
            Node n1 = pq.poll();
            Node n2 = pq.poll();
            //internal nodes have '#' as char
            Node n3 = new Node('#',n1.weight+n2.weight);
            n3.left = n1.weight < n2.weight ? n1 : n2;
            n3.right = n1.weight >= n2.weight ? n1 : n2;
            pq.offer(n3);
        }

        //traverse the tree, pq must have only one element now
        Node head = pq.poll();
        traverseTree(head, new ArrayList<Character>());
    }

    public static void traverseTree(Node head, List<Character> l)   {
        if(head.left == null &&
            head.right == null) {
            System.out.println(head.data+" -- "+l);
            return;
        }
        if(head.left != null) {
            List<Character> l1 = new ArrayList<>(l);
            l1.add('0');
            traverseTree(head.left, l1);
        }
        if(head.right != null) {
            List<Character> l2 = new ArrayList<>(l);
            l2.add('1');
            traverseTree(head.right, l2);
        }

    }
}
