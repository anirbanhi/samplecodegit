package com.apal.greedy;
/*
https://www.geeksforgeeks.org/greedy-algorithm-egyptian-fraction/
 */
public class EgyptianFraction {
    public static void main(String[] args) {
        Fraction f = new Fraction(2,3);
        System.out.println("Finding fraction of "+f.toString());
        findFractions(f);

        Fraction f1 = new Fraction(6,14);
        System.out.println("Finding fraction of "+f.toString());
        findFractions(f1);

    }

    public static class Fraction    {
        int nr; //numerator , top portion
        int dr; //denominator, bottom portion
        public Fraction(int top, int bottom)    {
            nr = top;
            dr = bottom;
        }

        @Override
        public String toString() {
            return "Fraction{" +
                     + nr +
                    " / " + dr +
                    '}';
        }
    }

    public static void findFractions(Fraction f)    {
        if(f.nr == 0)
            return;
        int ceil = 0;
        if(f.dr % f.nr == 0)
            ceil = f.dr / f.nr;
        else
            ceil = (f.dr / f.nr ) + 1;
        if(ceil == 0)
            return;
        System.out.println("1/"+ceil);
        Fraction newF = subtraction(f,new Fraction(1,ceil));
        findFractions(newF);
    }

    public static Fraction subtraction(Fraction f1, Fraction f2)   {
        int lcmBottom = findLCM(f1.dr, f2.dr);
        int topDiff = (lcmBottom / f1.dr) * f1.nr -
                            (lcmBottom / f2.dr) * f2.nr;
        return new Fraction(topDiff,lcmBottom);
    }

    public static int findLCM(int i1, int i2)   {
        return i1*i2;
    }

}
