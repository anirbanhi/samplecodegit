package com.apal.greedy;

public class ArrayUtils {
    public static void sortBothArraysBasedOnOne(int[] st, int[] end)    {
        int n = end.length;
        for(int i=0;i<n;i++)    {
            for(int j=i+1;j<n;j++)  {
                if(end[i] < end[j]) {
                    swap(end,i,j);
                    swap(st,i,j);
                }
            }
        }
    }

    public static void sortBothArraysBasedOnOneAsc(char[] st, int[] end)    {
        int n = end.length;
        for(int i=0;i<n;i++)    {
            for(int j=i+1;j<n;j++)  {
                if(end[i] > end[j]) {
                    swap(end,i,j);
                    swapchar(st,i,j);
                }
            }
        }
    }

    public static void swap(int[] a, int i, int j)  {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    public static void swapchar(char[] a, int i, int j)  {
        char temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }
    public static void printArray(int[] a)    {
        System.out.println();
        for(int i=0;i<a.length;i++) {
            System.out.printf("%4d",a[i]);
        }
        System.out.println();
    }
}
