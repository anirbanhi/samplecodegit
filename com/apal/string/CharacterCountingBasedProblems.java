package com.apal.string;

import java.util.ArrayList;

public class CharacterCountingBasedProblems {

    public static void main(String args[])  {
        CharacterCountingBasedProblems ccbp = new CharacterCountingBasedProblems();
        String str = "abcd";
        ccbp.findSubstrings(str);
    }

    public void findSubstrings(String str)    {
        char[] charstr = str.toCharArray();
        ArrayList<char[]> list = new ArrayList<>();
        for(int len=1;len<=str.length();len++)
        {
            for(int i=0;i<=charstr.length-len;i++){
                int j = i+len-1;
                char[] temp = new char[len];
                for(int k=i,l=0;k<=j;k++,l++){
                    temp[l]=charstr[k];
                }
                list.add(temp);
            }
        }

        for(char[] t:list)
        {
            System.out.println(t);
        }
    }
}
