package com.apal.string;

/*
 4) How to reverse String in Java using Iteration and Recursion? (solution)
 */
public class ReverseUsingRecursion {

    public static void main(String[] args) {
        new ReverseUsingRecursion().driver();
    }

    private void driver()   {
        StringBuffer sb = new StringBuffer();
        reverseString("abcdef",0,sb);
        System.out.println(sb.toString());
    }

    private String  reverseString(String s, int index, StringBuffer sb)    {

        if(index == s.length())
            return sb.toString();

        reverseString(s,index+1,sb);
        sb.append(s.charAt(index));
        return null;
    }
}
