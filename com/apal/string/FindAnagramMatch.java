package com.apal.string;

import java.util.HashMap;
import java.util.Map;

public class FindAnagramMatch {

    public static void main(String[] args) {
        new FindAnagramMatch().driver();
    }

    public void driver()    {

        /*
        String s = "dfabcer";
        String p = "abd";
        findpat(s,p);

        String s1 = "bcdxabcdy";
        String p1 = "bcdyabcdx";
        findpat(s1, p1);

        String s2 = "odicf";
        String p2 = "dc";
        findpat(s2, p2);

        String s3 = "aaacb";
        String p3 = "abc";
        findpat(s3, p3);
        */

        String s4 = "ppqp";
        String p4 = "pq";
        findAnagramMatch(s4, p4);
    }

     public void findpat(String s, String p)  {
         System.out.println("-----------------------------");
         int pl = p.length();
         Map<Character, Integer> patmap = getPatMap(p);
         boolean matchfound = false;
         int ws = 0;
         for(int we=0;we<s.length();we++)  {
            char ch = s.charAt(we);
            if(!(patmap.containsKey(ch)))  {
                ws=we+1;
                patmap = getPatMap(p);
            } else {
                if(patmap.containsKey(ch))  {
                    patmap.put(ch, patmap.get(ch)-1);
                    if(patmap.get(ch) == 0)
                        patmap.remove(ch);
                }
                if(we-ws+1 > pl)    {
                    char chgo = s.charAt(ws);
                    patmap.put(chgo,patmap.get(chgo)+1);
                }
                if(patmap.size() == 0)  {
                    System.out.println("match found");
                    matchfound = true;
                }
            }
         }
         if(matchfound == false)
            System.out.println("match not found");
     }

     public Map<Character, Integer> getPatMap(String p)   {
         Map<Character, Integer> patmap = new HashMap<>();

         for(int i=0;i<p.length();i++)  {
             char c = p.charAt(i);
             patmap.put(c, patmap.getOrDefault(c,0)+1);
         }

         return patmap;
     }

     public void findAnagramMatch(String s, String p)   {
        int ws = 0;
         Map<Character, Integer> patmap = new HashMap<>();

         for(int i=0;i<p.length();i++)  {
             char c = p.charAt(i);
             patmap.put(c, patmap.getOrDefault(c,0)+1);
         }

         int matched = 0;
         for(int we=0;we<s.length();we++)   {
             char rc = s.charAt(we);
             if(patmap.containsKey(rc)) {
                 patmap.put(rc, patmap.get(rc)-1);
                 if(patmap.get(rc) == 0)
                     matched++;
             }
             if(matched == patmap.size())
                 System.out.println("match found");

             if(we >= p.length() - 1)    {
                char chgo = s.charAt(ws);
                if(patmap.containsKey(chgo))    {
                    if(patmap.get(chgo) == 0)
                        matched--;
                    patmap.put(chgo, patmap.get(chgo)+1);
                }
             }
         }


     }
}