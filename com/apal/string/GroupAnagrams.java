package com.apal.string;

import java.util.*;

/*
https://leetcode.com/problems/group-anagrams/
 */
public class GroupAnagrams {
    public static void main(String[] args) {
        new GroupAnagrams().driver();
    }

    public void driver()    {
        String[] sa = new String[]{"eat","tea","tan","ate","nat","bat"};
        List<List<String>> resp = groupAnagrams(sa);
        for(List<String> a : resp)
            a.stream().forEach(System.out::println);
    }


    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String,List<String>> map = new HashMap<>();

        for(String s : strs)    {
            char[] keys = s.toCharArray();
            Arrays.sort(keys);
            String key = new String(keys);
            if(map.containsKey(key))    {
                map.get(key).add(s);
            } else {
                List<String> l = new ArrayList<>();
                l.add(s);
                map.put(key,l);
            }
        }

        List<List<String>> ll = new ArrayList<>();
        for(String k:map.keySet())  {
            ll.add(map.get(k));
        }
        return ll;
    }


}
