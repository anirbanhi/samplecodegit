package com.apal.string;

public class Kmp {
    public static void main(String[] args)  {

        String str = "abcdabcxabcdabcagh";
        String pat = "abcdabca";
        int[] suffix = createSuffixArray(pat);
        for(int i=0;i<suffix.length;i++) {
            System.out.print(pat.charAt(i)+" ");
            System.out.println(suffix[i]+" ");
        }

        System.out.println("Pattern found "+kmp(str,pat));
    }

    public static boolean kmp(String text, String pat)  {
        int[] suffix = createSuffixArray(pat);
        int i=0,j=0;

        while(i<text.length() && j<pat.length())    {
            if(text.charAt(i) == pat.charAt(j)) {
                i++;
                j++;
            }
            else {
                if(j!=0)
                {
                    j = suffix[j-1];
                }
                else
                    i++;
            }
        }
        if(j == pat.length())
            return true;
        else
            return false;
    }
    public static int[] createSuffixArray(String pattern)  {
        int n = pattern.length();
        int[] suffixArr = new int[n];
        suffixArr[0] = 0;
        for(int i=0,j=1;j<n;)    {
            if(pattern.charAt(j) == pattern.charAt(i))
            {
                suffixArr[j] = i+1;
                i++;
                j++;
            }
            else
            {
                if(i != 0)
                    i = suffixArr[i-1];
                else
                {
                    suffixArr[j] = 0;
                    j++;
                }
            }

        }
        return suffixArr;
    }
}
