package com.apal.binarytreesummation;

/*
https://www.geeksforgeeks.org/remove-all-nodes-which-lie-on-a-path-having-sum-less-than-k/
*/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RemoveNodesWhichDoesntLieOnPathWithSumK {

    public static void main(String[] args) {
        new RemoveNodesWhichDoesntLieOnPathWithSumK().driver();
    }

    public void driver()    {
        Node root = getTree();
        List<Node> list = new ArrayList<>();
        findNodes(root,list,20,0);
        Set<Node> set = new HashSet<>(list);
        for(Node n : list) {
            System.out.println(n.val);
        }
        deleteNodesFromAList(root, set);

        //---

        Node root1 = getTree();
        List<Node> list1 = new ArrayList<>();
        findNodes(root1,list1,45,0);
        Set<Node> set1 = new HashSet<>(list1);
        for(Node n : list1)
            System.out.println(n.val);
        deleteNodesFromAList(root1, set1);
    }

    public Node deleteNodesFromAList(Node root, Set<Node> set) {
        if (root == null)
            return null;

        root.left  = deleteNodesFromAList(root.left, set);
        root.right = deleteNodesFromAList(root.right, set);

        if(set.contains(root))
            return null;
        return root;
    }

    public Boolean findNodes(Node root, List<Node> l, int k, int cursum) {
        if (root == null)
            return null;


        Boolean left = findNodes(root.left, l, k, cursum + root.val);
        Boolean right = findNodes(root.right, l, k, cursum + root.val);

        if (left == null && right == null)    {
            if(cursum+root.val < k) {
                l.add(root);
                return true;
            } else {
                return false;
            }
        }
        if(left != null && right != null)   {
            if(left && right)   {
                l.add(root);
                return true;
            }
            return false;
        }
        if(left == null && right)    {
            l.add(root);
            return true;
        }
        if(right == null && left )    {
            l.add(root);
            return true;
        }
        return false;
    }

    public Node getTree()   {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);

        root.left.left.left = new Node(8);
        root.left.left.right = new Node(9);
        root.left.right.left = new Node(12);
        root.right.right.left = new Node(10);

        root.left.left.right.left = new Node(13);
        root.left.left.right.right = new Node(14);
        root.right.right.left.right = new Node(11);

        root.left.left.right.right.left = new Node(15);

        return root;
    }
}
