package com.apal.binarytreesummation;
/*
https://www.geeksforgeeks.org/maximum-sum-nodes-binary-tree-no-two-adjacent/

 */
public class MaxSumOfNonAdjacentNodes {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(1);
        root.right.left = new Node(4);
        root.right.right = new Node(5);
        root.right.right.right = new Node(6);

        MaxSumOfNonAdjacentNodes msj = new MaxSumOfNonAdjacentNodes();
        System.out.println("Maxsum = " + msj.findSum(root,0));
    }

    private int findSum(Node root, int sum) {
        if(root == null)
            return 0;
        int s1 = sum + root.val;
        int s2 = sum + findSum(root.left,sum);
        int s3 = sum + findSum(root.right,sum);
        System.out.println();
        return Math.max(s1,Math.max(s2,s3));
    }
}
