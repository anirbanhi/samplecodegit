package com.apal.binarytreesummation;
/*
https://www.geeksforgeeks.org/find-maximum-path-sum-two-leaves-binary-tree/

Given a binary tree in which each node element contains a number. Find the maximum possible sum from one leaf node to another.
The maximum sum path may or may not go through root. For example, in the following binary tree, the maximum sum is 27(3 + 6 + 9 + 0 – 1 + 10). Expected time complexity is O(n).

                  -15
            5                 6
    -8          1        3          9
2        6         100                  0
             101                    4       -1
        102                             10
  103

sum around max path = 433
 */

public class MaximumSumPath {
    public static void main(String[] args)  {
        Node root = new Node(-15);
        root.left = new Node(5);
        root.right = new Node(6);
        root.left.left = new Node(-8);
        root.left.left.left = new Node(2);
        root.left.left.right = new Node(6);
        root.left.right = new Node(1);

        root.right.left = new Node(3);
        root.right.right = new Node(9);
        root.right.right.right = new Node(0);
        root.right.right.right.right = new Node(-1);
        root.right.right.right.left = new Node(4);
        root.right.right.right.right.left = new Node(10);

        root.right.left.left = new Node(100);
        root.right.left.left.left = new Node(101);
        root.right.left.left.left.left = new Node(102);
        root.right.left.left.left.left.left = new Node(103);

        MaximumSumPath msp = new MaximumSumPath();
        Max max = msp.new Max();
        msp.maxPath(root,max);
        System.out.println("Max path length leaf to leaf = " + max.max);

        System.out.println("Max path length leaf to leaf through root = " + msp.maxPathThroughRoot(root));

        max.max = 0;
        msp.maxPathLengthAlongMaxSum(root,max);
        System.out.println("Max path length leaf to leaf where sum is max = " + max.max);

    }

    class Max{
        public Max(){}
        int max;
    }

    private int maxPath(Node root,Max max)  {
        if(root == null)
            return 0;
        if(root.left == null && root.right == null)
            return 1;

        int l = maxPath(root.left,max);
        int r = maxPath(root.right,max);

       // System.out.println("Node [ "+root.val+"]  == "+ l +" "+r);
        int sum = l + r + 1 ;
        max.max = sum > max.max ? sum : max.max;
        return Math.max(l,r) + 1;
    }

    private int maxPathThroughRoot(Node root)   {
        return maxHeight(root.left) +
                maxHeight(root.right) + 1 ;
    }
    private int maxHeight(Node root)   {
        if(root == null)
            return 0;
        int l = maxHeight(root.left);
        int r = maxHeight(root.right);

        //System.out.println("val [ "+root.val+" ]" + (Math.max(l,r)+1));
        return Math.max(l,r) + 1;
    }

    private int maxPathLengthAlongMaxSum(Node root, Max max)   {
        if(root == null)
            return 0;
        int l = maxPathLengthAlongMaxSum(root.left,max);
        int r = maxPathLengthAlongMaxSum(root.right,max);

        int sum = l + r + root.val;
        if(max.max < sum)
            max.max = sum;
        return Math.max(l,r) + root.val;
    }
}
