package com.apal.binarytreesummation;
/*
https://www.geeksforgeeks.org/find-pair-root-leaf-path-sum-equals-roots-data/

               1
            /    \
           /      \
          /        \
         2          3
        / \         /\
       /   \       /  \
      /     \     /    \
    9        6   4      5
     \      /   / \
      \    /   /   \
      10  11  12    7
 */

/*
    This would be challenging if we were told to find out
    all paths with a given sum and not just a pair.
    Good to be tried out.
 */

import java.util.HashSet;
import java.util.Set;

public class PairWithGivenSumAlongAPath {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(9);
        root.left.right = new Node(6);
        root.right.left = new Node(4);
        root.right.right = new Node(5);
        root.left.left.right = new Node(10);
        root.left.right.left = new Node(11);
        root.right.left.left = new Node(12);
        root.right.left.right = new Node(7);

        int x = 8;
        Set<Integer> set = new HashSet<>();
        PairWithGivenSumAlongAPath pgs = new PairWithGivenSumAlongAPath();
        System.out.println("Pair with sum "+x+" exists ? "+
                pgs.pairWithGivenSum(root,set,x));
    }

    private boolean pairWithGivenSum(Node root, Set<Integer> set, int x)  {
        if(root == null)
            return false;

        if(set.contains(x-root.val))
        {
            System.out.println(root.val);
            return true;
        }
        set.add(root.val);

        boolean l = pairWithGivenSum(root.left, set,x);
        boolean r = pairWithGivenSum(root.right, set,x);

        set.remove(root.val);

        return l || r;
    }
}
