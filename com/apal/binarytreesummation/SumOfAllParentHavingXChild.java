package com.apal.binarytreesummation;

/*
        4
       / \
      2   5
     / \ / \
    7  2 2  3

    https://www.geeksforgeeks.org/sum-parent-nodes-child-node-x/
 */
public class SumOfAllParentHavingXChild {
    public static void main(String[] args)  {
        Node root = new Node(4);
        root.left = new Node(2);
        root.right = new Node(5);
        root.left.left = new Node(7);
        root.left.right = new Node(2);
        root.right.left = new Node(2);
        root.right.right = new Node(3);

        int x = 2;
        SumOfAllParentHavingXChild spc = new SumOfAllParentHavingXChild();
        spc.sumOfAllParents(root,x);
    }

    private boolean sumOfAllParents(Node root, int x)  {
        if(root == null)
            return false;
        boolean l = sumOfAllParents(root.left, x);
        boolean r = sumOfAllParents(root.right, x);

        if(l||r)
            System.out.println("Parent "+root.val);
        return root.val == x;
    }
}