package com.apal.binarytreesummation;
/*
https://www.geeksforgeeks.org/diagonal-sum-binary-tree/
               1
            /    \
           /      \
          /        \
         2          3
        / \         /\
       /   \       /  \
      /     \     /    \
    9        6   4      5
     \      /   / \
      \    /   /   \
      10  11  12    7
 */

import java.util.*;

public class DiagonalSumOfABinaryTree {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(9);
        root.left.right = new Node(6);
        root.right.left = new Node(4);
        root.right.right = new Node(5);
        root.left.left.right = new Node(10);
        root.left.right.left = new Node(11);
        root.right.left.left = new Node(12);
        root.right.left.right = new Node(7);

        DiagonalSumOfABinaryTree dsb = new DiagonalSumOfABinaryTree();
        Map<Integer, List<Integer>> map = new HashMap<>();
        dsb.diagonalTraversal(root,0,map);
        for(Map.Entry<Integer,List<Integer>> entry : map.entrySet()){
            {
                for(int i : entry.getValue())
                    System.out.print(i+",");
                System.out.println();
            }
        }
    }

    private void diagonalTraversal(Node root, int diag,
                                   Map<Integer, List<Integer>> map)   {
        if(root == null)
            return;
        if(!map.containsKey(diag))
            map.put(diag,new ArrayList<>());
        map.get(diag).add(root.val);

        diagonalTraversal(root.left, diag+1, map);
        diagonalTraversal(root.right, diag, map);
    }

}
