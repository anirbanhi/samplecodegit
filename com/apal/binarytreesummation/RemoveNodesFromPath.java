package com.apal.binarytreesummation;

/*
https://www.geeksforgeeks.org/remove-all-nodes-which-lie-on-a-path-having-sum-less-than-k/

Remove all nodes which don’t lie in any path with sum>= k

Consider the following Binary Tree
          1
      /      \
     2        3
   /   \     /  \
  4     5   6    7
 / \    /       /
8   9  12      10
   / \           \
  13  14         11
      /
     15

For input k = 20, the tree should be changed to following
(Nodes with values 6 and 8 are deleted)
          1
      /      \
     2        3
   /   \        \
  4     5        7
   \    /       /
    9  12      10
   / \           \
  13  14         11
      /
     15

For input k = 45, the tree should be changed to following.
      1
    /
   2
  /
 4
  \
   9
    \
     14
     /
    15
 */

public class RemoveNodesFromPath {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);

        root.left.left.left = new Node(8);
        root.left.left.right = new Node(9);
        root.left.right.left = new Node(12);
        root.right.right.left = new Node(10);

        root.left.left.right.left = new Node(13);
        root.left.left.right.right = new Node(14);
        root.right.right.left.right = new Node(11);

        root.left.left.right.right.left = new Node(15);

        RemoveNodesFromPath rnp = new RemoveNodesFromPath();
        int max = 45;
        rnp.inorder(root);
        System.out.println();
        rnp.removePath(root,0,max);
        System.out.println();
        rnp.inorder(root);
    }

    private void inorder(Node root) {
        if(root == null)
            return;
        inorder(root.left);
        System.out.printf("%4d,",root.val);
        inorder(root.right);
    }
    private boolean removePath(Node root, int sum, int max) {
        if(root == null)
        {
            if(sum >= max)
                return true;
            else
                return false;
        }
        sum = sum + root.val;

        boolean l = removePath(root.left, sum, max);
        boolean r = removePath(root.right, sum, max);

        boolean res = l || r;
        //got false response from both
        if( res == false )
        {
            root.left = null;
            root.right = null;
            return false;
        }
        else    {   // one of l and r or both can be true
            if( !l ) //l is false
                root.left = null;
            else if( !r )   //l is false
                root.right = null;
            // both l and r are true; nothing to delete
                return true;
        }
    }
}
