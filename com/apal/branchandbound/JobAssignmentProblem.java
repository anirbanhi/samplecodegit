package com.apal.branchandbound;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class JobAssignmentProblem {

    public static void main(String[] args) {

    /*
            job0    job1    job2    job3
    p0      9       2       7       8
    p1      6       4       3       7
    p2      5       8       1       8
    p3      7       6       9       4

    */

        int[][] assignment =
                {
                        {9,2,7,8},
                        {6,4,3,7},
                        {5,8,1,8},
                        {7,6,9,4}
                };

        int count = assignment.length;

        Comparator<State> c = new Comparator<State>() {
            @Override
            public int compare(State o1, State o2) {
                return o1.cost - o2.cost;
            }
        };
        Queue<State> pq = new PriorityQueue<>(c);

        //find initial min cost possible

        for(int i=0;i<count;i++)    {
            mincost += findMin(assignment, -1,i);
        }

        State st = new State(0,0);
        int cost = findCost(assignment, st, mincost);

    }

   static class State   {
       /*
       person[] = {1,0,2,3}       means
       person0 is doing j1
       person1 is doing j0
       person2 is doing j2
       person3 is doing j3
       */
        int[] person =  new int[4];
        int cost;
        public State(int p, int j)  {
            person[p] = j;
        }
    }

    static int mincost = 0;

    public static int findMin(int[][]a, int r, int c)  {
        int min = Integer.MAX_VALUE;
        if(r < 0)   {
            for(int i = 0;i<a.length;i++)   {
                min = Math.min(min,a[i][c]);
            }
            return min;
        }
        if (c < 0)  {
            for(int i = 0;i<a.length;i++)   {
                min = Math.min(min,a[r][i]);
            }
            return min;
        }
        return -1;
    }
    public static int findCost(int[][] asg, State s, int minCost)   {
        int cost = 0;
        for(int i=0;i<4;i++)    {
            cost += findMin(asg,s);
        }
        return  -1;
    }

    public static int findMin(int[][] a, State s)   {
        return 0;
    }

}
