package com.apal.divide;

public class FindMin {

    public static void main(String[] args) {
        int[] a = {45,2,5,9,1,36,27,8};
        System.out.println("min is "+findMin(0,a.length-1,a));
    }

    public static int findMin(int l, int r, int[] a)   {
        int mid = (l + r) / 2;
        int min1 = 0, min2 = 0;
        if(l < r) {
            min1 = findMin(l, mid, a);
            min2 = findMin(mid + 1, r, a);
            if (min1 < min2) return min1;
            else return min2;
        }   else return a[l];
    }

}