package com.apal.binarymisc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
https://www.geeksforgeeks.org/find-mirror-given-node-binary-tree/

Find mirror of a given node in Binary tree


 */
public class FindMirrorNode {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);

        root.left.left = new Node(4);
        root.left.left.right = new Node(7);

        root.right.left = new Node(5);
        root.right.left.left = new Node(8);
        root.right.right = new Node(6);
        root.right.left.right = new Node(9);


        FindMirrorNode fm = new FindMirrorNode();
        int x = 7;
        fm.findMirrorNode(root,x,fm.new Dir(true));
        System.out.println( Arrays.toString(fm.path.toArray()) );

        //Below function should be seen
        fm.findMirrorNodeSimple(root,root,x);
    }

    class Dir{
        boolean isLeft;
        boolean pathExists;
        public Dir(boolean il)    {
            isLeft = il;
            pathExists = false;
        }
    }
    List<Integer> path = new ArrayList<>();
    private void findMirrorNode(Node root, int x, Dir dir)  {
        if(root == null)
            return;
        if(root.val == x)   {
            dir.pathExists = true;
        }
        Dir l = new Dir(true);
        findMirrorNode(root.left, x, l);
        Dir r = new Dir(false);
        findMirrorNode(root.right, x, r);

        if(l.pathExists)
        {
            path.add(1);
            dir.pathExists = true;
        }
        else if(r.pathExists)
        {
            path.add(0);
            dir.pathExists = true;
        }
        return;
    }

    private void findMirrorNodeSimple(Node root1,Node root2, int x)    {
        if(root1 == null)
            return;
        if(root1.val == x)
        {
            if(root2 != null)
                System.out.println("Mirror node is " + root2.val);
            else
                System.out.println("Mirror node is [ null ] " );
            return;
        }
        if(root2!=null) {
            findMirrorNodeSimple(root1.left, root2.right, x);
            findMirrorNodeSimple(root1.right, root2.left, x);
        }
        return;
    }
}
