package com.apal.binarymisc;
/*
https://practice.geeksforgeeks.org/problems/check-for-balanced-tree/1
 */
public class IsHeightBalanced {
    public static void main(String[] args) {

    }
    public boolean isBalanced(Node root) {
        if(root == null)    return true;
        isBal(root);
        boolean l = isBalanced(root.left);
        boolean r = isBalanced(root.right);
        return bal;
    }
    boolean bal = true;
    public int isBal(Node root)    {
        if(root == null)    return 0;
        int l = isBal(root.left);
        int r = isBal(root.right);
        if(Math.abs(l-r) > 1)
            bal = false;
        return 1+Math.max(l,r);
    }
}
