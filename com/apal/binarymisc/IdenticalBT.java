package com.apal.binarymisc;
/*
https://practice.geeksforgeeks.org/problems/determine-if-two-trees-are-identical/1
 */
public class IdenticalBT {

    public static void main(String[] args) {

    }

    public static boolean isIdentical(Node l, Node r)   {
        if(l==null && r==null)  return true;
        if( (l==null && r!=null) ||
            (l!=null && r==null))  return false;
        boolean eq = false;
        eq = l.val == r.val ? true : false;
        return eq && isIdentical(l.left,r.left) &&
                isIdentical(l.right,r.right);
    }
}