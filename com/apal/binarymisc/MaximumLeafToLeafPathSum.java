package com.apal.binarymisc;
/*
https://practice.geeksforgeeks.org/problems/maximum-path-sum/1#ExpectOP
 */
public class MaximumLeafToLeafPathSum {
    public static void main(String[] args) {

    }

    int maxPathSum(Node root)
    {
        if(root == null)
            return 0;
        superDFS(root);
        return max;
    }

    void superDFS(Node r)   {
        if(r==null) return;
        pathSum(r);
        superDFS(r.left);
        superDFS(r.right);
    }
    int max = Integer.MIN_VALUE;
    Integer pathSum(Node r)   {
        if(r==null) return null;
        if( (r.left == null && r.right == null) )
            return r.val;

        Integer l = pathSum(r.left);
        Integer rt = pathSum(r.right);

        if( (r.left != null && r.right != null) )   {
            int sum = 0;
            sum +=r.val;
            if(l != null)
                sum += l;
            if(rt != null)
                sum += rt;
            if(sum > max)
                max = sum;
        }
        if(l != null && rt != null)
            return r.val + Math.max(l,rt);
        if(l != null)
            return r.val + l;
        if(rt != null)
            return r.val + rt;

        return r.val;
    }
}
