package com.apal.binarymisc;

/*
https://practice.geeksforgeeks.org/problems/binary-tree-to-dll/1
 */
public class BTtoDLL {
    public static void main(String[] args) {
        Node r = new Node(10);
        r.left = new Node(12);
        r.right = new Node(15);
        r.left.left = new Node(25);
        r.left.right = new Node(30);
        r.right.left = new Node(36);
        Node dll = btToDll(r);

        while(dll.left!=null)
            dll=dll.left;
        while(dll!=null) {
            System.out.print(dll.val + " > ");
            dll=dll.right;
        }
    }

    public static Node btToDll(Node rt)    {
        if(rt==null)
            return null;
        Node l = btToDll(rt.left);
        if(l != null) {
            for(;l.right!=null;l=l.right);
            l.right = rt;
            rt.left = l;
        }
        Node r = btToDll(rt.right);
        if(r != null)   {
            for(;r.left!=null;r=r.left);
            r.left = rt;
            rt.right = r;
        }
        return rt;
    }
}