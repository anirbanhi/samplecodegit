package com.apal.binarymisc;
/*
https://www.geeksforgeeks.org/width-binary-tree-set-1/

Input :
           1
         /    \
        2       3
       / \     / \
      4   5   6   7
               \   \
                8   9
Output :
6

 */
public class VerticalWidth {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);
        root.right.left.right = new Node(8);
        root.right.right.right = new Node(9);

        VerticalWidth vw = new VerticalWidth();
        vw.findWidth(root,0);
        int width = vw.max + Math.abs(vw.min) + 1;
        System.out.println("Width = " + width);
    }

    private int min = Integer.MAX_VALUE;
    private int max = Integer.MIN_VALUE;

    private void findWidth(Node root, int w)    {
        if(root == null)
            return;
        if(w < min)
            min = w;
        if(w > max)
            max = w;
        findWidth(root.left,w-1);
        findWidth(root.right,w+1);
    }
}
