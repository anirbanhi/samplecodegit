package com.apal.binarymisc;
/*
https://practice.geeksforgeeks.org/problems/symmetric-tree/1
 */
public class SymmetricBT {
    public static void main(String[] args) {

    }

    public static boolean isSymmetric(Node root) {
        if(root == null ||
                (root.left == null && root.right == null) )
            return true;
        return isSym(root.left, root.right);
    }

    public static boolean isSym(Node l, Node r)   {
        if(l==null && r==null)  return true;
        if( (l==null && r!=null) ||
                (l!=null && r==null) ) return false;
        if(l.val != r.val)
            return false;
        return isSym(l.left,r.right) && isSym(l.right,l.left);
    }
}
