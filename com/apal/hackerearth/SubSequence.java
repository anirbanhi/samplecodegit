package com.apal.hackerearth;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SubSequence {
    public static void main(String[] args)  {
        String str = "nbawdhljzgovkmthtsmzmsunwgheqmplhmaddlnjhkcaskgzdnyhdqixwwtnailiudcxavzgfcczcotmmtroecsdjmaxpcyeifopmfqygqezbycxmdugsofqwlmbuborahaeeppbbckeqqchlmxhddpawlhlosunzhyurfgqlpfsdfkmrmgafiszqzkifczhntipngxazhpgyxqebakixhwuuokkolzqmmoloiltigsjsdiysznfeycdkunezvvgrtldyyigvjlpfgjucdasikxpthdyusihupdgkotemldiswiecmgrzmrrjmvcyhcuwpkrrsrwmkupwbnuwmfquedsfhsimzfecgjwxgczvqxptsgyoyjwmkjxwmskfwgfwpvbduystkvcroxawrsyvvdlkvpupbjzieonyocpvlwmrnmlillbfmehbksbuiqxwjmgstxvmtkowddpinxtdrbjjxqtudhwclqppblesarzexgltjxckepzvdlrelccejlflpoxjfmtbhocrtvkumccqmlfvtljyfvmpolkofocqgeymlnsrgqlrvxzfchqbnfmaglloithpbshttspcplqevgjmlyykyvcgravtfslmnnljxgpciknxoqzlzfpptjtytudlygkvprrqkwavygrhwvdhfaywaghlhiqxzviuweqzghfvkrgsuigwvhrghhyvakqgjtcdlbjvabndigvzqquhkeewrasuwqhazcdlwdylwusswvrmkbucyfeyhfqgyybmvzbdfkdcwrrgjqqgmzgbriwulsxqhgvwrxmvlgbjbuojmgeeyvuyiikrnhubhywkdmzvqsqemnjyzkcdmmgfsfivscdfyvsrznrgblvmmolapxjczuaizwvrtsizcoquxaiwvhcrjhjdynhsbjqcqeqmobwaobkrijmseolebiurmgutdydpzbhmljxotsqiqmcfahqhmmvbwgjyqevxedcbhxsdfwk";
        //char[] arr = new char[]{'a','a','a','a','b'};

        long s1 = System.currentTimeMillis();
        char[] arr = str.toCharArray();
        int[][] dp = new int[arr.length][arr.length];
        for(int i=0;i<arr.length;i++)
        {
            for(int j=0;j<arr.length;j++){
                dp[i][j] = -5;
            }
        }
        System.out.println(subseq(arr, 0, 1,dp)+1);
        long s2 = System.currentTimeMillis();
        System.out.println(s2-s1);

        System.out.println(subseq(arr));
        long s3 = System.currentTimeMillis();
        System.out.println(s3-s2);

    }



    public static int subseq(char[] num, int last, int cur, int[][] dp) {
        if (cur == num.length)
            return 0;
        if(dp[last][cur]==-5) {
            int c1 = 0;
            if (last == -1 || num[last] != num[cur]) {
                c1 = 1 + subseq(num, cur, cur + 1, dp);
            }
            int c2 = subseq(num, last, cur + 1, dp);
            dp[last][cur] = Math.max(c1, c2);
        }
        return dp[last][cur];
    }

    public static int subseq(char[] s) {
        int n = s.length;
        int cur=0;
        int i = 0;
        for(i=0;i<n-1;i++) {
            if (s[i] == s[i + 1]) {
                cur++;
            }
        }
        return n - cur;
    }

}
