package com.apal.hackerearth;

import java.util.Scanner;

public class PrintReverse {

    public static void main(String[] args)  {

        Scanner sc = new Scanner(System.in);
        String temp = sc.nextLine();
        int n = Integer.valueOf(temp); //no of elements
        int arr[] = new int[n];
        for(int i=0;i<n;i++)
        {
            temp = sc.nextLine();
            arr[i] = Integer.valueOf(temp);
        }

        int i=0;
        int j=arr.length -1;
        while(i<j){
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
            i++;
            j--;
        }
        i=0;
        while(i<arr.length)
        {
            System.out.println(arr[i]);
            i++;
        }
    }

}
