package com.apal.subset;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Permutation {
    public static void main(String[] args)  {
        int[] arr = {1,3,5};
        findAllPermutation(arr);
    }

    public static void findAllPermutation(int[] arr)   {
        Queue<List<Integer>> q = new LinkedList<>();
        List<Integer> s = new ArrayList<>();
        List<List<Integer>> res = new ArrayList<>();
        s.add(arr[0]);
        q.offer(s);
        for(int j=1;j<arr.length;j++) {
            int cnt = q.size();
            while (cnt>0) {
                List<Integer> temp = q.poll();
                for(int k=0;k<=temp.size();k++){
                    List<Integer> newl = new ArrayList<>(temp);
                    newl.add(k,arr[j]);
                    if(newl.size() == arr.length)
                        res.add(newl);
                    else
                        q.offer(newl);
                }
                cnt--;
            }
        }

        //print result
        for(List<Integer> l : res){
            for(Integer i : l)
                System.out.print(i+" ");
            System.out.println();
        }

    }
}
