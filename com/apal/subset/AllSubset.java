package com.apal.subset;

import java.util.ArrayList;
import java.util.List;

public class AllSubset {
    public static void main(String[] args)  {
        int[] arr = {1,3,5};
        findAllSubset(arr);
    }

    public static void findAllSubset(int[] arr)  {
        List<List<Integer>> list = new ArrayList<>();
        List<Integer> l = new ArrayList<>();
        list.add(l);


        for(int e : arr)    {
            int count = list.size();
            for(int i=0;i<count;i++)  {
                List<Integer> newl = new ArrayList<>(list.get(i));
                newl.add(e);
                list.add(newl);
            }
        }

        for(List<Integer> x : list)
        {
            for(int i : x)
                System.out.print(i+", ");
            System.out.println();
        }
    }
}
