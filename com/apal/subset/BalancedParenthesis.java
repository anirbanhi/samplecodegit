package com.apal.subset;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BalancedParenthesis {
    public static void main(String[] args)  {
        int count = 3;
        new BalancedParenthesis().findBalancedParenthesis(3);
    }

    public class BP{
        public int open;
        public int close;
        public String bp;

        public BP(int o, int c, String bp){
            this.open = o;
            this.close = c;
            this.bp = bp;
        }


    }

    public void findBalancedParenthesis(int n)    {
        Queue<BP> q = new LinkedList<>();
        List<String> res = new ArrayList<>();
        q.offer(new BP(1,0,"("));

        while(!q.isEmpty()){
            BP temp = q.poll();
            if(temp.open == n && temp.close == n)
            {
                res.add(temp.bp);
                continue;
            }
            if(temp.open < n)   {
                BP newbp = new BP(temp.open+1, temp.close, temp.bp+"(");
                q.offer(newbp);
            }
            if(temp.close < temp.open)  {
                BP newbp = new BP(temp.open, temp.close+1, temp.bp+")");
                q.offer(newbp);

            }
        }

        for(String s : res)
            System.out.println(s);
    }
}
