package com.apal.timerscheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimerSc {
    public static int count = 0;
    public static int count1 = 0;

    public static void main(String[] args)  {
        ScheduledExecutorService sc = Executors.newScheduledThreadPool(1);

        task1();

        sc.scheduleAtFixedRate(() -> simGetAferPost(sc ), 0, 2, TimeUnit.SECONDS);

        try {
            sc.awaitTermination(7, TimeUnit.DAYS);
        } catch (Exception e)   {
            e.printStackTrace();
        }

        task3();

        ScheduledExecutorService sc1 = Executors.newScheduledThreadPool(1);
        sc1.scheduleAtFixedRate(() -> simGetForUpdate(sc1 ), 0, 2, TimeUnit.SECONDS);

        try {
            sc1.awaitTermination(7, TimeUnit.DAYS);
        } catch (InterruptedException e)   {
            e.printStackTrace();
        }

        task4();
    }

    public static void task1() {
        System.out.println("Running task1...");
    }

    public static void simGetAferPost(ScheduledExecutorService sc) {
        if(count == 3)
            sc.shutdown();
        count++;
        System.out.println("Running simGetAferPost ...");
    }

    public static void simGetForUpdate(ScheduledExecutorService sc) {
        if(count1 == 5)
            sc.shutdown();
        count1++;
        System.out.println("Running simGetForUpdate...");
    }

    public static void task3() {
        System.out.println("Running task3...");
    }

    public static void task4() {
        System.out.println("Running task4...");
    }

}
