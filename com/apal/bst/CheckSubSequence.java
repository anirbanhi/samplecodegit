package com.apal.bst;
/*

    https://www.geeksforgeeks.org/check-if-given-sorted-sub-sequence-exists-in-binary-search-tree/

*/
public class CheckSubSequence {
    public static void main(String[] args)  {
        Node root = new Node(8);
        root.left = new Node(3);
        root.right = new Node(10);
        root.left.left = new Node(1);
        root.left.right = new Node(6);
        root.left.right.left = new Node(4);
        root.left.right.right = new Node(7);
        root.right.right = new Node(14);
        root.right.right.left = new Node(13);

        CheckSubSequence cs = new CheckSubSequence();
        int[] seq1 = {4, 6, 8, 14};
        int[] seq = {4, 6, 8, 12, 13};

        Index idx = cs.new Index();
        idx.indx = 0;
        cs.checkExistence(root,seq,idx);
        System.out.println("Subseq exists ? "+(idx.indx>=seq.length?"true":"false"));
    }

    class Index{
        int indx;
    }

    private void checkExistence(Node root, int[] seq, Index idx)   {
        if(root == null)
            return;
        if(idx.indx >= seq.length)
            return;
        checkExistence(root.left,seq,idx);

        if(root.val == seq[idx.indx])
            idx.indx++;

        checkExistence(root.right,seq,idx);
    }
}
