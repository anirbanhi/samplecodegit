package com.apal.bst;

public class BSTOperations {

    public static void main(String[] args) {
        new BSTOperations().driver();
    }

    public void driver()    {
        System.out.println("SearchBST(12) null == " + searchBSTIter(null,12));
        System.out.println("SearchBST(12) null == " + searchBSTRec(null,12));
        BST root = null;
        root = insertBST(root, 12);
        insertBST(root, 20);
        insertBST(root, 30);
        insertBST(root, 10);
        insertBST(root, 8);
        insertBST(root, 9);

        System.out.println("SearchBST(12) true == " + searchBSTIter(root,20));
        System.out.println("SearchBST(12) true == " + searchBSTRec(root,20));
        System.out.println("SearchBST(12) true == " + searchBSTIter(root,12));
        System.out.println("SearchBST(12) true == " + searchBSTRec(root,12));
        System.out.println("SearchBST(12) true == " + searchBSTIter(root,9));
        System.out.println("SearchBST(12) true == " + searchBSTRec(root,9));

        System.out.println();
        inorder(root);
        System.out.println();

        deleteFromBSTRec(root, 10);

        System.out.println();
        inorder(root);
        System.out.println();

    }

    public BST deleteFromBSTRec(BST root, int d)  {
        if(root == null)
            return null;
        if(root.val == d && root.left == null && root.right == null)    {
            root = null;
            return null;
        }

        if(d > root.val)
            root.right = deleteFromBSTRec(root.right, d);
        else if(d < root.val)
            root.left = deleteFromBSTRec(root.left, d);
        else {
            if(height(root.left) > height(root.right))  {
                BST q = inOrderPre(root.left);
                root.val = q.val;
                root.left = deleteFromBSTRec(root.left, q.val);
            } else   {
                BST q = inOrderSucc(root.right);
                root.val = q.val;
                root.right = deleteFromBSTRec(root.right, q.val);
            }
        }

        return root;
    }

    public int height(BST r)    {
        if(r == null)
            return 0;
        int l1 = height(r.left);
        int r1 = height(r.left);
        return Math.max(l1,r1) + 1;
    }

    public BST inOrderSucc(BST root) {
        while(root != null && root.left != null)
            root = root.left;
        return root;
    }

    public BST inOrderPre(BST root) {
        while(root != null && root.right != null)
            root = root.right;
        return root;
    }

    public void inorder(BST root)   {
        if(root == null)
            return;
        inorder(root.left);
        System.out.print(root.val+" , ");
        inorder(root.right);
    }
    public boolean searchBSTIter(BST root, int d)   {
        BST t = root;
        while(t != null) {
            if(t.val == d)
                return true;
            if(d > t.val)
                t = t.right;
            else
                t = t.left;
        }
        return false;
    }

    public boolean searchBSTRec(BST root, int d)   {
        if(root==null)
            return false;
        if(root.val == d)
            return true;
        else if (d > root.val)
            return searchBSTRec(root.right, d);
        else
            return searchBSTRec(root.left, d);
    }

    public BST insertBST(BST root, int d) {
        if(root == null)    {
            root = new BST(d);
        }
        if(d > root.val)
            root.right = insertBST(root.right, d);
        else if(d < root.val)
            root.left = insertBST(root.left, d);
        return root;
    }

    //this will also work
    public BST insertBSTMy(BST root, int d) {
        if(root == null)    {
            root = new BST(d);
        } else  {
            insertBSTRec(root,d,null);
        }
        return root;
    }

    public void insertBSTRec(BST cur, int d, BST prev)  {
        if(cur == null) {
            cur = new BST(d);
            if(d > prev.val)
                prev.right = cur;
            else
                prev.left = cur;
            return;
        }
        if(d == cur.val)
            return;
        else if(d > cur.val)
            insertBSTRec(cur.right,d,cur);
        else
            insertBSTRec(cur.left,d,cur);
    }

    class BST   {
        int val;
        BST left;
        BST right;

        public BST(int d)   {
            this.val = d;
            left = null;
            right = null;
        }
    }

}