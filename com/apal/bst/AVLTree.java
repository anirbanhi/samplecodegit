package com.apal.bst;

public class AVLTree {

    public static void main(String[] args) {
        new AVLTree().driver();
    }

    AVLNode rootGlobal;

    public void driver()    {
        rootGlobal = avlInsert(null, 30);
        avlInsert(rootGlobal, 20);
        avlInsert(rootGlobal, 10);
        //avlInsert(rootGlobal, 40);
        //avlInsert(rootGlobal, 50);
        //avlInsert(rootGlobal, 60);

        inoder(rootGlobal);
    }

    public void inoder(AVLNode root)    {
        if(root == null)
            return;
        inoder(root.left);
        System.out.println(root.val+" , ");
        inoder(root.right);
    }

    public AVLNode avlInsert(AVLNode root, int x) {
        if(root == null)    {
            AVLNode t = new AVLNode(x);
            return t;
        }
        if(x > root.val)
            root.right = avlInsert(root.right, x);
        else if (x < root.val)
            root.left = avlInsert(root.left, x);
        root.height = findHeight(root);

        //check LL, LR, RR, RL conditions
        int balance = findBalance(root);
        int balanceL = findBalance(root.left);
        int balanceR = findBalance(root.right);

        if(balance == 2 && balanceL == 1)   {   //LL
            return LLRotation(root);
        } else if(balance == 2 && balanceL == -1)    {  //LR
            return LRRotation(root);
        }else if(balance == -2 && balanceR == -1) {    //RR
            return RRRotation(root);
        }else if(balance == -2 && balanceR == 1) {     //RL
            return RLRotation(root);
        }

        return root;
    }

    public int findBalance(AVLNode root)    {
        if(root == null)
            return 0;

        int hl = root.left != null ? root.left.height : 0;
        int hr = root.right != null ? root.right.height : 0;

        return hl - hr;
    }

    public AVLNode LLRotation(AVLNode root) {
        AVLNode r = root;
        AVLNode rl = root.left;
        AVLNode rlr = rl.right;

        rl.right = r;
        r.left = rlr;

        r.height = findHeight(r);
        rl.height = findHeight(rl);

        if(rootGlobal == root)
            rootGlobal = rl;

        return rl;
    }

    public AVLNode LRRotation(AVLNode root) {
        return null;
    }

    public AVLNode RRRotation(AVLNode root) {
        return null;
    }

    public AVLNode RLRotation(AVLNode root) {
        return null;
    }

    public int findHeight(AVLNode root) {
        if(root == null)
            return 0;
        int hl = root.left != null ? root.left.height+1:0;
        int hr = root.right != null ? root.right.height+1:0;

        return hl>hr?hl:hr;
    }

    class AVLNode {
        int val;
        int height;
        AVLNode left;
        AVLNode right;

        public AVLNode(int x)   {
            this.val = x;
            height = 1;
            left = null;
            right = null;
        }
    }
}
