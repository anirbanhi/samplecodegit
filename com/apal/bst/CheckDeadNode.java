package com.apal.bst;
/*
    https://www.geeksforgeeks.org/simple-recursive-solution-check-whether-bst-contains-dead-end/
 */
public class CheckDeadNode {
    public static void main(String[] args)  {
        Node root = new Node(8);
        root.left = new Node(5);
        root.right = new Node(9);
        root.left.left = new Node(2);
        root.left.right = new Node(7);
        root.left.right.left = new Node(6);
        root.left.left.left = new Node(1);

        CheckDeadNode cdn = new CheckDeadNode();
        Limit l = cdn.new Limit(1,Integer.MAX_VALUE);

        cdn.hasDead1(root,l);

        System.out.println("Has Dead Node " );
    }

    class Limit{
        int left;
        int right;

        public Limit(int l, int r){
            left = l;
            right = r;
        }
    }

/*
    This is a little bit unorthodox solution. Simple solution is in doc.
    boolean deadEnd(Node root, int min, int max)
{
    // if the root is null or the recursion moves
    // after leaf node it will return false
    // i.e no dead end.
    if (root==null)
        return false;

    // if this occurs means dead end is present.
    if (min == max)
        return true;

    // heart of the recursion lies here.
    return deadEnd(root.left, min, root.data - 1)||
                deadEnd(root.right, root.data + 1, max);
}
*/

    private void hasDead1(Node root, Limit l)   {
        if(root == null)
            return;
        hasDead1(root.left,
                new Limit(l.left,root.val-1));
        hasDead1(root.right,
                new Limit(root.val+1,l.right));
        if(root.val == l.left && root.val == l.right)  {
            System.out.println("Found Dead Node " + root.val);
        }
    }
}
