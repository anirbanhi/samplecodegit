package com.apal.bst;

public class Node {
    int val;
    Node left;
    Node right;

    public Node(int v)  {
        this.val = v;
        this.left = null;
        this.right = null;
    }
}
