package com.apal.bst;
/*
https://www.geeksforgeeks.org/largest-number-bst-less-equal-n/

 */
public class LargestNoInBSTLessThanN {
    public static void main(String[] args)  {
        Node root = new Node(5);
        root.left = new Node(2);
        root.right = new Node(12);
        root.left.left = new Node(1);
        root.left.right = new Node(3);

        root.right.left = new Node(9);
        root.right.right = new Node(21);
        root.right.right.left = new Node(19);
        root.right.right.right = new Node(25);

        LargestNoInBSTLessThanN lnbt = new LargestNoInBSTLessThanN();
        Storage sto = lnbt.new Storage();
        int x = 1;
        lnbt.findInorderPredessor(root,x,sto);
        System.out.println("Greatest elem less than "+x+" is "+sto.inOrderPredecssor);
    }

    class Storage   {
        int inOrderPredecssor = -1;
    }

    private void findInorderPredessor(Node root, int x, Storage st) {
        if(root == null) {
            return;
        }
        findInorderPredessor(root.left,x,st);
        if(root.val >= x)  {
            return;
        }
        st.inOrderPredecssor = root.val;
        findInorderPredessor(root.right,x,st);
    }
}