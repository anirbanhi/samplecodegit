package com.apal.bst;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class BSTFromTraversals {
    public static void main(String[] args) {
        new BSTFromTraversals().driver();
    }

    public void driver()    {
        bst root = new bst(30);
        root.left = new bst(20);
        root.right = new bst(40);
        root.left.left = new bst(15);
        root.left.right = new bst(25);
        root.right.left = new bst(32);
        root.right.right = new bst(50);
        root.right.left.right = new bst(35);

        System.out.println();
        List<Integer> inlist = new ArrayList<>();
        inorder(root,inlist);
        System.out.println("inorder " +inlist.toString());

        List<Integer> prelist = new ArrayList<>();
        preOrder(root,prelist);
        System.out.println("preorder" + prelist.toString());

        List<Integer> postlist = new ArrayList<>();
        postOrder(root,postlist);
        System.out.println("postorder" + postlist.toString());

        //bst from pre order
        System.out.println("bst from preorder ");
        bst r = bstFromPreOrder(prelist);
        List<Integer> inList = new ArrayList<>();
        inorder(r, inList);
        System.out.println("inorder "+inList.toString());
    }

    public void inorder(bst r, List<Integer> sb)  {
        if(r == null)
            return;
        inorder(r.left,sb);
        sb.add(r.val);
        inorder(r.right,sb);
    }
    public void preOrder(bst r, List<Integer> sb)   {
        if(r == null)
            return;
        sb.add(r.val);
        inorder(r.left,sb);
        inorder(r.right,sb);
    }

    public void postOrder(bst r,List<Integer> sb)   {
        if(r == null)
            return;
        inorder(r.left,sb);
        inorder(r.right,sb);
        sb.add(r.val);
    }

    public bst bstFromPreOrder(List<Integer> preorder)    {
        Stack<bst> st = new Stack<>();
        bst r = new bst(preorder.get(0));
        bst t = r;
        for(int i=1;i<preorder.size();)  {
            int d = preorder.get(i);
            if(d > t.val)   {
                if(st.size() == 0)  {
                    bst p = new bst(d);
                    t.right = p;
                    t = p;
                    i++;
                } else if (st.size() > 0 && d < st.peek().val)  {
                    bst p = new bst(d);
                    t.right = p;
                    t = p;
                    i++;
                } else if (st.size() > 0){
                    t = st.pop();
                }
            } else {
                bst p = new bst(d);
                t.left = p;
                t = p;
                st.push(p);
                i++;
            }
        }
        return r;
    }

    public bst bstFromPostOrder(bst r)    {
        return null;
    }

    class bst {
        int val;
        bst left;
        bst right;
        public bst(int n)   {
            this.val = n;
            left = null;
            right = null;
        }
    }
}
