package com.apal.bst;
/*
https://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/

Another self made unorthodox solution in o(n)
But simple to understand

 */
public class FindInOrderSuccOfANode {
    public static void main(String[] args)  {
        Node root = new Node(20);
        root.left = new Node(8);
        root.right = new Node(22);
        root.left.left = new Node(4);
        root.left.right = new Node(12);
        root.left.right.left = new Node(10);
        root.left.right.right = new Node(14);
        root.right = new Node(22);

        int x = 22;
        FindInOrderSuccOfANode fios = new FindInOrderSuccOfANode();
        Res res = fios.new Res();
        fios.findInOrder(root,x,res);
        System.out.println("Inorder Successor of "+x+" is "+res.successor);
    }

    class Res{
        boolean found = false;
        int successor = -1;
    }
    private void findInOrder(Node root, int x, Res res)  {
        if(root == null)
            return;
        findInOrder(root.left,x,res);
        if(res.found) {
            res.successor = root.val;
            res.found = false;
            return;
        }
        if(x == root.val)   {
            res.found = true;
        }
        findInOrder(root.right,x,res);
        }
}
