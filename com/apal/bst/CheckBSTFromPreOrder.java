package com.apal.bst;

import java.util.Stack;

/*
https://www.geeksforgeeks.org/check-if-a-given-array-can-represent-preorder-traversal-of-binary-search-tree/

 */
public class CheckBSTFromPreOrder {
    public static void main(String[] args)  {
        int[] pre = {40, 30, 35, 80, 100};
        CheckBSTFromPreOrder cbpt = new CheckBSTFromPreOrder();
        int n = pre.length;
        cbpt.isPreOrder(pre,n);
    }

    private boolean isPreOrder(int[] pre, int n)    {
        Stack<Integer> st = new Stack<>();
        st.push(pre[0]);
        int root = Integer.MIN_VALUE;
        for(int i=1; i<n; i++)    {
            if(root > pre[i])
                return false;
            while(!st.isEmpty() && st.peek() < pre[i])   {
                root = st.peek();
                st.pop();
            }

        }
        return true;
    }
}