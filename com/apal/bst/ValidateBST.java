package com.apal.bst;

public class ValidateBST {
    public static void main(String[] args)  {
        Node root = new Node(4);
        root.left = new Node(2);
        root.right = new Node(5);

        root.left.left = new Node(1);
        root.left.right = new Node(3);

        ValidateBST vb = new ValidateBST();
        System.out.println("BST Validated "+vb.validateBST(root) );
    }

    private boolean validateBST(Node root)  {
        if(root == null)
            return true;
        boolean l = true;
        if(root.left != null && root.val <= root.left.val)
            l = false;
        boolean r = true;
        if(root.right != null && root.val >= root.right.val)
            r = false;
        boolean l1 = validateBST(root.left);
        boolean r1 = validateBST(root.right);

        return l && r && l1 && r1;
    }
}
