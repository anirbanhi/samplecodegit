package com.apal.advanced.segmenttree;

public class SumQuery {
    public static void main(String[] args)  {
        int[] ip = {1,3,-2,8,-7};
        SumQuery sq = new SumQuery();
        sq.printArray(ip,"input");

        int[] t = new int[4*ip.length];
        for(int i=0;i<t.length;i++)
            t[i] = 0;
        sq.constructSumSegmentTree(ip,t,1,0,ip.length-1);
        sq.printArray(t,"Sum Segment Tree");
        System.out.println(" SUM " + sq.findSumInRange(1,0,ip.length-1,1,3,t));
    }

    private void constructSumSegmentTree(int[] ip, int[] t, int v, int tl, int tr)  {
        if(tl == tr)
            t[v] = ip[tl];
        else {
            int tm = ( tl + tr ) / 2;
            constructSumSegmentTree(ip,t,v*2,tl,tm);
            constructSumSegmentTree(ip,t,v*2+1,tm+1,tr);
            t[v] = t[v*2] + t[v*2+1];
        }
    }
    private int findSumInRange(int v, int tl, int tr, int l, int r, int[] t)    {
        if(l>r)
            return 0;
        if(l == tl && r == tr)
            return t[v];
        else {
            int tm = (tl + tr) / 2;
            return findSumInRange(v*2,tl,tm,l,Math.min(r,tm),t) +
                    findSumInRange(v*2+1,tm+1,tr,Math.max(tm+1,l),r,t);
        }
    }

    private void printArray(int[] a, String txt)    {
        System.out.println("Printing "+txt);
        for(int i : a)
            System.out.print(i+" , ");
    }
}
