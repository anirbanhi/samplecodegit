package com.apal.thread.trylock;

import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class PCBuffer {
    private LinkedList<Integer> queue = new LinkedList<Integer>();
    private int capacity = 10;
    private Random rnd = new Random();
    ReentrantLock lock = new ReentrantLock();

    public void produce()   throws InterruptedException{
        lock.lock();
            while (queue.size() == capacity) {
                System.out.println("Queue full");
                wait();
            }
            int elem = rnd.nextInt(20);
            System.out.println("Producer >>>>>>>>>>>>>>>>>>>>> " + elem);
            queue.addLast(elem);
            notifyAll();
        lock.unlock();
    }

    public void consume() throws InterruptedException {
        lock.lock();
        while (queue.size() == 0) {
            System.out.println("Queue Empty");
            wait();
        }
        int elem = queue.removeFirst();
        System.out.println("Consumer <<< " + elem);
        notifyAll();
        lock.unlock();
    }
}
