package com.apal.thread.trylock;

public class ProducerConsumerMain {
    public static void main(String[] args)  {
        new ProducerConsumerMain().runTheLogic();
    }

    public void runTheLogic() {
        PCBuffer pcb = new PCBuffer();
        Runnable producer = new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i < 15) {
                    try {
                        pcb.produce();
                        i++;
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread tp1 = new Thread(producer);
        tp1.start();
        Thread tp2 = new Thread(producer);
        tp2.start();

        Runnable consumer = new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while(i < 10)
                {
                    try {
                        pcb.consume();
                        i++;
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread tc1 = new Thread(consumer);
        Thread tc2 = new Thread(consumer);
        Thread tc3 = new Thread(consumer);

        tc1.start();
        tc2.start();
        tc3.start();

        try {
            tp1.join();
            tp2.join();
            tc1.join();
            tc2.join();
            tc3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Main Exits");
    }
}
