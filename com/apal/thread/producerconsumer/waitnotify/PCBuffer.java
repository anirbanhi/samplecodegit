package com.apal.thread.producerconsumer.waitnotify;


import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class PCBuffer {
    private LinkedList<Integer> queue = new LinkedList<Integer>();
    private int capacity = 10;
    private Random rnd = new Random();

    public void produce()   throws InterruptedException{
        synchronized (this) {
            while(queue.size() == capacity) {
                System.out.println("Queue full");
                    wait();
            }
                int elem = rnd.nextInt(20);
                System.out.println("Producer >>>>>>>>>>>>>>>>>>>>> " + elem);
                queue.addLast(elem);
                notifyAll();
        }
    }

    public void consume() throws InterruptedException{
        synchronized (this) {
            while(queue.size() == 0) {
                System.out.println("Queue Empty");
                    wait();
            }
                int elem = queue.removeFirst();
                System.out.println("Consumer <<< " + elem);
                notifyAll();
        }
    }
}
