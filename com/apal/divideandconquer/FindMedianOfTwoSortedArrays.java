package com.apal.divideandconquer;

/*
https://leetcode.com/problems/median-of-two-sorted-arrays/
 */

import java.util.PriorityQueue;

public class FindMedianOfTwoSortedArrays {
    public static void main(String[] args) {
        new FindMedianOfTwoSortedArrays().driver();
    }

    public void driver()    {
        findMedianSortedArrays(new int[]{}, new int[]{});
    }


        public double findMedianSortedArrays(int[] nums1, int[] nums2) {

            PriorityQueue<Integer> minHeap = new PriorityQueue<>((t1,t2) -> t1 - t2);

            PriorityQueue<Integer> maxHeap = new PriorityQueue<>((t1,t2) -> t2 - t1);


            addElementsToHeap(nums1, minHeap, maxHeap);
            addElementsToHeap(nums2, minHeap, maxHeap);

            int n = nums1.length;
            int m = nums2.length;

            if( (n+m) % 2 == 0)  {
                return (minHeap.poll().doubleValue() + maxHeap.poll().doubleValue() ) / 2;
            }
            if(minHeap.size() > maxHeap.size())
                return minHeap.poll().doubleValue();

            return maxHeap.poll().doubleValue();
        }

        public void addElementsToHeap(int[] nums1, PriorityQueue<Integer> minHeap, PriorityQueue<Integer> maxHeap) {

            for(int i=0;i<nums1.length;i++)  {
                int elem = nums1[i];
                int minsize = minHeap.size();
                int maxsize = maxHeap.size();


                if(maxsize == 0) {
                    maxHeap.add(elem);
                    continue;
                }

                if(minsize == 0) {
                    if(elem < maxHeap.peek())   {
                        minHeap.add(maxHeap.poll());
                        maxHeap.add(elem);
                        continue;
                    } else {
                        minHeap.add(elem);
                        continue;
                    }
                }


                if(elem >= minHeap.peek())
                    minHeap.add(elem);
                else if(elem <= maxHeap.peek())
                    maxHeap.add(elem);
                else    {
                    if(minsize >= maxsize)
                        maxHeap.add(elem);
                    else
                        minHeap.add(elem);
                }

                minsize = minHeap.size();
                maxsize = maxHeap.size();

                if(minsize - maxsize > 1)   {
                    maxHeap.add(minHeap.poll());
                } else if (maxsize - minsize > 1)   {
                    minHeap.add(maxHeap.poll());
                }
            }

        }
    }

