package com.apal.divideandconquer;

public class LLMergeSort {

    public static void main(String[] args) {
        new LLMergeSort().driver();
    }

    public void driver()    {
        Node head = new Node(5);
        head.next = new Node(4);
        head.next.next = new Node(3);
        head.next.next.next = new Node(2);
        head.next.next.next.next = new Node(1);

        Node newHead = mergeSort(head);
        print(newHead);
    }

    public void print(Node head)    {
        while (head != null) {
            System.out.print(head.val + " -> ");
            head = head.next;
        }
    }

    class Node {
        int val;
        Node next;

        public Node(int x)  {
            this.val = x;
            this.next = null;
        }
    }

    public Node mergeSort(Node head) {
        if(head == null || head.next == null)
            return head;

        Node mid = findMid(head);
        Node hi = mid.next;
        mid.next = null;

        Node low = head;

        Node l = mergeSort(low);
        Node r = mergeSort(hi);

        Node ret =  merge(l,r);
        return ret;
    }

    public Node findMid(Node head)  {
        if(head == null)
            return head;

        Node slow = head;
        Node fast = head.next;

        while(fast != null) {
            fast = fast.next;
            if(fast != null)    {
                slow = slow.next;
                fast = fast.next;
            }
        }
        return slow;
    }

    public Node merge(Node l, Node h)   {
        if(l == null)
            return h;
        if(h == null)
            return l;

        Node k = new Node(-1);
        Node ret = k;
        while(l!=null && h!=null) {
            if(l.val < h.val) {
                k.next = l;
                l = l.next;
                k = k.next;
            } else {
                k.next = h;
                h = h.next;
                k = k.next;
            }
        }

        while(l!=null){
            k.next = l;
            l = l.next;
            k = k.next;
        }

        while(h!=null){
            k.next = h;
            h = h.next;
            k = k.next;
        }

        return ret.next;
    }

}