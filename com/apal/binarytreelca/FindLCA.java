package com.apal.binarytreelca;
/*
https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
         1
    2        3
4       5 6     7
 */
public class FindLCA {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);

        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);

        FindLCA fl = new FindLCA();
        int x = 3;
        int y = 7;
        fl.findLca(root,x,y);
        System.out.println("LCA "+fl.findLCAReturn(root,x,y));

        System.out.println("Find LCA Node "+fl.findLCANode(root,x,y).val);
    }

    private int findLca(Node root, int x, int y)   {
        if(root == null)
            return 0;

        int l1 = findLca(root.left, x, y);
        int l2 = findLca(root.right, x, y);

        if(root.val == x || root.val == y)    {
            if(l1 + l2 > 0)     {
                System.out.println("Found LCA node "+root.val);
                return 2;
            }
            else
                return 1;
        }

        if(l1 == 1 && l2 == 1)
            System.out.println("Found LCA node "+root.val);
        return l1 + l2;
    }

    private Integer findLCAReturn(Node root, int x, int y)  {
        if(root == null)
            return null;

        Integer l = findLCAReturn(root.left,x,y);

        Integer r = findLCAReturn(root.right,x,y);
        Integer res = null;
        if(root.val == x || root.val == y)  {
            if(l != null || r!= null)
                return root.val;
            else{
                if(l!=null)
                    return l;
                else if (r != null)
                    return r;
                else
                    return root.val;
            }
        }

        if(l != null && r != null)
            return root.val;
        else if(l != null)
            res = l;
            else if(r != null)
                res = r;
        return res;
    }

    private Node findLCANode(Node root, int x, int y)   {
        if(root == null)
            return null;
        if(root.val == x || root.val == y)
            return root;

        Node l = findLCANode(root.left,x,y);
        Node r = findLCANode(root.right,x,y);

        if(l != null && r != null)
            return root;
        Node res = l != null ? l : r != null ? r : null;
        return res;
    }
}
