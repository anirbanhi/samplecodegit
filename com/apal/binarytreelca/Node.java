package com.apal.binarytreelca;

public class Node {
    int val;
    Node right;
    Node left;

    public Node(int v){
        this.val = v;
        this.left = null;
        this.right = null;
    }
}
