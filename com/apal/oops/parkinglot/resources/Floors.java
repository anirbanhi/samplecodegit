package com.apal.oops.parkinglot.resources;

import java.util.ArrayList;
import java.util.List;

public class Floors {
    List<Floor> floors;
    int floorMax;

    public Floors() {
        floors = new ArrayList<>();
    }

    public void addFloor()  {
        int newFloorNo = floorMax + 1;
        Floor fl = new Floor(newFloorNo);
        floorMax = newFloorNo;
    }
}
