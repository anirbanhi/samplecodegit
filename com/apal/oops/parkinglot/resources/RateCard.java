package com.apal.oops.parkinglot.resources;

import java.util.HashMap;
import java.util.Map;

public class RateCard {
    Map<SpotType,Float> firstHourRate;
    Map<SpotType,Float> subsequentHourRate;
    Map<SpotType,Float> additionalRate;

    public RateCard()  {
        firstHourRate = new HashMap<>();
        firstHourRate.put(SpotType.SMALL,20.0f);
        firstHourRate.put(SpotType.MEDIUM,40.0f);
        firstHourRate.put(SpotType.LARGE,60.0f);

        subsequentHourRate = new HashMap<>();
        subsequentHourRate.put(SpotType.SMALL,10.0f);
        subsequentHourRate.put(SpotType.MEDIUM,20.0f);
        subsequentHourRate.put(SpotType.LARGE,30.0f);

        additionalRate = new HashMap<>();
        additionalRate.put(SpotType.SMALL,10.0f);
        additionalRate.put(SpotType.MEDIUM,20.0f);
        additionalRate.put(SpotType.LARGE,30.0f);
    }

    public float calculateAmount(float hourMin, SpotType type, boolean isAdditionalRate) {
        float price = 0.0f;
        if(hourMin > 0.0f)  {
            price += firstHourRate.get(type);
            hourMin = hourMin - 1.0f;
        }
        if(hourMin > 0.0f)
        {
            int hours = (int) hourMin;
            hours = hours +1 ;
            price = price + hours * subsequentHourRate.get(type);
        }
        if(isAdditionalRate)    {
            price = price + additionalRate.get(type);
        }
        return price;
    }
}
