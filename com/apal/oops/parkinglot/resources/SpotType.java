package com.apal.oops.parkinglot.resources;

public enum SpotType {
    SMALL,
    MEDIUM,
    LARGE;
}
