package com.apal.oops.parkinglot.resources;

public class ParkingMainRunner {

    public static void main(String[] args)    {

        ParkingLot p1 = new ParkingLot();
        Ticket t1 = p1.entry("DL2CAP-7147",SpotType.MEDIUM);
        Ticket t2 = p1.entry("DL2CAP-7145",SpotType.MEDIUM);
        Ticket t3 = p1.entry("DL2CAP-7141",SpotType.MEDIUM);


        if(t3 != null)
            p1.exit(t3);
        if(t2 != null)
            p1.exit(t2);
        if(t1 != null)
            p1.exit(t1);

        Ticket t4 = p1.entry("DL2CAP-7198",SpotType.MEDIUM);
        if(t4 != null)
            p1.exit(t4);
    }

}
