package com.apal.oops.parkinglot.resources;

import java.util.Date;

public class Ticket {
    Spot spot;
    IVehicle vehicle;
    Date entryTime;
    RateCard rateCard;

    public Ticket(Spot _spot, IVehicle _vehicle, RateCard _rateCard)    {
        this.spot = _spot;
        this.vehicle = _vehicle;
        this.rateCard = _rateCard;
        this.entryTime = new Date();
    }


}
