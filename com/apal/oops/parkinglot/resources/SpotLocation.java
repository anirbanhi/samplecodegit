package com.apal.oops.parkinglot.resources;

public class SpotLocation{
    int floorNo;
    String locId;
    String friendlyName;

    public SpotLocation(int _floor, String _locId, String _friendlyName)    {
        this.floorNo = _floor;
        this.locId = _locId;
        this.friendlyName = _friendlyName;
    }

}
