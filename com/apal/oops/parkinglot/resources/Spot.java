package com.apal.oops.parkinglot.resources;

import java.util.Date;

public class Spot {
    SpotLocation loc;
    boolean isActive;
    boolean isAvailable;
    SpotType spotType;
    Date dateTime;

    public Spot(SpotLocation _loc, SpotType _spotType)   {
        this.spotType = _spotType;
        this.loc = _loc;
        this.isActive = true;
        this.isAvailable = true;
    }


}
