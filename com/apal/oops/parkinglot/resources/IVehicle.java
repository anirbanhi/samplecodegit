package com.apal.oops.parkinglot.resources;

public abstract class IVehicle {
    String regno = null;
    SpotType type = null;

    public String getRegno() {
        return regno;
    }

    public SpotType getType() {
        return type;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public void setType(SpotType type) {
        this.type = type;
    }
}
