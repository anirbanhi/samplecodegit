package com.apal.oops.parkinglot.resources;

import java.util.*;

public class ParkingLot {
    RateCard rateCard;
    List<Spot> spots;
    Scanner sc;
    Floors floors;

    public ParkingLot() {
        rateCard = new RateCard();
        spots = new ArrayList<>();
        spots.add(new Spot(new SpotLocation(1,"F1-01","1st Floor, right side"),SpotType.MEDIUM));
        spots.add(new Spot(new SpotLocation(1,"F1-02","1st Floor, right side"),SpotType.MEDIUM));
        spots.add(new Spot(new SpotLocation(1,"F1-03","1st Floor, right side"),SpotType.SMALL));
        spots.add(new Spot(new SpotLocation(1,"F1-04","1st Floor, right side"),SpotType.SMALL));
        spots.add(new Spot(new SpotLocation(1,"F1-05","1st Floor, right side"),SpotType.LARGE));

        this.sc = new Scanner(System.in);
    }

    public Ticket entry(String regno,SpotType stype) {
        IVehicle vehicle = new Car(regno,stype);
        Spot spot = findNextAvailableSpot(vehicle.getType());
        if(spot == null)      {
            String consent = getInputFromConsole("Do you want to find in higher size "+vehicle.regno);
            if(consent.trim().equalsIgnoreCase("Y")) {
                spot = findNextAvailableSpotWithHigherType(vehicle.getType());
                if (spot == null) {
                    System.out.println("Sorry no space available");
                    return null;
                }
            }
            else
            {
                System.out.println("Sorry no space available");
                return null;
            }
        }
        //here means spot != null
        Ticket ticket = new Ticket(spot,vehicle,rateCard);
        spot.isAvailable = false;
        return ticket;
    }

    public String getInputFromConsole(String display)   {
        System.out.println(display);
        String response = sc.nextLine();
        return response;
    }

    public void exit(String regno) {

    }

    public void exit(Ticket ticket) {
        //find logic to calucalate time difference
        Random rand = new Random();
        int randDuration = rand.nextInt(10);
        float amount = ticket.rateCard.calculateAmount(randDuration,ticket.spot.spotType,true);

        while(true)    {
            String response = getInputFromConsole("Please pay Rs. "+amount+" for "+ticket.vehicle.regno);
            if(response.trim().equalsIgnoreCase("Y"))
            {
                System.out.println("Thank You, please visit again");
                ticket.spot.isAvailable = true;
                break;
            }
        }


    }

    public Spot findNextAvailableSpot(SpotType type) {
        for(Spot s : spots) {
            if(s.isActive && s.isAvailable && (s.spotType == type)) {
                return s;
            }
        }
        return null;
    }

    public Spot findNextAvailableSpotWithHigherType(SpotType type) {
        for(Spot s : spots) {
            if(s.isActive && s.isAvailable && (s.spotType.ordinal() > type.ordinal())) {
                return s;
            }
        }
        return null;
    }
}
