package com.apal.interval;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*
    This is from educative
    Given an array of intervals, find the next interval of each interval. In a list of intervals, for an interval ‘i’ its next interval ‘j’ will have the smallest ‘start’ greater than or equal to the ‘end’ of ‘i’.

    Write a function to return an array containing indices of the next interval of each input interval. If there is no next interval of a given interval, return -1. It is given that none of the intervals have the same start point.

 */
public class NextInterval {
    public static void main(String[] args) {
        new NextInterval().driver();
    }
    public void driver()    {
        int[][] ip1 = {{2,3},{3,4},{5,6}};
        int[] op1 = {1,2,-1};
        System.out.println(Arrays.toString(op1)+"  "+Arrays.toString(findNextInterval(ip1)));

        int[][] ip2 = {{3,4},{1,5},{4,6}};
        int[] op2 = {2,-1,-1};
        System.out.println(Arrays.toString(op2)+"  "+Arrays.toString(findNextInterval(ip2)));
    }
    public int[] findNextInterval(int[][] a) {
        int[] first = new int[a.length];
        //Arrays.sort(a,comp);
        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0;i<a.length;i++)  {
            map.put(a[i][0],i);
            first[i]=a[i][0];
        }

        Arrays.sort(first);
        int[] op = new int[a.length];
        Arrays.fill(op,-1);
        for(int i=0;i<a.length;i++)  {
            int n = a[i][1];
            if(map.containsKey(n))
                op[i]=map.get(n);
            else {
                int nextElem = findNextElement(first,n);
                if(nextElem != -1)
                    op[i]=nextElem;
            }
        }

        //System.out.println(Arrays.toString(op));
        return op;
    }

    //find nearest next element just next to x using Binary Search
    public int findNextElement(int[] a, int x)  {
        int l=0;
        int h=a.length-1;
        int mid=0;
        int last=-1;
        while(l<=h) {
            mid = l + (h-l)/2;
            if(x>a[mid]){
                l=mid+1;
            }else{
                h=mid-1;
                last=mid;
            }
        }
        return last;
    }
}
