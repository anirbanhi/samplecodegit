package com.apal.interval;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

/*
We are given a list of Jobs. Each job has a Start time, an End time, and a CPU load when it is running. Our goal is to find the maximum CPU load at any time if all the jobs are running on the same machine.

Example 1:

Jobs: [[1,4,3], [2,5,4], [7,9,6]]
Output: 7
Explanation: Since [1,4,3] and [2,5,4] overlap, their maximum CPU load (3+4=7) will be when both the
jobs are running at the same time i.e., during the time interval (2,4).
 */

public class MaximumCPULoad {

    public static void main(String[] args) {
        new MaximumCPULoad().driver();
    }

    public int findMaxCPULoad(List<Job> jobs) {
        // TODO: Write your code here
        PriorityQueue<Job> pq = new PriorityQueue<>(
                (j1,j2) -> j1.start-j2.start
        );

        for(Job j : jobs)
            pq.offer(j);
        int max = 0;
        while(pq.size()>1)    {
            Job j1 = pq.remove();
            Job j2 = pq.remove();
            max = Math.max(j1.cpuLoad,max);
            max = Math.max(j2.cpuLoad,max);
            if((j1.start >= j2.start && j1.start < j2.end)
                    || (j2.start >= j1.start && j2.start < j1.end) )    {
                int st = Math.max(j1.start,j2.start);
                int end = Math.min(j1.end,j2.end);
                Job j = new Job(st,end,j1.cpuLoad+j2.cpuLoad);
                pq.offer(j);
                max = Math.max(j.cpuLoad, max);
            }
        }
        if(pq.size()>0) {
            Job j = pq.remove();
            max = Math.max(j.cpuLoad, max);
        }

        return max;
    }

    public void driver()    {
        List<Job> input = new ArrayList<Job>(Arrays.asList(new Job(1, 4, 3), new Job(2, 5, 4), new Job(7, 9, 6)));
        System.out.println("Maximum CPU load at any time: " + findMaxCPULoad(input));

        input = new ArrayList<Job>(Arrays.asList(new Job(6, 7, 10), new Job(2, 4, 11), new Job(8, 12, 15)));
        System.out.println("Maximum CPU load at any time: " + findMaxCPULoad(input));

        input = new ArrayList<Job>(Arrays.asList(new Job(1, 4, 2), new Job(2, 4, 1), new Job(3, 6, 5)));
        System.out.println("Maximum CPU load at any time: " + findMaxCPULoad(input));
    }

    class Job {
        int start;
        int end;
        int cpuLoad;

        public Job(int start, int end, int cpuLoad) {
            this.start = start;
            this.end = end;
            this.cpuLoad = cpuLoad;
        }
    };
}