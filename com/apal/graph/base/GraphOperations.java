package com.apal.graph.base;

public interface GraphOperations {

    boolean adDirdEdge(String src, String dest);

    boolean adDirdEdge(int src, int dest);

    void printGraph();

}
