package com.apal.graph.base;

import java.util.*;

public class FindShortestPathOptimised {

    public static void main(String[] args) {
        new FindShortestPathOptimised().driver();
    }

    public void driver()    {
        GraphAdjListWeighted g = GraphAdjListWeighted.sampleGraph();
        int src = 0;
        int dest = 5;
        //if this is a character graph, use Map<Char,Int>
        //initially set all to null, except src which should be 0
        Map<Integer, Integer> dist = new HashMap<>();
        for(Integer vertex : dist.keySet()) {
            dist.put(vertex, null);
        }

        //List<Integer> topoList = getTopoOrder(g);
        findShortestPath(g,0,5,dist);

        findShortestDijkstra(g);
    }

    public void findShortestPath(GraphAdjListWeighted g, int src, int dest,
                                 Map<Integer, Integer> dist)  {
        List<Integer> topoList = getTopoOrder(g);
        dist.put(0,0);
        for(Integer node : topoList)    {
            List<GraphAdjListWeighted.Edge> edges = g.getEdges(node);
            for(GraphAdjListWeighted.Edge e : edges)    {
                int newdist = e.weight + dist.get(node);
                if(dist.get(e.to) == null)
                    dist.put(e.to, newdist);
                else if(newdist < dist.get(e.to)) {
                    dist.put(e.to, newdist);
                }
            }
        }

        //print the final distance
        for(Integer key : dist.keySet())
            System.out.println(key + " -- " + dist.get(key));
    }

    private List<Integer> getTopoOrder(GraphAdjListWeighted g)    {
        int noOfVertices = g.getNoofvertices();
        int cur = 0;
        Map<Integer, Boolean> visited = new HashMap<>();
        for(int i=0;i<noOfVertices;i++)
            visited.put(i,false);
        LinkedList<Integer> ll = new LinkedList<>();
        findTopoOrder(g,cur,visited,ll);
        for(int i : ll)
            System.out.print(i+" -> ");
        return ll;
    }

    private void findTopoOrder(GraphAdjListWeighted g, int cur, Map<Integer,
            Boolean> visited, LinkedList<Integer> ll)   {
        if(visited.get(cur))   return;
        //go down dfs manner
        //get all neighbouring vertices
        List<Integer> neighbours = g.getNeighbours(cur);
        for(Integer n : neighbours) {
            if(!visited.get(n))  {
                findTopoOrder(g,n,visited,ll);
            }
        }
        ll.addFirst(cur);
        visited.put(cur, true);
    }

    private void findShortestDijkstra(GraphAdjListWeighted g)   {
        int startnode = 0;
        PQEntry start = new PQEntry(startnode,0);
        Set<Integer> visited = new HashSet<>();
        Map<Integer, Integer> distmap = new HashMap<>();
        for(Integer nd : distmap.keySet())
            distmap.put(nd, null);
        distmap.put(startnode,0);

        PriorityQueue<PQEntry> pq = new PriorityQueue<>(
                (n1,n2) -> n1.dist - n2.dist
        );
        pq.offer(start);

        while (!pq.isEmpty())    {
            PQEntry entry = pq.remove();
            int node = entry.dest;
            int dist = entry.dist;
            if(visited.contains(node))
                continue;
            List<GraphAdjListWeighted.Edge> neighbours = g.getEdges(node);
            for(GraphAdjListWeighted.Edge e : neighbours)   {
                int node1 = e.to;
                int newdist = e.weight + dist;
                if(distmap.get(node1) == null)
                    distmap.put(node1, newdist);
                else if (distmap.get(node1) > newdist)
                    distmap.put(node1, newdist);
                PQEntry pq1 = new PQEntry(node1, distmap.get(node1));
                pq.add(pq1);
            }
            visited.add(node);
        }

        //print distance map
        System.out.println(" ************************************ ");
        for(Integer key : distmap.keySet())
            System.out.println(key + " - " + distmap.get(key));
    }

    class PQEntry   {
        int dest;
        int dist;
        public PQEntry(int dest1, int dist1)    {
            dest = dest1;
            dist = dist1;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            PQEntry pqEntry = (PQEntry) o;
            return dest == pqEntry.dest &&
                    dist == pqEntry.dist;
        }

        @Override
        public int hashCode() {
            return Objects.hash(dest, dist);
        }
    }
}