package com.apal.graph.base;


import java.util.*;

public class GraphTraversals {
    public static void main(String[] args) {
        int [][] graph =
                {{0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 1, 1, 1, 0, 0, 0},
                {0, 1, 0, 1, 0, 0, 0, 0},
                {0, 1, 1, 0, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 1, 0, 1, 1},
                {0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 0, 1, 0, 0}};
        int vertex = 8;
        bfsGraphMatrix(graph, vertex);

        int[] visited = new int[graph.length];
        for(int i =0;i<graph.length;i++)
            visited[i] = 0;
        System.out.println();
        System.out.println("DFS ");
        dfsAdjMatrix(graph,visited,0);
        System.out.println();
        System.out.println("BFS");
        Map<Integer, List<Integer>> g = new HashMap<>();
        List<Integer> l0 = new ArrayList<>();l0.add(5);
        g.put(0,l0);
        List<Integer> l1 = new ArrayList<>();l1.add(2);l1.add(3);l1.add(4);
        g.put(1,l1);
        List<Integer> l2 = new ArrayList<>();l2.add(2);l2.add(1);l2.add(3);
        g.put(2,l2);
        List<Integer> l3 = new ArrayList<>();
        l3.add(1);l3.add(2);l3.add(4);l3.add(5);
        g.put(3,l3);
        List<Integer> l4 = new ArrayList<>();
        l4.add(1);l4.add(3);l4.add(5);
        g.put(4,l4);
        List<Integer> l5 = new ArrayList<>();
        l5.add(3);l5.add(4);l5.add(6);l5.add(7);
        g.put(5,l5);
        List<Integer> l6 = new ArrayList<>();
        l6.add(5);
        g.put(6,l6);
        List<Integer> l7 = new ArrayList<>();
        l7.add(5);
        g.put(7,l7);
        System.out.println("new bfs ");
        bfsGraphAdjList(g,g.size());
        System.out.println("");
        System.out.println("DFS Using Stack");
        dfsAdjMatrix(graph);
        System.out.println("DFS adj list: ");
        dfsAdjList(g);
    }

    public static void bfsGraphMatrix(int[][] g, int vc) {
        if(vc <= 0)
            return;
        Queue<Integer> q = new LinkedList<>();
        q.offer(0);
        Set<Integer> set = new HashSet<>();
        while( !q.isEmpty() ) {
            int vertex = q.remove();
            if(set.contains(vertex))
                continue;
            List<Integer> adjVertices = findAdjVerticesForMatrix(vertex,g);
            for(Integer v : adjVertices)
            {
                if(!set.contains(v))
                    q.offer(v);
            }
            set.add(vertex);
            System.out.print(vertex+" -> ");
        }
    }

    private static List<Integer> findAdjVerticesForMatrix(int v, int[][] g) {
            List<Integer> list = new ArrayList<>();
            for(int i=0;i<g.length;i++) {
                if(g[v][i] != 0)
                    list.add(i);
            }
            return list;
    }

    public static void bfsGraphAdjList(Map<Integer, List<Integer>> g,
                                       int vc) {
        Queue<Integer> q = new LinkedList<>();
        if(vc <= 0)
            return;
        int vertex = 0;
        q.offer(vertex);
        Set<Integer> set = new HashSet<>();
        set.add(vertex);
        while (!q.isEmpty())    {
            vertex = q.remove();
            System.out.print(vertex+" -> ");
            List<Integer> adjList = g.get(vertex);
            for(int v : adjList)    {
                if(!set.contains(v)) {
                    q.offer(v);
                    set.add(v);
                }
            }
        }
    }

    public static void dfsAdjMatrix(int[][] g, int[] visited, int v)  {
        if(visited[v] == 0) {
            visited[v] = 1;
            System.out.print(v + " -> ");
            for(int i=0;i<g.length;i++) {
                if(g[v][i] == 1 && visited[i] == 0)
                    dfsAdjMatrix(g,visited,i);
            }
        }
    }

    public static void dfsAdjMatrix(int[][] g) {
        int len = g.length;
        int[] v = new int[len];
        Stack<Integer> st = new Stack<>();
        st.push(0);
        while (!st.isEmpty())   {
            int top = st.pop();
            if(v[top] == 0) {
                System.out.print(top + " -> ");
                v[top] = 1;
                for (int i =0; i<len;i++) {
                    if (g[top][i] == 1 && v[i] == 0)
                        st.push(i);
                }
            }
        }
        System.out.println();
    }

    public static void dfsAdjList(Map<Integer, List<Integer>> g) {
        Stack<Integer> st = new Stack<>();
        Set<Integer> set = new HashSet<>();
        st.push(0);
        set.add(0);
        while (!st.isEmpty())  {
            int vertex = st.pop();
            System.out.print(vertex+" -> ");
            List<Integer> adjlist = g.get(vertex);
            for(int v:adjlist)  {
                if(!set.contains(v)) {
                    set.add(v);
                    st.push(v);
                }
            }
        }
        System.out.println();
    }
}