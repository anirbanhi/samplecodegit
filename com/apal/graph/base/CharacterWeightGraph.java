package com.apal.graph.base;

import java.util.List;
import java.util.Map;

public class CharacterWeightGraph {
    public int verticesCount;
    public Map<Character, List<Edge>> edges;
    public CharacterWeightGraph(int n, Map<Character, List<Edge>> edges)   {
        this.edges = edges;
        this.verticesCount = n;
    }
}

class Edge  {
    char dest;
    int weight;
    public Edge(char d, int w)  {
        this.dest = d;
        this.weight = w;
    }
}