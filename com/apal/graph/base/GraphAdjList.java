package com.apal.graph.base;

import java.util.*;

public class GraphAdjList {

    public int verticesCount;
    public Map<String, LinkedList<String>> map = new HashMap<>();

    public GraphAdjList(int verticesCount) {
        this.verticesCount = verticesCount;
    }



    public void bfs(){
        System.out.println();
        System.out.println("---------------------- BFS ---------------------- ");
        LinkedList<String> q = new LinkedList<>();
        q.addLast("A");

        String key = null;
        Set<String> set = new HashSet<>();
        set.add("A");
        while(!q.isEmpty())
        {
            key = q.removeFirst();
            System.out.print(key+" ");
            List<String> list = map.get(key);
            if(list != null)
            {
                for(String s : list)
                {
                    if(!set.contains(s))
                    {
                        q.addLast(s);
                        set.add(s);
                    }
                }
            }
        }

    }

    public void adDirdEdge(String a, String b) {
    }
}

