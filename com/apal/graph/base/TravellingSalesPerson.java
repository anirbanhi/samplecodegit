package com.apal.graph.base;

import java.util.*;
/*
This is brute force
complexity: O(2^n)
 */
public class TravellingSalesPerson {
    public static void main(String[] args) {
        new TravellingSalesPerson().driver();
    }

    public void driver()    {
        int[][] g = getGraph();
        int n = g.length;
        int[] v = new int[n];
        v[0] = 1;
        List<Integer> list = new ArrayList<Integer>();
        list.add(0);
        int cost = tsp(g,v,0,n,list);
        System.out.println(cost);
    }

    public int tsp(int[][] g, int[] v, int cur, int n, List<Integer> list)    {
        Queue<Integer> pq = new PriorityQueue<>(
                new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o1.compareTo(o2);
                    }
                }
        );
        boolean last = true;
        for(int i=1; i<n; i++)    {
            if(v[i] == 0 && i != cur && g[cur][i] > 0)   {
                v[i] = 1;
                list.add(i);
                int c = tsp(g,v,i,n,list) + g[cur][i];
                pq.add(c);
               // System.out.println(c);
                last = false;
                v[i] = 0;
                list.remove(list.size()-1);
            }
        }
        if(last) {
            System.out.println("At last "+cur+" "+g[cur][0]);
            System.out.println(list.toString());
            return g[cur][0];
        }

        v[cur] = 0;
        System.out.println(pq.toString());
        System.out.println("Return "+pq.peek());
        return pq.poll();
    }

    public int[][] getGraph()  {
        //since this is TSP, mostly there will be path
        //from 1 vertex to another vertex. So, better to use
        //adjacency matrix

        int[][] g =
                {
                        {0,10,15,20},
                        {5,0,9,10},
                        {6,13,0,12},
                        {8,8,9,0}
        };
        return g;
    }
}