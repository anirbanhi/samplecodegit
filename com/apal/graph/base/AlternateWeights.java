package com.apal.graph.base;

/*
https://www.geeksforgeeks.org/move-weighting-scale-alternate-given-constraints/
 */

import java.util.Arrays;

public class AlternateWeights {
    public static void main(String[] args) {
        int[] w = { 2, 3, 5, 6 };
        int steps = 10;
        int[] stepop = new int[steps];
        for(int i=0;i<stepop.length;i++)
            stepop[i] = -1;
        stepop[0] = w[0];
        boolean res = findWeightsBFS(stepop,1,w,0,true);
        System.out.println("Solution found "+res);
        System.out.println(Arrays.toString(stepop));
    }

    public static boolean findWeightsBFS(int[] stepop, int stepcount,
                                      int[] w, int cur,boolean right) {
        if(stepcount >= stepop.length)  {
            for(int i:stepop)
                if(i == -1) return false;
            return true;
        }
        for(int i=0;i<w.length;i++) {
            int wt = w[i];
            if(stepcount > 1 && stepop[stepcount-2] == wt)
                continue;
                if(right)   {
                    if(stepcount > 0 && wt > stepop[stepcount-1])    {
                        stepop[stepcount] = wt;
                        boolean res =
                                findWeightsBFS(stepop,stepcount+1,
                                w,i,false);
                        if(res) return true;
                        else {
                            stepop[stepcount] = -1;
                        }
                    }
                } else  {
                    if(stepcount > 0 && wt < stepop[stepcount-1])    {
                        stepop[stepcount] = wt;
                        boolean res =
                                findWeightsBFS(stepop,stepcount+1,
                                        w,i,true);
                        if(res) return true;
                        else {
                            stepop[stepcount] = -1;
                        }
                    }
                }
        }
        return false;
    }
}
