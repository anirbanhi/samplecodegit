package com.apal.graph.base;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
    For sample graph see
    \samplecode\src\com\apal\graph\base\FindCycleInADirectedGraph.bmp
 */

public class FindCycleInADirecetedGraph {
    public static void main(String[] args)  {
        new FindCycleInADirecetedGraph().driver();
    }

    public void driver()    {
        GraphAdjListWeighted g = GraphAdjListWeighted.
                sampleDirectedCyclicGraph();

        boolean[] visited = new boolean[g.getNoofvertices()];
        Arrays.fill(visited, false);
        Set<Integer> white = new HashSet<>();
        Set<Integer> grey = new HashSet<>();
        Set<Integer> black = new HashSet<>();

        //initially put all vertices in white set
        for(int i=0;i<g.getNoofvertices();i++)
            white.add(i);

        //call after moving one vertex from white set to grey set
        white.remove(0);
        grey.add(0);
        boolean isCycleExists = basedfs(g,0,visited,white,grey,black);
        System.out.println("Cycle exists : "+isCycleExists);

    }

    public boolean basedfs(GraphAdjListWeighted g, int cur, boolean[] visited,
                        Set<Integer> white,Set<Integer> grey,Set<Integer> black) {
        List<Integer> neighbours = g.getNeighbours(cur);
        visited[cur] = true;
        //System.out.println(cur);
        //System.out.println("grey "+grey);
        for(int ngh : neighbours)   {
            if(!black.contains(ngh))   {
                if(grey.contains(ngh)) {
                    //System.out.println("Cycle exists");
                    return true;
                }
                white.remove(ngh);
                grey.add(ngh);
                boolean isCycleExists = basedfs(g, ngh, visited,white,grey,black);
                if(isCycleExists)   return true;
            }
        }
        grey.remove(cur);
        black.add(cur);
        return false;
        //System.out.println("No cycle exists");
    }
}
