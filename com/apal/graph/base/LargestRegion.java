package com.apal.graph.base;

/*
https://www.geeksforgeeks.org/find-length-largest-region-boolean-matrix/
 */

public class LargestRegion {
    public static void main(String[] args) {
        int[][] g =
                {
                 {0, 0, 1, 1, 0},
                 {1, 0, 1, 1, 0},
                 {0, 1, 0, 0, 0},
                 {0, 0, 0, 0, 1}
                };
        int rc = 4;
        int cc = 5;
        int[][] visited = new int[rc][cc];
        for(int i=0;i<rc;i++) {
            for(int j=0;j<cc;j++) {
                visited[i][j] = 0;
            }
        }
        int[][] next =
                {
                    {1,-1,0,0, 1,1,-1,-1},
                    {0,0,1,-1, 1,-1,1,-1}
                };

        for(int i=0;i<rc;i++) {
            for(int j=0;j<cc;j++) {
                if(g[i][j] == 1 && visited[i][j] == 0)    {
                    visited[i][j] = 1;
                    int t = findRegion(g,i,j,visited,next,1) + 1;
                    System.out.println(i+" - "+j+" = "+t);
                    max = Math.max(t,max);
                }
            }
        }
        System.out.println("Max region is "+max);
    }
    static int max = 0;
    public static int findRegion(int[][] g, int r, int c, int[][] v,
                                     int[][] next,int cnt)  {
        int tot = 0;
        for(int i=0;i<next[0].length;i++)   {
            int r1 = r + next[0][i];
            int c1 = c + next[1][i];
            if(isSafe(g,r1,c1,v))   {
                v[r1][c1] = 1;
                tot += findRegion(g,r1,c1,v,next,cnt) + 1;
            }
        }
        return tot;
    }

    public static boolean isSafe(int[][] g, int r,int c,int[][] visited)  {
        int n = g.length;
        if(r < 0 || r >= n || c < 0 || c >= n)
            return false;
        if(g[r][c] == 0 || visited[r][c] == 1)
            return false;
        return true;
    }
}
