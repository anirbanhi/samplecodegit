package com.apal.graph.base;

import java.util.Arrays;
import java.util.List;

public class ColorAGraph {
    public static void main(String[] args)  {
        new ColorAGraph().driver();
    }
    public void driver()    {

        GraphAdjListNumber g = new GraphAdjListNumber(5);
        int[] colormap = new int[g.getNoOfVertices()];
        Arrays.fill(colormap, -1);
    }

    int color = 0;
    public boolean colorGraph(GraphAdjListNumber g, int cur, int[] colormap)    {
        for(int i=0; i<=color; i++)   {
            boolean isvalid = isValid(colormap, cur, i, g);
            if(isvalid) {
                colormap[cur] = i;
                List<Integer> adjlist = g.getAdjList(cur);
                boolean res = false;
                for(int adj : adjlist) {
                  res = colorGraph(g, adj, colormap);
                 if(res == false)
                     break;
                }
            }
        }
        return true;
    }

    public boolean isValid(int[] colormap, int vertex,
                           int color, GraphAdjListNumber g)    {
        //find vertices which has this color
        for(int i : colormap)   {
            if(colormap[i] == color)    {
                if(g.isValidEdge(i, vertex))
                    return false;
            }
        }
        return true;
    }
}
