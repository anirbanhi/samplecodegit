package com.apal.graph.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphAdjListWeighted {

    private int noofvertices;
    private Map<Integer, List<Edge>> adjList;
    public GraphAdjListWeighted(int n)   {
        //vertices no are starting from 0, so
        //if total vertices are 10. it will be 0 to 9
        noofvertices = n;
        adjList = new HashMap<>();
        for(int i=0;i<noofvertices;i++)
            adjList.put(i, new ArrayList<Edge>());
    }
    static class Edge{
        int from;
        int to;
        int weight;
        public Edge(int f, int to1, int weight1)   {
            this.from = f;
            this.to = to1;
            this.weight = weight1;
        }

        @Override
        public String toString() {
            return "Edge{" +
                    "from=" + from +
                    ", to=" + to +
                    ", weight=" + weight +
                    '}';
        }
    }

    public void addEdge(int src, int dest, int weight)   {
        Edge e = new Edge(src, dest, weight);
        adjList.get(src).add(e);
    }
    public int getNoofvertices() {
        return noofvertices;
    }

    public List<Edge> getEdges(int src) {
        return adjList.get(src);
    }

    public Map<Integer, List<Edge>> getAdjList() {
        return adjList;
    }

    public List<Integer> getNeighbours(int cur) {
        List<Edge> listEdges = adjList.get(cur);
        List<Integer> neighbours = new ArrayList<>();
        for (Edge e : listEdges)
            neighbours.add(e.to);
        return neighbours;
    }

    public int getWeight(int src, int dest)  {
        List<Edge> edges = adjList.get(src);
        for(Edge e : edges) {
            if(e.to == dest)
                return e.weight;
        }
        //dest doesnot exist.
        return -1;
    }

    public static GraphAdjListWeighted sampleGraph()   {
        //6 vertices Graph
        //FindShortestPath.bmp
        GraphAdjListWeighted g = new GraphAdjListWeighted(6);
        g.addEdge(0,1,5);
        g.addEdge(0,2,1);
        g.addEdge(1,2,2);
        g.addEdge(1,3,3);
        //g.addEdge(1,3,20);

        //g.addEdge(2,1,3);
        g.addEdge(2,4,12);
        g.addEdge(3,2,3);
        g.addEdge(3,4,2);
        g.addEdge(3,5,6);

        g.addEdge(4,5,1);
        return g;
    }

    public static GraphAdjListWeighted sampleDirectedCyclicGraph()  {
        GraphAdjListWeighted g = new GraphAdjListWeighted(6);
        g.addEdge(0,1,1);
        g.addEdge(0,2,1);
        g.addEdge(0,3,1);
        g.addEdge(1,2,1);
        g.addEdge(3,4,1);
        g.addEdge(4,5,1);
        g.addEdge(5,3,1);

        return g;
    }
}