package com.apal.graph.base;

import java.util.*;

public class GraphAdjListNumber {
    /*
            This is 0 indexed, so count = 4 means we have 5 vertices
            0,1,2,3,4
     */
    public GraphAdjListNumber(int count)  {
        this.noOfVertices = count;
        adjList = new HashMap<>();
        for(int i=0;i<noOfVertices;i++) {
            adjList.put(i, new LinkedList<>());
        }
    }

    private Map<Integer, List<Integer>> adjList;

    private int noOfVertices;

    public void addEdge(int vertex, int destVertex) {
        adjList.get(vertex).add(destVertex);
    }


    public List<Integer> getAdjList(int vertex) {
        return adjList.get(vertex);
    }

    public boolean isValidEdge(int from, int to)    {
        List<Integer> list = adjList.get(from);
        if(list.contains(to))
            return true;
        else
            return false;
    }

    public int getNoOfVertices()    {
        return this.noOfVertices;
    }
}