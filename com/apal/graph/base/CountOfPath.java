package com.apal.graph.base;

import java.util.Arrays;

public class CountOfPath {
    public static void main(String[] args) {
        int[][] g = {
                {0,1,1,0,1},
                {0,0,0,1,1},
                {0,0,0,0,1},
                {0,0,1,0,0},
                {0,0,0,0,0},
        };
        int vc = g.length;
        int[][] sol = new int[vc][vc];
        findReachability(g);
        int[] visited = new int[g.length];
        for(int i=0;i<g.length;i++)
            visited[i] = 0;
        //dfs wont work , we need to use bfs
        findCountOfPath(g,4,visited,0);
        System.out.println("Cont of path "+cnt);
    }

    public static int cnt = 0;
    public static void findCountOfPath(int[][]g, int dest,
                                      int[] visited,int v)    {
        if(visited[v] == 0) {
            visited[v] = 1;
            if(v == dest)   {
                cnt++;
                visited[v] = 0;
                return;
            }
            for(int i=0;i<g.length;i++) {
                if(visited[i] == 0 && g[v][i] == 1) {
                    findCountOfPath(g,dest,visited,i);
                }
            }
        }
    }

    public static void findReachability(int[][] g)    {
        int[][] sol = new int[g.length][g.length];
        for(int k=0;k<g.length;k++) {
            for(int l=0;l<g.length;l++) {
                sol[k][l] = 0;
            }
        }
        int[] visited = new int[g.length];
        for(int i=0;i<g.length;i++) {
            for(int j=0;j<visited.length;j++)
                visited[j] = 0;
            dfs(g,i,sol,visited,i);
        }
        for(int i=0;i<g.length;i++)
            System.out.println(Arrays.toString(sol[i]));
    }

    public static void dfs(int[][] g, int v, int[][] sol,
                           int[] visited, int src)    {
        if(visited[v] == 0) {
            sol[src][v] = 1;
            visited[v] = 1;
            for(int i =0;i<g.length;i++)    {
                if(g[v][i] == 1 && visited[i] == 0)
                    dfs(g,i,sol,visited,src);
            }
        }
    }

}
