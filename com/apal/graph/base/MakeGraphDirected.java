package com.apal.graph.base;

/*
    For Used Graph image see CenterOfGraph.jpg
    This functionality uses CenterOfGraph usage as
    well.
    Graph should not have any cycle in the tree
    Another term used for this task is rooting a tree
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MakeGraphDirected {

    public static void main(String[] args) {
        new MakeGraphDirected().driver();
    }

    public void driver()    {
        CenterOfAGraph cog = new CenterOfAGraph();
        CenterOfAGraph.Graph1 g = cog.getGraph();
        List<Integer> center = cog.findCenter(g);
        int centerVertex = center.get(0);
        makeGraphRooted(g,centerVertex);
    }

    public void makeGraphRooted(CenterOfAGraph.Graph1 g, int cv)   {
        Map<Integer, List<Integer>> newadjlist = new HashMap<>();
        bfs(g,cv,newadjlist,-1);
        print(newadjlist);
    }

    public void bfs(CenterOfAGraph.Graph1 g, int v,
                    Map<Integer, List<Integer>> adjlist2, int parent)   {
        List<Integer> adj = g.adjList.get(v);
        for(int i : adj) {
            if(i!=parent) {
                if(adjlist2.get(v) == null)
                    adjlist2.put(v,new ArrayList<>());
                adjlist2.get(v).add(i);
                bfs(g, i, adjlist2,v);
            }
        }
    }

    public void print(Map<Integer, List<Integer>> adjmap) {
        System.out.println();
        for(Integer i : adjmap.keySet()) {
            System.out.print(i+" -> ");
            for(int j : adjmap.get(i))
                System.out.print(j+" ");
            System.out.println();
        }
    }
}