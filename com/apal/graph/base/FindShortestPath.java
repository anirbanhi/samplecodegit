package com.apal.graph.base;

import java.util.*;

/*
This is a by-product of Dijkstra's algorithm.
This one is for undirected Graph.
Assume there is no -ve weight
Assume there is no cycle (positive / negative both)
Dijkstra we need a PQ and
 */

/*
    For example visualization see FindShortestPath.bmp
 */
public class FindShortestPath {
    public static void main(String[] args)  {
        new FindShortestPath().driver();
    }
    public void driver()    {
        GraphAdjListWeighted g = GraphAdjListWeighted.sampleGraph();
        PriorityQueue<PQNode> pq = new PriorityQueue<>(
                new Comparator<PQNode>() {
                    @Override
                    public int compare(PQNode o1, PQNode o2) {
                        return o1.cost - o2.cost;
                    }
                }
        );
        int startNode = 0;
        int destNode = 5;
        pq.offer(new PQNode(startNode, 0));
        //dist array to store optimum distance from startNode
        //initialize it with -1, means inf
        //initialize start node with 0
        int[] dist = new int[g.getNoofvertices()];
        Arrays.fill(dist, -1);
        dist[startNode] = 0;

        boolean[] visited = new boolean[g.getNoofvertices()];
        //findshortestpath(g,pq,dist,visited);
        printPath(g,pq,dist,visited,startNode,destNode);
    }

    private class PQNode{
        int dest;   //from source, which is given or fixed
        int cost;
        public PQNode(int d, int c) {
            this.dest = d;
            this.cost = c;
        }
    }

    public void printPath(GraphAdjListWeighted g,
                          PriorityQueue<PQNode> pq,
                          int[] dist,
                          boolean[] visited,
                          int src,
                          int dest) {
        List<Integer> path = new LinkedList<>();
        int[] parent = new int[g.getNoofvertices()];
        while(!pq.isEmpty())    {
            PQNode cur = pq.remove();
            int curVertex = cur.dest;
            if(visited[curVertex])
                continue;
            if(curVertex == dest)  {
                break;
            }
            List<Integer> neighbours = g.getNeighbours(cur.dest);
            visited[curVertex] = true;
            for(int ngh : neighbours)   {
                if(visited[ngh])
                    continue;
                int temp = dist[curVertex] +
                        g.getWeight(curVertex, ngh);
                if(dist[ngh] == -1) {
                    dist[ngh] = temp;
                    parent[ngh] = curVertex;
                }
                else {
                    if (dist[ngh] > temp) {
                        dist[ngh] = temp;
                        parent[ngh] = curVertex;
                    }
                }
                PQNode pn = new PQNode(ngh, dist[ngh]);
                pq.offer(pn);
            }
        }

        //print the path from src to dest
        System.out.println();
        int prev = -1;
        int cur = -1;
        cur = dest;
        for(int i =0;i<g.getNoofvertices();i++) {
            path.add(cur);
            if(cur == src)
                break;
            prev = parent[cur];
            cur = prev;
        }
        Collections.reverse(path);
        System.out.println();
        for(int p : path)
            System.out.print(p+" -> ");
    }

    public void findshortestpath(GraphAdjListWeighted g,
                                 PriorityQueue<PQNode> pq,
                                 int[] dist,
                                 boolean[] visited)  {
        while(!pq.isEmpty())    {
            PQNode cur = pq.remove();
            int curVertex = cur.dest;
            if(visited[curVertex])
                continue;
            List<Integer> neighbours = g.getNeighbours(cur.dest);
            visited[curVertex] = true;
            for(int ngh : neighbours)   {
                if(visited[ngh])
                    continue;
                int temp = dist[curVertex] +
                        g.getWeight(curVertex, ngh);
                if(dist[ngh] == -1)
                    dist[ngh] = temp;
                else {
                    if (dist[ngh] > temp)
                        dist[ngh] = temp;
                }
                PQNode pn = new PQNode(ngh, dist[ngh]);
                pq.offer(pn);
            }
        }
        //print the result
        for(int i=0;i<g.getNoofvertices();i++)
            System.out.println(i+" "+dist[i]);
    }
}