package com.apal.graph.base;

import com.apal.graph.base.GraphAdjListWeighted.Edge;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

/*
This will use the Graph FindShortestPath.bmp & GraphAdjListWeighted.java
 */

public class FindSpanningTree {
    public static void main(String[] args)  {
        new FindSpanningTree().driver();
    }
    public void driver()    {
        GraphAdjListWeighted g = GraphAdjListWeighted.sampleGraph();
        findSpanningTree(g);
    }

    public void findSpanningTree(GraphAdjListWeighted g)  {
        int n = g.getNoofvertices();
        //take one PQ
        PriorityQueue<Edge> pq = new PriorityQueue<>((e1,e2) -> e1.weight-e2.weight);
        //take one random vertex and add to PQ
        int v = new Random().nextInt(n);
        pq.offer(new Edge(v,v, Integer.MAX_VALUE));
        //visited set, set to false bydefault
        boolean[] visited = new boolean[n];

        List<Edge> spanningTree = new ArrayList<>();
        int edgeCount = 1;
        while(!pq.isEmpty() || edgeCount < n-1) {
            //this block is to find any missing vertices which have
            // all outward direction edges.
            if(pq.isEmpty())    {
                for(int k=0;k<visited.length;k++)   {
                    if(!visited[k])
                        pq.offer(new Edge(k,k,Integer.MAX_VALUE));
                }
            }

            Edge temp = pq.poll();
            v = temp.to;
            if(visited[v])  continue;
            //BFS on all neighbours of v
            List<Edge> edges = g.getEdges(v);
            visited[v] = true;
            //to remove self loops and discard initial dummy edge
            if(temp.from != temp.to) {
                spanningTree.add(temp);
                edgeCount++;
            }
            for(Edge e : edges)   {
                if(visited[e.to])   continue;
                pq.offer(e);
            }
        }

        //print the spanning tree edges
        for(Edge e : spanningTree)
            System.out.println(e);
    }

    public void primsSpanningTree() {
        GraphAdjListWeighted g = GraphAdjListWeighted.sampleDirectedCyclicGraph();
        PriorityQueue<Edge> pq =new PriorityQueue<>((e1,e2)->e1.weight-e2.weight);
        
    }
}