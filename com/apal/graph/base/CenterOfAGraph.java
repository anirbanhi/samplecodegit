package com.apal.graph.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
        Assume connected graph.
        To find center of a connected undirected graph
        This can be used to make a undirected graph to a
        directed graph.
        For Used Graph image see CenterOfGraph.jpg
*/

public class CenterOfAGraph {

    public static void main(String[] args) {
        new CenterOfAGraph().driver();
    }

    public void driver()    {
        Graph1 g = getGraph();
        findCenter(g);
    }

    public List<Integer> findCenter(Graph1 g)    {
        int n = g.verticesCount;
        //find leaves
        int[] deg = new int[n];
        int[] degOrig = new int[n];
        for(int i=0;i<n;i++)    {
            deg[i] = g.adjList.get(i).size();
            degOrig[i] = g.adjList.get(i).size();
        }

        //nodes having 1 deg is leaf
        while(true) {
            //find leaves
            List<Integer> leaves = new ArrayList<>();
            for(int i=0;i<n;i++)    {
                if (deg[i] == 1)
                    leaves.add(i);
            }
            for(int i : leaves) {
                deg[i] = 0;
                for (int j : g.adjList.get(i)) {
                    if (deg[j] > 1)
                        deg[j] -= 1;
                }
            }
            if (countMinusOne(deg))
                break;
        }

        System.out.println("Center is ");
        List<Integer> center = new ArrayList<>();
        for(int i=0;i<deg.length;i++)   {
            if(deg[i] > 0) {
                System.out.println(i);
                center.add(i);
            }
        }
        return center;
    }

    private int findCenterFromDeg(int[] degOrig, int[] deg)    {
        List<Integer> v = new ArrayList<>();
        for(int i=0;i<deg.length;i++)   {
            if(deg[i] > 0)
                v.add(i);
        }
        if(v.size() == 1)
            return v.get(0);
        if(degOrig[v.get(0)] > degOrig[v.get(1)])
            return v.get(0);
        else
            return v.get(1);
    }

    private boolean countMinusOne(int[] a)  {
        int j=0;
        for(int i=0;i<a.length;i++)
            if(a[i] > 0)    {
                j++;
            }
        if(j <= 2 )
            return true;
        return false;
    }

    class Graph1    {
        int verticesCount;
        Map<Integer, List<Integer>> adjList;
        public Graph1(int n,Map<Integer,List<Integer>> l)    {
            this.verticesCount = n;
            this.adjList = l;
        }
    }

    public Graph1 getGraph()   {
        int n = 10;
        Map<Integer,List<Integer>> map = new HashMap<>();
        Graph1 g = new Graph1(n,map);

        for(int i=0;i<n;i++)
            map.put(i,new ArrayList<>());
        map.get(0).add(1);
        map.get(1).add(0);map.get(1).add(3);map.get(1).add(4);
        map.get(2).add(3);
        map.get(3).add(1);map.get(3).add(2);
        map.get(3).add(6);map.get(3).add(7);
        map.get(4).add(1);map.get(4).add(5);map.get(4).add(8);
        map.get(5).add(4);
        map.get(6).add(3);map.get(6).add(9);
        map.get(7).add(3);
        map.get(8).add(4);
        map.get(9).add(6);
        return g;
    }
}
