package com.apal.graph.base;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CharacterGraph {
    int verticesCount;
    Map<Character, List<Character>> adjMap = new HashMap<>();

    public CharacterGraph(int n, Map<Character, List<Character>> adjMap)    {
        this.verticesCount = n;
        this.adjMap = adjMap;
    }
}
