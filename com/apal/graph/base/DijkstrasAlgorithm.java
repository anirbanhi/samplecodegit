package com.apal.graph.base;

/*
Dijkstras shortest path on a graph
Single Source Shortest Path
Starting node is needed.
time complexity O(e*log(v))

no negative edge is allowed. Only positive edge weight.
 */


import java.util.*;

public class DijkstrasAlgorithm {
    public static void main(String[] args) {
        new DijkstrasAlgorithm().driver();
    }

    public void driver()    {
        ShortestAndLongestPathInADAG shl = new ShortestAndLongestPathInADAG();
        CharacterWeightGraph cwg = shl.getGraph();
        Comparator<Touple> comp = new Comparator<Touple>() {
            @Override
            public int compare(Touple t1, Touple t2) {
                return t1.dist - t2.dist;
            }
        };
        PriorityQueue<Touple> pq = new PriorityQueue<>(comp);
        Map<Character, Integer> dist = new HashMap<>();
        Map<Character, Boolean> visited = new HashMap<>();
        for(Character c : cwg.edges.keySet()) {
            dist.put(c, null);
            visited.put(c,false);
        }

        dist.put('A',0);
        //visited.put('A',true);
        pq.offer(new Touple('A',0));

        while(!pq.isEmpty())    {
            Touple t = pq.poll();
            if(visited.get(t.node))
                continue;
            for(Edge e : cwg.edges.get(t.node)) {
                int d = dist.get(t.node);
                if(visited.get(e.dest))
                    continue;
                if(dist.get(e.dest) == null)
                    dist.put(e.dest,d+e.weight);
                else
                    dist.put(e.dest,Math.min(d+e.dest,dist.get(e.dest)));
                pq.offer(new Touple(e.dest,dist.get(e.dest)));
            }
            visited.put(t.node,true);
        }
        for(char ch:dist.keySet())
            System.out.println(ch+" - "+dist.get(ch));
    }

    class Touple    {
        Character node;
        int dist;
        public Touple(Character ch, int d)  {
            node = ch;
            dist = d;
        }
    }
}