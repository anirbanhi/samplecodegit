package com.apal.graph.base;

public class GraphMatrix implements GraphOperations{

    public int[][] matrix;
    public int verticesCount;

    public GraphMatrix(int[][] matrix){
        this.matrix = matrix;
    }

    public static GraphMatrix getSampleGraphMatrix()       {
        int [][] matrix = {
                {0,1,1,0,0,0},
                {0,0,0,1,0,0},
                {0,0,0,1,1,1},
                {0,0,0,0,0,0},
                {0,0,0,0,0,0},
                {1,0,0,0,0,0}
        };
        GraphMatrix gm = new GraphMatrix(matrix);
        return gm;
    }

    @Override
    public boolean adDirdEdge(String src, String dest) {
        throw new RuntimeException("No Such Method");
    }

    @Override
    public boolean adDirdEdge(int src, int dest) {
        if(src < verticesCount && dest < verticesCount)
        {    matrix[src][dest] = 1;
             return true;
        } else {
            return false;
        }
    }

    @Override
    public void printGraph() {
        for(int i=0;i<verticesCount;i++){
            System.out.println(i + " -> ");
            for(int j=0;j<verticesCount;j++){
                if(matrix[i][j] == 1)
                    System.out.print(j+" , ");
            }
        }

    }
}
