package com.apal.graph.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
        To counter -ve weights put a check while doing edge relaxation
        Consider a edge only if its +ve.
 */
public class ShortestAndLongestPathInADAG {

    public static void main(String[] args) {
        new ShortestAndLongestPathInADAG().driver();
    }

    public void driver()    {
        CharacterWeightGraph cwg = getGraph();
        int n = cwg.verticesCount;
        Map<Character,Integer> dist = new HashMap<>();
        //null means infinity
        for(char ch = 'A';ch<='H';ch++)
            dist.put(ch,null);

        //do topological sort: here we are assuming it
        //else can be solved as in TopologicalSort.java
        List<Character> topo = new ArrayList<>();
        for(char ch = 'A';ch<='H';ch++)
            topo.add(ch);

        dist.put('A',0);
        findShortestPath(cwg,topo,dist);
        //print result
        for(char ch : dist.keySet())
            System.out.println(ch+" - "+dist.get(ch));
    }

    //Shortest path from A
    //If any other vertex then start from that vertex.
    //Or construct the same array and then deduct the value from right of that starting value
    public void findShortestPath(CharacterWeightGraph cwg,
                                 List<Character> topo,
                                 Map<Character,Integer> dist)  {
        for(char ch : topo) {
            for (Edge e : cwg.edges.get(ch)) {
                if(e.weight > 0) {
                    char d = e.dest;
                    int newwt = dist.get(ch) + e.weight;
                    if (dist.get(d) == null)
                        dist.put(d, newwt);
                    else
                        dist.put(d, Math.min(newwt, dist.get(d)));
                }
            }
        }
    }

    public CharacterWeightGraph getGraph()  {
        Map<Character, List<Edge>> adjlist = new HashMap<>();
        for(char ch='A';ch<='H';ch++)
            adjlist.put(ch,new ArrayList<>());
        //create edges
        adjlist.get('A').add(new Edge('B',3));
        adjlist.get('A').add(new Edge('C',6));
        adjlist.get('B').add(new Edge('C',4));
        adjlist.get('B').add(new Edge('D',4));
        adjlist.get('B').add(new Edge('E',11));
        adjlist.get('C').add(new Edge('D',8));
        adjlist.get('C').add(new Edge('G',11));
        adjlist.get('D').add(new Edge('E',-40));
        adjlist.get('D').add(new Edge('G',2));
        adjlist.get('D').add(new Edge('F',5));
        adjlist.get('E').add(new Edge('H',9));
        adjlist.get('F').add(new Edge('H',1));
        adjlist.get('G').add(new Edge('H',2));
        CharacterWeightGraph cwg = new CharacterWeightGraph(adjlist.keySet().size(),adjlist);
        return cwg;
    }
}