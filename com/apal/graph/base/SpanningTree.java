package com.apal.graph.base;

import com.apal.oops.chessgame.Queen;

import java.util.*;

public class SpanningTree {
    public static void main(String[] args) {
        int[][] g = {
                { 0, 2, 0, 6, 0 },
                { 2, 0, 3, 8, 5 },
                { 0, 3, 0, 0, 7 },
                { 6, 8, 0, 0, 9 },
                { 0, 5, 7, 9, 0 } };
        findMinSpanningTree(g);
    }

    static class SpanningVertex {
        int vertex;
        int weight;

        public SpanningVertex(int v, int w) {
            vertex = v;
            weight = w;
        }
    }
    public static void findMinSpanningTree(int[][] g)    {
        Comparator<SpanningVertex> comp = new Comparator<SpanningVertex>() {
            @Override
            public int compare(SpanningVertex o1, SpanningVertex o2) {
                return o1.weight - o2.weight;
            }
        };
        Queue<SpanningVertex> pq = new PriorityQueue<>(comp);
        int n = g.length;
        int v = new Random().nextInt(n);
        int cost = 0;
        int j = 0;
        int[] visited = new int[n];
        Arrays.fill(visited, 0);
        pq.offer(new SpanningVertex(v,Integer.MAX_VALUE));
        System.out.println(" Min Spanning Tree is ");

        //for all vertices
        while(j < n) {
            v = pq.peek().vertex;
            int min = Integer.MAX_VALUE;
            int minIndex = 0;
            //for all neighbours
            for(int i=0;i<n;i++)
            {
                if(g[v][i] == 0 || i==v || visited[i] != 0)    continue;
                if(g[v][i] < min)
                {
                    min = g[v][i];
                    minIndex = i;
                }
            }
            System.out.print(v+" ");
            visited[v] = 1;
            cost += min;
            pq.offer(new SpanningVertex(minIndex, min));
            j++;
        }
        System.out.println();
        System.out.println("Total cost = "+cost);
    }
}