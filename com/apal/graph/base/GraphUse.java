package com.apal.graph.base;

import java.util.*;

public class GraphUse {
    public static void main(String[] args)  {
        GraphAdjList graph;
        graph = new GraphAdjList(5);
        graph.adDirdEdge("A", "B");
        graph.adDirdEdge("A", "C");
        graph.adDirdEdge("B", "D");
        graph.adDirdEdge("C", "D");
        graph.adDirdEdge("C", "F");
        graph.adDirdEdge("F", "A");

        //graph.printGraph();

        graph.bfs();

        String[][] arr = {
                {"A","B"},
                {"A","C"},
                {"B","D"},
                {"C","D"},
                {"C","E"}
        };

        toplogicalSort(arr);

    }

    Map<String, Integer> indeg = new HashMap<>();
    public static List<String> toplogicalSort(String[][] arr)   {
        Map<String, Integer> indegm = new HashMap<>();
        Map<String, List<String>> gm = new HashMap<>();
        int vcount;
        Set<String> vs = new HashSet<>();
        for(int i=0;i<arr.length;i++){
            vs.add(arr[i][0]);
            vs.add(arr[i][1]);
        }
        vcount = vs.size();

        for(String s : vs){
            List<String> list = new ArrayList<>();
            gm.put(s,list);
            indegm.put(s,0);
        }

        for(int i=0;i<arr.length;i++){
            String src = arr[i][0];
            String dest = arr[i][1];

            gm.get(src).add(dest);
            indegm.put(dest,indegm.get(dest)+1);
        }

        Queue<String> q = new LinkedList<String>();
        for(Map.Entry<String,Integer> entry : indegm.entrySet()){
            if(entry.getValue() == 0)
                q.add(entry.getKey());
        }
        return null;
    }
}
