package com.apal.graph.base;

import java.util.Arrays;

public class FindCycle {

    public static void main(String[] args) {
        int[][] g = {
                {0,1,1,1,0},
                {1,0,1,0,0},
                {1,1,0,0,0},
                {1,0,0,0,1},
                {0,0,0,1,0}
        };
        int edgeCount = 5;
        int vertexCount = 5;
        int[][] edges = new int[2][edgeCount];
        int[] parentSet = new int[vertexCount];
        int[][] sol = new int[2][vertexCount];

        //create and store edges in 2d array.
        //if this was a edge list, then this was not required.
        int edgeIndex = 0;
        for(int i=0;i<g.length;i++) {
            for(int j=i+1;j<g.length;j++) {
                if(g[i][j] == 1)    {
                    edges[0][edgeIndex] = i;
                    edges[1][edgeIndex] = j;
                    edgeIndex++;
                }
            }
        }
        //-1 means each vertex is his own parent
        //this is initialization step.
        for(int i=0;i<vertexCount;i++)
            parentSet[i] = -1;

        findCycleInAGraph(edges, parentSet);
    }

    public static void findCycleInAGraph(int[][] edges, int[] parentSet)  {
        for(int i=0;i<edges[0].length;i++) {
            int v1 = edges[0][i];
            int v2 = edges[1][i];
            int p1 = findParent(parentSet, v1);
            int p2 = findParent(parentSet, v2);
            //both are own parent,
            if(p1 != p2)    {
                if(Math.abs(parentSet[p1]) >
                        Math.abs(parentSet[p2]))    {
                    //p1 > p2
                    parentSet[p1] = parentSet[p1] + parentSet[p2];
                    parentSet[p2] = p1;
                }
                else    {
                    //p2 >= p1
                    parentSet[p2] = parentSet[p1] + parentSet[p2];
                    parentSet[p1] = p2;
                }
            } else if(parentSet[v1] == -1 &&
                    parentSet[v2] == -1)    {
                if(v1 > v2) {
                    parentSet[v1] = -2;
                    parentSet[v2] = v1;
                }   else    {
                    parentSet[v2] = -2;
                    parentSet[v1] = v2;
                }
            } else if(p1 == p2)    {
                System.out.println("Cycle exists ");
                return;
            }
        }
        System.out.println("No cycle exists + "+ Arrays.toString(parentSet));
    }

    public static int findParent(int[] parentSet, int v)    {
        int p = parentSet[v];
        while(p >= 0) {
            v = p;
            p = parentSet[v];
        }
        return v;
    }

}
