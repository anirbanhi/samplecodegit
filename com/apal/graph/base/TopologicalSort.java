package com.apal.graph.base;

import java.util.*;

/*
It should be acyclic
It should be directed, if not then Find center and then Root it.
 */

public class TopologicalSort {

    public static void main(String[] args) {
        new TopologicalSort().driver();
    }

    public void driver()    {
        CharacterGraph cg = getGraph();
        //create visited map
        Map<Character, Boolean> visited = new HashMap<>();
        for(char c : cg.adjMap.keySet())
            visited.put(c,false);
        //store o/p
        LinkedList<Character> oplist = new LinkedList<>();
        toplogicalSortDriver(cg,visited,oplist);
        for(char i : oplist)
            System.out.print(i+" -> ");
    }

    public void toplogicalSortDriver(CharacterGraph cg,
                               Map<Character, Boolean> v,
                               LinkedList<Character> oplist)    {
        for(Character ch : cg.adjMap.keySet())  {
            if(v.get(ch) == false)  {
                topoSort(cg,v,oplist,ch);
            }
        }
    }

    private void topoSort(CharacterGraph cg, Map<Character, Boolean> v,
                          LinkedList<Character> op, Character node)    {
        v.put(node,true);
        for(char adj : cg.adjMap.get(node)) {
            if(v.get(adj) == false)
                topoSort(cg,v,op,adj);
        }
        op.addFirst(node);
    }

    public CharacterGraph getGraph()  {
        Map<Character, List<Character>> adjmap = new HashMap<>();
        for(int ch='A';ch<='M';ch++)
            adjmap.put((char)ch ,new ArrayList<>());
        adjmap.get('A').add('D');
        adjmap.get('B').add('D');
        adjmap.get('C').add('A');adjmap.get('C').add('B');
        adjmap.get('D').add('G');adjmap.get('D').add('H');
        adjmap.get('E').add('A');adjmap.get('E').add('D');adjmap.get('E').add('F');
        adjmap.get('F').add('K');adjmap.get('F').add('J');
        adjmap.get('G').add('I');
        adjmap.get('H').add('I');adjmap.get('H').add('J');
        adjmap.get('I').add('L');
        adjmap.get('J').add('L');adjmap.get('J').add('M');
        adjmap.get('K').add('J');

        CharacterGraph cg = new CharacterGraph(13,adjmap);
        return cg;
    }

}