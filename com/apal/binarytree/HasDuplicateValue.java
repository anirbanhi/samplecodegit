package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-binary-tree-not-bst-duplicate-values/
 */
import java.util.HashSet;

public class HasDuplicateValue {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.right.right = new TreeNode(2);

        HasDuplicateValue hdp = new HasDuplicateValue();
        HashSet<Integer> set = new HashSet<>();
        System.out.println("Has Duplicate " + hdp.hasDuplicate(root,set));
    }

    private boolean hasDuplicate(TreeNode root, HashSet<Integer> set) {
        if(root == null)
            return false;
        if(set.contains(root.val))
            return true;
        else
            set.add(root.val);
        return hasDuplicate(root.left,set)
                || hasDuplicate(root.right,set);
    }
}
