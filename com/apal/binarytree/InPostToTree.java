package com.apal.binarytree;

/*
https://leetcode.com/explore/challenge/card/july-leetcoding-challenge/547/week-4-july-22nd-july-28th/3403/
 */
public class InPostToTree {
    public static void main(String[] args) {

        int[] in = {9,3,15,20,7};
        int[] post = {9,15,7,20,3};
        start(in, post);
    }

    public static TreeNode start(int[] in, int[] post)  {
        if(in.length != post.length)    return null;
        if(in.length == 0)  return null;

        postindex = post.length - 1;
        TreeNode t = constructTree(in, post, 0,in.length-1);
        return t;
    }
    public static void printInorder(TreeNode root)  {
        if(root != null) {
            printInorder(root.left);
            System.out.println(root.val + " ");
            printInorder(root.right);
        }
    }
    static int postindex;
    public static TreeNode constructTree(int[] in, int[] post,
                                     int inst, int inend)  {
        if(inst > inend)
            return null;
        if(postindex >= 0) {
            int inindx = findInPos(in, inst, inend, post[postindex--]);
            TreeNode root = new TreeNode(in[inindx]);
            TreeNode r = constructTree(in, post, inindx + 1, inend);
            TreeNode l = constructTree(in, post, inst, inindx - 1);
            root.left = l;
            root.right = r;
            return root;
        } else return null;
    }

    public static int findInPos(int[] in, int inst, int inend, int x)   {
        int i =0;
        for(i=inst;i<=inend;i++)    {
            if(in[i] == x)
                return i;
        }
        return i;
    }
}
