package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/write-a-c-program-to-delete-a-tree/
 */
public class DeleteBinaryTree {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);

        deleteTree(root);
    }

    public static void deleteTree(TreeNode root) {
        if(root == null)    return;
        deleteTree(root.left);
        deleteTree(root.right);
        System.out.println(root.val+" deleted ");
        root = null;
    }
}
