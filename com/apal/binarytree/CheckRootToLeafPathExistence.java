package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-root-leaf-path-given-sequence/
 */
import java.util.Arrays;

public class CheckRootToLeafPathExistence {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(9);

        CheckRootToLeafPathExistence chk = new CheckRootToLeafPathExistence();
        int[] seq = {1,2,5};
        System.out.println("Path "+ Arrays.toString(seq)+" exists ? "+chk.checkPath(root,seq,0));
    }

    private boolean checkPath(TreeNode root, int[] seq, int indx)   {
        if(root == null && indx > seq.length )
            return false;
        if(root == null && indx == seq.length )
            return true;

        if(root.val == seq[indx]){
            boolean l = checkPath(root.left,seq,indx+1);
            boolean r = checkPath(root.right,seq,indx+1);
            return l || r;
        }

//        boolean l = checkPath(root.left,seq,indx);
//        boolean r = checkPath(root.right,seq,indx);
//
//        return l || r;
        return false;
    }

}
