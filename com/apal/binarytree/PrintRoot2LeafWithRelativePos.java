package com.apal.binarytree;

import java.util.ArrayList;
import java.util.List;

/*
https://www.geeksforgeeks.org/print-all-root-to-leaf-paths-with-there-relative-positions/
 */
public class PrintRoot2LeafWithRelativePos {

    public void PrintAllRoot2LeafPaths() {

    }

    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(9);

        PrintRoot2LeafWithRelativePos prl = new PrintRoot2LeafWithRelativePos();
        List<op> path = new ArrayList<>();
        List<List<op>> all = new ArrayList<>();
        prl.printRelativePath(root,0,path,all);
        for(List<op> l : all){
            for(op o : l)
            {
                int cnt = o.width - prl.minWidth;
                while(cnt>0)
                {
                    System.out.print("-");
                    cnt--;
                }
                System.out.println(o.val);
            }
            System.out.println("**********************");
        }
    }

    private int minWidth = Integer.MAX_VALUE;
    private boolean printRelativePath(TreeNode root, int w, List<op> path, List<List<op>> all)    {
        if(root == null)
            return false;
        path.add(new op(root.val,w));
        minWidth = Math.min(minWidth, w);

        boolean l = printRelativePath(root.left,w-1,path,all);
        boolean r = printRelativePath(root.right,w+1,path,all);
        boolean res = false;
        if(root.left == null && root.right == null)
        {
            all.add(new ArrayList<>(path));
            res = true; //this is important, don't return true from here
        }

        //if you return from inside if loop, then below remove
        //code will not be executed and path will contain
        //all values. so pls take care.
        path.remove(path.size()-1);
        return res || l || r;
    }
    class op    {
        int val;
        int width;
        public op(int v, int w){
            this.val = v;
            this.width = w;
        }

        @Override
        public String toString() {
            return "op{" +
                    "val=" + val +
                    ", width=" + width +
                    '}';
        }
    }
}
