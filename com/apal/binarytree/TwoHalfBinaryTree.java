package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-if-removing-an-edge-can-divide-a-binary-tree-in-two-halves/
 */
public class TwoHalfBinaryTree {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(1);
        root.right = new TreeNode(6);
        root.left.left = new TreeNode(3);
        root.right.left = new TreeNode(7);
        root.right.right = new TreeNode(4);
        root.right.right.left = new TreeNode(1);

        TwoHalfBinaryTree tbt = new TwoHalfBinaryTree();
        int cnt = tbt.countNodes(root);
        System.out.println("Total nodes " + cnt);
        System.out.println("IsEqualHAlf " + tbt.isEqualHalf(root,cnt));

    }
    private int countNodes(TreeNode root)   {
        if(root == null)
            return 0;
        return countNodes(root.left) + countNodes(root.right) + 1;
    }

    private boolean isEqualHalf(TreeNode root, int total)  {
        if(root!=null) {
            int l = countNodes(root.left);
            int r = countNodes(root.right);
            int nb = l + r + 1;
            if (nb + nb  == total)
                return true;
            else {
                return isEqualHalf(root.left, total) ||
                        isEqualHalf(root.right, total);
            }
        }
        return false;
    }
}
