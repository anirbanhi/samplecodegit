package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/binary-tree-data-structure/binary-tree-checking-printing/
 */
public class HeightBalancedBT {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(40);
        root.left = new TreeNode(10);
        root.right = new TreeNode(100);
        root.right.left = new TreeNode(60);
        root.right.right = new TreeNode(150);
        root.right.right.right = new TreeNode(200);

        HeightBalancedBT hbt = new HeightBalancedBT();
        hbt.findHeight(root);
        System.out.println("Balanced or not " + hbt.isbalanced);
    }
    private boolean isbalanced = true;
    private int findHeight(TreeNode root) {
        if(root == null)
            return 0;

        int l = findHeight(root.left);
        int r = findHeight(root.right);

        if(Math.abs(l-r) > 1)
            isbalanced = false;
        return Math.max(l,r)+1;
    }
}
