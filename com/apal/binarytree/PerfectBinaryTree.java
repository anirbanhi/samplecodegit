package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-weather-given-binary-tree-perfect-not/
 */
public class PerfectBinaryTree {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(10);
        root.left = new TreeNode(20);
        root.right = new TreeNode(30);
        root.left.left = new TreeNode(40);
        root.left.right = new TreeNode(50);
        root.right.left = new TreeNode(60);
        root.right.right = new TreeNode(70);
        root.right.right.left = new TreeNode(80);

        PerfectBinaryTree pbt = new PerfectBinaryTree();
        System.out.println("Perfect binary ? " + pbt.isPerfect(root));
    }
    private boolean isPerfect(TreeNode root) {
        if(root == null)
            return true;
        boolean l = isPerfect(root.left);
        boolean r = isPerfect(root.right);

        boolean res = false;

        if((root.left != null && root.right != null)
            || root.left == null & root.right == null  )
            res = true;

        return res && l && r;
    }
}
