package com.apal.binarytree;

import java.util.HashMap;
import java.util.Map;

/*
https://www.geeksforgeeks.org/root-leaf-paths-equal-lengths-binary-tree/
 */
public class FindAllPathLengths {
    public static void main(String[] args) {
        new FindAllPathLengths().driver();
    }
    public void driver()    {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(9);
        Map<Integer,Integer> map = new HashMap<>();
        findPathLength(root,1,map);
        for(Integer key : map.keySet())
            System.out.println(key+" - "+map.get(key));
    }
    public void findPathLength(TreeNode root, int path,
                               Map<Integer, Integer> map)   {
        if(root == null)
            return;
        if(root.left == null && root.right == null)
        {
            map.put(path,map.getOrDefault(path,0)+1);
            return;
        }
        findPathLength(root.left,path+1,map);
        findPathLength(root.right,path+1,map);
    }
}
