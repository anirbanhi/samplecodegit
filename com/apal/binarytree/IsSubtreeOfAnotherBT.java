package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-binary-tree-subtree-another-binary-tree-set-2/
 */
class Node{
    char val;
    Node left;
    Node right;

    public Node(char ch)    {
        this.val = ch;
        this.left = null;
        this.right = null;
    }
}

public class IsSubtreeOfAnotherBT {

    public static void main(String[] args)  {
        //pr = parentRoot
        //cr = childRoot
        Node pr = new Node('z');
        pr.left = new Node('x');
        pr.right = new Node('e');
        pr.left.left = new Node('a');
        pr.left.left.right = new Node('c');
        pr.left.right = new Node('b');
        pr.right.right = new Node('k');

        Node cr = new Node('x');
        cr.left = new Node('a');
        cr.right = new Node('b');
        cr.left.right = new Node('c');
        //cr.right.left = new Node('h');

        IsSubtreeOfAnotherBT isbt = new IsSubtreeOfAnotherBT();
        System.out.println("Is sub tree "+isbt.findElem(pr,cr));
    }

    private boolean findElem(Node p, Node c)  {
        if(p==null)
            return false;
        boolean res = false;
        if(p.val == c.val)
            res = isSubTree(p,c);
        return res || findElem(p.left,c) ||  findElem(p.right,c);
    }

    private boolean isSubTree(Node p, Node c) {
        if(p==null && c==null)
            return  true;
        if( (p==null && c!=null) ||
                (p!=null && c==null) )
            return false;
        boolean res = false;
        if(p.val == c.val)
            res = true;
        return res && isSubTree(p.left,c.left)
                && isSubTree(p.right,c.right);
    }
}
