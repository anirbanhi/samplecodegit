package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/bottom-view-binary-tree/
 */
import java.util.HashMap;
import java.util.Map;

public class BottomView {
    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.right = new TreeNode(7);

        TreeNode root1 = new TreeNode(20);
        root1.left = new TreeNode(8);
        root1.right = new TreeNode(22);
        root1.left.left = new TreeNode(5);
        root1.left.right = new TreeNode(3);
        root1.right.right = new TreeNode(25);
        root1.left.right.left = new TreeNode(10);
        root1.left.right.right = new TreeNode(14);

        BottomView bv = new BottomView();
        bv.printTree(root1);
        System.out.println();

        Map<Integer, Integer> map = new HashMap<>();
        Map<Integer, Integer> map1 = new HashMap<>();
        bv.findBottomView(root1, map, 0, map1,0);
        System.out.println("bottom view is "+bv.min+" "+bv.max);
        for(int i=bv.min;i<=bv.max;i++) {
            System.out.print(map.get(i)+" ");
        }
    }

    public void printTree(TreeNode tn) {
        if(tn == null)
            return;
        printTree(tn.left);
        System.out.print(tn.val+" , ");
        printTree(tn.right);
    }

    public void findBottomView(TreeNode root,
                               Map<Integer, Integer> map,
                               int pos,Map<Integer, Integer> map1, int h)    {
        if(root == null)
            return;
        if(map.get(pos)!=null)  {
            if(h > map1.get(pos))
            {
                map.put(pos, root.val);
                map1.put(pos, h);
            }
        }   else    {
            map.put(pos, root.val);
            map1.put(pos, h);
        }

        if(root.left != null) {
            min = Math.min(min, pos-1);
            findBottomView(root.left, map, pos - 1,map1,h+1);
        }
        if(root.right != null) {
            max = Math.max(max, pos+1);
            findBottomView(root.right, map, pos + 1,map1,h+1);
        }
    }
}