package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/print-longest-leaf-leaf-path-binary-tree/
 */
/*
    //Find longest path and the node
    //Start from that node and find left highest to node.left
    //Start from that node and find right highest to node.right
 */

import java.util.ArrayList;
import java.util.List;

public class PrintLongestLeaf2LEafPath {
    public static void main(String[] args)  {
        new PrintLongestLeaf2LEafPath().driver();
    }
    public void driver()    {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right =new  TreeNode(3);
        root.left.left =new  TreeNode(4);
        root.left.right =new  TreeNode(5);
        root.left.right.left =new  TreeNode(6);
        root.left.right.right =new  TreeNode(7);
        root.left.left.right =new  TreeNode(8);
        root.left.left.right.left =new  TreeNode(9);

        List<Integer> l = new ArrayList<>();
        //l.add(r.val);
        dfs(root,l);
        System.out.println(" [[9 8 4 2 5 6]] "+maxl);
    }

    int max = 0;
    List<Integer> maxl = new ArrayList<>();
    public int dfs(TreeNode r, List<Integer> p) {

        if(r==null) return 0;
        if(r.left==null&&r.right==null)
            return 1;
        p.add(r.val);
        int l1 = dfs(r.left,p);
        int r1 = dfs(r.right,p);
        int len = l1 + r1 + 1;

        if(len > max)   {
            max = len;
            maxl = new ArrayList<>(p);
        }

        p.remove(p.size()-1);
        return Math.max(l1,r1)+1;
    }
}
