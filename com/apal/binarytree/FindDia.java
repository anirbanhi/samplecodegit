package com.apal.binarytree;
/*
Find maximum diameter.
 */
public class FindDia {
    public static void main(String[] args) {
        new FindDia().driver();
    }

    void driver()   {
        TreeNode r = new TreeNode(1);
        r.left = new TreeNode(2);
        r.right = new TreeNode(3);
        r.left.left = new TreeNode(4);
        r.right.left = new TreeNode(5);
        r.right.right = new TreeNode(6);
        dfs(r);
        System.out.println("Max dia "+max);
    }
    void dfs(TreeNode r)    {
        if(r==null) return;
        findDia(r);
        dfs(r.left);
        dfs(r.right);
    }
    int max = 0;
    void findDia(TreeNode r) {
        if(r==null) return;
        int dia = findHeight(r.left)+findHeight(r.right) +1;
        if(dia > max)
            max = dia;
    }
    int findHeight(TreeNode r) {
        if(r==null) return 0;
        int l1 = findHeight(r.left);
        int r1 = findHeight(r.right);
        return 1 + Math.max(l1,r1);
    }
}