package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-if-a-given-binary-tree-is-sumtree/
 */
public class IsSumTree {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(26);
        root.left = new TreeNode(10);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(6);
        root.right.right = new TreeNode(3);

        IsSumTree ist = new IsSumTree();
        System.out.println("IS Sum Tree "+ist.findSum(root)+"  " + ist.isequal);
    }
    private boolean isequal = true;
    private int findSum(TreeNode root)  {
        if(root == null)
            return 0;
        int l = findSum(root.left);
        int r = findSum(root.right);
        int sum = l + r + root.val;
        if(root.val == (l+r) )
        {
            if(isequal)
                isequal = true;
        }
        //Below if check is important since for leaf nodes.
        //root.val = l + r fails. As l and r comes as 0 for null
        //nodes.
        else if(root.left == null && root.right == null) {
            if (isequal)
                isequal = true;
        } else
            isequal = false;

        return sum;
    }
}
