package com.apal.binarytree;
/*
https://practice.geeksforgeeks.org/problems/check-for-bst/1
 */
public class CheckBST {
    public static void main(String[] args) {
        Node root = new Node('D');
        System.out.println(checkBST(root));
    }

    public static boolean checkBST(Node root)    {
        return check(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    public static boolean check(Node root, int min, int max)    {
        if(root == null)  return true;
        boolean left = true;
        boolean right = true;
        if(root.left != null &&
                (root.val < root.left.val ||
                root.val < min))
            left = false;
        if(root.right != null &&
                (root.val > root.right.val ||
                        root.val > max))
            right = false;

        return left && right && check(root.left,min,root.val)
                && check(root.right,root.val,max);
    }
}