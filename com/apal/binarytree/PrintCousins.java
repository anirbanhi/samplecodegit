package com.apal.binarytree;

/*
https://www.geeksforgeeks.org/print-cousins-of-a-given-node-in-binary-tree/
 */
public class PrintCousins {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(9);

        PrintCousins pc = new PrintCousins();
        int x = 9;
        pc.findSiblings(root,x,0);
        System.out.println("Find siblings of " + x + " is " + pc.siblings);

        //After siblings finding and level finding ,
        // do a level order search and when level matched with elements
        //print them which are not siblings.
        //for duplicity level can be hashset as well.
        //not doing the level order search, earlier programs can be seen
    }

    private Integer siblings ;
    private Integer keyLevel;
    private boolean findSiblings(TreeNode root, int x, int level)  {
        if(root == null)
            return false;
        if(root.val == x)
        {
            keyLevel = level;
            return true;
        }
        boolean l = findSiblings(root.left,x,level+1);
        boolean r = findSiblings(root.right,x,level+1);

        if(l || r)
        {
            if(root.left!=null && root.left.val == x)
                siblings = root.right == null ? null : root.right.val;
            else if(root.right!=null && root.right.val == x)
                siblings = root.left == null ? null : root.left.val;
            return false;
        }
        return l || r;
    }
}
