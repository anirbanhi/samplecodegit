package com.apal.binarytree;

class MaximumPathSum {

    public static int findMaximumPathSum(TreeNode root) {
        // TODO: Write your code here
        //return -1;
        maxsum = Integer.MIN_VALUE;
        return findSum(root);
    }

    private static int maxsum ;
    private static int findSum(TreeNode root)   {
        if(root == null)
            return 0;
        int l = findSum(root.left);
        int r = findSum(root.right);
        int sum = root.val;
        if(l>0)
            sum = sum + l;
        if(r>0)
            sum = sum + r;
        maxsum = Math.max(sum,maxsum);
        return sum;
    }
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        System.out.println("Maximum Path Sum: " + MaximumPathSum.findMaximumPathSum(root));

        root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.right.left = new TreeNode(5);
        root.right.right = new TreeNode(6);
        root.right.left.left = new TreeNode(7);
        root.right.left.right = new TreeNode(8);
        root.right.right.left = new TreeNode(9);
        System.out.println("Maximum Path Sum: " + MaximumPathSum.findMaximumPathSum(root));

        root = new TreeNode(-1);
        root.left = new TreeNode(-3);
        System.out.println("Maximum Path Sum: " + MaximumPathSum.findMaximumPathSum(root));
    }
}
