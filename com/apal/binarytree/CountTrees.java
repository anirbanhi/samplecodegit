package com.apal.binarytree;

/*
https://www.geeksforgeeks.org/succinct-encoding-of-binary-tree/

Count number of different binary trees
 */

public class CountTrees {
    public static void main(String[] args) {
        new CountTrees().driver();
    }
    public void driver() {
        System.out.println(" 1 " + findCount(1));
        System.out.println(" 4 " + findCount(4));
        System.out.println(" 5 " + findCount(5));
        System.out.println(" 6 " + findCount(6));
    }
    public int findCount(int n) {
        if(n <= 1)
            return 1;
        int sum = 0;
        int left, right;
        for(int i=1;i<=n;i++)
        {
            left = findCount(i-1);
            right = findCount(n-i);

            sum += left * right;
        }
        return sum;
    }
}
