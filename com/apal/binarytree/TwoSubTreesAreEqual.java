package com.apal.binarytree;


import java.util.Stack;

/*
https://www.geeksforgeeks.org/iterative-function-check-two-trees-identical/
 */
public class TwoSubTreesAreEqual {
    public static void main(String[] args) {
        new TwoSubTreesAreEqual().driver();
    }
    public void driver()    {
        TreeNode r1 = new TreeNode(1);
        r1.left = new TreeNode(2);
        r1.right = new TreeNode(3);

        TreeNode r2 = new TreeNode(1);
        r2.left = new TreeNode(2);
        r2.right = new TreeNode(3);

        System.out.println("Isidentical "+isIdentitical(r1,r2));
    }
    public boolean isIdentitical(TreeNode r1, TreeNode r2)  {
        Stack<TreeNode> st1 = new java.util.Stack<>();
        st1.push(r1);
        Stack<TreeNode> st2 = new java.util.Stack<>();
        st2.push(r2);
        while (!st1.isEmpty() && !st2.isEmpty())    {
            TreeNode t1 = st1.pop();
            TreeNode t2 = st2.pop();
            if(t1.val != t2.val)
                return false;
            if( (t1.left!=null && t2.left==null) ||
                    (t1.left==null && t2.left!=null) )
                return false;
            if( (t1.right!=null && t2.right==null) ||
                    (t1.right==null && t2.right!=null) )
                return false;
            if(t1.left!=null && t2.left!=null) {
                st1.push(t1.left);
                st2.push(t2.left);
            }
            if(t1.right!=null && t2.right!=null) {
                st1.push(t1.right);
                st2.push(t2.right);
            }
        }
        return true;
    }
}
