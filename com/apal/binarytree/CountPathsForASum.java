package com.apal.binarytree;

import java.util.ArrayList;
import java.util.List;
/*
Given a binary tree and a number ‘S’,
find all paths in the tree such that the sum of all the node values
 of each path equals ‘S’.
 Please note that the paths can start or end at any node but all
 paths must follow direction from parent to child (top to bottom).


 There are three paths with sum '12':7 -> 5, 1 -> 9 -> 2, and 9 -> 3
            1
      7             9
   6      5      2     3

 */
public class CountPathsForASum {
    public static void main(String[] args) {
        new CountPathsForASum().driver();
    }

    public void driver()    {
        TreeNode r = new TreeNode(1);
        r.left = new TreeNode(7);
        r.right = new TreeNode(9);
        r.left.left = new TreeNode(6);
        r.left.right = new TreeNode(5);
        r.right.left = new TreeNode(2);
        r.right.right = new TreeNode(3);

        findPathsDFS(r,12);
        System.out.println("Count = "+count);
    }

    void findPathsDFS(TreeNode r, int target)  {
        if(r==null) return;
        findSum(r,target,0,new ArrayList<>());
        findPathsDFS(r.left,target);
        findPathsDFS(r.right,target);
    }

    int count = 0;
    void findSum(TreeNode r, int t, int s, List<Integer> l)  {
        if(r==null) return;
        s = s + r.val;
        l.add(r.val);
        if(s == t) {
            count++;
            System.out.println(l);
            return;
        } else {
            findSum(r.left, t, s, l);
            findSum(r.right, t, s, l);
        }
        l.remove(l.size()-1);
    }
}
