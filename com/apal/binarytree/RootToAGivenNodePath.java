package com.apal.binarytree;

import java.util.*;

/*
https://www.geeksforgeeks.org/binary-tree-data-structure/binary-tree-checking-printing/
 */
public class RootToAGivenNodePath {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(9);

        RootToAGivenNodePath rag = new RootToAGivenNodePath();
        int x = 9;
        List<Integer> path = new ArrayList<>();
        rag.findNodePath(root,x,path);
        Collections.reverse(path);
        System.out.println("Path to "+x+" from root is "+
                Arrays.toString(path.toArray()));
    }



    private boolean findNodePath(TreeNode root, int x,List<Integer> path)  {
        if(root == null)
            return false;
        boolean l = findNodePath(root.left,x,path);
        boolean r = findNodePath(root.right,x,path);
        if(root.val == x)
        {
            path.add(x);
            return true;
        }
        if(l || r)
            path.add(root.val);
        return l || r;
    }
}
