package com.apal.binarytree;

import java.util.LinkedList;
import java.util.Queue;

public class CoveredUncoveredSum {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(9);
        root.left = new TreeNode(4);
        root.right = new TreeNode(7);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(10);
        root.right.left = new TreeNode(12);
        root.right.right = new TreeNode(2);

        CoveredUncoveredSum cus = new CoveredUncoveredSum();
        System.out.println(" isequal " + cus.findSum(root));
    }

    private boolean findSum(TreeNode root)  {
        Queue<TreeNode> q = new LinkedList<>();
        q.offer(root);
        int csum = 0;
        int usum = 0;
        while(!q.isEmpty()) {
            int size = q.size();
            int cnt = 1;
            while(cnt <= size)  {
                TreeNode temp = q.poll();
                if(cnt == 1 || cnt == size)
                    csum = csum + temp.val;
                else
                    usum = usum + temp.val;
                if(temp.left != null)
                    q.offer(temp.left);
                if(temp.right != null)
                    q.offer(temp.right);
                cnt++;
            }
        }
        return csum==usum;
    }
}
