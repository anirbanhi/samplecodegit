package com.apal.binarytree;

/*
https://www.geeksforgeeks.org/given-a-binary-tree-print-out-all-of-its-root-to-leaf-paths-one-per-line/
 */
import java.util.ArrayList;
import java.util.List;

public class PrintAllRoot2LeafPaths {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(9);

        List<Integer> path = new ArrayList<>();
        List<List<Integer>> allpath = new ArrayList<>();

        PrintAllRoot2LeafPaths pap = new PrintAllRoot2LeafPaths();
        pap.printPath(root,path,allpath);

        for(List<Integer> l : allpath)
        {
            for(Integer i : l)
                System.out.print(i+" , ");
            System.out.println();
        }
    }
    private void printPath(TreeNode root,List<Integer> path,
                           List<List<Integer>> allpath)    {
        if(root == null)
            return;
        path.add(root.val);
        if(root.left == null && root.right == null)
            allpath.add(new ArrayList<>(path));

        printPath(root.left,path,allpath);
        printPath(root.right,path,allpath);

        path.remove(path.size()-1);
    }
}
