package com.apal.binarytree;

import com.apal.arrays.searching.FindPeak;

/*
https://www.geeksforgeeks.org/print-nodes-at-k-distance-from-root/
 */
public class FindNodeWithKDistFromRoot {
    public static void main(String[] args) {
        new FindNodeWithKDistFromRoot().driver();
    }
    public void driver()    {
        TreeNode r = new TreeNode(1);
        r.left = new TreeNode(2);
        r.right = new TreeNode(3);
        r.left.left = new TreeNode(4);
        r.left.right = new TreeNode(5);
        r.right.left = new TreeNode(6);
        r.right.right = new TreeNode(7);
        r.right.left.right = new TreeNode(8);
        findKDist(r,3);
    }

    public void findKDist(TreeNode r, int k)    {
        if(r==null) return;

        if(k==0)    {
            System.out.println(r.val);
            return;
        }

        findKDist(r.left,k-1);
        findKDist(r.right,k-1);
    }
}
