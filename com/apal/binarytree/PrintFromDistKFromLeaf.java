package com.apal.binarytree;

import java.util.ArrayList;
import java.util.List;

/*
https://www.geeksforgeeks.org/print-nodes-distance-k-leaf-node/
 */
public class PrintFromDistKFromLeaf {

    public static void main(String[] args) {
        new PrintFromDistKFromLeaf().driver();
    }
    public void driver()    {
        TreeNode r = new TreeNode(1);
        r.left = new TreeNode(2);
        r.right = new TreeNode(3);
        r.left.left = new TreeNode(4);
        r.left.right = new TreeNode(5);
        r.right.left = new TreeNode(6);
        r.right.right = new TreeNode(7);
        r.right.left.right = new TreeNode(8);
        dfs(r,1);
       // dfs(r,2);
        int[] v = new int[8];
        findLeafsInSingleShot(r,1,1,v,new ArrayList<>(),0);
    }

    public void findLeafsInSingleShot(TreeNode root, int k,
                                      final int target,
                                      int[] v, List<Integer> l, int indx) {

        if(root==null)  return;
        l.add(root.val);
        if(root.left==null && root.right==null)
        {
            int p = l.size()-target;
            if(p>-1 && v[p] == 0) {
                System.out.println(p);
                v[p] = 1;
                return;
            }
        }
        findLeafsInSingleShot(root.left,k-1,target,v,l,indx+1);
        findLeafsInSingleShot(root.right,k-1,target,v,l,indx+1);
        l.remove(l.size()-1);
    }

    public void dfs(TreeNode root, int k)   {
        if(root==null)  return;
        if(findLeaf(root,k))
            System.out.println(root.val);
        dfs(root.left,k);
        dfs(root.right,k);
    }

    public boolean findLeaf(TreeNode r, int k) {
        if(r == null)   return false;
        if(k==0 && r.left==null & r.right==null)
            return true;
        if(k < 0)   return false;
        return findLeaf(r.left,k-1) ||
                findLeaf(r.right,k-1);
    }
}