package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/check-for-children-sum-property-in-a-binary-tree/

 */
public class CheckSumProperty {
    public static void main(String[] args)  {
        TreeNode root = new TreeNode(10);
        root.left = new TreeNode(8);
        root.right = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(2);

        CheckSumProperty cs = new CheckSumProperty();
        System.out.println("Is sum " + cs.checkSum(root));
    }

    private boolean checkSum(TreeNode root) {
        if(root == null)
            return true;
        if(root.left == null && root.right == null)
            return true;

        int sum = root.val;
        if(root.left != null)
            sum = sum - root.left.val;
        if(root.right != null)
            sum = sum - root.right.val;

        if(sum == 0)
            return checkSum(root.left) && checkSum(root.right);
        else return false;
    }
}
