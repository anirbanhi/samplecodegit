package com.apal.binarytree;
/*
https://www.geeksforgeeks.org/find-count-of-singly-subtrees/
 */
public class NoOfUniqueSubtrees {

    public static void main(String[] args) {
        new NoOfUniqueSubtrees().driver();
    }

    public void driver()    {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(1);
        root.right = new TreeNode(5);
        root.left.left = new TreeNode(5);
        root.left.right = new TreeNode(5);
        root.right.right = new TreeNode(5);

        findNoOfSubtrees(root);
        System.out.println("No of subtree "+count);
    }

    int count = 0;
    public int findNoOfSubtrees(TreeNode root)   {
        Integer l = null;
        Integer r = null;

        if(root.left != null)
            l = findNoOfSubtrees(root.left);
        if(root.right != null)
            r = findNoOfSubtrees(root.right);

        if( (l != null && l == root.val) &&
            (r != null && r == root.val) )
        {
            count++;
            return root.val;
        }
        if( l != null && l == root.val )    {
            count++;
            return root.val;
        }
        if( r != null && r == root.val )    {
            count++;
            return root.val;
        }
        if(l == null && r == null)  {
            count++;
            return root.val;
        }

        return root.val;
    }

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    };


}