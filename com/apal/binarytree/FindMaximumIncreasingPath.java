package com.apal.binarytree;

/*
https://www.geeksforgeeks.org/maximum-consecutive-increasing-path-length-in-binary-tree/
 */

public class FindMaximumIncreasingPath {
    public static void main(String[] args) {
        new FindMaximumIncreasingPath().driver();
    }
    public void driver()    {
        TreeNode r = new TreeNode(5);
        r.left = new TreeNode(8);
        r.right = new TreeNode(11);
        r.left.left = new TreeNode(9);
        r.left.left.left = new TreeNode(6);
        r.right.right = new TreeNode(10);
        r.right.right.left = new TreeNode(15);

        findMaxPathLength(r,0);
        System.out.println("Max = "+max);
    }
    int max = Integer.MIN_VALUE;
    public void findMaxPathLength(TreeNode root, int cnt)    {
        if(cnt > max)
            max = cnt;
        if(root==null)
            return;
        if(root.left != null) {
            if (root.left.val > root.val) {
                findMaxPathLength(root.left, cnt + 1);
            }
            else
                findMaxPathLength(root.left, 0);
        }
        if(root.right != null) {
            if(root.right.val > root.val) {
                findMaxPathLength(root.right, cnt + 1);
            }
            else
                findMaxPathLength(root.right, 0);
        }
    }
}