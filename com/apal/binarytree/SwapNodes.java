package com.apal.binarytree;

/*
https://www.geeksforgeeks.org/swap-nodes-binary-tree-every-kth-level/
 */

public class SwapNodes {

    public static void main(String[] args) {
        new SwapNodes().driver();
    }

    public void driver()    {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.right.left = new TreeNode(7);
        root.right.right = new TreeNode(8);

        swapNodes(root, 1,1);
        System.out.println();
    }

    public TreeNode swapNodes(TreeNode root, int h, int target) {
        if(root == null)
            return null;

        if( h == target ) {
            TreeNode l = swapNodes(root.left, 1, target);
            TreeNode r = swapNodes(root.right, 1, target);

            root.left = r;
            root.right = l;

        } else {
            TreeNode l = swapNodes(root.left, h+1, target);
            TreeNode r = swapNodes(root.right, h+1, target);
        }
        return root;
    }

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    };
}