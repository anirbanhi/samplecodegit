package com.apal.backtracking;

import java.util.*;

/*
https://leetcode.com/problems/letter-combinations-of-a-phone-number/
 */
public class LetterCombinations {
    public static void main(String[] args) {
        new LetterCombinations().driver();
    }

    public void driver()    {
        letterCombinations("");
    }

    public List<String> letterCombinations(String digits) {
        if(digits==null)
            return new ArrayList<>();
        if(digits.length()==0)
            return new ArrayList<>();
        List<String> res = new ArrayList<>();
        for(int i=0;i<digits.length();i++)  {
            Character ch = digits.charAt(i);
            findKeyCombinations(res,ch);
        }

        int size = digits.length();
        List<String> finalRes = new ArrayList<>();
        for(String s : res) {
            if(s.length()==size) {
                System.out.println(s);
                finalRes.add(s);
            }
        }
        return finalRes;
    }

    public void findKeyCombinations(List<String> list, Character digit)   {
        Integer key = Integer.parseInt(digit.toString());
        Map<Integer,List<String>> keypad = new HashMap<>();
        keypad.put(2, Arrays.asList("a","b","c"));
        keypad.put(3, Arrays.asList("d","e","f"));
        keypad.put(4, Arrays.asList("g","h","i"));
        keypad.put(5, Arrays.asList("j","k","l"));
        keypad.put(6, Arrays.asList("m","n","o"));
        keypad.put(7, Arrays.asList("p","q","r","s"));
        keypad.put(8, Arrays.asList("t","u","v"));
        keypad.put(9, Arrays.asList("w","x","y","z"));

        List<String> chars = keypad.get(key);
        int size = list.size();
        for(int i=0;i<size;i++) {
            String chExist = list.get(i);
            for(String ch:chars)    {
                String newCh = chExist + ch;
                list.add(newCh);
            }
        }
        if(list.size()==0)  {
            for(String ch:chars)    {
                list.add(ch);
            }
        }

    }
}
