package com.apal.backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
https://leetcode.com/problems/permutations/
 */

public class Permutations {

    public static void main(String[] args) {
        new Permutations().driver();
    }

    public void driver(){
        permute(new int[]{1,2,3});
    }

    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> a = new ArrayList<>();
        for(int i=0;i<nums.length;i++)
            a.add(nums[i]);
        permBT(a,res,0,nums.length);
        return res;
    }

    public void permBT(List<Integer> a, List<List<Integer>> res, int indx, int n)    {

        if(indx==n){
            res.add(new ArrayList<>(a));
            System.out.println(Arrays.toString(a.toArray()));
            //return;
        }
        for(int i=indx;i<n;i++){
            swap(a,indx,i);
            permBT(a,res,indx+1,n);
            swap(a,indx,i);
        }
    }

    public void swap(List<Integer> a, int i, int j) {
        int temp = a.get(i);
        a.set(i,a.get(j));
        a.set(j,temp);
    }

}