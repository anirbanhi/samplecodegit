package com.apal.backtracking;
/*
https://www.geeksforgeeks.org/m-coloring-problem-backtracking-5/
 */
import java.util.Arrays;

public class GraphColoring {
    public static void main(String[] args) {
        int[][] graph =
                {
                        {0, 1, 1, 1},
                        {1, 0, 1, 0},
                        {1, 1, 0, 1},
                        {1, 0, 1, 0}
                };
        int[][] graph1 =
                {
                    {1, 1, 1, 1},
                    {1, 1, 1, 1},
                    {1, 1, 1, 1},
                    {1, 1, 1, 1}
                };
        int[] sol = new int[graph.length];
        for(int i=0;i<sol.length;i++)
            sol[i] = -1;
        GraphColoring gc = new GraphColoring();
        boolean res = gc.findGraphcoloring(graph, sol,0,3);
        System.out.println("Coloring exists "+res);
        System.out.println(Arrays.toString(sol));
    }

    public boolean findGraphcoloring(int[][] graph, int[] sol,
                                  int v, int m) {
        for(int j=1;j<=m;j++)    {
            if(isSafe(graph,sol,v,j)) {
                sol[v] = j;
                boolean flag = true;
                for(int k=0;k<sol.length;k++) {
                    if (sol[k] == -1)
                        flag = false;
                }
                if(flag)    return true;
                for(int i=0;i<graph[v].length;i++) {
                    if(graph[v][i] == 1 && sol[i]==-1)
                    {
                        boolean res =
                                findGraphcoloring(graph,sol,i,m);
                        if(res)
                            return true;
                    }
                }
            }
            else continue;
        }
        sol[v] = -1;
        return false;
    }

    public boolean isSafe(int[][] graph, int[] sol, int v, int c) {

        for(int i=0;i<graph[v].length;i++) {
            if(graph[v][i] == 1 && sol[i] == c)
                return false;
        }
        return true;
    }
}
