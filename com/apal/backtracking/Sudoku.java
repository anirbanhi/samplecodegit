package com.apal.backtracking;
/*
https://www.geeksforgeeks.org/sudoku-backtracking-7/
 */

public class Sudoku {
    public static void main(String[] args) {

        int[][] sudoku =
                {   {3, 0, 6, 5, 0, 8, 4, 0, 0},
                    {5, 2, 0, 0, 0, 0, 0, 0, 0},
                    {0, 8, 7, 0, 0, 0, 0, 3, 1},
                    {0, 0, 3, 0, 1, 0, 0, 8, 0},
                    {9, 0, 0, 8, 6, 3, 0, 0, 5},
                    {0, 5, 0, 0, 9, 0, 6, 0, 0},
                    {1, 3, 0, 0, 0, 0, 2, 5, 0},
                    {0, 0, 0, 0, 0, 0, 0, 7, 4},
                    {0, 0, 5, 2, 0, 6, 3, 0, 0}
                };
        Sudoku sd = new Sudoku();
        sd.print2dArray(sudoku);
        boolean res = sd.findSol(sudoku);
        System.out.println("Solution exist "+res);
        sd.print2dArray(sudoku);
    }

    public void print2dArray(int[][] array) {
        for(int i=0;i<array.length;i++) {
            for(int j=0;j<array.length;j++) {
                System.out.printf("%4d",array[i][j]);
            }
            System.out.println();
        }
    }

    public boolean findSol(int[][] sudoku)    {
        for(int i=0;i<sudoku.length;i++)    {
            for(int j=0;j<sudoku.length;j++)    {
                if(sudoku[i][j] == 0)   {
                    for(int k=1;k<=sudoku.length;k++)   {
                        boolean issafe = isSafe(sudoku,i,j,k);
                        if(issafe)  {
                            sudoku[i][j] = k;
                            if(findSol(sudoku))
                                return true;
                            else
                                sudoku[i][j] = 0;
                        } else
                            sudoku[i][j] = 0;
                    }
                    return false;
                }else continue;
            }
        }
        return true;
    }
    public boolean isSafe(int[][] sudoku, int r, int c, int val) {
        for(int i=0;i<sudoku.length;i++)
        {
            if(sudoku[r][i] == val || sudoku[i][c] == val)
                return false;
        }
        int rowStart = r - r % 3;
        int colStart = c - c % 3;
        for (int i = rowStart; i < rowStart + 3; i++) {
            for (int j = colStart; j < colStart + 3; j++) {
                if (sudoku[i][j] == val)
                    return false;
            }
        }
        return true;
    }

}