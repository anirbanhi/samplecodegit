package com.apal.backtracking;

public class KnightTour {
    public static void main(String[] args) {
        int[][] sol = new int[8][8];
        for (int x = 0; x < 8; x++)
            for (int y = 0; y < 8; y++)
                sol[x][y] = -1;

            sol[0][0] = 0;
        KnightTour kt = new KnightTour();
        boolean res = kt.findSolution(sol,0,0,1);
        System.out.println("Solution exists "+res);
    }
    public boolean findSolution(int[][] sol, int x, int y, int moveCount) {
        if(moveCount == 64)
            return true;
        if(moveCount == 62) {
            System.out.println("Finding solution at [" + x + "][" + y + "]  " + moveCount);
            printMat(sol);
        }
        //System.out.println("Finding solution at ["+x+"]["+y+"]  "+moveCount);
        int nextX = x + 2;
        int nextY = y + 1;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
        nextX = x + 2;
        nextY = y - 1;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
         nextX = x - 2;
         nextY = y + 1;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
         nextX = x - 2;
         nextY = y - 1;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
         nextX = x + 1;
         nextY = y + 2;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
         nextX = x - 1;
         nextY = y + 2;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
         nextX = x + 1;
         nextY = y - 2;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
         nextX = x - 1;
         nextY = y - 2;
        if(isSafe(sol,nextX,nextY)) {
            sol[nextX][nextY] = moveCount;
            if(findSolution(sol,nextX,nextY,moveCount+1))
                return true;
            else
                sol[nextX][nextY] = -1;
        }
        //sol[x][y] = -1;
        return false;
    }

    public boolean isSafe(int[][] sol, int x, int y) {
        if(x >= 0 && x < 8 && y >= 0 && y < 8 && sol[x][y] == -1)
            return true;
        else
            return false;
    }

    public void printMat(int[][] sol)   {
        for(int i=0;i<8;i++) {
            for (int j = 0; j < 8; j++)
                System.out.printf("%4d ", sol[i][j]);
            System.out.println();
        }
    }
}
