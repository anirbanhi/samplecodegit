package com.apal.backtracking;

import java.util.*;

public class KeyPad {
    public static void main(String[] args) {
        Map<Integer, List<Character>> keypad =
                new HashMap<>();
        for(int i=0;i<6;i++)    {
            List<Character> l = new ArrayList<>();
            for(int j=1;j<=3;j++) {
                int char1 = 64 + (3 * i) + j;
                l.add(( char)char1 );
            }
            if(i+2 == 7)
                l.add('S');
            keypad.put(i+2,l);
        }
        List<Character> l = new ArrayList<>();
        l.add('T');l.add('U');l.add('V');
        keypad.put(8,l);
        l = new ArrayList<>();
        l.add('W');l.add('X');l.add('Y');l.add('Z');
        keypad.put(9,l);
        keypad.put(1,new ArrayList<>());

        for(int k : keypad.keySet()) {
            System.out.print(k+" ");
            for(char c : keypad.get(k))
                System.out.print(c+" ");
            System.out.println();
        }

        KeyPad kp = new KeyPad();
        kp.findCombinations("234",keypad,0,"");
    }

    public void findCombinations(String num,
                Map<Integer,List<Character>> keypad, int level,
                String s)  {
        if(level == num.length()) {
            System.out.println(s);
            return;
        }
        int key = Integer.parseInt(String.valueOf(num.charAt(level)));
        List<Character> chlist = keypad.get(key);
        for(int i=0; i<chlist.size(); i++)    {
            String s1 = s + chlist.get(i);
            findCombinations(num, keypad,level+1,s1);
        }
    }
}
