package com.apal.backtracking;
/*
https://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
 */
public class permutationActual {

    public static void main(String[] args) {
        String s = "ABC";
        int n = s.length();

        permutationActual pa = new permutationActual();
        pa.findPermutation(s,0,n);
    }

    public void findPermutation(String s,int cur, int n)   {
        if(cur == n-1) {
            System.out.println(s);
        }
        else
            for(int i=cur;i<n;i++)  {
                String s1 = swap(s,cur,i);
                findPermutation(s1,cur+1,n);
            }
    }

    public String swap(String s, int i, int j) {
        char ic = s.charAt(i);
        char jc = s.charAt(j);
        StringBuilder sb = new StringBuilder(s);
        sb.setCharAt(i,jc);
        sb.setCharAt(j,ic);
        return sb.toString();
    }
}
