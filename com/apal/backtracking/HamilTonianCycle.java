package com.apal.backtracking;

/*

https://www.geeksforgeeks.org/hamiltonian-cycle-backtracking-6/

 */

public class HamilTonianCycle {

    public static void main(String[] args) {
        int graph1[][] =
                {{0, 1, 0, 1, 0},
                {1, 0, 1, 1, 1},
                {0, 1, 0, 0, 1},
                {1, 1, 0, 0, 1},
                {0, 1, 1, 1, 0},
        };
        int[] sol = new int[graph1.length+1];
        for(int i:sol)
            sol[i] = -1;
        HamilTonianCycle hmc = new HamilTonianCycle();
        sol[0] = 0;
        boolean res = hmc.findHmiltonCycle(graph1,sol,1,0);
        System.out.println("Solution exists "+res);
    }

    public boolean findHmiltonCycle(int[][] g, int[] sol, int v, int indx)  {
        int adjucentVertices = g[v].length;
        sol[indx] = v;
        for(int i=0;i<adjucentVertices;i++) {
            if(g[v][i] == 1)    {
                boolean res = findHmiltonCycle(g,sol,i,indx+1);
                if(res) {
                    if (checkForCompletion(sol, g.length))
                        return true;
                }
            }
        }
        sol[indx] = -1;
        return false;
    }

    public boolean isSafe(int[][] g, int[] sol, int v, int solpos)  {
        for(int i=0;i<sol.length;i++)   {
            if(sol[i] == v)
                return false;
        }
        if(g[solpos-1][v] == 0)
            return false;
        return true;
    }

    public boolean checkForCompletion(int[] sol, int vertexCount)   {
        //0 to vertexCount
        boolean found = true;
        for(int i=0;i<vertexCount;i++)  {
            found = false;
            for(int j=0;j<sol.length;j++)   {
                if(sol[j] == i)
                    found = true;
            }
            if(found == false)
                return false;
        }
        if(sol[0] == sol[sol.length-1])
            return true;
        else
            return false;
    }
}