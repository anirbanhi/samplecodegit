package com.apal.backtracking;

/*
https://www.geeksforgeeks.org/boggle-set-2-using-trie/
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Boggle {
    public static void main(String[] args) {
        String[] dictionary = {"GEEKS", "FOR", "QUIZ", "GO"};
        char[][] boggle   =
                {
                    {'G', 'I', 'Z'},
                    {'U', 'E', 'K'},
                    {'Q', 'S', 'E'}
                };
        int n = boggle.length;

        //initialize -1 to every element in solution matrix
        int[][] sol = new int[n][n];
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
                sol[i][j] = -1;

        // all possible movement direction
        int[] x = {1,-1,0,0, 1,1, -1,-1};
        int[] y = {0,0,1,-1, 1,-1, 1,-1};

        //add dictionary words into trie
        Boggle b = new Boggle();
        Trie head = new Trie();
        for(String s : dictionary)  {
            b.addToTrie(head,s);
        }
        System.out.println("******  Displaying Trie content  ******");
        b.displayTrie(head, new ArrayList<Character>());
        System.out.println("****** ****** ****** ******");

        //call on each item to find matching words
        for(int i=0;i<n;i++) {
            for (int j = 0; j < n; j++) {
                if (sol[i][j] == -1) {
                    List<Character> l = new ArrayList<>();
                    l.add(boggle[i][j]);
                    sol[i][j] = 1;
                    boolean found =
                            b.findMatchedWords(boggle, sol, i, j, head, x, y, l);
                    if (found) System.out.println(l);
                }
            }
        }
    }

    public void addToTrie(Trie head, String s)   {
        Trie t = head;

        for(int i=0;i<s.length();i++)   {
            char c = s.charAt(i);
            if(t.childMap != null)  {
                if(t.childMap.get(c) == null) {
                    t.childMap.put(c,new Trie());
                }
                t = t.childMap.get(c);
            } else {
                t = new Trie();
                t.childMap.put(c,new Trie());
            }
        }
        t.isWord = true;
    }

    //  method to print all contents of Trie
    public void displayTrie(Trie head, List<Character> l)   {
        if(head == null)
            return;
        if(head.isWord == true) System.out.println(l);
        Trie t = head;
        for(char c : t.childMap.keySet())   {
            List<Character> l1 = new ArrayList<>(l);
            l1.add(c);
            displayTrie(t.childMap.get(c),l1);
        }
    }

    //Trie class structure to support words
    public static class Trie  {
        boolean isWord = false;
        Map<Character, Trie> childMap;
        public Trie() {
            childMap = new HashMap<>();
        }
    }

    public boolean findMatchedWords(char[][] g, int[][] sol, int r, int c,
                                 Trie trie,int[] x, int[] y,
                                    List<Character> l)  {
        if(matchTrieWord(trie, l))
            return true;

        //can move in all 8 directions, so i<8
        for(int i=0;i<8;i++)    {
            int r1 = r + x[i];
            int c1 = c + y[i];
            boolean isSafe = isSafe(g,sol,r1,c1,l,trie);
            if(isSafe)  {
                sol[r1][c1] = 1;
                l.add(g[r1][c1]);
                boolean res = findMatchedWords(g,sol,r1,c1,trie,x,y,l);
                if(res) return true;
                l.remove(l.size()-1);
                sol[r1][c1] = -1;
            }
        }
        sol[r][c] = -1;
        return false;
    }

    //find whether this move is safe or not ?
    public boolean isSafe(char[][] g, int[][] sol, int r, int c,
                            List<Character> l,Trie t) {
        int n = g.length;
        if(r < 0 || r >= n || c < 0 || c >= n)
            return false;
        if(sol[r][c] != -1)
            return false;

        List<Character> l1 = new ArrayList<>(l);
        l1.add(g[r][c]);
        return matchTrie(t,l1);
    }

    public boolean matchTrie(Trie head,List<Character> l) {
        for(int i=0;i<l.size();i++) {
            char c = l.get(i);
            if(head.childMap.get(c) == null)
                return false;
            else head = head.childMap.get(c);
        }
        return true;
    }

    public boolean matchTrieWord(Trie head, List<Character> l)   {
        for(int i=0;i<l.size();i++) {
            char c = l.get(i);
            if(head.childMap.get(c) == null)
                return false;
            else head = head.childMap.get(c);
        }
        return head.isWord;
    }
}