package com.apal.backtracking;
/*
https://practice.geeksforgeeks.org/problems/n-queen-problem0315/1
 */

import java.util.ArrayList;
import java.util.Arrays;

public class NQueen {

    public static void main(String[] args) {
        new NQueen().driver();
    }

    private void driver()   {
        nqueenStart(9);
    }

    private ArrayList<ArrayList<Integer>> nqueenStart(int n)   {
        for(int col=0;col<n;col++)  {
            int[] res = new int[n];
            Arrays.fill(res, -1);
            res[0] = col;
            queen(1,res, n);
        }

        for(ArrayList<Integer> s : listRes) {
            System.out.println(Arrays.toString(s.toArray()));
        }

        return listRes;
    }

    ArrayList<ArrayList<Integer>> listRes = new ArrayList<>();
    private void queen(int row, int[] res, int n)   {
        if(row == n) {
            Arrays.parallelSetAll(res,x->res[x]+1);
            ArrayList<Integer> l = new ArrayList<>();
            for(int i=0;i<n;i++)
                l.add(res[i]);
            listRes.add(l);
            return;
        } if(row > n) return;

        for(int col=0; col<n; col++)  {
            //System.out.println("row"  + row + "col "+col);
            if(check(row,col,res, n))  {
                int[] resCopy = new int[n];
                resCopy = Arrays.copyOf(res,n);
                resCopy[row] = col;
                queen(row+1, resCopy, n);
                //res[row] = -1;
            }
        }

    }

    private boolean check(int row, int col, int[] res, int n)  {
        int diag1 = row+col;
        int diag2 = row-col;

        for(int i=0;i<n;i++)    {
            if(res[i] == -1)    continue;
            if(res[i] == col)
                return false;
            int otherDiag1 = i + res[i];
            int otherDiag2 = i - res[i];

            if(otherDiag1 == diag1 || otherDiag2 == diag2)
                return false;
        }
        return true;
    }

}
