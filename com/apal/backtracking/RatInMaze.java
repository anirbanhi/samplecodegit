package com.apal.backtracking;

import java.util.ArrayList;
import java.util.Arrays;

/*
    https://practice.geeksforgeeks.org/problems/rat-in-a-maze-problem/1
*/

public class RatInMaze {

    public static void main(String[] args) {

        driver();

    }

    static void driver() {
        int[][] mat = {{1, 0, 0, 0},
                {1, 1, 0, 1},
                {1, 1, 0, 0},
                {0, 1, 1, 1}};
       // findPath(mat,4);

        int[][] mat1 = {{1, 0},
                {1, 0}};
        findPath(mat1, 2);
    }

    public static ArrayList<String> findPath(int[][] m, int n) {
        // Your code here
        int[][] path = new int[n][n];
        ArrayList<String> result = new ArrayList<>();
        //path[0][0] = 1;
        findPathDetail(m,path,0,0,n,"",result);
        if(result.size() == 0) {
            result.add("-1");
        }
        return result;
    }

    public static void findPathDetail(int[][] m, int[][] path, int x, int y, int n,String op,ArrayList<String> result) {
        if(x==n || y==n)    {
            //found output
            return;
        }
        if(check(m,path,x,y,n))  {
            if(x==n-1 && y==n-1)    {
                System.out.println(op);
                result.add(op);
            }

            int[][] p1 = new int[n][n];
            int[][] p2 = new int[n][n];
            int[][] p3 = new int[n][n];
            int[][] p4 = new int[n][n];

            for(int i=0;i<n;i++) {
                p1[i] = Arrays.copyOf(path[i], n);
                p2[i] = Arrays.copyOf(path[i], n);
                p3[i] = Arrays.copyOf(path[i], n);
                p4[i] = Arrays.copyOf(path[i], n);
            }
            if(x+1<n) {
                p1[x][y] = 1;
                findPathDetail(m, p1, x + 1, y, n,op+"D",result);
            }
            if(y+1<n) {
                p2[x][y] = 1;
                findPathDetail(m, p2, x, y + 1, n,op+"R",result);
            }
            if(x-1>-1)  {
                p3[x][y] = 1;
                findPathDetail(m, p3, x - 1, y, n,op+"U",result);
            }
            if(y-1>-1)  {
                p4[x][y] = 1;
                findPathDetail(m, p4, x, y - 1, n,op+"L",result);
            }
        }
    }

    public static boolean check(int[][] m, int[][] path, int x, int y, int n) {
        if(x >= n || y >= n)
            return false;
        if(m[x][y] == 0 || path[x][y] == 1)
            return false;
        return true;
    }

}