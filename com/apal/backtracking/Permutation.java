package com.apal.backtracking;
/*
https://www.geeksforgeeks.org/powet-set-lexicographic-order/
 */
import java.util.ArrayList;
import java.util.List;

public class Permutation {
    public static void main(String[] args) {
        char[] ip = {'a','b','c'};
        int n = ip.length;
        List<Character> l = new ArrayList<>();
        List<List<Character>> res = new ArrayList<>();
        Permutation pm = new Permutation();
        pm.printLexicographicllySortedSubset(ip,l,res,n,-1);

        for(List<Character> i : res)    {
                System.out.println(i);
        }
    }

    public void printLexicographicllySortedSubset(char[] ip, List<Character> l1,
                                   List<List<Character>> res, int n, int cur)   {
        System.out.println(l1);
        res.add(new ArrayList<>(l1));

        for(int i=cur+1;i<n;i++)    {
            List<Character> l = new ArrayList<>(l1);
            l.add(ip[i]);
            printLexicographicllySortedSubset(ip,l,res,n,i);
            l.remove(l.size()-1);
        }
    }
}
