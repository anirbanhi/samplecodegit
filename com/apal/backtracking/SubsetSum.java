package com.apal.backtracking;
/*
https://www.geeksforgeeks.org/subset-sum-backtracking-4/
 */
import java.util.ArrayList;
import java.util.List;

public class SubsetSum {
    public static void main(String[] args) {
        int[] a =  {10, 7, 5, 18, 12, 20, 15};
        int x = 35;
        int[] sol = new int[a.length];
        SubsetSum ss = new SubsetSum();
        ss.findSum(0,a,0,x,new ArrayList<>());
    }

    public void findSum(int st, int[] a, int sum, int x,
                   List<Integer> list)   {
        if(sum == x) {
            System.out.println("Found ");
            for(int i:list)
                System.out.print(i+" ");
            return;
        }
        if(st == a.length || sum > x)
            return;

        for(int i=st;i<a.length;i++)    {
            List<Integer> l = new ArrayList<>(list);
            l.add(a[i]);
            findSum(i+1,a,sum+a[i],x,l);
        }
    }
}
