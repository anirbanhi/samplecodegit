package com.apal.java8.exercise.hortsman.lambda;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ListFileExample {
    public static void main(String[] args)  {
        ListFileExample lfe = new ListFileExample();
        lfe.sortFiles();
    }

    /**
     * Using the listFiles(FileFilter) and isDirectory methods of the java.io.File class,
     * write a method that returns all subdirectories of a given directory. Use a
     * lambda expression instead of a FileFilter object. Repeat with a method
     * expression.
     */
    public void listDirs()  {
        File f = new File("D:\\important\\work\\2018\\oci");
        File[] files = f.listFiles(s -> s.isDirectory());
        for(File f1 : files)
            System.out.println(f1.getName());

        File f1 = new File("D:\\important\\work\\2018\\oci");
        File[] files1 = f1.listFiles(File::isDirectory);
        for(File f2 : files1)
            System.out.println(f2.getName());
    }

    /**
     * Using the list(FilenameFilter) method of the java.io.File class, write a method
     * that returns all files in a given directory with a given extension. Use a lambda
     * expression, not a FilenameFilter. Which variables from the enclosing scope does
     * it capture?
     */
    public void listFiles() {
        File f = new File("D:\\important\\work\\2018\\oci");
        File[] files = f.listFiles(s->s.getName().contains("txt") );
    }

    /**
     * Sort File[] based on type, like dir first then file, using lambda **
     */
    public void sortFiles() {
        File f1 = new File("D:\\important\\work\\2018\\oci");
        File[] files1 = f1.listFiles();

        Comparator<File> c = (File o1,File o2) ->
                o1.isDirectory() && o2.isDirectory() ? o1.getName().compareTo(o2.getName()) :(o1.isDirectory()?-1:(o2.isDirectory()?1:o1.getName().compareTo(o2.getName()))) ;
        Arrays.sort(files1,c);
        for(File f2 : files1)
            System.out.println(f2.getName());
    }


    
}
