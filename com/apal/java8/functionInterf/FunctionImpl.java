package com.apal.java8.functionInterf;

import java.util.function.Function;

public class FunctionImpl {
    public static void main(String[] args)  {

        Function<String,Integer> findLengthFunc = s -> s.length();
        System.out.println(findLengthFunc.apply("abc"));

        Function<String,String> funcRemoveSpace = s-> s.replaceAll("\\s","");
        System.out.println(funcRemoveSpace.apply("this is good"));

        Function<String,Integer> funcCountNoOfSpaces = s-> {
            int n=0;
            for(int i=0;i<s.length();i++)
                if(s.charAt(i)==' ')
                    n++;
            return n;
        };
        System.out.println(funcCountNoOfSpaces.apply("this is good"));

    }
}
