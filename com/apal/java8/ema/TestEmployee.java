package com.apal.java8.ema;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class TestEmployee {
    public static void main(String[] args)  {
        ArrayList<Employee> emplist = new ArrayList<>();

        emplist.add(new Employee("durga","CEO",30000,"Hyderabad"));
        emplist.add(new Employee("Sunny","Manager",20000,"Hyderabad"));
        emplist.add(new Employee("Mallika","Manager",20000,"Bangalore"));
        emplist.add(new Employee("Kareena","Lead",15000,"Hyderabad"));
        emplist.add(new Employee("Katrina","Lead",15000,"Hyderabad"));

        emplist.add(new Employee("Anushka","Developer",10000,"Hyderabad"));
        emplist.add(new Employee("Kanushka","Developer",10000,"Hyderabad"));
        emplist.add(new Employee("Sowmya","Developer",10000,"Bangalore"));
        emplist.add(new Employee("Ramya","Developer",10000,"Bangalore"));

        System.out.println(emplist);

        System.out.println("Select employee which employees are manager or not ");
        Predicate<Employee> p = e -> e.designation.equalsIgnoreCase("Manager");
        display(emplist,p);

        System.out.println("Select employee who are based on Bangalore ");
        Predicate<Employee> p1 = e -> e.city.equalsIgnoreCase("Bangalore");
        display(emplist,p1);

        System.out.println("Select employee whose salary is less than 20000 ");
        Predicate<Employee> p2 = e -> e.salary<20000;
        display(emplist,p2);

        System.out.println("Predicate Join: Select employee who are in bangalore and who are managers");
        Predicate<Employee> p3 = p.and(p1);
        display(emplist,p3);

        System.out.println("Predicate Join: Select employee who are either manager or salary less than 20000");
        Predicate<Employee> p4 = p1.or(p2);
        display(emplist,p4);

        System.out.println("Predicate Join: Select employee who are not manager ");
        display(emplist,p.negate());

        System.out.println("Ceo employee is ");
        Predicate<Employee> predicateceo = Predicate.isEqual(new Employee("durga","CEO",30000,"Hyderabad"));
        for (Employee e : emplist)  {
            if (predicateceo.test(e))
                System.out.println(e);
        }
    }

    public static void display(List<Employee> emplist, Predicate<Employee> p)   {
        for(Employee e : emplist)   {
            if(p.test(e))
                System.out.println(e);
        }
        System.out.println("********************************************************************************* \n");
    }
}
