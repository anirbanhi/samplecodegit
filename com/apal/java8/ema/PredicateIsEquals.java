package com.apal.java8.ema;

import java.util.function.Predicate;

public class PredicateIsEquals {
    public static void main(String[] args)  {
        Predicate<String> p = Predicate.isEqual("Durga");
        System.out.println(p.test("Durga"));
        System.out.println(p.test("Soft"));
    }
}
