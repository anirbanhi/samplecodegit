package com.apal.java8;


import java.util.*;

public class Java8Lambda {
    public static void main(String[] args)  {
        List<String> listOfELem = new ArrayList<>();
        listOfELem.add("acb");
        listOfELem.add("jcb");
        listOfELem.add("kjd");
        listOfELem.add("gdj");
        //System.out.println(listOfELem);

        /*listOfELem.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        */
        //listOfELem.sort((o1,o2)->o1.compareTo(o2));
        //System.out.println(listOfELem);

        TreeSet<String> ts = new TreeSet<>((I1,I2)->((I1.compareTo(I2)>0)?-1:(I1.compareTo(I2)<0)?1:0));
        ts.add("acb");
        ts.add("jcb");
        ts.add("kjd");
        ts.add("gdj");
//        System.out.println(ts);


        TreeMap<Integer,String> tm = new TreeMap<>();
        tm.put(100,"Durga");
        tm.put(200,"Sunny");
        tm.put(300,"Bunny");
        tm.put(400,"Chinny");
        tm.put(500,"Vhinny");

        System.out.println(tm);
    }
}
