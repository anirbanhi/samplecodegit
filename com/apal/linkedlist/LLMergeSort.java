package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/merge-sort-for-linked-list/

Whole algo was developped by me without seeing the code.
Easy to implement.
Divide and conquer technology

 */
public class LLMergeSort {

    public static void main(String[] args)  {
        Node h1 = new Node(8);
        h1.next = new Node(6);
        h1.next.next = new Node(1);
        h1.next.next.next  = new Node(4);
        h1.next.next.next.next = new Node(3);
        h1.next.next.next.next.next = new Node(5);
        h1.next.next.next.next.next.next = new Node(7);
        h1.next.next.next.next.next.next.next = new Node(2);

        LLBasicOperations.printLinkList(h1,"Before sort");
        Node res = new LLMergeSort().mergeSort(h1);
        LLBasicOperations.printLinkList(res,"After sort");
    }

    public Node mergeSort(Node head) {
        if(head == null)
            return null;
        if(head.next == null)
            return head;
        Node l1 = head;
        Node l2 = divide(l1);
        LLBasicOperations.printLinkList(l1," L1");
        LLBasicOperations.printLinkList(l2," L2");
        Node m1 = mergeSort(l1);
        Node m2 = mergeSort(l2);
        Node c = combine(m1,m2);
        LLBasicOperations.printLinkList(c,"----- intermediate ----- ");
        return c;
    }

    public Node divide(Node h)    {
        if(h == null)
            return null;
        if(h.next != null && h.next.next == null) {
            Node mid = h.next;
            h.next = null;
            return mid;
        }
        if(h.next == null)  {
            return  null;
        }
        Node slow = h;
        Node fast = h;
        while(slow!= null && fast!= null && fast.next!=null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        //when out of loop, then slow is at mid.
        Node h2 = slow.next;
        slow.next = null;
        return h2;
    }

    public Node combine(Node l1, Node l2)   {
        LLBasicOperations.
                printLinkList(l1, "********** l1 **********");
        LLBasicOperations.
                printLinkList(l2, "********** l2 **********");

        if(l1 != null && l2 == null)
            return l1;
        if(l1 == null && l2 != null)
            return l2;
        if(l1 == null && l2 == null)
            return null;

        Node res = null;
        Node resHead = null;
        if(l1.val > l2.val) {
            resHead = l2;
            l2 = l2.next;
        }
        else {
            resHead = l1;
            l1 = l1.next;
        }
        res = resHead;
        while(l1 != null && l2 != null) {
            if(l1.val < l2.val)    {
                res.next = l1;
                l1 = l1.next;
            }
            else {
                res.next = l2;
                l2 = l2.next;
            }
            res = res.next;
        }
        while(l1 != null) {
            res.next = l1;
            l1 = l1.next;
            res = res.next;
        }
        while(l2 != null) {
            res.next = l2;
            l2 = l2.next;
            res = res.next;
        }
        res.next = null;
        LLBasicOperations.
                printLinkList(resHead, "********** RES **********");
        return resHead;
    }

}