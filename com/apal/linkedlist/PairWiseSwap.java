package com.apal.linkedlist;

import java.util.LinkedList;
import java.util.Queue;

public class PairWiseSwap {
    public static void main(String[] args)   {
        Node head = LLBasicOperations.getList();
        LLBasicOperations.printLinkList(head,"before");

        PairWiseSwap pws = new PairWiseSwap();
        Node newHead = pws.pairWiseSwap(head);

        LLBasicOperations.printLinkList(newHead,"after");
        Queue<Integer> q = new LinkedList<Integer>();
    }

    private Node pairWiseSwap(Node head) {
        Node cur = head;
        Node prev = null;
        int k = 0;
        while(cur != null && k < 2) {
            Node temp = cur.next;
            cur.next = prev;
            prev = cur;
            cur = temp;
            k ++;
        }
        if(cur != null)
            head.next = pairWiseSwap(cur);
        return prev;
    }
}
