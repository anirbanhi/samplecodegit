package com.apal.linkedlist;

public class DeleteOddElements {
    public static void main(String[] args)  {
        DeleteOddElements doe = new DeleteOddElements();
        Node head = LLBasicOperations.getList();
        LLBasicOperations.printLinkList(head,"");
        Node newHead = doe.deleteOdd(head);
        LLBasicOperations.printLinkList(newHead,"");

    }

    private Node deleteOdd(Node head)   {
        Node tempHead = new Node(0);
        tempHead.next = head;
        Node prev = tempHead;
        Node cur = prev.next;

        while(cur != null) {
            if(cur.val % 2 == 1)    {
                prev.next = cur.next;
                cur = cur.next;
            }   else {
                prev = cur;
                cur = cur.next;
            }
        }
        return tempHead.next;
    }
}
