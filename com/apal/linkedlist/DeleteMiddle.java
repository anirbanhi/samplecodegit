package com.apal.linkedlist;

/*
https://www.geeksforgeeks.org/delete-middle-of-linked-list/
 */

public class DeleteMiddle {
    public static void main(String[] args)  {
        Node h = new Node(1);
        h.next = new Node(2);
        h.next.next = new Node(3);
        h.next.next.next = new Node(4);
        h.next.next.next.next = new Node(5);
        //h.next.next.next.next.next = new Node(6);

        LLBasicOperations.printLinkList(h,"");
        DeleteMiddle dm = new DeleteMiddle();
        dm.deleteMiddle(h);
        LLBasicOperations.printLinkList(h,"");
    }

    private void deleteMiddle(Node head) {
        Node s = head;
        Node f = head;
        Node prev = null;
        while(s!= null && f!= null && f.next != null) {
            prev = s;
            s = s.next;
            f = f.next.next;
        }
        System.out.println(s.val);
        prev.next = prev.next.next;

    }
}