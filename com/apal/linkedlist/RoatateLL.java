package com.apal.linkedlist;
/*
https://practice.geeksforgeeks.org/problems/rotate-a-linked-list/1
 */
public class RoatateLL {
    public static void main(String[] args) {
        Node h = new Node(1);
        h.next = new Node(2);
        h.next.next = new Node(3);
        h.next.next.next = new Node(4);
        h.next.next.next.next = new Node(5);
        h.next.next.next.next.next = new Node(6);
        h.next.next.next.next.next.next = new Node(7);
        h.next.next.next.next.next.next.next = new Node(8);
        h.next.next.next.next.next.next.next.next = new Node(9);
        h.next.next.next.next.next.next.next.next.next = null;

        RoatateLL rl = new RoatateLL();
        LLBasicOperations.printLinkList(h,"Before Op");
        Node rn = rl.rotateLL(h, 3);
        LLBasicOperations.printLinkList(rn,"After Op");
    }

    public Node rotateLL(Node head, int k) {
        Node t = head;
        Node h1 = t;
        Node temp = h1;
        int cnt = 0;
        Node prev = null;
        while (temp != null && cnt <= k)    {
            cnt++;
            prev = temp;
            temp = temp.next;
        }
        prev.next = null;
        Node h2 = temp;
        LLBasicOperations.printLinkList(h1,"part1");
        LLBasicOperations.printLinkList(h2,"part2");
        Node h1r = reverseLL(h1);
        Node h2r = reverseLL(h2);
        LLBasicOperations.printLinkList(h1r,"part1 rev");
        LLBasicOperations.printLinkList(h2r,"part2 rev");
        h1.next = h2r;
        LLBasicOperations.printLinkList(h1r,"rev");
        Node finalr = reverseLL(h1r);
        return finalr;
    }

    public Node reverseLL(Node r)   {
        Node prev = null;
        Node cur = r;
        while(cur != null) {
            Node t = cur.next;
            cur.next = prev;
            prev = cur;
            cur = t;
        }
        return prev;
    }
}
