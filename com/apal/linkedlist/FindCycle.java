package com.apal.linkedlist;

public class FindCycle {
    public static void main(String[] args)  {
        Node head = new Node(10);
        head.next = new Node(20);
        head.next.next = new Node(30);
        head.next.next.next = new Node(40);
        head.next.next.next.next = new Node(50);
        head.next.next.next.next.next = head.next.next;

        FindCycle fc = new FindCycle();
        System.out.println("Cycle exist "+fc.findCycle(head));
    }

    public boolean findCycle(Node head)  {
        if(head == null)
            return false;
        Node slow = head;
        Node fast = head.next;
        while (slow != null && fast != null && fast.next!= null)    {
            slow = slow.next;
            fast = fast.next.next;
            if(slow == fast)
                return false;
        }
        return true;
    }
}