package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/reverse-a-list-in-groups-of-given-size/
 */
public class ReverseInGroupOfKElem {
    public static void main(String[] args)  {
        Node h1 = new Node(1);
        h1.next = new Node(2);
        h1.next.next = new Node(3);
        Node loopStartPoint  = new Node(4);
        h1.next.next.next = loopStartPoint;
        h1.next.next.next.next = new Node(5);
        h1.next.next.next.next.next = new Node(6);
        h1.next.next.next.next.next.next = new Node(7);
        h1.next.next.next.next.next.next.next = new Node(8);
        h1.next.next.next.next.next.next.next.next = new Node(9);
        h1.next.next.next.next.next.next.next.next.next = new Node(10);

        int k = 4;
        ReverseInGroupOfKElem rvl = new ReverseInGroupOfKElem();
        rvl.printLinkList(h1,"");
        Node newHead = rvl.reverseInGroup(h1,k);
        rvl.printLinkList(newHead,"");
    }
    private Node globalHead;

    public Node reverseInGroup(Node head, int k )   {
        Node cur = head;
        Node prev = null;
        int cnt = 1;
        while(cnt <= k && cur != null) {
            Node temp = cur.next;
            cur.next = prev;
            prev = cur;
            cur = temp;
            cnt++;
        }
        System.out.println("head value "+head.val);
        if(cur != null)
            head.next = reverseInGroup(cur,k);
        return prev;
    }

    public void printLinkList(Node head, String str)    {
        System.out.println("Printing Link List "+str);
        Node temp = head;
        while(temp != null)
        {
            System.out.print(temp.val+" , ");
            temp = temp.next;
        }
        System.out.println();
    }
}
