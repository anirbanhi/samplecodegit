package com.apal.linkedlist;

public class Node {
    public int val;
    public Node next;

    public Node(int n)  {
        val = n;
        next = null;
    }
}
