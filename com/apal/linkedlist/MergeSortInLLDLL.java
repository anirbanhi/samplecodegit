package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/merge-sort-for-linked-list/
 */
public class MergeSortInLLDLL {

    public static void main(String[] args)  {
        Node head = new Node(10);
        head.next = new Node(1);
        head.next.next = new Node(5);
        head.next.next.next = new Node(3);
        head.next.next.next.next = new Node(7);
        head.next.next.next.next.next = new Node(4);

        MergeSortInLLDLL msi = new MergeSortInLLDLL();
        LLBasicOperations.printLinkList(head,"");
        Node newHead = msi.mergeSort(head);
        LLBasicOperations.printLinkList(newHead,"");
    }

    public Node mergeSort(Node head) {
        if(head == null || head.next == null)
            return head;
        Node mid = findMiddle(head);
        Node midNext = mid.next;
        mid.next = null;
        Node l = mergeSort(head);
        Node r = mergeSort(midNext);
        Node merged = merge(l,r);
        return merged;
    }

    public Node merge(Node l, Node r)   {
        if (l == null)
            return r;
        if (r == null)
            return l;
        Node res = null;
        if(l.val <= r.val)  {
            res = l;
            res.next = merge(l.next,r);
        } else {
            res = r;
            res.next = merge(l,r.next);
        }
        return res;
    }

    public Node findMiddle(Node head)   {
        if(head == null)
            return head;
        Node f = head;
        Node s = head;
        while(f.next != null && f.next.next != null)    {
            s = s.next;
            f = f.next.next;
        }
        return s;
    }

}