package com.apal.linkedlist;


public class ReverseADLL {
    public static void main(String[] args)  {
        DLNode a1 = new DLNode(8);
        DLNode a2 = new DLNode(4);
        DLNode a3 = new DLNode(3);
        DLNode a4 = new DLNode(10);
        DLNode a5 = new DLNode(6);
        DLNode a6 = new DLNode(5);

        DLNode head = a1;
        a1.next = a2;        a2.prev = a1;
        a2.next = a3;        a3.prev = a2;
        a3.next = a4;        a4.prev = a3;
        a4.next = a5;        a5.prev = a4;
        a5.next = a6;        a6.prev = a5;
        //a6.next = a1;        a1.prev = a6;

        ReverseADLL rv = new ReverseADLL();
        rv.printDLL(head);
        DLNode h = rv.reverse(head);
        rv.printDLL(h);
    }

    private DLNode reverse(DLNode head) {
        DLNode cur = head;
        DLNode temp = null;
        while(cur != null)  {
            DLNode next = cur.next;
            //swap next and prev pointers
             temp = cur.prev;
            cur.prev = cur.next;
            cur.next = temp;
            //increment cur
            cur = next;
        }
        if (temp != null) {
            head = temp.prev;
        }
        return head;
    }

    private void printDLL(DLNode head)  {
        DLNode t = head;
        System.out.println();
        while (t != null && t.next != head)  {
            System.out.print(t.val+", ");
            t = t.next;
        }
        System.out.println();
    }
}
