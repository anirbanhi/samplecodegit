package com.apal.linkedlist;

public class DeleteOddFromCircularLL {
    public static void main(String[] args)  {
        Node chead = LLBasicOperations.getCircularList();
        LLBasicOperations.printCircularLinkList(chead,"");
        DeleteOddFromCircularLL dl = new DeleteOddFromCircularLL();
        Node newChead = dl.deleteOddFromCircular(chead);
        LLBasicOperations.printCircularLinkList(newChead,"");
    }

    private Node deleteOddFromCircular(Node head)    {
        Node cur = head.next;
        Node prev = head;
        while(cur!=null && cur.next!= head)    {
            cur = cur.next;
        }

        prev = cur;
        cur = head.next;
        Node last = prev;
        System.out.println("Last = "+prev.val);
        while(cur != head) {
            if(cur.val % 2 == 1)    {
                if(cur == head) {
                    head = head.next;
                    last.next = head;
                }
                prev.next = cur.next;
                cur = cur.next;
            }   else    {
                prev = cur;
                cur = cur.next;
            }
        }
        return head;
    }
}
