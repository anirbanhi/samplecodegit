package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/linked-list-set-3-deleting-node/

This program covers all possible test cases.
null case
one node deletion
2 node deletion
3 node and middle deletion
Boundary node deletion
Illegal value deletion
 */
public class NodeDeletionFromLL {
    public static void main(String[] args)  {
        NodeDeletionFromLL nodl = new NodeDeletionFromLL();

        //1 node deletion
        Node head = new Node(10);
        head = nodl.nodeDeletion(head,10);
        String res1 = "";
        assert nodl.flattenLL(head).equalsIgnoreCase(res1)  : "Failed 1";

        //2 node deletion
        head = new Node(10);
        head.next = new Node(20);
        head = nodl.nodeDeletion(head,20);
        String res2 = "10,";
        assert nodl.flattenLL(head).equalsIgnoreCase(res2)  : "Failed 2";

        //2 node deletion
        head = new Node(10);
        head.next = new Node(20);
        head = nodl.nodeDeletion(head,10);
        String res3 = "20,";
        assert nodl.flattenLL(head).equalsIgnoreCase(res3)  : "Failed 3";

        //2 node deletion
        head = new Node(10);
        head.next = new Node(20);
        head = nodl.nodeDeletion(head,30);
        String res4 = "10,20,";
        assert nodl.flattenLL(head).equalsIgnoreCase(res4)  : "Failed 4";

        //3 node deletion
        head = new Node(10);
        head.next = new Node(20);
        head.next.next = new Node(30);
        head = nodl.nodeDeletion(head,20);
        String res5 = "10,30,";
        assert nodl.flattenLL(head).equalsIgnoreCase(res5)  : "Failed 5";

        //3 node deletion
        head = new Node(10);
        head.next = new Node(20);
        head.next.next = new Node(30);
        head = nodl.nodeDeletion(head,30);
        String res6 = "10,20,";
        assert nodl.flattenLL(head).equalsIgnoreCase(res6)  : "Failed 6";

        //3 node deletion
        head = new Node(10);
        head.next = new Node(20);
        head.next.next = new Node(30);
        head = nodl.nodeDeletion(head,10);
        String res7 = "20,30,";
        assert nodl.flattenLL(head).equalsIgnoreCase(res7)  : "Failed 7";

        //3 node deletion
        head = null;
        head = nodl.nodeDeletion(head,10);
        String res8 = "";
        assert nodl.flattenLL(head).equalsIgnoreCase(res8)  : "Failed 8";

    }

    public String flattenLL(Node head)  {
        String res = "";
        Node temp = head;
        while(temp != null)
        {
            res+=temp.val+",";
            temp = temp.next;
        }
        //System.out.println(res);
        return res;
    }

    public static void printLinkList(Node head, String str)    {
        System.out.println("Printing Link List "+str);
        Node temp = head;
        while(temp != null)
        {
            System.out.print(temp.val+" , ");
            temp = temp.next;
        }
        System.out.println();
    }

    public Node nodeDeletion(Node head, int x)  {
        if(head == null)
            return head;
        Node prev = null;
        Node cur = head;
        Node next = cur.next;

        while(cur.next != null) {
            if(cur.val == x)
                break;
            prev = cur;
            cur = cur.next;
            next = cur.next;
        }
        if(cur.val == x)    {
            if(prev != null)
            {
                prev.next = next;
                cur = null;
            }
            else    {
                head = head.next;
            }
        }
        return head;
    }
}
