package com.apal.linkedlist;

public class DLNode {
    public int val;
    public DLNode next;
    public DLNode prev;

    public DLNode(int v)    {
        this.val = v;
        next = null;
        prev = null;
    }
}
