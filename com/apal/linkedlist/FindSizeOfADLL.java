package com.apal.linkedlist;

/*
https://www.geeksforgeeks.org/program-find-size-doubly-linked-list/
 */

public class FindSizeOfADLL {
    public static void main(String[] args)  {
        DLNode a1 = new DLNode(8);
        DLNode a2 = new DLNode(4);
        DLNode a3 = new DLNode(3);
        DLNode a4 = new DLNode(10);
        DLNode a5 = new DLNode(6);
        DLNode a6 = new DLNode(5);

        DLNode head = a1;
        a1.next = a2;        a2.prev = a1;
        a2.next = a3;        a3.prev = a2;
        a3.next = a4;        a4.prev = a3;
        a4.next = a5;        a5.prev = a4;
        a5.next = a6;        a6.prev = a5;
        a6.next = a1;        a1.prev = a6;

        FindSizeOfADLL fsd = new FindSizeOfADLL();
        int cnt = fsd.findSize(head);
        System.out.println("count = " + cnt);
    }

    private int findSize(DLNode head) {
        DLNode t = head;
        int cnt = 0;
        while(t != null && t.next != head)
        {
            cnt++;
            t = t.next;
        }
        return cnt;
    }
}
