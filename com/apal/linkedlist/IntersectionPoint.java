package com.apal.linkedlist;

/*
https://practice.geeksforgeeks.org/problems/intersection-point-in-y-shapped-linked-lists/1/
 */

public class IntersectionPoint {
    public static void main(String[] args)  {
        Node intersection = new Node(15);
        intersection.next = new Node(30);

        Node h1 = new Node(3);
        h1.next = new Node(6);
        h1.next.next = new Node(9);
        h1.next.next.next = intersection;
        Node h2 = new Node(10);
        h2.next = intersection;

        LLBasicOperations.printLinkList(h1,"");
        LLBasicOperations.printLinkList(h2,"");

        IntersectionPoint ip = new IntersectionPoint();
        ip.findIntersectionPoint(h1,h2);
    }

    private void findIntersectionPoint(Node h1, Node h2)    {
        int l1 = findSize(h1);
        int l2 = findSize(h2);

        if(l1 >= l2){
            findInter(h1,h2,l1-l2);
        }else{
            findInter(h2,h1,l2-l1);
        }
    }

    private void findInter(Node l, Node s, int skip) {
        while(l != null && skip > 0)    {
            l = l.next;
            skip--;
        }
        while(l != null && s != null)   {
            if(l == s)  {
                System.out.println("Found intersection "+l.val);
                return;
            }
            l = l.next;
            s = s.next;
        }
    }

    private int findSize(Node h)    {
        Node t = h;
        int cnt = 0;
        while(t != null)
        {
            t = t.next;
            cnt++;
        }
        return cnt;
    }
}