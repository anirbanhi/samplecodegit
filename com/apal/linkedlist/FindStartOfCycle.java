package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/find-length-of-loop-in-linked-list/
 */
public class FindStartOfCycle {
    public static void main(String[] args)  {
        Node h1 = new Node(1);
        h1.next = new Node(2);
        h1.next.next = new Node(3);
        Node loopStartPoint  = new Node(4);
        h1.next.next.next = loopStartPoint;
        h1.next.next.next.next = new Node(5);
        h1.next.next.next.next.next = new Node(6);
        h1.next.next.next.next.next.next = new Node(7);
        h1.next.next.next.next.next.next.next = loopStartPoint;

        FindStartOfCycle fsc = new FindStartOfCycle();
        System.out.println("Cycle exists "+fsc.findStart(h1));
    }

    public boolean findStart(Node head) {
        Node slow = head;
        Node fast = head;
        while(slow!=null && fast!= null && fast.next!=null) {
            slow = slow.next;
            fast = fast.next.next;
            if(fast == null || fast.next == null)
            {
                System.out.println("Cycle doesn't exist");
                return false;
            }
            if(slow == fast)
            {
                Node temp = slow.next;
                int cnt = 1;
                while(temp != slow) {
                    temp = temp.next;
                    cnt++;
                }
                System.out.println("loop size = "+cnt);
                return true;
            }
        }
        return false;
    }
}
