package com.apal.linkedlist;

public class RemoveDuplicate {
    public static void main(String[] args)  {
        RemoveDuplicate rmd = new RemoveDuplicate();

        Node h = new Node(1);
        h.next = new Node(2);
        h.next.next = new Node(2);
        h.next.next.next = new Node(3);
        h.next.next.next.next = new Node(3);
        h.next.next.next.next.next = new Node(4);

        LLBasicOperations.printLinkList(h, "");
        //Node newHead = rmd.removeDuplicateFromSorted(h);
        Node newHead = rmd.removeDuplicateKeepDistinctFromSorted(h);
        LLBasicOperations.printLinkList(newHead, "");
    }

    private Node removeDuplicateFromSorted(Node head)  {
        Node cur = head;
        Node prev = null;
        while(cur.next != null) {
            if(cur.val == cur.next.val)
            {
                Node temp = cur.next;
                cur.next = cur.next.next;
                temp = null;
            }
            else
            {
                cur = cur.next;
            }
        }
        return head;
    }

    private Node removeDuplicateKeepDistinctFromSorted(Node head)  {
        if(head == null) return null;
        Node FakeHead = new Node(0);
        FakeHead.next = head;
        Node pre = FakeHead;
        Node cur = head;
        while(cur != null)  {
            while(cur.next != null && cur.val == cur.next.val)    {
                cur = cur.next;
            }
            if(pre.next == cur) {
                pre = pre.next;
            }
            else    {
                pre.next = cur.next;
            }
            cur = cur.next;
        }
        return FakeHead.next;
    }
}