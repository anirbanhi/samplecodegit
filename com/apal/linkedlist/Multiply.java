package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/multiply-two-numbers-represented-linked-lists/
 */
public class Multiply {
    public static void main(String[] args)  {
        Node h1 = new Node(9);
        h1.next = new Node(4);
        h1.next.next = new Node(6);

        Node h2 = new Node(8);
        h2.next = new Node(4);

        Multiply m = new Multiply();
        m.multiply(h1,h2);
    }

    public void multiply(Node h1, Node h2)  {
        if(h1 == null && h2 == null)
            return;
        if(h1 == null && h2 != null)
            multiply(h1,h2.next);
        else if(h1 != null && h2 == null)
            multiply(h1.next,h2);
        else multiply(h1.next,h2.next);

        System.out.println();
        if(h1!=null)
            System.out.print("h1 -> " + h1.val);
        if(h2!=null)
            System.out.print("h2 -> " + h2.val);
    }

}
