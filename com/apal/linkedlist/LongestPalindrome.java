package com.apal.linkedlist;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class LongestPalindrome {
    public static void main(String[] args)  {
        LongestPalindrome lpd = new LongestPalindrome();
        String s1 = "1234";
        lpd.findPalindrome(s1,0);
    }

    public void findPalindrome(String str, int indx)    {
        Queue<String> q = new LinkedList<>();
        q.add(str.substring(indx,indx+1));
        int size = 0;
        indx++;
        while(indx < str.length()) {
            size = q.size();
            String cur = str.substring(indx,indx+1);
            q.offer(cur);
            System.out.println(cur);
            while(size > 0) {
                String l = q.poll();
                String l1 = l + cur;
                q.offer(l1);
                size --;
                System.out.println(l1);
            }
            indx++;
        }
    }

    public boolean isPalindrome(String str, int r, int t)   {
        while(r < t) {
            if (str.charAt(r) != str.charAt(t)) {
                return false;
            }
            r++;
            t--;
        }
        return true;
    }
}
