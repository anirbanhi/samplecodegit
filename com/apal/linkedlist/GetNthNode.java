package com.apal.linkedlist;
/*
https://www.geeksforgeeks.org/write-a-function-to-get-nth-node-in-a-linked-list/
 */
public class GetNthNode {
    public static void main(String[] args)  {
        Node head = new Node(1);
        head.next = new Node(10);
        head.next.next = new Node(30);
        head.next.next.next = new Node(14);
        int n = 2;
        System.out.println(n+"th node is "+getNthNode(head,n));
    }

    public static int getNthNode(Node head, int pos)    {
        if(head == null)
            return -1;
        Node temp = head;
        while(temp!=null)
        {
            if(pos == 0)
                return temp.val;
            else {
                pos--;
                temp = temp.next;
            }
        }
        return -1;
    }
}
