package com.apal.linkedlist;

public class LLBasicOperations {
    public static void main(String[] args)  {

    }
    public static Node getList()    {
        Node h1 = new Node(1);
        h1.next = new Node(2);
        h1.next.next = new Node(3);
        Node loopStartPoint  = new Node(4);
        h1.next.next.next = loopStartPoint;
        h1.next.next.next.next = new Node(5);
        h1.next.next.next.next.next = new Node(6);
        h1.next.next.next.next.next.next = new Node(7);
        h1.next.next.next.next.next.next.next = new Node(8);
        h1.next.next.next.next.next.next.next.next = new Node(9);
     //   h1.next.next.next.next.next.next.next.next.next = new Node(10);

        return h1;
    }

    public static Node getCircularList()    {
        Node h1 = new Node(1);
        h1.next = new Node(2);
        h1.next.next = new Node(3);
        Node loopStartPoint  = new Node(4);
        h1.next.next.next = loopStartPoint;
        h1.next.next.next.next = new Node(5);
        h1.next.next.next.next.next = new Node(6);
        h1.next.next.next.next.next.next = new Node(7);
        h1.next.next.next.next.next.next.next = new Node(8);
        h1.next.next.next.next.next.next.next.next = new Node(9);
        h1.next.next.next.next.next.next.next.next.next = h1;

        return h1;
    }

    public static void printLinkList(Node head, String str)    {
        System.out.println("Printing Link List "+str);
        Node temp = head;
        while(temp != null)
        {
            System.out.print(temp.val+" , ");
            temp = temp.next;
        }
        System.out.println();
    }
    public static void printCircularLinkList(Node head, String str)    {
        System.out.println("Printing Link List "+str);
        Node temp = head;
        while(temp != null && temp.next != head)
        {
            System.out.print(temp.val+" , ");
            temp = temp.next;
        }
        System.out.print(temp.val+" , ");
        System.out.println();
    }

    public static int countNodesInCircularLL(Node h)    {
        Node temp = h;
        int cnt = 1;
        while(temp != null && temp.next != h)
        {
            temp = temp.next;
            cnt++;
        }
        return cnt;
    }

    public Node addAtHead(Node head, int x) {
        Node temp = new Node(x);
        temp.next = head;
        head = temp;
        return head;
    }
}
