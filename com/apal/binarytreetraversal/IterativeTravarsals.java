package com.apal.binarytreetraversal;

import java.util.Stack;

public class IterativeTravarsals {
    public static void main(String[] args)  {
        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);

        IterativeTravarsals it = new IterativeTravarsals();
        it.preOrder(root);
        System.out.println();
        it.inOrder(root);
        System.out.println();
        it.postOrder(root);
    }

    private void preOrder(Node root)    {
        Stack<Node> st = new Stack<>();
        Node temp = root;
        while(!st.isEmpty() || temp!=null)    {
            if(temp != null)    {
                System.out.print(temp.val+" , ");
                st.push(temp);
                temp = temp.left;
            }
            else    {
                temp = st.pop();
                temp=temp.right;
            }
        }
    }

    private void inOrder(Node root)    {
        Stack<Node> st = new Stack<>();
        Node temp = root;
        while(!st.isEmpty() || temp!=null)    {
            if(temp != null)    {
                st.push(temp);
                temp = temp.left;
            }
            else    {
                temp = st.pop();
                System.out.print(temp.val+" , ");
                temp=temp.right;
            }
        }
    }

    private void postOrder(Node root)    {
        Stack<Node> st = new Stack<>();
        Node cur = root;
        while(!st.isEmpty() || cur!=null)    {
            if(cur != null)    {
                st.push(cur);
                cur = cur.left;
            }
            else    {
                Node temp = st.peek().right;
                if(temp == null){
                    temp = st.pop();
                    System.out.print(temp.val+" , ");
                    while(!st.isEmpty() && temp == st.peek().right) {
                        temp = st.pop();
                        System.out.print(temp.val+" , ");
                    }
                }else   {
                    cur = temp;
                }
            }
        }
    }

    private void levelOrder(Node root)  {

    }
}
