package com.apal.designpattern.singleton.loggersystemex;

public class TestLogger {
    public static void main(String[] args)  {
        Thread t1 = new Thread(()->{
           LoggerSys ls = new LoggerSys();
           ls.write("from thread1");
        });
        Thread t2 = new Thread(()->{
            LoggerSys ls = new LoggerSys();
            ls.write("from thread2");
        });

        t1.start();
        t2.start();
    }

}
