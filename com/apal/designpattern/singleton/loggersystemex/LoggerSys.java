package com.apal.designpattern.singleton.loggersystemex;

import java.io.File;
import java.io.FileOutputStream;

public class LoggerSys{

    File logFile=new File("abc.txt");
    FileOutputStream fos;

    public LoggerSys()  {
        try {
            fos = new FileOutputStream(logFile);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void write(String str)   {
        try{
            fos.write(str.getBytes());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
