package com.apal.designpattern.singleton.loggersystemex.working;

import java.io.File;
import java.io.FileOutputStream;

public class LoggerSysSingleton {

    File logFile = new File("abc.txt");
    FileOutputStream fos;

    private LoggerSysSingleton()  {
        try {
            fos = new FileOutputStream(logFile);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private static class LoggerHolder{
        private static LoggerSysSingleton logholder = new LoggerSysSingleton();

    }

    public static LoggerSysSingleton getInstance()  {
        return LoggerHolder.logholder;
    }

    public void write(String str)   {
        try{
            fos.write(str.getBytes());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
