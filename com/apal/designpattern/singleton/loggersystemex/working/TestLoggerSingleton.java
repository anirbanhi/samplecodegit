package com.apal.designpattern.singleton.loggersystemex.working;

public class TestLoggerSingleton {
    public static void main(String[] args)  {
        Thread t1 = new Thread(()->{
           LoggerSysSingleton ls = LoggerSysSingleton.getInstance() ;
           for(int i=0;i<20;i++)
                   ls.write("  from thread1 "+i);
        });
        Thread t2 = new Thread(()->{
            LoggerSysSingleton ls = LoggerSysSingleton.getInstance();
            for(int i=0;i<20;i++)
                ls.write("  from thread2 "+i);
        });

        t1.start();
        t2.start();
    }

}
