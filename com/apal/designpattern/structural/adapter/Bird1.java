package com.apal.designpattern.structural.adapter;

public class Bird1 implements Bird {
    @Override
    public void fly() {
        System.out.println("Bird1 is flying ");
    }

    @Override
    public void makeSound() {
        System.out.println("Bird1 Panck Panck ");
    }
}
