package com.apal.designpattern.structural.adapter;

public class Bird3 implements Bird{
    @Override
    public void fly() {
        System.out.println("Bird3 is flying ");
    }

    @Override
    public void makeSound() {
        System.out.println("Bird3 Panck Panck ");
    }
}
