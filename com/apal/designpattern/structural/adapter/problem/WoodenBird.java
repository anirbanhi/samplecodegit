package com.apal.designpattern.structural.adapter.problem;

public class WoodenBird {
    public void pressButton()   {
        System.out.println("Wooden Bird sound");
    }
}