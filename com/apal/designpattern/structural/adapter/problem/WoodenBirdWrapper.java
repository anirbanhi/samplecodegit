package com.apal.designpattern.structural.adapter.problem;

import com.apal.designpattern.structural.adapter.Bird;

public class WoodenBirdWrapper implements Bird {

    WoodenBird wb;

    public WoodenBirdWrapper(WoodenBird _wb)  {
        this.wb = _wb;
    }

    @Override
    public void makeSound() {
        wb.pressButton();
    }

    @Override
    public void fly() {

    }
}
