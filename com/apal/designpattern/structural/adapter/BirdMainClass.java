package com.apal.designpattern.structural.adapter;

import com.apal.designpattern.structural.adapter.problem.WoodenBird;
import com.apal.designpattern.structural.adapter.problem.WoodenBirdWrapper;

import java.util.ArrayList;
import java.util.List;

public class BirdMainClass {
    public static void main(String[] args)
    {
        List<Bird> birdList = new ArrayList<>();
        birdList.add(new Bird1());
        birdList.add(new Bird2());
        birdList.add(new Bird3());

        //add problematic class
        birdList.add(new WoodenBirdWrapper(new WoodenBird()));

        for(Bird b : birdList)
        {
            b.fly();
            b.makeSound();
        }
    }
}
