package com.apal.designpattern.structural.adapter;

public interface Bird {
    void fly();
    void makeSound();
}
