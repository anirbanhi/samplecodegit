package com.apal.designpattern.structural.adapter;

public class Bird2 implements Bird{
    @Override
    public void fly() {
        System.out.println("Bird2 is flying ");
    }

    @Override
    public void makeSound() {
        System.out.println("Bird2 Panck Panck ");
    }
}
