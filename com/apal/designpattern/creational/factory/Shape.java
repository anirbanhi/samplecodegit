package com.apal.designpattern.creational.factory;

public interface Shape {

    int getArea();

    int getPerimeter();

    void drawShape();
}
