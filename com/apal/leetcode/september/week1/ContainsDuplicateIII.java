package com.apal.leetcode.september.week1;

public class ContainsDuplicateIII {
    public static void main(String[] args) {
        new ContainsDuplicateIII().driver();
    }
    public void driver()    {
        System.out.println("true - "+
                containsNearbyAlmostDuplicate(new int[]{1,2,3,1},3,0));
        System.out.println("true - "+
                containsNearbyAlmostDuplicate(new int[]{1,0,1,1},1,2));
        System.out.println("false - "+
                containsNearbyAlmostDuplicate(new int[]{1,5,9,1,5,9},2,3));
        System.out.println("false - "+
                containsNearbyAlmostDuplicate(new int[]{-1,2147483647},
                        1,2147483647));
        System.out.println("false - "+
                containsNearbyAlmostDuplicate(new int[]{2147483647,-2147483647},
                        1,2147483647));
    }
    public boolean containsNearbyAlmostDuplicate(int[] nums,
                                                 int k, int t) {
        for(int i=0;i<nums.length;i++) {
            for(int j=i+1;j<nums.length && j <= i+k;j++)  {
                long diff = (long)nums[i] - (long)nums[j];
                System.out.println(diff);
                if(Math.abs(diff) <= t)
                    return true;
            }
        }
        return false;
    }
}
