package com.apal.leetcode.september.week1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/554/week-1-september-1st-september-7th/3445/
 */
public class LargestTimeFromDigits {
    public static void main(String[] args) {
        new LargestTimeFromDigits().driver();
    }

    public void driver()    {

      //  System.out.println(largestTimeFromDigits(new int[]{1,2,3,4}));

     //   System.out.println(largestTimeFromDigits(new int[]{5,5,5,5}));

        System.out.println(largestTimeFromDigits(new int[]{2,0,6,6}));

    }

        public String largestTimeFromDigits(int[] A) {
            String ans = "";
            for (int i = 0; i < 4; ++i) {
                for (int j = 0; j < 4; ++j) {
                    for (int k = 0; k < 4; ++k) {
                        if (i == j || i == k || j == k) continue; // avoid duplicate among i, j & k.
                        String h = "" + A[i] + A[j], m = "" + A[k] + A[6 - i - j - k], t = h + ":" + m; // hour, minutes, & time.
                        if (h.compareTo("24") < 0 && m.compareTo("60") < 0 && ans.compareTo(t) < 0) ans = t; // hour < 24; minute < 60; update result.
                    }
                }
            }
            return ans;
        }
}
