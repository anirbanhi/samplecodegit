package com.apal.leetcode.julyweek4;

public class ClimbingStairs {
    public static void main(String[] args) {
        climb(2);
        climb(3);
        climb(4);
    }
    public static int climb(int n)  {
        if(n <=0)   return 0;
        int[][] dp = new int[n][2];
        dp[0][0] = 1;
        dp[0][1] = 0;
        dp[1][0] = 2;
        dp[1][1] = 1;
        for(int i=2;i<n;i++)    {
            dp[i][0] = Math.max(dp[i-1][0]+1,dp[i-2][1]+1);
            dp[i][1] = Math.max(dp[i-1][0]+1,dp[i-1][1]+1);
        }
        System.out.println(Math.max(dp[n-1][0],dp[n-1][1]));
        return Math.max(dp[n-1][0],dp[n-1][1]);
    }
}
