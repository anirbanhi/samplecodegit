package com.apal.leetcode.julyweek4;
/*
https://leetcode.com/explore/challenge/card/july-leetcoding-challenge/548/week-5-july-29th-july-31st/3405/

DP

 */

public class BuyAndSellStockWithCoolDown {

    public static void main(String[] args) {
        int[] p = {1,2,3,0,2};
        findMaxProfit(p);
    }

    public static int findMaxProfit(int[] s)   {
        if(s.length <= 1)   return 0;
        if(s.length == 2)   {
            if(s[1] > s[0]) return s[1] - s[0];
            else return 0;
        }
        //have more than 2 day stock prices

        int[][] dp = new int[s.length][2];
        //base condition
        //we have stock on day i = dp[i][1]
        dp[0][1] = -s[0];
        dp[1][1] = Math.max(-s[1],dp[0][1]);

        //we don't have stock on day i = dp[i][0]
        dp[0][0] = 0;
        if(s[1] > s[0])
            dp[1][0] = s[1] - s[0];
        else
            dp[1][0] = 0;

        int cost = 0;
        for(int i=2;i<s.length;i++) {
            dp[i][1] = Math.max(dp[i-2][0]-s[i],dp[i-1][1]);

            dp[i][0] = Math.max(dp[i-1][1] + s[i],dp[i-1][0]);
        }
        /*
        for(int i=0;i<s.length;i++) {
                System.out.println(dp[i][0]+"  "+dp[i][1]);
        }   */
        return dp[s.length-1][0];
    }

}