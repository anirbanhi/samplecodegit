package com.apal.leetcode.julyweek4;

/*
https://leetcode.com/explore/challenge/card/july-leetcoding-challenge/547/week-4-july-22nd-july-28th/3404/
https://www.youtube.com/watch?v=ySTQCRya6B0
 */

import java.util.*;

public class TaskScheduler {
    public static void main(String[] args) {

        char[] tasks = {'A','A','A','B','B','B'};
        int n = 2;

        HashMap<Character, Integer> map = new HashMap<>();
        for(char c : tasks) {
            map.put(c, map.getOrDefault(c, 0) + 1);
        }
        PriorityQueue<Integer> maxHeap = new PriorityQueue<Integer>((a, b) -> b - a);
        maxHeap.addAll(map.values());

        int cycles = 0;
        while(!maxHeap.isEmpty()) {
            List<Integer> temp = new ArrayList<>();
            for(int i = 0; i <= n ; i++) {
                if(!maxHeap.isEmpty())  {
                    temp.add(maxHeap.remove());
                }
            }

            for(int i : temp)   {
                if(--i > 0) {
                    maxHeap.add(i);
                }
            }
            cycles += maxHeap.isEmpty() ? temp.size() : n + 1;
        }
        System.out.println("Answer = "+cycles);
    }
}