package com.apal.leetcode.julyweek4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
https://leetcode.com/explore/featured/card/july-leetcoding-challenge/548/week-5-july-29th-july-31st/3406/
 */
public class WordBreakBT {

    public static void main(String[] args) {
        //String[] dict = {"cats", "dog", "sand", "and", "cat"};
        //String[] dict = {"apple","pen","applepen","pine","pineapple"};
        String[] dict = {"aaaa", "aa"};
        //String[] dict = {"a"};
        Trie head = createTrie(dict);
        List<Character> l = new ArrayList<>();
        displayTrie(head, l);

        //String sentence = "catsanddog";
        //String sentence = "pineapplepenapple";
        //String sentence = "a";
        String sentence = "aaaaaaa";
        List<Character> l1 = new ArrayList<>();
        List<List<Character>> res = new ArrayList<>();
        int[] pos = new int[sentence.length()];
        fl = new ArrayList<>();
        findWords(sentence, head, 0, res, pos);
        System.out.println("*************");
        List<String> op = new ArrayList<>();
        int tot = characterCount(sentence);
        for(String s : fl)  {
            if(characterCount(s) == tot)
                op.add(s);
        }
        System.out.println(op);
    }
    static List<String> fl;
    public static boolean findWords(String s, Trie head, int indx,
                                 List<List<Character>> res,
                                    int[] pos)  {
        if(indx == s.length())  {
            addToResult(res);
            return false;
        }
        List<Character> l = new ArrayList<>();
        Trie t = head;
            for (int i = indx; i < s.length(); i++) {
                char ch = s.charAt(i);
                if (t.child.get(ch) != null) {
                    t = t.child.get(ch);
                    if (t.isWord && pos[i] == 0) {
                        //t = head;
                        l.add(ch);
                        List<List<Character>> res1 = new ArrayList<>(res);
                        res1.add(l);
                        findWords(s, head, i+1, res1, pos);
                        //l = new ArrayList<>();
                    } else {
                        l.add(ch);
                    }
                }
                if (i == s.length() - 1) {
                    addToResult(res);
                    res = new ArrayList<>();
                }
            }
        return true;
    }

    public static int characterCount(String s) {
        int c = 0;
        for(int i = 0; i < s.length();i++)
            if(s.charAt(i) != ' ')
                c++;
        return c;
    }
    public static void addToResult(List<List<Character>> res) {
        StringBuilder s1 = new StringBuilder();
        for(int k=0;k<res.size();k++)   {
            List<Character> l5 = res.get(k);
            for (Character c : l5) {
                s1.append(c);
            }
            if(!(k==res.size()-1))
                s1.append(" ");
        }
        if(s1.length() > 0)
            fl.add(s1.toString());
    }

    static class Trie   {
        Map<Character, Trie>    child;
        boolean isWord = false;

        public Trie()   {
            child = new HashMap<>();
            isWord = false;
        }
    }

    public static Trie createTrie(String[] ip) {
        Trie head = new Trie();
        for(String s : ip)  {
            Trie t = head;
            for(int i=0;i<s.length();i++)   {
                char ch = s.charAt(i);
                if(t.child.get(ch) == null) {
                    t.child.put(ch, new Trie());
                }
                t = t.child.get(ch);
                if(i == s.length()-1)
                    t.isWord = true;
            }
        }
        return head;
    }

    public static void displayTrie(Trie head, List<Character> l)   {
        if(head.isWord)
            System.out.println(l);
        for(Character ch : head.child.keySet()) {
            l.add(ch);
            displayTrie(head.child.get(ch), l);
            l.remove(l.size()-1);
        }
    }
}