package com.apal.leetcode.julyweek4;

import java.util.Arrays;
import java.util.Stack;

/*
https://leetcode.com/explore/challenge/card/july-leetcoding-challenge/548/week-5-july-29th-july-31st/3405/
 */
public class BuyAndSellStock {
    public static void main(String[] args) {
        //int[] stock = {1,2,3,0,2};
        int[] stock = {48,12,60,93,97,42,25,64,17,56,85,93,9,48,52,42,58,85,81,84,69,36,1,54,23,15,72,15,11,94};
        /*
        Output: 3
        Explanation: transactions = [buy, sell, cooldown, buy, sell]
         */
        int[] v = new int[stock.length];
        findProfit(stock,-1,v);
        System.out.println("Max cost = "+max);
    }
    public static int max = Integer.MIN_VALUE;
    public static void findProfit(int[] s, int indx, int[] v) {
        if(indx==s.length-1)  {
            //finished
            int cost = calculateProfit(s,v);
            //System.out.println(Arrays.toString(v)+" cost = "+cost);
            if(cost > max)  max = cost;
            return;
        }
        /*
        0 - not used
        1 - buy
        2 - sell
        3 - skip / cooldown
         */
        for(int i = indx+1;i<s.length;i++) {
            for(int j=1;j<=3;j++)   {
                if(isSafe(i,v,j,s)) {
                    int[] v1 = Arrays.copyOf(v,v.length);
                    v1[i] = j;
                    findProfit(s,i,v1);
                }
            }
        }

    }

    public static boolean isSafe(int indx, int[] v,int op,int[] s)  {
        int buycost = -1;
        if(v[indx] != 0)    return false;
        if(indx == 0)    {
            if(op == 1 || op == 3)  return true;
            else return false;
        }
        Stack<Boolean> st = new Stack<>();
        for(int i=0;i<indx;i++) {
            if(v[i] == 0)   return false;
            if(v[i] == 1)   {
                if(!st.isEmpty())   return false;
                else    {
                    buycost = s[i];
                    st.push(true);
                }
            } else if (v[i] == 2) {
                if(st.isEmpty())  return false;
                else
                    st.pop();
            }
        }
        if( op == 1 && v[indx-1] == 2)  return false;
        if(op == 1) {
            if(st.isEmpty())  return true;
            else return false;
        } else if (op == 2) {
            if(!st.isEmpty()){
                if(s[indx] > buycost)
                    return true;
                else
                    return false;
            }
            else return false;
        }
        return true;
    }

    public static int calculateProfit(int[] s, int[] v) {
        int cost = 0;
        for(int i=0;i<v.length;i++)    {
            if(v[i] == 1)   cost -= s[i];
            if(v[i] == 2)   cost += s[i];
        }
        return cost;
    }
}
