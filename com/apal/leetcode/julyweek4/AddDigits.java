package com.apal.leetcode.julyweek4;

public class AddDigits {
    public static void main(String[] args) {
        System.out.println(addDigits(38));
        System.out.println(addDigits(0));
    }
    public static int addDigits(int num)    {
        if (num == 0)
            return 0;
        int numbk = num;
        int r = 0;
        int c = 0;
        int digitCount = 0;
        while(num > 0) {
            r = num % 10;
            c += r;
            num = num  / 10;
            digitCount++;
        }
        if(digitCount == 1)
            return numbk;
        return addDigits(c);
    }
}
