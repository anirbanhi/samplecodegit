package com.apal.leetcode.julyweek4;


import java.util.*;

/*
https://leetcode.com/explore/featured/card/july-leetcoding-challenge/548/week-5-july-29th-july-31st/3406/
 */
public class WordBreak {

    public static void main(String[] args) {
        //String[] dict = {"cats", "dog", "sand", "and", "cat"};
        String[] dict = {"apple","pen","applepen","pine","pineapple"};
        Trie head = createTrie(dict);
        List<Character> l = new ArrayList<>();
        displayTrie(head, l);

        //String sentence = "catsanddog";
        String sentence = "pineapplepenapple";
        List<Character> l1 = new ArrayList<>();
        List<List<Character>> res = new ArrayList<>();
        int[] pos = new int[sentence.length()];
        findWords(sentence, head, 0, l1, res, pos);

    }

    public static boolean findWords(String s, Trie head, int indx,
                                 List<Character> l, List<List<Character>> res,
                                    int[] pos)  {
        List<String> fl = new ArrayList<>();
        if(indx == s.length())  {
            return false;
        }
        Trie t = head;
        boolean firstBreak = true;
        while(firstBreak) {
            firstBreak = false;
            for (int i = 0; i < s.length(); i++) {
                char ch = s.charAt(i);
                if (t.child.get(ch) != null) {
                    t = t.child.get(ch);
                    if (t.isWord && pos[i] == 0) {
                        t = head;
                        l.add(ch);
                        res.add(l);
                        l = new ArrayList<>();
                        if (!firstBreak) {
                            pos[i] = 1;
                            firstBreak = true;
                        }
                    } else {
                        l.add(ch);
                    }
                }
                if (i == s.length() - 1) {
                    StringBuilder s1 = new StringBuilder();
                    for(int k=0;k<res.size();k++)   {
                        List<Character> l5 = res.get(k);
                        for (Character c : l5) {
                            s1.append(c);
                        }
                        if(!(k==res.size()-1))
                            s1.append(" ");
                    }
                    if(s1.length() > 0)
                        fl.add(s1.toString());
                    res = new ArrayList<>();
                }
            }
        }
        System.out.println("*************");
        System.out.println(fl);
        return true;
    }

    static class Trie   {
        Map<Character, Trie>    child;
        boolean isWord = false;

        public Trie()   {
            child = new HashMap<>();
            isWord = false;
        }
    }

    public static Trie createTrie(String[] ip) {
        Trie head = new Trie();
        for(String s : ip)  {
            Trie t = head;
            for(int i=0;i<s.length();i++)   {
                char ch = s.charAt(i);
                if(t.child.get(ch) == null) {
                    t.child.put(ch, new Trie());
                }
                t = t.child.get(ch);
                if(i == s.length()-1)
                    t.isWord = true;
            }
        }
        return head;
    }

    public static void displayTrie(Trie head, List<Character> l)   {
        if(head.isWord)
            System.out.println(l);
        for(Character ch : head.child.keySet()) {
            l.add(ch);
            displayTrie(head.child.get(ch), l);
            l.remove(l.size()-1);
        }
    }
}