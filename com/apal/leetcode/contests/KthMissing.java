package com.apal.leetcode.contests;

public class KthMissing {
    public static void main(String[] args) {
        //int[] arr = {2,3,4,7,11};
        //int  k = 5;
        int[] arr = {1,2,3,4};
        int k = 2;
        int m = 0;
        int[] a = new int[arr[arr.length-1]+k+1];
        for(int i=0;i<arr.length;i++)   {
            a[arr[i]] = 1;
        }
        int i = 1;
        for(i=1;i<a.length;i++) {
            if(a[i] == 0)
                m++;
            if(m == k)
                System.out.println(i);
        }

    }
}
