package com.apal.leetcode.contests;
/*
https://leetcode.com/contest/weekly-contest-200/problems/count-good-triplets/
 */
public class GoodTriplets {

    public static void main(String[] args) {
        int[] arr = {3,0,1,1,9,7};
        int  a = 7, b = 2, c = 3;
        GoodTriplets gt = new GoodTriplets();
        System.out.println(gt.countGoodTriplets(arr,a,b,c));
    }

    public int countGoodTriplets(int[] arr, int a, int b, int c) {
        int n = arr.length;
        int cnt = 0;
        for(int i=0;i<n-2;i++)   {
            for(int j=i+1;j<n-1;j++)    {
                if(Math.abs(arr[i] - arr[j]) > a) continue;
                for(int k=j+1;k<n;k++)  {
                    if( Math.abs(arr[j] - arr[k]) > b
                    || Math.abs(arr[i] - arr[k]) > c ) continue;
                    if(isSafe(arr,i, j, k,a, b, c))
                        cnt++;
                }
            }
        }
        return cnt;
    }

    public boolean isSafe(int[] arr,int i, int j, int k,
                          int a, int b, int c) {
        if( (Math.abs(arr[i] - arr[j]) <= a ) &&
                (Math.abs(arr[j] - arr[k]) <= b) &&
                (Math.abs(arr[i] - arr[k]) <= c) )    return true;
        return false;
    }

}