package com.apal.leetcode.contests;

import java.util.HashMap;
import java.util.Map;

public class AwesomeString {
    public static void main(String[] args) {
        System.out.println(longestAwesome("3242415"));
        System.out.println(longestAwesome("12345678"));
    }

    public static int longestAwesome(String s) {
     char[] a = s.toCharArray();
     int wl = 0;
     int we = 0;
     Map<Character, Integer> map = new HashMap<>();
     boolean isodd = false;
     for(int i=0;we < a.length;)   {
         char ch = a[we];
         if(map.get(ch) == null)
             map.put(ch,1);
         while (isodd && wl < we)  {
             char ch1 = a[wl];
             int v = map.get(ch);
             if(v > 0)
                map.put(ch1, v-1);
             else
                map.remove(ch1);
             isodd = checkMapForOdd(map);
             wl++;
         }
         we++;
     }
     int cnt = map.size();
     System.out.println("map.size "+map.size());
    if(cnt < a.length)
        cnt++;
    return cnt;
    }

    public static boolean checkMapForOdd(Map<Character, Integer> map)    {
        for(char c : map.keySet())  {
            int x = map.get(c);
            if(x % 2 != 0)  return true;
        }
        return false;
    }
}
