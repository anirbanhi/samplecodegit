package com.apal.leetcode.contests;

import java.util.Stack;

public class ParenthesisBalance {
    public static void main(String[] args) {
        System.out.println(minIns("(()))"));
        System.out.println(minIns("())"));
        System.out.println(minIns("))())("));
        System.out.println(minIns("(((((("));
        System.out.println(minIns(")))))))"));
        System.out.println(minIns("(()))(()))()())))"));
    }

    public static int minIns(String s)  {
        char[] a = s.toCharArray();
        int l = 0;
        int r = 0;
        int mr = 0;
        Stack<Character> st = new Stack<>();
        int cnt = 0;
        for(int i=0;i<a.length;i++) {
            char ch = a[i];
            if(ch == '(') {
                st.push(ch);
                l++;
            }
            if(ch == ')')   {
                if(l > 0)   {
                    if(r > 0)   {
                        r = r - 1;
                        l = l - 1;
                    } else {
                        r = r + 1;
                    }
                } else  {
                    cnt++;
                }
            }

        }
        System.out.println(s+ "  ( "+l+" ) "+r+" cnt "+cnt);
        int tot = 0;
        if(l != 0 && r != 0)    {
            int lreq = 2 * l;
            tot += 1;
            l = l -1;
        }
        tot += 2 * l;
        if(cnt != 0) {
            if (cnt % 2 == 0)
                tot += cnt / 2;
            else
                tot += ((cnt + 1) / 2) + 1;
        }

        return tot;
    }
}
