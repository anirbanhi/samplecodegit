package com.apal.leetcode.arrays;

import java.util.Arrays;

public class LeetCode_548_SplitArrayEqualSum {

    public static void main(String[] args) {
        new LeetCode_548_SplitArrayEqualSum().driver();
    }

    public void driver()    {
        int[] a1 = {1,2,1,1,3};
        int[] a2 = {1,1,1,1,1,5};
        int[] a3 = {5,2,10};

        findEqualSum(a1);
        findEqualSum(a2);
        findEqualSum(a3);
    }

    public void findEqualSum(int[] a)   {
        int[] prefix = new int[a.length];
        int sum = 0;
        for(int i=0;i<a.length;i++) {
            sum = sum + a[i];
            prefix[i] = sum;
        }
        for(int i=0;i<prefix.length;i++) {
            if (prefix[i] == (sum / 2)) {
                System.out.println(Arrays.toString(a) + " Can divide by 2 ");
                return;
            }
        }
        System.out.println(Arrays.toString(a) + " Can't divide by 2 ");
    }
}