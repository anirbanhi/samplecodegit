package com.apal.leetcode.arrays;

public class LeetCode_560_SubarraySum {

    public static void main(String[] args) {
        new LeetCode_560_SubarraySum().driver();
    }

    public void driver()    {
        int[] a = {1,2,3};
        int[] a1 = {1,1,1};
        int[] a2 = {1};
        System.out.println(subArraySum(a2, 1));
    }

    public int subArraySum(int[] a, int k)   {
        int sum = 0;
        int cnt = 0;
        int ws = 0;
        for(int we=0;we<a.length;we++)  {
            sum += a[we];
            if(sum == k)
                cnt++;
            else if (sum > k) {
                while(sum > k && ws < we)  {
                    sum = sum - a[ws];
                    if(sum == k) {
                        cnt++;
                    }
                    ws++;
                }
            }
        }
        return cnt;
    }

}