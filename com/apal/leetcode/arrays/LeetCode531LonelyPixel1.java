package com.apal.leetcode.arrays;

/*

https://leetcode.com/problems/lonely-pixel-i/

 */

/*

 Given a picture consisting of black and white pixels, find the number of black lonely pixels.
The picture is represented by a 2D char array consisting of 'B' and 'W', which means black and white pixels respectively.
A black lonely pixel is character 'B' that located at a specific position where the same row and same column don't have any other black pixels.
Example:

Input:
[['W', 'W', 'B'],
 ['W', 'B', 'W'],
 ['B', 'W', 'W']]

Output: 3
Explanation: All the three 'B's are black lonely pixels.

https://cheonhyangzhang.gitbooks.io/leetcode-solutions/content/solutions-501-550/533-lonely-pixel-ii.html
 */

public class LeetCode531LonelyPixel1 {
    public static void main(String[] args) {

    }
}
