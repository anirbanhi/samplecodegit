package com.apal.leetcode.arrays;

import java.util.*;

/*
https://leetcode.com/problems/k-diff-pairs-in-an-array/
 */
public class LeetCode532KDiffPairs {

    public static void main(String[] args) {
        int[] nums = {3,1,4,1,5};
        int k = 2;
        int[] nums1 = {1,3,1,5,4};
        int k1 = 0;
        int[] nums2 = {1,1,1,1,1};
        int k2 = 0;

        int res = new LeetCode532KDiffPairs().findPairs(nums1, k1);
        System.out.println(res);
    }
        public int findPairsForZero(int[] nums) {
            Arrays.sort(nums);
            int cnt = 0;
            for(int i=0,j=1;i<nums.length-1;i++)  {
                j = i+1;
                if(nums[j] == nums[i]) {
                    cnt++;
                    /*while(j < nums.length &&
                            nums[j] == nums[i])
                        j++;
                        */
                }
            }
            return cnt;
        }

        public int findPairs(int[] nums, int k) {
            int n = nums.length;
            if(k == 0)
                return findPairsForZero(nums);

            Set<Integer> set = new HashSet<>();
            for(int i=0;i<n;i++)    {
                    set.add(nums[i]);
            }

            Set<Pair> setDup = new HashSet<>();

            for(int i=0;i<n;i++)    {
                int t1 = nums[i]+k;
                int t2 = nums[i]-k;

                if(set.contains(t1))
                    setDup.add(new Pair(nums[i], t1));

                if(set.contains(t2))
                    setDup.add(new Pair(nums[i], t2));
            }
            return setDup.size();
        }

        class Pair  {
        //s = smaller, b = bigger
            int s;
            int b;
            public Pair(int a1, int b1) {
                if(a1 > b1) {
                    s = a1;
                    b = b1;
                } else
                {
                    b = a1;
                    s = b1;
                }
            }
            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                Pair pair = (Pair) o;
                return s == pair.s &&
                        b == pair.b;
            }
            @Override
            public int hashCode() {
                return Objects.hash(s, b);
            }
        }
}