package com.apal.leetcode.august.week3;

import com.apal.linkedlist.LLBasicOperations;
import com.apal.linkedlist.Node;

import java.util.HashMap;
import java.util.Map;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/551/week-3-august-15th-august-21st/3430/
 */
public class ReorderList {
    public class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
  }
    public static void main(String[] args) {
        new ReorderList().driver();
    }
    public void driver()    {
        ListNode h = new ListNode(1);
        h.next = new ListNode(2);
        h.next.next = new ListNode(3);
        h.next.next.next = new ListNode(4);
//        h.next.next.next.next = new ListNode(5);
        printLinkList(h,"before");
        reorderList(h);
        printLinkList(h,"after");
    }

    public static void printLinkList(ListNode head, String str)    {
        System.out.println("Printing Link List "+str);
        ListNode temp = head;
        while(temp != null)
        {
            System.out.print(temp.val+" , ");
            temp = temp.next;
        }
        System.out.println();
    }

    public void reorderList(ListNode h) {
        if(h == null)   return;
        if(h.next == null)  return;
        if(h.next.next == null)  return;

        Map<Integer,Integer> map = new HashMap<>();
        ListNode t = h;
        int i = 0;
        while (t != null)   {
            map.put(i,t.val);
            t = t.next;
            i++;

        }
        int len = i;
        ListNode th = null;
        ListNode prev = null;
        if(len % 2 == 0) {
            for (int j = 0; j < len / 2; j++) {
                ListNode t1 = new ListNode(map.get(j));
                ListNode t2 = new ListNode(map.get(len-1-j));
                t1.next = t2;
                if(prev != null)
                    prev.next = t1;
                else
                    th = t1;
                prev = t2;
            }
            //h = th;
            copyToOriginal(h,th);
            return;
        }   else    {
            for (int j = 0; j <= len / 2; j++) {
                ListNode t1 = new ListNode(map.get(j));
                ListNode t2 = new ListNode(map.get(len-1-j));
                t1.next = t2;
                if(prev != null)
                    prev.next = t1;
                else
                    th = t1;
                if(j == (len-1-j)) {
                    t1.next = null;
                    //h = th;
                    copyToOriginal(h,th);
                    return;
                }
                prev = t2;
            }
        }
    }

    public void copyToOriginal(ListNode ho, ListNode h) {
        while(h!=null)  {
            ho.val = h.val;
            ho = ho.next;
            h = h.next;
        }

    }
}