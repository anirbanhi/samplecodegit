package com.apal.leetcode.august.week3;


import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/551/week-3-august-15th-august-21st/3425/
 */
public class NonOverlappingIntervals {
    public static void main(String[] args) {
        NonOverlappingIntervals noi = new NonOverlappingIntervals();
        noi.driver();
    }
    public void driver()    {
        int[][] g = {{1,2}, {2,3}, {3,4}, {1,3}};
        System.out.println("1 - "+eraseOverlapIntervals(g));

        int[][] g1 = {{1,2},{1,2},{1,2}};
        System.out.println("2 - "+eraseOverlapIntervals(g1));

        int[][] g2 = {{1,2},{2,3}};
        System.out.println("0 - "+eraseOverlapIntervals(g2));

        int[][] g3 = {{1,2}, {2,3}, {3,4}, {-100,-2}, {5,7}};
        System.out.println("0 - "+eraseOverlapIntervals(g3));
    }

    public int eraseOverlapIntervals(int[][] g) {
        Comparator<int[]> c = new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if(o1[1] != o2[1])  {
                    return o1[1] - o2[1];
                } else
                    return o1[0] - o2[0];
            }
        };
        PriorityQueue<int[]> pq = new PriorityQueue<>(c);
        for(int i=0;i<g.length;i++)
            pq.offer(g[i]);
        int cnt = 0;
        int[] prev = {Integer.MIN_VALUE,Integer.MIN_VALUE};
        int[] cur = new int[2];
        while(pq.size()>0) {;
            cur = pq.remove();
            if(prev[1] <= cur[0])   {
                System.out.println(Arrays.toString(cur));
                cnt++;
                prev = cur;
            }
        }
        return g.length - cnt;
    }
}