package com.apal.leetcode.august.week1;
/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3410/

 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Your MyHashSet object will be instantiated and called as such:
 * MyHashSet obj = new MyHashSet();
 * obj.add(key);
 * obj.remove(key);
 * boolean param_3 = obj.contains(key);
 */
 
public class DesignHashSet {

    public static void main(String[] args) {
        DesignHashSet ds = new DesignHashSet();
        ds.useHS();
    }
    public void useHS() {
        MyHashSet obj = new MyHashSet();
        obj.add(9);
        obj.remove(19);
        obj.add(14);
        obj.remove(19);
        obj.remove(9);
        obj.add(0);
        obj.add(3);
        obj.add(4);
        obj.add(0);
        obj.remove(9);


    }
    class MyHashSet {

        int mod = 1000000;
        int cnt = 0;
        int[] a ;
        boolean[] c ;
        int getHash(int n, int mod1)   {
            return n%mod1;
        }
        /** Initialize your data structure here. */
        public MyHashSet() {
            a = new int[mod];
            c = new boolean[mod];
            Arrays.fill(a, -1);
        }

        public void add(int key) {
            if(contains(key))   return;
            int indx = getHash(key, mod);
            int i = indx;
            while(true)    {
                if(a[i] == -1 || c[i] == false) {
                    a[i] = key;
                    cnt++;
                    c[i] = true;
                    return;
                } else {
                    i++;
                    if(i >= mod)
                        i = i % mod;
                }
            }
        }

        public void remove(int key) {
            int indx = getHash(key, mod);
            int i = indx;
            while(true)    {
                if(a[i] == -1 || c[i] == false) break;
                if(a[i] == key && c[i] == true) {
                    cnt--;
                    c[i] = false;
                    return;
                } else {
                    i++;
                    if(i >= mod)
                        i = i % mod;
                }
            }
        }

        /** Returns true if this set contains the specified element */
        public boolean contains(int key) {
            int indx = getHash(key, mod);
            int i = indx;
            while(true)    {
                if(c[i] == false)
                    return false;
                if(a[i] == key && c[i] == true) {
                    return true;
                } else if(a[i] != key && c[i] == true){
                    i++;
                    if(i >= mod)
                        i = i % mod;
                }
            }
        }

        public void checkLoadAndRehash()    {
            float load = cnt / mod;
            if(load < 0.75f)    return;
            int newmod = mod*2;
            int[] newa = new int[newmod];
            boolean[] newc = new boolean[newmod];
            for(int i=0;i<mod;i++)  {
                if(c[i]) {
                    int hash = getHash(a[i], newmod);
                    newa[hash] = a[i];
                    newc[hash] = true;
                }
            }
        }
    }
}
