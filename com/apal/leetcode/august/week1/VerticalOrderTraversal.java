package com.apal.leetcode.august.week1;

import java.util.*;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3415/
 */
public class VerticalOrderTraversal {
    public static void main(String[] args) {
        new VerticalOrderTraversal().test();
    }
    public void test()  {
        TreeNode r = new TreeNode(3);
        r.left = new TreeNode(9);
        r.right = new TreeNode(20);
        r.right.left = new TreeNode(15);
        r.right.right = new TreeNode(7);

        System.out.println(verticalTraversal(r));

        TreeNode r1 = new TreeNode(0);
        r1.left = new TreeNode(2);
        r1.right = new TreeNode(1);
        r1.left.left = new TreeNode(3);
        r1.left.left.left = new TreeNode(4);
        r1.left.left.right = new TreeNode(5);
        r1.left.left.left.right = new TreeNode(7);
        r1.left.left.left.right.left = new TreeNode(10);
        r1.left.left.left.right.right = new TreeNode(8);
        r1.left.left.right.left = new TreeNode(6);
        r1.left.left.right.left.left = new TreeNode(11);
        r1.left.left.right.left.right = new TreeNode(9);
        System.out.println(verticalTraversal(r1));
    }
      public class TreeNode {
          int val;
          TreeNode left;
          TreeNode right;
          TreeNode() {}
          TreeNode(int val) { this.val = val; }
          TreeNode(int val, TreeNode left, TreeNode right) {
              this.val = val;
              this.left = left;
              this.right = right;
          }
      }

    public List<List<Integer>> verticalTraversal(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if(root == null)    return res;

        Queue<TreeNode> q = new LinkedList<>();
        Map<Integer, Map<Integer, List<Integer>>> map = new HashMap<>();
        Map<String, Integer> horizPosMap = new HashMap<>();
        q.offer(root);
        int level = 0;
        //level order traversal
        while(!q.isEmpty())  {
            int size = q.size();
            for(int i=0;i<size;i++) {
                TreeNode t = q.poll();
                String key = t.val+"|"+level;
                int horizPos = 0;
                int newlevel = level + 1;
                if(horizPosMap.get(key) != null)    {
                    horizPos = horizPosMap.get(key);
                    horizPosMap.remove(key);
                }
                Map<Integer, List<Integer>> lm = map.get(horizPos);
                if(lm==null) {
                    lm = new HashMap<>();
                    List<Integer> l = new ArrayList<>();
                    l.add(t.val);
                    lm.put(newlevel, l);
                    map.put(horizPos,lm);

                } else {
                    List<Integer> l = lm.get(newlevel);
                    if(l == null)   {
                        l = new ArrayList<>();
                        l.add(t.val);
                        lm.put(newlevel,l);
                    }   else {
                        l.add(t.val);
                        Collections.sort(l);
                    }
                }
                if(t.left != null) {
                    horizPosMap.put(t.left.val+"|"+newlevel,horizPos-1);
                    q.offer(t.left);
                }
                if(t.right != null) {
                    horizPosMap.put(t.right.val + "|" + newlevel, horizPos + 1);
                    q.offer(t.right);
                }
            }
            level++;
        }
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for(Integer r : map.keySet())   {
            if(r < min) min = r;
            if(r > max) max = r;
        }
        for(int i=min;i<=max;i++)   {
            if(map.get(i) == null)  continue;
            Map<Integer, List<Integer>> hm1 = map.get(i);
            List<Integer> lo = new ArrayList<>();
            for(int j=0;j<level+1;j++)    {
                if(hm1.get(j) != null)
                    lo.addAll(hm1.get(j));
            }
            res.add(lo);
        }
        return res;
    }
}