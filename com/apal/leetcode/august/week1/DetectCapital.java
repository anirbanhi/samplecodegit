package com.apal.leetcode.august.week1;

public class DetectCapital {
    public static void main(String[] args) {
        System.out.println("USA "+detect("USA"));
        System.out.println("leetcode "+detect("leetcode"));
        System.out.println("GooGl "+detect("GooGl"));
    }

    public static boolean detect(String word)   {
        if(word.length() < 2)  return true;
        int capcnt = 0;
        boolean fisrtCap =  word.charAt(0) <= 'Z';
        boolean secndCap =  word.charAt(1) <= 'Z';
        if(!fisrtCap&&secndCap) return false;
        int len = word.length();
        for(char ch : word.toCharArray())   {
            if(ch >= 65 && ch <= 91 )
                capcnt++;
        }
        if(capcnt == len)   return true;
        if(capcnt == 0) return true;
        if(capcnt == 1 && Character.isUpperCase(word.charAt(0)))   return true;
        return false;
    }


}
