package com.apal.leetcode.august.week1;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3412/
 */

public class PowerOf4 {

    public static void main(String[] args) {
        System.out.println("24 "+isPowerOfFour(24));
        System.out.println("7 "+isPowerOfFour(7));
        System.out.println("256 "+isPowerOfFour(256));
        System.out.println("0 "+isPowerOfFour(0));
        System.out.println("1 "+isPowerOfFour(1));
        System.out.println("5 "+isPowerOfFour(5));
    }

    public static boolean isPowerOfFour(int num) {
            if(anyOddBitIsSet(num)) return false;
            if(anyMultipleBitsAreSet(num)) return false;
            if(onlyOne4PowerBitIsSet(num))  return true;
            return false;
    }

    public static boolean onlyOne4PowerBitIsSet(int num)    {
        for(int i=0;i<32;i=i+2)   {
            if(isIthBitSet(num,i)) {
                return true;
            }
        }
        return false;
    }

    public static boolean anyMultipleBitsAreSet(int num)  {
        boolean flag = false;
        for(int i=0;i<32;i=i+2)   {
            if(isIthBitSet(num,i)) {
                if(!flag)
                    flag = true;
                else
                    return true;
            }
        }
        return flag;
    }

    public static boolean anyOddBitIsSet(int num)  {
        for(int i=1;i<32;i=i+2)   {
            if(isIthBitSet(num,i))
                return true;
        }
        return false;
    }

    public static boolean isIthBitSet(int num, int x)   {
        int mask = 1 << x;
        if( (num & mask) == 0)  return false;
        return true;
    }

}