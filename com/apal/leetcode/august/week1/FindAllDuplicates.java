package com.apal.leetcode.august.week1;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3414/
 */

import java.util.ArrayList;
import java.util.List;

public class FindAllDuplicates {
    public static void main(String[] args) {
        int[] a = {4,3,2,7,8,2,3,1};
        System.out.println(findDuplicates(a));
    }

    public static List<Integer> findDuplicates(int[] nums) {
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<nums.length;i++)  {
            int k = nums[i];
            if(k<0) {
                k = -k;
            }
            if(nums[k-1] < 0)
                list.add(k);
            else
                nums[k-1] = -nums[k-1];
        }
        return list;
    }
}