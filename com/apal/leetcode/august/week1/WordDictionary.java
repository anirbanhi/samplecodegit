package com.apal.leetcode.august.week1;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3413/
 */


import java.util.HashMap;
import java.util.Map;

public class WordDictionary {

    public static void main(String[] args) {
        WordDictionary wd = new WordDictionary();
        wd.addWord("bad");
        wd.addWord("dad");
        wd.addWord("mad");
        System.out.println("wd.search(\"pad\") - false = "+wd.search("pad"));
        System.out.println("wd.search(\"bad\") - true = "+wd.search("bad"));
        System.out.println("wd.search(\".ad\") - true = "+wd.search(".ad"));
        System.out.println("wd.search(\"b..\") - true = "+wd.search("b.."));

        wd = new WordDictionary();
        wd.addWord("a");
        wd.addWord("a");
        System.out.println("wd.search(\".\") - true = "+wd.search("."));
        System.out.println("wd.search(\"a\") - true = "+wd.search("a"));
        System.out.println("wd.search(\"aa\") - false = "+wd.search("aa"));
        System.out.println("wd.search(\"a\") - true = "+wd.search("a"));
        System.out.println("wd.search(\".a\") - false = "+wd.search(".a"));
        System.out.println("wd.search(\"a.\") - false = "+wd.search("a."));
    }

    class Trie  {
        Map<Character, Trie>    child;
        boolean isword;

        public Trie()   {
            isword = false;
            child = new HashMap<>();
        }
    }

    Trie head;
    /** Initialize your data structure here. */
    public WordDictionary() {
        head = new Trie();
    }

    /** Adds a word into the data structure. */
    public void addWord(String word) {
        Trie t = head;
        int len = word.length();
        for(int i=0;i<len;i++)    {
            char ch = word.charAt(i);
            if(t.child.get(ch) != null) {
                t = t.child.get(ch);
                if(i == len-1)  {
                    t.isword = true;
                }
            } else {
                Trie t1 = new Trie();
                t.child.put(ch,t1);
                t= t1;
                if(i == len-1)  {
                    t.isword = true;
                }
            }
        }
    }

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean search(String word) {
        return searchRec(word,head);
    }

    public boolean searchRec(String word, Trie th) {
        Trie t = th;
        int len = word.length();
        for(int i=0;i<len;i++)  {
            char ch = word.charAt(i);
            if(ch == '.')   {
                //if(i == len-1 && t.child.size() != 0)  return true;
                for(Character c : t.child.keySet())   {
                    boolean resp =
                            searchRec(word.substring(i+1),t.child.get(c));
                    if(resp)    return true;
                }
            }
            if(t.child.get(ch) != null) {
                t = t.child.get(ch);
                if(i == len-1)    {
                    if(t.isword)
                        return true;
                    else
                        return false;
                }
            } else return false;
        }
        return t.isword;
    }
}