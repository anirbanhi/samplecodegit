package com.apal.leetcode.august.week1;
/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3411/
 */
public class ValidPalindrome {
    public static void main(String[] args) {
        String s1 = "A man, a plan, a canal: Panama";
        String s2 = "race a car";


        System.out.println(isPalindrome(s1));
        System.out.println(isPalindrome(s2));
    }

    public static boolean isPalindrome(String s) {
        char[] a = new char[s.length()];
        a = s.toCharArray();
        return isPal(a,0,a.length-1);
    }

    public static boolean isPal(char[] a, int i, int j) {
        for(int k=0,l=a.length-1;k<l;) {
            char c1 = a[k];
            char c2 = a[l];
            if(!Character.isLetterOrDigit(c1))  {
                 k++;
                 continue;
            }
            if(!Character.isLetterOrDigit(c2))  {
                l--;
                continue;
            }
            if(Character.isDigit(c1) && Character.isDigit(c2))  {
                if(c1 == c2)    {
                k++;    l--;    continue;
                }else
                    return false;
            }
            if(Character.toLowerCase(c1) ==
                    Character.toLowerCase(c2))  {
                k++;    l--;    continue;
            }   return false;

        }
        return true;
    }
}
