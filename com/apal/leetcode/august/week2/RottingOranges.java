package com.apal.leetcode.august.week2;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/550/week-2-august-8th-august-14th/3418/
 */

public class RottingOranges {
    public static void main(String[] args) {
        new RottingOranges().driver();
    }

    public void driver()    {
        int[][] a1 = {
                {2,1,1},
                {1,1,0},
                {0,1,1}};
        int o1 = 4;
        System.out.println(o1+" - "+orangesRotting(a1));

        int[][] a2 = {{2,1,1}, {0,1,1}, {1,0,1}};
        int o2 = -1;
        System.out.println(o2+" - "+orangesRotting(a2));

        int[][] a3 = {{0,2}};
        int o3 = 0;
        System.out.println(o3+" - "+orangesRotting(a3));

        int[][] a4 = {{0}};
        int o4 = 0;
        System.out.println(o4+" - "+orangesRotting(a4));

        int[][] a5 = {{1},{2},{2}};
        int o5 = 1;
        System.out.println(o5+" - "+orangesRotting(a5));

        int[][] a6 = {{2,1,0,2}};
        int o6 = 1;
        System.out.println(o6+" - "+orangesRotting(a6));

        int[][] a7 = {{1},{2},{1},{2}};
        int o7 = 1;
        System.out.println(o7+" - "+orangesRotting(a7));

    }

    public int orangesRotting(int[][] g) {
        maxl = 0;
        countOneMax = 0;
        cntOneFound = 0;

        int rl = g.length;
        int cl = g[0].length;
        int cntOne = 0;
        int cntTwo = 0;
        int r1 = -1;
        int c1 = -1;
        boolean found = true;
        for(int i=0;i<rl;i++)   {
            for(int j=0;j<cl;j++)   {
                if(g[i][j] == 2 && found) {
                    r1 = i;
                    c1 = j;
                    found = false;
                }
                if(g[i][j] == 1)
                    cntOne++;
                if(g[i][j] == 2)
                    cntTwo++;
            }
        }
        countOneMax = cntOne;
        if(cntOne == 0) return 0;

        if(r1==-1 && c1 ==-1)   {
            return -1;
        }

        int[][] v = new int[rl][cl];
        v[r1][c1] = 1;
        rot(g,v,r1,c1,0);

        int cnt = 0;
        for(int i=0;i<rl;i++)   {
            for(int j=0;j<cl;j++)   {
                if(g[i][j] == 1)    return -1;
                if(v[i][j] == 1)
                    cnt ++;
            }
        }

        //System.out.println("initial "+cntTot+" found "+cnt+" maxl "+maxl);
      //  if((cntOne+cntTwo) == cnt)
            return maxl;
       // return -1;
    }

    int maxl = 0;
    int countOneMax = 0;
    int cntOneFound = 0;
    public boolean rot(int[][] g, int[][] v, int r, int c, int l)   {
        if(r < 0 || c < 0)  return false;
        if(r >= g.length || c >= g[0].length)  return false;
        if(g[r][c] == 1)    return false;
        if(cntOneFound == countOneMax)  return false;
        int r1=0;
        int c1 = 0;
        if(l > maxl)    {
            maxl = l;
        }
        boolean hasonefound = false;
        r1 = r + 1; c1 = c;
        if(isSafe(g,v,r1,c1))   {
            v[r1][c1] = 1;
            if(g[r1][c1] == 1) {
                g[r1][c1] = 2;
                cntOneFound++;
                if((l+1) > maxl)    maxl = l+1;
                hasonefound = true;
            }
            hasonefound = hasonefound || rot(g,v,r1,c1,l+1);
        }
        r1 = r - 1; c1 = c;
        if(isSafe(g,v,r1,c1))   {
            v[r1][c1] = 1;
            if(g[r1][c1] == 1) {
                g[r1][c1] = 2;
                cntOneFound++;
                if((l+1) > maxl)    maxl = l+1;
                hasonefound = true;
            }
            hasonefound = hasonefound || rot(g,v,r1,c1,l+1);
        }
        r1 = r; c1 = c + 1;
        if(isSafe(g,v,r1,c1))   {
            v[r1][c1] = 1;
            if(g[r1][c1] == 1) {
                g[r1][c1] = 2;
                cntOneFound++;
                if((l+1) > maxl)    maxl = l+1;
                hasonefound = true;
            }
            hasonefound = hasonefound || rot(g,v,r1,c1,l+1);
        }
        r1 = r; c1 = c - 1;
        if(isSafe(g,v,r1,c1))   {
            v[r1][c1] = 1;
            if(g[r1][c1] == 1) {
                g[r1][c1] = 2;
                cntOneFound++;
                if((l+1) > maxl)    maxl = l+1;
                hasonefound = true;
            }
            hasonefound = hasonefound || rot(g,v,r1,c1,l+1);
        }
        if(!hasonefound)    maxl--;
        return hasonefound;
    }

    public boolean isSafe(int[][] g, int[][] v, int r, int c) {
        if(r < 0 || c < 0)  return false;
        if(r >= g.length || c >= g[0].length)   return false;
        if(v[r][c] == 1 || g[r][c] == 0)    return false;
        //g[r][c] = 1 / 2 | fresh / rotten
        return true;
    }
}