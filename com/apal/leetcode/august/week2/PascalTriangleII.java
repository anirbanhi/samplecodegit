package com.apal.leetcode.august.week2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/550/week-2-august-8th-august-14th/3421/
 */
public class PascalTriangleII {

    public static void main(String[] args) {
        PascalTriangleII ps = new PascalTriangleII();
        ps.driver();
    }

    public void driver()    {
        System.out.println(getRow(0));
        System.out.println(getRow(1));
        System.out.println(getRow(2));
        System.out.println(getRow(3));
        System.out.println(getRow(4));
        System.out.println(getRow(5));
    }

    public List<Integer> getRow(int ri) {
        if(ri == 0) {
            List<Integer> l = new ArrayList<>();
            l.add(1);
            return l;
        }
        if(ri == 1) {
            List<Integer> l = new ArrayList<>();
            l.add(1);l.add(1);
            return l;
        }
        List<Integer> prev = new ArrayList<>();
        prev.add(1);prev.add(1);
        for(int i=2;i<=ri;i++)   {
            List<Integer> res = getval(i,prev);
            if(i==ri)
                return res;
            prev = new ArrayList<>();
            prev.addAll(res);
        }
        return null;
    }

    public List<Integer> getval(int k, List<Integer> prev)   {
        List<Integer> res = new ArrayList<>();
        res.add(0,1);
        for(int i=1;i<k;i++)  {
            res.add(i,prev.get(i-1)+prev.get(i));
        }
        res.add(k,1);
        return res;
    }

}