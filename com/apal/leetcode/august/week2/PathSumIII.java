package com.apal.leetcode.august.week2;

/*
[1,0,1,1,2,0,-1,0,1,-1,0,-1,0,1,0]
2
Output: 11
Expected: 13
 */

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/550/week-2-august-8th-august-14th/3417/
 */
public class PathSumIII {
    public static void main(String[] args) {
        new PathSumIII().driver();
    }
    public class TreeNode {
      int val;
      public TreeNode left;
      public TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
    public void driver()    {
        TreeNode r = new TreeNode(10);
        r.left = new TreeNode(5);
        r.right = new TreeNode(-3);
        r.left.left = new TreeNode(3);
        r.left.right = new TreeNode(2);
        r.left.left.left = new TreeNode(3);
        r.left.left.right = new TreeNode(-2);
        r.left.right.right = new TreeNode(1);
        r.right.right = new TreeNode(11);
       System.out.println("3 - " + pathSum(r,8));

        TreeNode r1 = new TreeNode(1);
        r1.right = new TreeNode(2);
        r1.right.right = new TreeNode(3);
        r1.right.right.right = new TreeNode(4);
        r1.right.right.right.right = new TreeNode(5);
        System.out.println("2 - "+pathSum(r1,3));

        TreeNode r2 = new TreeNode(0);
        r2.left = new TreeNode(1);
        r2.right = new TreeNode(1);
        System.out.println("4 - " +pathSum(r2,1));
    }

    int cnt = 0;
    public int pathSum(TreeNode root, int sum) {
        cnt = 0;
        findPaths(root,sum,0,new HashMap<Key, Boolean>());
        return cnt;
    }

    public void findPaths(TreeNode r, int target, Integer sum,
                          Map<Key,Boolean> map) {
        if(r == null)   return;
        Key k = new Key(r,sum);
        if(map.containsKey(k))  return;
        map.put(k,Boolean.TRUE);

        if(Objects.isNull(sum))
            sum = new Integer(r.val);
        else
            sum += r.val;
        if(sum == target)   cnt++;

        findPaths(r.left,target,sum,map);
        findPaths(r.right,target,sum,map);
        findPaths(r.left,target,null,map);
        findPaths(r.right,target,null,map);
    }

    class Key   {
        TreeNode node;
        TreeNode l;
        TreeNode r;
        //Integer sum;
        String sumStr;
        public Key(TreeNode tn, Integer s) {
            node = tn;
            //sum = s;
            if(Objects.isNull(s))
                sumStr = "null";
            else
                sumStr = Integer.toString(s);
            l = tn.left;
            r = tn.right;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Key key = (Key) o;
            return node.equals(key.node) &&
                    Objects.equals(l, key.l) &&
                    Objects.equals(r, key.r) &&
                    sumStr.equals(key.sumStr);
        }

        @Override
        public int hashCode() {
            return Objects.hash(node, l, r, sumStr);
        }
    }
}