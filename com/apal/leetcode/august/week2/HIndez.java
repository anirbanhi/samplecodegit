package com.apal.leetcode.august.week2;

import java.util.Arrays;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/550/week-2-august-8th-august-14th/3420/
 */
public class HIndez {
    public static void main(String[] args) {
        new HIndez().driver();
    }
    public void driver()    {
        int[] a = {3,0,6,1,5};
        int[] a2 = {1};
        int[] a3 = {0};
        int[] a4 = {1,1};
        int[] a5 = {10,12};
        int[] a6 = {};
        System.out.println("3 - " +hIndex(a));
        System.out.println("1 - " +hIndex(a2));
        System.out.println("1 - " +hIndex(a4));
        System.out.println("0 - " +hIndex(a3));
        System.out.println("2 - " +hIndex(a5));
        System.out.println("0 - " +hIndex(a6));
    }

    public int hIndex(int[] a) {
        if(a.length == 0)   return 0;
        int len = a.length;
        //Arrays.sort(a);

        for(int i=len;i>-1;i--)  {
            int cnt = 0;
            for(int j=len-1;j>-1;j--)  {
                if(a[j]>=i)
                    cnt++;
            }
            if(cnt>=i)
                return i;
        }
        return 0;
    }
}