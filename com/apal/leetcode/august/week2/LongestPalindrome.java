package com.apal.leetcode.august.week2;

import com.apal.backtracking.HamilTonianCycle;

import javax.print.attribute.HashAttributeSet;
import java.util.HashMap;
import java.util.Map;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/550/week-2-august-8th-august-14th/3423/
 */
public class LongestPalindrome {
    public static void main(String[] args) {
        new LongestPalindrome().driver();
    }
    public void driver()    {
        System.out.println("7 - "+longestPalindrome("abccccdd"));
    }

    public int longestPalindrome(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0;i<s.length();i++)   {
            char ch = s.charAt(i);
            map.put(ch,map.getOrDefault(ch,0)+1);
        }
        boolean gotmiddle = false;
        int cnt = 0;
        for(char c : map.keySet())  {

            int n = map.get(c);
            System.out.println(c+" "+n+" cnt = "+cnt);
            if(n==1)   {
                gotmiddle = true;
                continue;
            }
            if(n%2 == 0)    {
                cnt += n;
                continue;
            }
            if(n%2 != 0)    {
                cnt += n-1;
                if(n>2 && !gotmiddle)
                    gotmiddle = true;
            }
        }
        if(gotmiddle)
            cnt++;
        return cnt;
    }
}
