package com.apal.leetcode.august.week4;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/552/week-4-august-22nd-august-28th/3438/
 */
public class FindRightInterval {
    public static void main(String[] args) {
        new FindRightInterval().driver();
    }

    public void driver()    {
        int[][] a1 = {{1,4}, {2,3}, {3,4}};
        int[][] a2 = {{3,4}, {2,3}, {1,2}};
        int[][] a3 = {{1,2}};

        System.out.println("[-1, 2, -1] " + Arrays.toString(findRightInterval(a1)));
        System.out.println("[-1, 0, 1] " + Arrays.toString(findRightInterval(a2)));
        System.out.println("[-1] " + Arrays.toString(findRightInterval(a3)));
    }

    public int[] findRightInterval(int[][] intervals) {
        int len = intervals.length;
        if(len == 0)
            return new int[]{};
        if(len == 1)
            return new int[]{-1};
        PriorityQueue<Elem> pq = new PriorityQueue<>(comp);
        for(int i=0;i<intervals.length;i++) {
            Elem e = new Elem(intervals[i][0],intervals[i][1],i);
            pq.add(e);
        }

        int[] resp = new int[len];
        Arrays.fill(resp, -1);

        while(pq.size() > 0)    {
            Elem e = pq.remove();
            if(pq.size() > 0) {
                Elem next = pq.peek();
                resp[e.pos] = next.pos;
            } else {
                resp[e.pos] = -1;
            }
        }
        return resp;
    }

    class Elem  {
        int st;
        int end;
        int pos;
        public Elem(int st, int end, int p)    {
            this.st = st;
            this.end = end;
            this.pos = p;
        }
    }

    Comparator<Elem> comp = new Comparator<Elem>() {
        @Override
        public int compare(Elem o1, Elem o2) {
            int ret = 0;
            ret = o2.end - o1.end;
            if(ret!=0)  return ret;
            ret = o2.st - o1.st;
            if(ret!=0)  return ret;
            ret = o2.pos - o1.pos;
            if(ret!=0)  return ret;
            return ret;
        }
    };
}