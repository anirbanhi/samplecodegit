package com.apal.leetcode.august.week4;
/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/552/week-4-august-22nd-august-28th/3436/
 */
public class MinimumCostForTickets {

    public static void main(String[] args) {

        new MinimumCostForTickets().driver();

    }

    public void driver()    {
        int[] days = {1,4,6,7,8,20};
        int[] costs = {2,7,15};
        //System.out.println("11 - "+mincostTickets(days,costs));

        int[] days1 = {1,2,3,4,5,6,7,8,9,10,30,31};
        int[] costs1 = {2,7,15};
        //System.out.println("17 - "+mincostTickets(days1,costs1));

        int[] days2 = {1,4,6,9,10,11,12,13,14,15,16,17,18,20,21,22,23,27,28};
        int[] costs2 = {3,13,45};
        System.out.println("17 - "+mincostTickets(days2,costs2));
    }

    public int mincostTickets(int[] days, int[] costs) {
        min = Integer.MAX_VALUE;
        int[][] dp = new int[days.length][30];
        minCost(days,costs,0,0,0);
        return min;
    }

    int min;
    public void minCost(int[] d, int[] c, int prev,
                        int cur, int cost)    {
        System.out.println("prev= "+prev+" cur = "+cur+" cost = "+cost);
        if(cur >= d.length) {
            if(cost < min) {
                min = cost;
            }
            return;
        } else {
            int days = 0;
            if (cur > 0)
                days = d[cur] - d[cur - 1];
            if (prev > 0 && prev >= days) {
                minCost(d, c, prev - days, cur + 1, cost);
            }
            minCost(d, c, 0, cur + 1, cost + c[0]);
            minCost(d, c, 6, cur + 1, cost + c[1]);
            minCost(d, c, 29, cur + 1, cost + c[2]);
        }
    }

}