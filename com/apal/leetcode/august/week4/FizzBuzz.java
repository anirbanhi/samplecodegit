package com.apal.leetcode.august.week4;

import java.util.ArrayList;
import java.util.List;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/552/week-4-august-22nd-august-28th/3437/
 */
public class FizzBuzz {
    public static void main(String[] args) {
        new FizzBuzz().driver();
    }

    public void driver()    {
        System.out.println(fizzBuzz(15));
    }

    public List<String> fizzBuzz(int n) {
        List<String> op = new ArrayList<>();
        for(int i=1;i<=n;i++)   {
            if(i%3 == 0 && i%5 == 0)
                op.add("FizzBuzz");
            else if(i%3 == 0)
                op.add("Fizz");
            else if(i%5 == 0)
                op.add("Buzz");
            else
                op.add(String.valueOf(i));
        }
        return op;
    }
}
