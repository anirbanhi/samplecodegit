package com.apal.leetcode.august.week4;
/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/552/week-4-august-22nd-august-28th/3435/
 */
public class SumOfLeftLeaves {
    public static void main(String[] args) {
        new SumOfLeftLeaves().driver();
    }

    public class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }

    public void driver()    {
        TreeNode r = new TreeNode(3);
        r.left = new TreeNode(9);
        r.right = new TreeNode(20);
        r.right.left = new TreeNode(15);
        r.right.right = new TreeNode(7);

        System.out.println("24 "+sumOfLeftLeaves(r));
    }
    public int sumOfLeftLeaves(TreeNode root) {
        return sumOfLeft(root,false);
    }

    public int sumOfLeft(TreeNode r, boolean isLeft)    {
        if(r==null) return 0;
        int ret = 0;
        if(isLeft && r.left==null && r.right==null)
            ret = r.val;
        ret += sumOfLeft(r.left,true)+
                sumOfLeft(r.right,false);
        return ret;
    }
}
