package com.apal.leetcode.august.week4;

/*
https://leetcode.com/explore/challenge/card/august-leetcoding-challenge/549/week-1-august-1st-august-7th/3413/
 */

import java.util.*;

public class StreamChecker {

    public static void main(String[] args) {
        String[] words = {"cd","f","kl"};
        StreamChecker streamChecker = new StreamChecker(words);
        System.out.println("false "+streamChecker.query('a'));
        System.out.println("false "+streamChecker.query('b'));
        System.out.println("false "+streamChecker.query('c'));
        System.out.println("true "+streamChecker.query('d'));
        System.out.println("false "+streamChecker.query('e'));

        System.out.println("true "+streamChecker.query('f'));
        System.out.println("false "+streamChecker.query('g'));
        System.out.println("false "+streamChecker.query('h'));
        System.out.println("false "+streamChecker.query('i'));
        System.out.println("false "+streamChecker.query('j'));

        System.out.println("false "+streamChecker.query('k'));
        System.out.println("true "+streamChecker.query('l'));
    }

    class Trie  {
        Map<Character, Trie>    child;
        boolean isword;

        public Trie()   {
            isword = false;
            child = new HashMap<>();
        }
    }
    Trie head;
    Queue<Character> q;
    int trieLen;
    /** Initialize your data structure here. */
    public StreamChecker(String[] words) {
        head = new Trie();
        for(String s : words) {
            addWord(s);
            trieLen = Math.max(s.length(),trieLen);
        }
        q = new LinkedList<>();
    }
    /** Adds a word into the data structure. */
    public void addWord(String word) {
        Trie t = head;
        int len = word.length();
        for(int i=0;i<len;i++)    {
            char ch = word.charAt(i);
            if(t.child.get(ch) != null) {
                t = t.child.get(ch);
                if(i == len-1)  {
                    t.isword = true;
                }
            } else {
                Trie t1 = new Trie();
                t.child.put(ch,t1);
                t= t1;
                if(i == len-1)  {
                    t.isword = true;
                }
            }
        }
    }

    /** Returns if the word is in the data structure. A word could contain the dot character '.' to represent any one letter. */
    public boolean query(char ch) {
        return queryWithRec(ch);
    }

    public boolean queryWithRec(char ch)    {
        int size = q.size();
        if(size == trieLen) {
            q.poll();
        }
        q.offer(ch);
        List<String> words = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        for(Character c : q)    {
            sb.append(c);
        }
        String allqs = sb.toString();
        int allqsLen = allqs.length();
        for(int i=allqsLen-1;i>=0;i--) {
            String s = allqs.substring(i,allqsLen);
            words.add(s);
        }

        for(String s : words)   {
            if(searchRec(s,head))
                return true;
        }
        return false;
    }
    public boolean searchRec(String word, Trie th) {
        Trie t = th;
        int len = word.length();
        for(int i=0;i<len;i++)  {
            char ch = word.charAt(i);
            if(ch == '.')   {
                //if(i == len-1 && t.child.size() != 0)  return true;
                for(Character c : t.child.keySet())   {
                    boolean resp =
                            searchRec(word.substring(i+1),t.child.get(c));
                    if(resp)    return true;
                }
            }
            if(t.child.get(ch) != null) {
                t = t.child.get(ch);
                if(i == len-1)    {
                    if(t.isword)
                        return true;
                    else
                        return false;
                }
            } else return false;
        }
        return t.isword;
    }
}