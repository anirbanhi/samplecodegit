package com.apal.leetcode.design.questions;

/*
https://leetcode.com/problems/serialize-and-deserialize-binary-tree/
*/

public class LeetCode_297_Serialize {

    public static void main(String[] args) {
        new LeetCode_297_Serialize().driver();
    }

    public void driver()    {
        TreeNode head = new TreeNode(1);
        head.left = new TreeNode(2);
        head.right = new TreeNode(3);
        head.right.left = new TreeNode(4);
        head.right.right = new TreeNode(5);

        serialize(head, -1,false);
        System.out.println();
        for(int i : arr)
            System.out.print(i+" -> ");
    }

    int[] arr = new int[10];
    public void serialize(TreeNode h, int parent, boolean isleft) {
        int indx = -1;
        if(isleft)
            indx = parent*2 ;
        else
            indx = parent*2 + 1;
        if(parent == -1)
            indx = 0;
        if(h != null)
            arr[indx] = h.val;
        else {
            arr[indx] = -99;
            return;
        }
        serialize(h.left, indx, true);
        serialize(h.right, indx, false);
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
}