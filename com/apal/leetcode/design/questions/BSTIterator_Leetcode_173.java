package com.apal.leetcode.design.questions;

import java.util.Stack;

public class BSTIterator_Leetcode_173 {

    public static void main(String[] args) {
        new BSTIterator_Leetcode_173().driver();
    }

    Stack<Noded> stack = new Stack<>();

    public void driver()    {
        Noded head = buildtree();
        inorder(head);
        leftmostiterator(head);

        System.out.println();
        System.out.println(next());
        System.out.println(next());
        System.out.println(next());
        System.out.println(next());
    }

    public int next()   {
        Noded top = stack.pop();

        //stack will always contain inorder sorted data.
        //suppose the sub-tree is now looking like this.
        /*
                    5
             3              13
                        9       15
                    7       11

            stack is now having 13
            if 13 has to be returned, before returning, we need to store
            right side of 13 in sorted order as well in stack.
            so, popped 13. stack is now empty.
            next insert 11 , 15

         */

        if(top.right != null)   {
            leftmostiterator(top.right);
        }
        return top.data;
    }

    public void leftmostiterator(Noded t)  {
        while(t != null)
        {
            stack.push(t);
            t = t.left;
        }
    }

    public void inorder(Noded h)   {
        if(h==null)
            return;
        inorder(h.left);
        System.out.print(h.data + " -> ");
        inorder(h.right);
    }

    public Noded buildtree() {
        Noded head = new Noded(5);
        head.left = new Noded(3);
        head.right = new Noded(13);
        head.right.right = new Noded(15);
        head.right.left = new Noded(9);
        head.right.left.left = new Noded(7);
        head.right.left.right = new Noded(11);
        return head;
    }

    class Noded {
        int data;
        Noded left;
        Noded right;

        public Noded(int v)  {
            this.data = v;
            left = null;
            right = null;
        }
    }
}