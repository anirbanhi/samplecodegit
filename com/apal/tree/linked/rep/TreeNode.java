package com.apal.tree.linked.rep;

public class TreeNode<T> {
    T data;
    TreeNode<T> right;
    TreeNode<T> left;

    public TreeNode(T dataInput)   {
        data = dataInput;
        this.right = null;
        this.left = null;
    }
}
