package com.apal.tree.linked.rep;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class LinkedTreeMain {
    public static void main(String[] args)  {
        TreeLinkedNodeMethods<Integer> treeLinked = new TreeLinkedNodeMethods<>();
        TreeNode<Integer> root = treeLinked.createTree();
        Function<String,Boolean> funcPrint = s -> {
                                                    System.out.println(s);
                                                    return true;
                                                  };
        //funcPrint.apply("Pre order recursive ");

        TreeLinkedNodeTravarsalMethods treeMethod = new TreeLinkedNodeTravarsalMethods();
        //treeMethod.findNthNodeInInorder(root,6);
        //treeLinked.inOrder(root);
        //treeLinked.levelOrder(root);
        //treeLinked.levelOrderWithLine(root);
        //treeLinked.reverseLevelOrder(root);
        //System.out.println("Height is "+treeMethod.findHeight(root));
        //treeMethod.printLeafNodes(root);
        //CheckingAndPrinting cp = new CheckingAndPrinting();

        //int node = 3;
        //System.out.println(" height of "+node+" is "+cp.findHeight(root,node,0));
        //System.out.println(" depth of "+node+" is "+cp.findDepth(root,node,0));

        //treeLinked.preOrderRecursive(root);
        treeLinked.preOrder(root);
        //TreeLinkedNodeMethods.ViewConstant vc = new TreeLinkedNodeMethods.ViewConstant();
        //System.out.println("");
        //treeLinked.leftView(root,vc,1); //passing 0 will not print root, so choose accordingly
        //vc.maxLevel = 0;
        //System.out.println("");
        //treeLinked.rightView(root,vc,1);
        //Integer[] path = new Integer[50];
        //treeLinked.printKlengthPath(root,path,0);

        //TopView And BottomView based on BiFunction, one sinple change does the reverse. - START
        System.out.println();
        BiFunction<Integer,Integer,Boolean> biTop = (a,b) -> a > b ? true : false;
        BiFunction<Integer,Integer,Boolean> biBottom = (a,b) -> a < b ? true : false;
        Map<Integer, TreeLinkedNodeMethods.NodeContainer<Integer>> map = new HashMap<>();
        map = new HashMap<>();

        treeLinked.printTopView(root,0,0,map,biBottom);
        for(Integer i : map.keySet())
        {
            System.out.print(map.get(i).node.data +" ");
        }
        System.out.println();
        //TopView And BottomView based on BiFunction, one sinple change does the reverse. - END
    }
}
