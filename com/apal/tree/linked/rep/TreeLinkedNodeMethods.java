package com.apal.tree.linked.rep;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class TreeLinkedNodeMethods<T> {
    public TreeNode<Integer> createTree()    {
        TreeNode<Integer> root = new TreeNode<>(5);
        root.left = new TreeNode<>(8);
        root.right = new TreeNode<>(6);
        root.left.right = new TreeNode<>(9);
        root.left.right.left = new TreeNode<>(4);
        root.left.right.right = new TreeNode<>(2);
        root.right.left = new TreeNode<>(3);

        return root;
    }

    /**
     *
     *
     * @return
     */
    public TreeNode<Integer> createBigTree()    {
        TreeNode<Integer> root = new TreeNode<>(1);

        root.left = new TreeNode<>(2);
        root.right = new TreeNode<>(3);

        root.left.left = new TreeNode<>(4);
        root.left.right = new TreeNode<>(5);

        root.right.left = new TreeNode<>(6);
        root.right.right = new TreeNode<>(7);

        root.left.left.left = new TreeNode<>(8);
        root.left.left.right = new TreeNode<>(9);
        root.left.right.left = new TreeNode<>(10);
        root.left.right.right = new TreeNode<>(11);

        root.right.left.left = new TreeNode<>(12);
        root.right.left.right = new TreeNode<>(13);
        root.right.right.left = new TreeNode<>(14);
        root.right.right.right = new TreeNode<>(15);

        return root;
    }

    public void preOrderRecursive(TreeNode<T> root) {
        Stack<TreeNode<T>> st = new Stack<>();
        st.push(root);
        Predicate<TreeNode<T>> pIsNull = n -> n == null;
        Function<TreeNode<T>,TreeNode<T>> fStPush = n -> pIsNull.negate().test(n)?st.push(n):n;
        System.out.println("PreOrder Iterative \n");
        while(!st.isEmpty()) {
            TreeNode<T> node = st.pop();
            if(pIsNull.test(node))
                continue;
            System.out.print(node.data+"-> ");
            fStPush.apply(node.right);
            fStPush.apply(node.left);
        }
        System.out.println();
    }

    public void preOrder(TreeNode<T> root )  {
        if(root == null)
            return;
        System.out.print(root.data+" , ");
        preOrder(root.left);
        preOrder(root.right);
    }

    public void inOrder(TreeNode<T> root )  {
        if(root == null)
            return;
        inOrder(root.left);
        System.out.print(root.data+" , ");
        inOrder(root.right);
    }

    public void postOrder(TreeNode<T> root )  {
        if(root == null)
            return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.data+" , ");
    }

    public void levelOrder(TreeNode<Integer> root)    {
        Consumer<String> c = s -> System.out.println(s);
        c.accept("");
        LinkedList<TreeNode> q = new LinkedList<>();

        if(root == null)
            return;
        q.add(root);
        while (!q.isEmpty())    {
            TreeNode<Integer> tn = q.removeFirst();
            System.out.print(tn.data +" ->");

            if(tn.left != null)
                q.addLast(tn.left);
            if(tn.right != null)
                q.addLast(tn.right);
        }
        c.accept("");
    }

    public void levelOrderWithLine(TreeNode<Integer> root)    {
        Consumer<String> c = s -> System.out.println(s);
        c.accept("");
        TreeNode<Integer> blank = new TreeNode<>(Integer.MAX_VALUE);
        LinkedList<TreeNode> q = new LinkedList<>();

        if(root == null)
            return;
        q.addLast(root);
        q.addLast(blank);
        while (!q.isEmpty())    {
            TreeNode<Integer> tn = q.removeFirst();
            if(tn==blank)
            {
                System.out.println("");
                if(!q.isEmpty())
                    q.addLast(blank);
            }
            else {
               System.out.print(tn.data + " ->");
                if (tn.left != null)
                    q.addLast(tn.left);
                if (tn.right != null)
                    q.addLast(tn.right);
            }
        }
        c.accept("");
    }

    /**
     *
     * @param root
     * spiral_order.gif
     */
    public void levelOrderSpiral(TreeNode<Integer> root)    {
        Consumer<String> c = s -> System.out.println(s);
        c.accept("");
        TreeNode<Integer> blank = new TreeNode<>(Integer.MAX_VALUE);
        LinkedList<TreeNode> q = new LinkedList<>();
        if(root == null)
            return;

    }


    public void reverseLevelOrder(TreeNode<Integer> root)    {
        Consumer<String> c = s -> System.out.println(s);

        c.accept("");
        LinkedList<TreeNode> q = new LinkedList<>();

        if(root == null)
            return;
        q.add(root);
        while (!q.isEmpty())    {
            TreeNode<Integer> tn = q.removeFirst();
            System.out.print(tn.data +" ->");

            if(tn.right != null)
                q.addLast(tn.right);
            if(tn.left != null)
                q.addLast(tn.left);
        }
        c.accept("");
    }

    static class ViewConstant{
        int maxLevel;
    }
    public void leftView(TreeNode<T> root,ViewConstant vc,int level)  {
        if(root == null)
            return;
        if(vc.maxLevel < level)
        {
            System.out.print(root.data+" -> ");
            vc.maxLevel = level;
        }
        leftView(root.left,vc,level+1);
        leftView(root.right,vc,level+1);
    }
    public void rightView(TreeNode<T> root,ViewConstant vc,int level)  {
        if(root == null)
            return;
        if(vc.maxLevel < level)
        {
            System.out.print(root.data+" -> ");
            vc.maxLevel = level;
        }
        rightView(root.right,vc,level+1);
        rightView(root.left,vc,level+1);
    }

    /**
     * Print all Root to Leaf path
     * @param root
     */
    public void printKlengthPath(TreeNode<T> root,T[] path,int indx)   {
        if(root == null)
            return;
        path[indx]=root.data;
        if((root.left == null) && (root.right == null))
            printPath(path,indx);
        indx = indx + 1;
        printKlengthPath(root.left,path,indx);
        printKlengthPath(root.right,path,indx);
    }

    public void printPath(T[] path, int indx) {
        System.out.println();
        for(int i=0;i<=indx;i++)
            System.out.print(path[i]+" -> ");
        System.out.println();
    }

    public static class NodeContainer<T>{
        public TreeNode<T> node;
        public int depth;

        public void compareAndSet(TreeNode<T> node1, int depth1, BiFunction<Integer,Integer,Boolean> bi)  {
            if(bi.apply(depth1,this.depth))
            {
                this.depth = depth1;
                this.node = node1;
            }
        }
    }
    public void printTopView(TreeNode<T> root, int horizLevel,int vertLevel, Map<Integer,NodeContainer<T>> map,BiFunction<Integer,Integer,Boolean> bif)  {
        if(root == null)
            return;
        if(map.get(horizLevel) == null)
        {
            NodeContainer<T> nc = new NodeContainer<>();
            nc.node = root;
            nc.depth = vertLevel;
            map.put(horizLevel,nc);
        }
        else
        {
            NodeContainer<T> nc = map.get(horizLevel);
            nc.compareAndSet(root,vertLevel,bif);
        }
        printTopView(root.left,horizLevel-1,vertLevel+1,map,bif);
        printTopView(root.right,horizLevel+1,vertLevel+1,map,bif);
    }

    public void printNodesInLongestPath(TreeNode root)   {

    }
}
