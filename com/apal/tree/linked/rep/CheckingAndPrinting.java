package com.apal.tree.linked.rep;

import java.util.function.Predicate;

public class CheckingAndPrinting {


    public void childrenSum(TreeNode<Integer> root) {
        if(root == null)
            return;
        int left1=0,right1=0;
        Predicate<TreeNode<Integer>> p = t -> t != null;

        if(p.test(root.left))
            left1 = root.left.data;
        if(p.test(root.right))
            left1 = root.right.data;

        if(root.data == (left1 + right1))
            System.out.println(root.data);
        childrenSum(root.left);
        childrenSum(root.right);
    }

    public void checkSumCoveredUncovered(TreeNode<Integer> root)   {

    }

    public void sumLeftUnCovered(TreeNode<Integer> root)  {
        Predicate<TreeNode<Integer>> pNtNull = t -> t != null;
        if(root == null)
            return;
        if(root.left != null)
        {
            System.out.println(root.data);
            sumLeftUnCovered(root.left);
        }
        else{
            System.out.println(root.data);
            sumLeftUnCovered(root.right);
        }
    }

    public void sumRightUnCovered(TreeNode<Integer> root)  {
        if(root == null)
            return;
        if(root.right != null)
        {
            System.out.println(root.data);
            sumLeftUnCovered(root.right);
        }
        else{
            System.out.println(root.data);
            sumLeftUnCovered(root.left);
        }
    }

    public void isSibling(TreeNode<Integer> root, int a, int b)
    {

    }

    public int findHeight(TreeNode<Integer> root, int a, int height) {
        Predicate<TreeNode<Integer>> pNull = t -> t == null;
        if(pNull.test(root))
            return 0;

        if(root.data == a)
            return height;

        //if loop is needed after search in left subtree
        //as to check right subtree also, if not found in right
        //then what ever has come out. Return that.
        int levLeft = findHeight(root.left,a,height+1);
        if(levLeft != 0)
            return levLeft;

        int levRight =  findHeight(root.right,a,height+1);
        return levRight;
    }

    public int findDepth(TreeNode<Integer> root, int a, int depth)  {
        if(root == null)
            return 0;
        if(root.data == a)
            return depth;
        int left = findDepth(root.left,a,depth+1);
        if(left != 0)
            return left;
        return findDepth(root.right,a,depth+1);
    }

}
