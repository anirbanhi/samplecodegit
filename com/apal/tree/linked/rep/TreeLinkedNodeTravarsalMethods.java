package com.apal.tree.linked.rep;

public class TreeLinkedNodeTravarsalMethods {

    int count = 0;
    public void findNthNodeInInorder(TreeNode<Integer> root, int n)   {
        if(root == null)
            return;
        if(count <= n)  {
            findNthNodeInInorder(root.left,n);
            count++;
            if(count == n)
                System.out.println(n+"th node in inorder is "+root.data);
            findNthNodeInInorder(root.right,n);
        }
    }

    public int findHeight(TreeNode<Integer> root) {
        if(root == null)
            return 0;

        int left = findHeight(root.left);
        int right = findHeight(root.left);
        return Math.max(left,right) + 1;
    }

    /**
     *
     * @param root
     * TO:DO
     */
    public void boundaryTraversal(TreeNode<Integer> root)   {

    }

    public void printLeafNodes(TreeNode<Integer> root)  {
        if(root == null)
            return;
        if(root.left ==null && root.right == null)  {
            System.out.println(root.data);
        }
        printLeafNodes(root.left);
        printLeafNodes(root.right);
    }




}
