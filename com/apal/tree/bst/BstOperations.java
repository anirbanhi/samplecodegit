package com.apal.tree.bst;

public class BstOperations {
    public static void main(String[] args)  {
        BstOperations bstops = new BstOperations();
    }

    private Node root;

    public Node getRoot(){return root;}

    public boolean insert(Node root, int data){
        if(root == null)
        {
            root = new Node(data);
            return true;
        }
        Node curNode = root;

        while(curNode != null)  {
            Node left = curNode.left;
            Node right = curNode.right;

            if(data < curNode.data){
                if(left == null){
                    Node temp = new Node(data);
                    curNode.left = temp;
                    return true;
                }
                curNode = left;
            } else {
                if(right == null){
                    Node temp = new Node(data);
                    curNode.right = temp;
                    return true;
                }
                curNode = right;
            }
        }
        return false;
    }

    public boolean delete(int data) {
        if(root == null)
        {
            root = new Node(data);
            return true;
        }
        Node curNode = root;
        return true;
    }
}
