package com.apal.tree.array.rep;

import java.util.Stack;
import java.util.function.Consumer;

public class BinaryTree {

    /*
        @ tree.jpg
     */


    public void preOrderIterative(int[] tree,int indx)   {



        Stack<Integer> st = new Stack<>();
        int n = tree.length;

        while(tree[indx] != Integer.MIN_VALUE || !st.isEmpty()) {
            if (tree[indx] != Integer.MIN_VALUE) {
                System.out.print(tree[indx]+"->");
                if(indx < n/2)
                    st.push(indx);
                indx = indx * 2;
            } else {
                indx = st.pop();
                if(indx < n/2)
                    indx = indx * 2 +  1;
            }
        }
    }

    public void inOrderIterative(int[] tree,int indx)   {
        Stack<Integer> st = new Stack<>();
        int n = tree.length;

        while(tree[indx] != Integer.MIN_VALUE || !st.isEmpty()) {
            if (tree[indx] != Integer.MIN_VALUE) {
                if(indx < n/2)
                    st.push(indx);
                indx = indx * 2;
            } else {
                indx = st.pop();
                //if (tree[indx] != Integer.MIN_VALUE)
                    System.out.print(tree[indx]+"->");
                if(indx < n/2)
                    indx = indx * 2 +  1;
            }
        }
    }


    public void postOrderIterative(int[] tree,int indx)   {
        Stack<Integer> st = new Stack<>();
        int n = tree.length;

        while(tree[indx] != Integer.MIN_VALUE || !st.isEmpty()) {
            if (tree[indx] != Integer.MIN_VALUE) {
                if(indx < n/2)
                    st.push(indx);
                indx = indx * 2;
            } else {
                indx = st.pop();
                if(indx > 0)    {
                    st.push(-indx);
                    if(indx < n/2)
                        indx = indx * 2 +  1;
                }
                else
                {
                    System.out.print(tree[Math.abs(indx)]+"->");
                    indx = 0;
                }
            }
        }
    }

    public void preorder(int[] tree, int indx) {
        int n = tree.length;
        if(tree[indx] != Integer.MIN_VALUE)
            System.out.print(tree[indx]+"->");
        if(indx < n/2)
            preorder(tree,2*indx);
        if(indx < n/2)
            preorder(tree,2*indx + 1);
    }

    public void inorder(int[] tree, int indx) {
        int n = tree.length;
        if(indx < n/2)
            inorder(tree,2*indx);
        if(tree[indx] != Integer.MIN_VALUE)
            System.out.print(tree[indx]+"->");
        if(indx < n/2)
            inorder(tree,2*indx + 1);
    }

    public void postorder(int[] tree, int indx) {
        int n = tree.length;
        if(indx < n/2)
            postorder(tree,2*indx);
        if(indx < n/2)
            postorder(tree,2*indx + 1);
        if(tree[indx] != Integer.MIN_VALUE)
            System.out.print(tree[indx]+"->");
    }

    public int[] createTree(int n)    {
        int[] btree = new int[4*n];

        for(int i=0;i<btree.length;i++)
            btree[i] = Integer.MIN_VALUE;

        btree[1] = 5;
        addLeftChild(btree,5,8);
        addRightChild(btree,5,6);

        addRightChild(btree,8,9);
        addLeftChild(btree,6,3);

        addLeftChild(btree,9,4);
        addRightChild(btree,9,2);

        return btree;
    }

    public void addLeftChild(int[] tree, int parent, int no)  {
        int l = 2*findIndexOfNo(tree,parent);
        tree[l] = no;
    }

    public void addRightChild(int[] tree, int parent, int no)  {
        int r = 2*findIndexOfNo(tree,parent) + 1;
        tree[r] = no;
    }

    public int findIndexOfNo(int[] tree, int no)    {
        int i=0;
        for(i=0;i<tree.length;i++){
            if(tree[i] == no)
                break;
        }
        return i;
    }

}
