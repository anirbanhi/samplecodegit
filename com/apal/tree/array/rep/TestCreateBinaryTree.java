package com.apal.tree.array.rep;

public class TestCreateBinaryTree {
    public static void main(String[] args)  {
        BinaryTree bt = new BinaryTree();
        int[] tree = bt.createTree(7);

        bt.inorder(tree,1);
        System.out.println();
        bt.preorder(tree,1);
        System.out.println();
        bt.postorder(tree,1);
        System.out.println("\n Pre order iterative ");
        bt.preOrderIterative(tree,1);
        System.out.println("\n In order iterative ");
        bt.inOrderIterative (tree,1);
        System.out.println("\n Post order iterative ");
        bt.postOrderIterative (tree,1);
    }
}
