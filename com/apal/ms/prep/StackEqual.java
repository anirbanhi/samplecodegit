package com.apal.ms.prep;

/*
Given two Stacks S1 and S2, the task is to check if both the stacks are equal or not in the same order without losing the original stacks. If both the stacks are equal, then print “Yes “. Otherwise, print “No”.

Examples:

    Input: S1 = {3, 4, 2, 1}, S2 = {3, 4, 2, 1}
    Output: Yes

    Input: S1 = {3, 4, 6}, S2 = {7, 2, 1}
    Output: No
 */
public class StackEqual {
    public static void main(String[] args) {
        new StackEqual().driver();
    }

    public void driver()    {
        int[] t1 = {3, 4, 2, 1};
        int[] t2 = {3, 4, 2, 1};
        System.out.println("isequal : true "+isEqual(t1,t2,0,0));

        int[] s1 = {3, 4, 6};
        int[] s2 = {7, 2, 1};
        System.out.println("isequal : false "+isEqual(s1,s2,0,0));
    }

    private boolean isEqual(int[] a1, int[] a2, int i1, int i2)   {
        if(i1 >= a1.length-1 && i2 >= a2.length-1)
            return true;

        if(i1 >= a1.length-1 || i2 >= a2.length-1)
            return false;

        int stop1,stop2;

        stop1 = a1[i1];
        stop2 = a2[i2];

        if(stop1 == stop2)  {
            return isEqual(a1,a2,i1+1,i2+1);
        } else
            return false;

    }
}
