package com.apal.binarysearch;

/*
This is a educative problem
 */
public class FirstElementEqualToIndex {

    public static void main(String[] args) {
        new FirstElementEqualToIndex().driver();
    }

    public void driver()    {
        int[] nums1 = new int[] {1, 5, 8, 9, 11, 13, 15, 19, 21};
        int[] nums2 = new int[] {-8, -2, -1, 0, 2, 5, 8, 9};
        int[] nums3 = new int[] {-1, 0, 1, 2, 3, 4, 5, 6, 7, 9};
        int[] nums4 = new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

        int[] nums5 = new int[] {-10,-5,0,3,7};
        int[] nums6 = new int[] {0,2,5,8,17};
        int[] nums7 = new int[] {-10,-5,3,4,7,9};

        firstElemEqToIndex(nums1);
        firstElemEqToIndex(nums2);
        firstElemEqToIndex(nums3);
        firstElemEqToIndex(nums4);
        firstElemEqToIndex(nums5);
        firstElemEqToIndex(nums6);
        firstElemEqToIndex(nums7);
    }

    public int firstElemEqToIndex(int[] nums)    {

        int lo = 0;
        int hi = nums.length - 1;
        int mid = 0;
        int min = Integer.MAX_VALUE;
        while (lo <= hi)    {
            mid = lo + (hi - lo) / 2;
            if(mid == nums[mid]) {
                min = Math.min(min, mid);
                hi = mid-1;
            } else if(nums[mid] < mid) {
                lo = lo == mid ? mid+1 : mid;
            } else
                hi = hi == mid ? mid-1 : mid;
        }
        System.out.println("min "+min);
        return 0;
    }

}