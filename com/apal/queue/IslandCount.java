package com.apal.queue;

import java.util.HashMap;
import java.util.Map;

public class IslandCount {
    public static void main(String[] args)  {
        IslandCount ic = new IslandCount();
        int[][] map = {{1,1,0,0,0},{1,1,0,0,0},{0,0,1,0,0},{0,0,0,1,1}};
        Map<String,Boolean> visited = new HashMap<>();
        int count = 0;
        for(int i=0;i<4;i++)
        {
            for(int j=0;j<5;j++){
                int res = ic.countIsland(map, i , j, visited);
                if(res > 0)
                    count ++;
                System.out.println(" island "+i+","+j+"  "+res);
            }
        }
    }

    public int countIsland(int[][] map, int row, int col, Map<String,Boolean> visited)   {
        if(row < 0 || col < 0 || row > 3 || col > 4 )
            return  0;
        String key = "r"+row+"c"+col;
        if(visited.getOrDefault(key, false) == true)
            return  0;
        if(map[row][col] == 1) {
            visited.put(key,true);
            int right = countIsland(map, row + 1, col,visited);
            int left = countIsland(map, row - 1, col,visited);
            int up = countIsland(map, row, col + 1,visited);
            int down = countIsland(map, row, col - 1,visited);
            return right+left+up+down+1;
        }
        return 0;
    }
}
