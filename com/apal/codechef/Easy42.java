package com.apal.codechef;

import java.util.Scanner;

public class Easy42 {
    public static void main(String[] args)  {
        Scanner sc = new Scanner(System.in);
        int num = 0;
        StringBuilder sb = new StringBuilder();
        while(true){
            num = sc.nextInt();
            if(num==42){
                break;
            }
            sb.append(num).append("\n");
        }
        System.out.println(sb);
    }
}
