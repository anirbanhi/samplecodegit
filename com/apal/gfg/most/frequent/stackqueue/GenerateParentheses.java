package com.apal.gfg.most.frequent.stackqueue;

import java.util.ArrayList;
import java.util.List;

/*
https://leetcode.com/problems/generate-parentheses/
*/

public class GenerateParentheses {

    public static void main(String[] args) {
        new GenerateParentheses().driver();
    }

    public void driver()    {
        System.out.println("[\"((()))\",\"(()())\",\"(())()\",\"()(())\",\"()()()\"] "+generateParenthesis(3));
        System.out.println("[\"()\"]"+ generateParenthesis(1));
    }

    public List<String> generateParenthesis(int n) {
        List<String> list = new ArrayList<>();
        generateAllPossible(0,0,list,"",n);
      //  for(String s : list)
           // System.out.println(s);
        return list;
    }

    public void generateAllPossible(int open,int close, List<String> list, String temp, int n)   {
        if(open<0 || close <0 || open>n || close>n)
            return;
        if(open==n && close==n) {
            list.add(temp);
            return;
        }
        if(open<n)  {
            generateAllPossible(open+1,close,list,temp+"(",n);
            if(close<open)
                generateAllPossible(open,close+1,list,temp+")",n);
        } else {
            if(close<open)
                generateAllPossible(open,close+1,list,temp+")",n);
        }
    }
}