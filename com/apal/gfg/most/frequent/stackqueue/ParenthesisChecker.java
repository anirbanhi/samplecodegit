package com.apal.gfg.most.frequent.stackqueue;

/*
https://practice.geeksforgeeks.org/problems/parenthesis-checker2744/1
 */

import java.util.Stack;

public class ParenthesisChecker {

    public static void main(String[] args) {
        new ParenthesisChecker().driver();
    }

    private void driver()   {
        String s1 = "[()]{}{[()()]()}";
        System.out.println(s1+" => "+ispar(s1));

        String s2 = "[(])";
        System.out.println(s2+" => "+ispar(s2));
    }

    public boolean ispar(String s)    {
        Stack<Character> st = new Stack<>();
        int n = s.length();
        int i = 0;
        while( i < n )  {
            char ch = s.charAt(i);
            switch (ch) {
                case '(': st.push(ch); break;
                case '{': st.push(ch); break;
                case '[': st.push(ch); break;

                case ')':
                {
                    if(st.isEmpty())
                        return false;
                    else {
                        if(st.pop() != '(')
                            return false;
                    }
                    break;
                }
                case '}':
                {
                    if(st.isEmpty())
                        return false;
                    else {
                        if(st.pop() != '{')
                            return false;
                    }
                    break;
                }
                case ']':
                {
                    if(st.isEmpty())
                        return false;
                    else {
                        if(st.pop() != '[')
                            return false;
                    }
                    break;
                }
            }
            i++;
        }
        return true;
    }

}