package com.apal.gfg.most.frequent.stackqueue;

/*
https://leetcode.com/problems/valid-parentheses/
 */

import java.util.Stack;

public class ValidParentheses {

    public static void main(String[] args) {
        new ValidParentheses().driver();
    }

    public void driver()    {
        //System.out.println("true "+isValid("()"));
        //System.out.println("true "+isValid("()[]{}"));
        //System.out.println("false "+isValid("(]"));
        //System.out.println("false "+isValid("([)]"));
        //System.out.println("true "+isValid("{[]}"));
        System.out.println("false "+isValid("([}}])"));

    }

    public boolean isValid(String s) {
        int size = s.length();
        Stack<Character> st = new Stack<>();
        for(int i=0;i<size;i++) {
            char ch = s.charAt(i);
            int resp = getNo(ch);
            if(resp==0)
                return false;
            if(resp>0)
                st.push(ch);
            else if(resp<0) {
                if(st.size() == 0)
                    return false;
                char sttop = st.peek();
                if(Math.negateExact(getNo(sttop)) == resp) {
                    st.pop();
                    continue;
                }
                else
                    st.push(ch);
            }
        }
        if(st.size()>0)
            return false;
        return true;
    }

    public int getNo(Character ch) {
        switch (ch) {
            case '(':   return 1;
            case '{':   return 2;
            case '[':   return 3;
            case ')':   return -1;
            case '}':   return -2;
            case ']':   return -3;
        }
        return 0;
    }


}