package com.apal.gfg.most.frequent.stackqueue;

/*
https://practice.geeksforgeeks.org/problems/next-larger-element-1587115620/1
 */

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterElement {

    public static void main(String[] args) {
        new NextGreaterElement().driver();
    }

    private void driver()   {
        long[] a1 = {1, 3, 2, 4};
        nextLargerElement(a1,a1.length);

        long[] a2 = {6, 8, 0, 1, 3};
        nextLargerElement(a2,a2.length);
    }

   static class ElemWithPos   {
        long elem;
        int pos;

        public ElemWithPos(long elem, int pos)   {
            this.elem = elem;
            this.pos = pos;
        }
    }

    private long[] nextLargerElement(long[] arr, int n)
    {
        System.out.println("Input: "+ Arrays.toString(arr));
        // Your code here
        long[] op = new long[n];
        Stack<ElemWithPos> st = new Stack<>();

        for(int i=0; i<arr.length; i++)   {
            long cur = arr[i];
            while(st.size() >0 && st.peek().elem <= cur) {
                ElemWithPos top = st.pop();
                op[top.pos] = cur;
            }
            st.push(new ElemWithPos(cur, i));
        }
        while(st.size() > 0)    {
            ElemWithPos top = st.pop();
            op[top.pos] = -1;
        }

        System.out.println("Output: "+ Arrays.toString(op));
        return op;
    }

}
