package com.apal.gfg.most.frequent.stackqueue;

/*
https://practice.geeksforgeeks.org/problems/stack-using-two-queues/1
*/

import java.util.LinkedList;
import java.util.Queue;

public class StackUsingQueues {

    public static void main(String[] args) {
        new StackUsingQueues().driver();
    }

    private void driver()   {
        push(2);
        push(3);
        System.out.println(pop());
        push(4);
        System.out.println(pop());
    }

    Queue<Integer> q1 = new LinkedList<Integer>();

    Queue<Integer> q2 = new LinkedList<Integer>();

    //Function to push an element into stack using two queues.
    void push(int a)
    {
        // Your code here
        if(q2.isEmpty())
            q1.add(a);
        else q2.add(a);
    }

    //Function to pop an element from stack using two queues.
    int pop()
    {
        // Your code here
        if(q1.isEmpty() && q2.isEmpty())
            return -1;
        if(q2.isEmpty())    {
            while(q1.size() >1) {
                q2.add(q1.remove());
            }
            return q1.remove();
        } else {
            if(q1.isEmpty())    {
                while (q2.size() >1)    {
                    q1.add(q2.remove());
                }
                return q2.remove();
            }
        }
        return -1;
    }

}