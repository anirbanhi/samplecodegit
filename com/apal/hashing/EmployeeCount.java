package com.apal.hashing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
https://www.geeksforgeeks.org/find-number-of-employees-under-every-manager/
 */

public class EmployeeCount {
    public static void main(String[] args) {
        Map<Character, Character> map = new HashMap<>();
        map.put('A','C');
        map.put('B','C');
        map.put('C','F');
        map.put('D','E');
        map.put('E','F');
        map.put('F','F');

        //construct reverse map
        Map<Character, List<Character>> revmap = new HashMap<>();
        char ceo = ' ';
        for(Character c : map.keySet()) {
            char p = map.get(c);
            List<Character> l = revmap.get(p);
            if(l == null)
                l = new ArrayList<>();
            if(c == map.get(c)) {
                ceo = c;
            } else {
                l.add(c);
            }
            revmap.put(p,l);
        }
        findRep(revmap, ceo);
    }

    public static int findRep(Map<Character,List<Character>> map, char c) {
        int cnt = 0;
        if(map.get(c) != null) {
            for (Character ch : map.get(c)) {
                cnt += findRep(map, ch);
            }
        }
        System.out.println(c+" -> "+cnt);
        return cnt+1;
    }
}
