package com.apal.hashing;

import java.util.*;

public class FindSymmetricPairs {
    public static void main(String[] args) {
        int[][] arr = {{11, 20}, {30, 40}, {5, 10}, {40, 30}, {10, 5}};

        Comparator<int[]> comp = new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                if(o1[0] == o2[0] && o1[1] == o2[1])
                    return 0;
                return -1;
            }
        };
        int[] rev = new int[2];
        Map<int[], Boolean> map = new HashMap<>();
        System.out.println("arr.length "+arr.length);
        for(int i=0;i<arr.length;i++)   {
            rev[0] = arr[i][1];
            rev[1] = arr[i][0];
            map.put(arr[i],true);
            if(map.containsKey(rev))
                System.out.println("Found "+rev.toString());
        }
    }
}
