package com.apal.sorting;

import com.apal.global.Util;

/*
    1.
 */
public class InsertionSort {

    public static void main(String[] args)  {
        int[] arr = {9,10,5,8,2,1};
        Util.print(arr,"Before Sort");
        InsertionSort is = new InsertionSort();
        is.insertionSort(arr);
        Util.print(arr,"After Sorting");
    }

    public void insertionSort(int[] arr) {
        int j=0;
        int x =0;
        int n = arr.length;
        for(int i=1;i<n;i++) {
            j = i-1;
            x = arr[i];
            while ((j > -1) && arr[j] > x) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = x;
        }
    }
}

