package com.apal.sorting;

public class MergeSort {

    public static void main(String[] args)  {
        MergeSort ms = new MergeSort();
        int[] a = {8,15,3,2,58,14,69,54,1};
        //int[] a = {14,10};
        ms.print(a);
        int len = a.length-1;
        //ms.merge(a,0,len/2,len);
        ms.recursiveMergeSort(a,0,len);
        ms.print(a);
    }

    private void mergeSort(int[] a) {
        int len = a.length;
        int i = 0;
        for(i=2;i<=len;i=i*2)    {
            int h = 0;
            System.out.println("**************"+i+"***************");
            for(int l=0;l+i-1<len;) {
                h=l+i-1;
                int mid = (l+h)/2;
                System.out.println("calling mergesort "+l+" "+h);
                print(a);
                merge(a,l,mid,h);
                print(a);
                l = l + i;
            }
        }
        if(i/2 < len)
            merge(a,0,i/2-1,len-1);

    }
    //this merges 2 sorted list into b
    //list1 = l to mid
    //list2 = mid+1 to h
    private void merge(int[] a, int l, int  mid, int h) {
        int b[] = new int[a.length+1];
        int i = l;
        int j = mid + 1;
        int k = l;
        while(i<=mid && j<=h) {
            if(a[i] < a[j])
            {
                b[k] = a[i];
                i++;
            }
            else
            {
                b[k] = a[j];
                j++;
            }
            k++;
        }
        while(i<=mid)  {
            b[k] = a[i];
            k++;
            i++;
        }
        while(j<=h)  {
            b[k] = a[j];
            k++;
            j++;
        }
        for(i=l;i<=h;i++)
            a[i] = b[i];
    }

    private void print(int[] a) {
        System.out.println();
        for(int i=0;i<a.length;i++)
            System.out.print(a[i]+", ");
        System.out.println();
    }

    private void recursiveMergeSort(int[] a, int l, int h)    {
        if(l<h){
            int mid = (l+h)/2;
            recursiveMergeSort(a,l,mid);
            recursiveMergeSort(a,mid+1,h);
            merge(a,l,mid,h);
        }
    }

}