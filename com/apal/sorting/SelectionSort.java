package com.apal.sorting;

import com.apal.global.Util;

/*
    1.  In place
    2.  Intermediate result useful, can find kth smallest / greatest number
    3.  Doesn't maintain order.
    4.  Complexity O(n^2)
    5.  Can't determine whether the elements are already sorted or not
 */
public class SelectionSort {
    public static void main(String[] args)  {
        int[] arr = {2,5,9,1,4};
        Util.print(arr,"Before Sorting");
        new SelectionSort().selectionSort(arr);
        Util.print(arr,"After Sorting");
    }

    public void selectionSort(int[] arr) {
        int n = arr.length;

        for(int i=0;i<n;i++){
            int k=i;
            for(int j=k+1;j<n;j++)    {
                if(arr[j]<arr[k])
                    k=j;
            }
            Util.swap(arr,i,k);
        }

    }
}
