package com.apal.sorting;

import java.util.Arrays;

public class HeapSort {

    public static void main(String[] args) {
        new HeapSort().driver();
    }

    public void driver()    {
        int[] a = new int[]{90, 20, 70, 50, 30, 80, 10, 40};
        System.out.println(Arrays.toString(a));
        heapsort(a);
        System.out.println(Arrays.toString(a));
    }

    public void heapsort(int[] a)  {
        for(int i=1;i<a.length;i++)
            heapify(a,i);

        int n = a.length-1;
        for(int i=0;i<a.length;i++)
            removeHeapify(0,n-i,a,true);
    }

    public void heapify(int[] a, int i)   {
        int pindex = (i-1)/2;
        if(pindex < 0)  return;
        if(a[pindex] > a[i]) {
            swap(a,i,pindex);
            heapify(a,pindex);
        }
    }

    private void removeHeapify(int pindx, int count, int[] heap, boolean isMaxheap)    {
        if(pindx >= count)
            return;
        int lChildIndx = pindx * 2 + 1 ;
        int rChildIndx = pindx * 2 + 2 ;
        int indx  = lChildIndx;

        if(lChildIndx > count)
            return;
        if(lChildIndx == count)
            indx = lChildIndx;
        else {
            if (rChildIndx <= count) {
                if (isMaxheap) {
                    if ((heap[rChildIndx] > heap[lChildIndx]))
                        indx = rChildIndx;
                } else {
                    if ((heap[rChildIndx] < heap[lChildIndx]))
                        indx = rChildIndx;
                }
            }
        }
        if(isMaxheap) {
            if (heap[pindx] < heap[indx]) {
                swap(heap,pindx, indx);
                removeHeapify(indx,count,heap,true);
            }
        } else {
            if (heap[pindx] > heap[indx]) {
                swap(heap, pindx, indx);
                removeHeapify(indx,count,heap,true);
            }
        }
    }

    private void swap(int[] a, int i, int j)    {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

}