package com.apal.sorting;

import java.util.Arrays;

public class QuickSort {

    public static void main(String[] args)  {
        //int[] a = {43,6,12,34,78,4,1,90,45};
        int[] a = {10, 80, 30, 90, 40, 50, 70};
        System.out.println(Arrays.toString(a));

        new QuickSort().quickSort(a,0,a.length-1);

        System.out.println(Arrays.toString(a));
    }

    public void quickSort(int[] a, int lo, int hi)  {
        if(lo < hi) {
            int p = partition(a,lo,hi);
            quickSort(a,lo,p-1);
            quickSort(a,p+1,hi);
        }
    }

    public int partition(int[] a,int lo, int hi)  {
        int l = lo;
        int i = l+1;
        int j = hi;
        int pivot = a[l];   //can be any random element at index i
                            //from low to high
        while( i <= j )  {
            while(i<=hi && a[i]<pivot)  i++;
            while(j>=lo && a[j]>pivot)  j--;
            if(i<j) {
                swap(a,i,j);
                i++;
                j--;
            }
        }
        swap(a,l,j);
        return j;
    }

    private void swap(int[] a, int i, int j)    {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
}