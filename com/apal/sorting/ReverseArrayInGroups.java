package com.apal.sorting;

/*
    https://practice.geeksforgeeks.org/problems/reverse-array-in-groups0255/1
*/

import java.util.Arrays;

public class ReverseArrayInGroups {

    public static void main(String[] args) {
        new ReverseArrayInGroups().driver();
    }

    public void driver()    {
        int[] a1 = {1,2,3,4,5};
        reverseInGroups(a1,5, 3);

        int[] a2 = {5,6,8,9};
        reverseInGroups(a2,4, 3);
    }

    void reverseInGroups(int[] arr, int n, int k) {
        System.out.println("Before "+ Arrays.toString(arr));
        int st=0;
        int end=n;

        for(int j=0;j<n;j=j+k)    {
            reverse(arr,j,j+k);
        }
        System.out.println("After "+ Arrays.toString(arr));
    }

    void reverse(int[] a, int st, int end)  {
        if(end>=a.length)
            end=a.length;
        int i=st;
        int j=end-1;
        int temp;
        while (i<j) {
            temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }
    }

}
