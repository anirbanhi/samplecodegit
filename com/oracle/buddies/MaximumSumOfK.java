package com.oracle.buddies;

import com.apal.global.Util;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;


/*
        Complexity O((n-k)*k) ~ O(nk)
        Constant extra Space
 */

public class MaximumSumOfK {

    public static void main(String[] args)  {
        MaximumSumOfK ms = new MaximumSumOfK();
        int[] arr = {10,5,2,7,8,7};
        //int[] arr = {5,12,2,9,3};
        int k = 3;

        Util.print(arr,"Original Array");
        //ms.findKMax(arr,k);
        ms.findMaxOrderN(arr,k);

        Util.print(arr,"Original Array");
        ms.findMaxOrderN(arr,4);
        //Util.print(arr,"First K sorted Array");
    }

    public void findMaxOrderN(int[] arr, int k) {
        //https://leetcode.com/problems/sliding-window-maximum/discuss/65884/Java-O(n)-solution-using-deque-with-explanation
        int n = arr.length;
        Deque<Integer> dq = new ArrayDeque<Integer>();

        for(int i=0;i<n;i++)    {

            while(!dq.isEmpty() && (dq.peek() < (i - k +1))) {
                dq.pollFirst();
            }

            while(!dq.isEmpty() && (arr[dq.peekLast()] < arr[i]))    {
                dq.pollLast();
            }

            dq.offerLast(i);
            if(i >= (k-1))
                System.out.println(arr[dq.peek()]);
        }
    }

    public void findKMax(int[] arr,int k)  {
        int n = arr.length;
        //find max in k elemets
        for(int i=0;i<(n-k);i=i+1)    {
            int l = i;
            for(int j=l+1;j<(i+k);j++)   {
                if(arr[j] > arr[l])
                    l = j;
            }
            System.out.println(arr[l]);
        }
    }

}
