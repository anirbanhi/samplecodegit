package com.oracle.demo;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class UpdateTenantTable {

    static final String DB_URL = "jdbc:oracle:thin:@slc12atu.us.oracle.com:1521/xe";
    static final String USER = "sys as sysdba";
    static final String PASS = "oracle";
    static final String QUERY = "SELECT * FROM global_idaas.tenant";

    public static void main(String[] args) {
        Set<String> infraTenants = new HashSet<>();
        infraTenants.add("idcs-ca");infraTenants.add("idcs-oracle");infraTenants.add("idcs-cloudinfra");

        // Open a connection
        try(Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = conn.createStatement();
        ) {
            System.out.println("Connection AutoCommit: " + conn.getAutoCommit());
            //String sql = "UPDATE global_idaas.tenant SET u_vc_size = 'free' WHERE U_VC_NAME = 'tenantatu2' ";
            String sql = "UPDATE global_idaas.tenant SET u_dt_cr_date = to_date('2020/01/15','YYYY/MM/DD') WHERE U_VC_NAME = 'tenantatu2' ";
            //String sql = "UPDATE global_idaas.tenant SET U_BL_IFLEX_4 = 0 WHERE U_VC_NAME = 'tenantatu1' ";       //internal
            //String sql = "UPDATE global_idaas.tenant SET U_IN_IFLEX_8 = 120 WHERE U_VC_NAME = 'tenantatu1' ";         //expiryInDays

            stmt.executeUpdate(sql);
            ResultSet rs = stmt.executeQuery(QUERY);

            while(rs.next())    {
                //Display values
                if(infraTenants.contains(rs.getString("U_VC_NAME")) )
                    continue;

                System.out.print("TenantName: [" + rs.getString("U_VC_NAME")+"]");
                System.out.print(" , LicenseType: [" + rs.getString("U_VC_SIZE")+"]");
                System.out.print(" , Created: [" + rs.getString("U_DT_CR_DATE")+"]");
                System.out.print(" , Internal: [" + rs.getBoolean("U_BL_IFLEX_4")+"]");
                System.out.print(" , InternalExpiryInDays: [" + rs.getInt("U_IN_IFLEX_8")+"]");
                System.out.println();
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}