package com.book.my.show.account;

import com.book.my.show.cinemas.City;

public interface Account {
    void browseMovies(City c);

    void browseCinemas(City c);
}
