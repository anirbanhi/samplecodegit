package com.book.my.show.account;

import com.book.my.show.Payment.Payment;
import com.book.my.show.booking.Show;
import com.book.my.show.cinemas.Seat;
import com.book.my.show.enums.PaymentMode;

import java.util.List;

public class FrontOfficeExecutive extends DefaultAccount implements RegisteredAccount {

    @Override
    public void bookTicket(Show s, List<Seat> seats, PaymentMode mode, String name, String cardno) {

    }

    @Override
    public void viewBooking() {

    }

    @Override
    public void cancelBooking() {

    }

    @Override
    public void changePassword() {

    }

    @Override
    public void addFrontOfficeExecutive() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void modifyRegisterdUsers() {
        throw new UnsupportedOperationException();
    }
}
