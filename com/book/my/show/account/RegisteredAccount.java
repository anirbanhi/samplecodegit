package com.book.my.show.account;

import com.book.my.show.Payment.Payment;
import com.book.my.show.booking.Show;
import com.book.my.show.booking.ShowSeat;
import com.book.my.show.cinemas.Seat;
import com.book.my.show.enums.PaymentMode;

import java.util.List;

public interface RegisteredAccount extends Account {
    void bookTicket(Show s, List<Seat> seats, PaymentMode mode, String name, String cardno);

    void viewBooking();

    void cancelBooking();

    void changePassword();

    void addFrontOfficeExecutive();

    void modifyRegisterdUsers();
}
