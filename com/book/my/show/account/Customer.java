package com.book.my.show.account;

import com.book.my.show.Payment.Payment;
import com.book.my.show.Payment.PaymnetUtil;
import com.book.my.show.booking.Booking;
import com.book.my.show.booking.Show;
import com.book.my.show.booking.ShowSeat;
import com.book.my.show.cinemas.Seat;
import com.book.my.show.enums.PaymentMode;

import java.util.ArrayList;
import java.util.List;

public class Customer extends DefaultAccount implements RegisteredAccount {

    List<Booking> bookingList;

    @Override
    public void bookTicket(Show s, List<Seat> seats, PaymentMode mode, String name, String cardno) {
        List<ShowSeat> seatsToBeBooked = new ArrayList<>();
        for(Seat st:seats)  {
            for(ShowSeat sst : s.getShowSeatList())
            {
                if(sst.isSeatAvailable(st.getRow(),st.getCol()))
                    seatsToBeBooked.add(sst);
            }
        }
        float price = 0.0f;
        if(seatsToBeBooked.size() == seats.size()){
            System.out.println("All Seats Are available. Going to be booked.");
            for(ShowSeat s1 : seatsToBeBooked)
            {
                s1.setIsavailable(true);
                price += s1.getPrice();
            }

            Payment payment = PaymnetUtil.getPaymentObject(mode,"Name","cardno",price);
            Booking booking = new Booking(s,seatsToBeBooked,this,payment);
            bookingList.add(booking);
            System.out.println("Your Booking is Complete");
        }else{
            System.out.println("Some seats are not available. Return");
        }
    }

    @Override
    public void viewBooking() {
        System.out.println("Displaying all bookings");
        for(Booking b : bookingList)
            System.out.println(b);
    }

    @Override
    public void cancelBooking() {

    }

    @Override
    public void changePassword() {

    }

    @Override
    public void addFrontOfficeExecutive() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void modifyRegisterdUsers() {
        throw new UnsupportedOperationException();
    }
}
