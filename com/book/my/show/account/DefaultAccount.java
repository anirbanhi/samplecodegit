package com.book.my.show.account;

import com.book.my.show.cinemas.CinemaHall;
import com.book.my.show.cinemas.City;
import com.book.my.show.movie.Catalogue;
import com.book.my.show.movie.Movie;

import java.util.List;
import java.util.Set;

public class DefaultAccount implements Account {

    @Override
    public void browseMovies(City c) {
        System.out.println("You are browsing all Movies for city "+c);
        Catalogue cl = c.getCatalogue();
        Set<Movie> movieSet = cl.getAllMoviesSet();
        for(Movie m : movieSet)
            System.out.println(m);
    }

    @Override
    public void browseCinemas(City c) {
        System.out.println("You are browsing all CinemaHall for city "+c);
        List<CinemaHall> cinemaHallList = c.getCinemaHallList();
        for(CinemaHall ch : cinemaHallList)
            System.out.println(ch);
    }

}
