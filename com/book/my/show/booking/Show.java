package com.book.my.show.booking;

import com.book.my.show.cinemas.CinemaHall;
import com.book.my.show.cinemas.Screen;
import com.book.my.show.cinemas.Seat;
import com.book.my.show.movie.Movie;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Show {
    Movie movie;
    CinemaHall cinemaHall;
    Screen screen;
    Date date;
    Date time;
    List<ShowSeat> showSeatList;

    public Show(Movie mv, CinemaHall ch, Screen sc)   {
        this.movie = mv;
        this.cinemaHall = ch;
        this.screen = sc;
        showSeatList = new ArrayList<>();
        List<Seat> seatList = screen.getSeatList();
        for(Seat st : seatList)
            showSeatList.add(new ShowSeat(st));
    }

    public CinemaHall getCinemaHall() {
        return cinemaHall;
    }

    public List<ShowSeat> getShowSeatList() {
        return showSeatList;
    }
}
