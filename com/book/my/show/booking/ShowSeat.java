package com.book.my.show.booking;

import com.book.my.show.cinemas.Seat;

public class ShowSeat {
    Seat seat;
    boolean isavailable;
    float price;

    public ShowSeat(Seat st)   {
        this.seat = st;
        this.isavailable = true;
    }

    public boolean isSeatAvailable(int row, char col)   {
        if( (seat.getRow() == row) &&
                (seat.getCol() == col) )
        {
            return isavailable;
        }
        throw new RuntimeException("Invalid Row, Col "+row+","+col);
    }

    public boolean isIsavailable() {
        return isavailable;
    }

    public void setIsavailable(boolean isavailable) {
        this.isavailable = isavailable;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
