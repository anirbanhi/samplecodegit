package com.book.my.show.booking;

import com.book.my.show.Payment.Payment;
import com.book.my.show.account.RegisteredAccount;

import java.util.List;

public class Booking {
    Show show;
    List<ShowSeat> showSeatList;
    RegisteredAccount registeredAccount;
    Payment payment;

    public Booking(Show show, List<ShowSeat> showSeatList, RegisteredAccount registeredAccount, Payment payment) {
        this.show = show;
        this.showSeatList = showSeatList;
        this.registeredAccount = registeredAccount;
        this.payment = payment;
    }
}
