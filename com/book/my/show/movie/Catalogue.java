package com.book.my.show.movie;

import com.book.my.show.booking.Show;
import com.book.my.show.cinemas.CinemaHall;
import com.book.my.show.enums.MovieLanguage;
import com.book.my.show.enums.MovieZonre;

import java.util.*;

public class Catalogue {
    Set<Movie>  allMoviesSet;
    Map<MovieZonre, List<Movie>> zonreMovieMap;
    Map<MovieLanguage, List<Movie>> languageMovieMap;
    Map<CinemaHall, Set<Movie>>  cinemaHallMovieMap;
    Map<Movie, Set<CinemaHall>> movieCinemaHallMap;

    public Catalogue()  {
        allMoviesSet = new HashSet<>();
        zonreMovieMap = new HashMap<>();
        languageMovieMap = new HashMap<>();
        cinemaHallMovieMap = new HashMap<>();
        movieCinemaHallMap = new HashMap<>();
    }
    public void addMovie(Movie m)  {
        if(allMoviesSet.contains(m))
            throw new RuntimeException(m+" already exist");
        else
        {
            allMoviesSet.add(m);
            if(zonreMovieMap.get(m.movieZonre) == null)
                zonreMovieMap.put(m.movieZonre, new ArrayList<>());
            zonreMovieMap.get(m.movieZonre).add(m);
            if(languageMovieMap.get(m.movieLanguage) == null)
                languageMovieMap.put(m.movieLanguage, new ArrayList<>());
            languageMovieMap.get(m.movieLanguage).add(m);
        }
    }

    public void deleteMovie(Movie m)    {
        //delete movie and all related entries from all maps.
    }

    public void addMovieShows(Movie m, Show s)  {
        CinemaHall ch = s.getCinemaHall();
        if(!allMoviesSet.contains(m))
            throw new RuntimeException(m+" does not exist");
        if(cinemaHallMovieMap.get(ch) == null)
            cinemaHallMovieMap.put(ch,new HashSet<>());
        cinemaHallMovieMap.get(ch).add(m);
        if(movieCinemaHallMap.get(m) == null)
            movieCinemaHallMap.put(m,new HashSet<>());
        movieCinemaHallMap.get(m).add(ch);
    }

    public Set<Movie> getAllMoviesSet() {
        return allMoviesSet;
    }
}
