package com.book.my.show.movie;

import com.book.my.show.booking.Show;
import com.book.my.show.enums.MovieLanguage;
import com.book.my.show.enums.MovieZonre;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class Movie {
    String movieName;
    Date releaseDate;
    MovieZonre movieZonre;
    String details;
    MovieLanguage movieLanguage;
    List<Show> showList;

    public List<Show> getShowList() {
        return showList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return Objects.equals(movieName, movie.movieName) &&
                Objects.equals(releaseDate, movie.releaseDate) &&
                movieZonre == movie.movieZonre &&
                Objects.equals(details, movie.details) &&
                movieLanguage == movie.movieLanguage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(movieName, releaseDate, movieZonre, details, movieLanguage);
    }

    @Override
    public String toString() {
        return "Movie{" +
                "movieName='" + movieName + '\'' +
                ", releaseDate=" + releaseDate +
                ", movieZonre=" + movieZonre +
                ", details='" + details + '\'' +
                ", movieLanguage=" + movieLanguage +
                '}';
    }
}
