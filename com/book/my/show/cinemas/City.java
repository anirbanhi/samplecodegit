package com.book.my.show.cinemas;

import com.book.my.show.movie.Catalogue;

import java.util.List;

public class City {
    String cityName;
    List<CinemaHall> cinemaHallList;
    Catalogue catalogue;

    public String getCityName() {
        return cityName;
    }

    public List<CinemaHall> getCinemaHallList() {
        return cinemaHallList;
    }

    public Catalogue getCatalogue() {
        return catalogue;
    }

    public void addCinemaHall(CinemaHall ch)  {
        for(CinemaHall ch1 : cinemaHallList)    {
            if(ch1.equals(ch))
                throw new RuntimeException(ch+" Exists");
        }
        cinemaHallList.add(ch);
    }
}
