package com.book.my.show.cinemas;

import java.util.List;
import java.util.Objects;

public class CinemaHall {
    String cinemaHallName;
    String zipCode;
    String address;
    List<Screen> screenList;

    public void addScreen(Screen sc)    {
        for(Screen sc1:screenList)  {
            if(sc1.screenNo == sc.screenNo)
                throw new RuntimeException(sc+" Exists");
        }
        screenList.add(sc);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CinemaHall that = (CinemaHall) o;
        return Objects.equals(cinemaHallName, that.cinemaHallName) &&
                Objects.equals(zipCode, that.zipCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cinemaHallName, zipCode);
    }

    @Override
    public String toString() {
        return "CinemaHall{" +
                "cinemaHallName='" + cinemaHallName + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
