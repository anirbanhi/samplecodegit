package com.book.my.show.cinemas;

import com.book.my.show.enums.SeatType;

import java.util.Objects;

public class Seat {
    int row;
    char col;
    SeatType seatType;
    boolean isavailable;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seat seat = (Seat) o;
        return row == seat.row &&
                col == seat.col &&
                seatType == seat.seatType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col, seatType);
    }

    @Override
    public String toString() {
        return "Seat{" +
                "row=" + row +
                ", col=" + col +
                '}';
    }

    public int getRow() {
        return row;
    }

    public char getCol() {
        return col;
    }
}
