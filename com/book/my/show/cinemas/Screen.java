package com.book.my.show.cinemas;

import java.util.List;

public class Screen {
    int screenNo;
    List<Seat> seatList;

    public void addSeat(Seat st)    {
        for(Seat st1:seatList)  {
            if(st1.equals(st))
                throw new RuntimeException(st+" Exists");
        }
        seatList.add(st);
    }

    @Override
    public String toString() {
        return "Screen{" +
                "screenNo=" + screenNo +
                '}';
    }

    public List<Seat> getSeatList() {
        return seatList;
    }
}
