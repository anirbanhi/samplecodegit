package com.book.my.show.enums;

public enum MovieLanguage {
    English,
    Hindi,
    Bengali,
    Kannada,
    Telugu,
    Marathi,
}
