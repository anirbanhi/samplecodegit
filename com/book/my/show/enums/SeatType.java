package com.book.my.show.enums;

public enum SeatType {
    Classic, Superior, Executive;
}
