package com.book.my.show.enums;

public enum PaymentMode {
    Cash,
    CreditCard,
    DebitCard,
}
