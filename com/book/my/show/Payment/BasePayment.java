package com.book.my.show.Payment;

import com.book.my.show.Payment.Payment;

public abstract class BasePayment implements Payment {

    public String nameOnCard;
    public String cardNo;
    public float amountPayed;

    @Override
    public float amountPayed() {
        return amountPayed;
    }

    @Override
    public String cardNo() {
        return cardNo;
    }

    @Override
    public String nameOnCard() {
        return nameOnCard;
    }
}
