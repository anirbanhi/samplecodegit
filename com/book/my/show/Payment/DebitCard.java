package com.book.my.show.Payment;

import com.book.my.show.enums.PaymentMode;

public class DebitCard extends BasePayment implements Payment {

    public DebitCard(String name, String cardno, float amnt) {
        this.nameOnCard = name;
        this.cardNo = cardno;
        this.amountPayed = amnt;
    }

    @Override
    public PaymentMode paymentMode() {
        return PaymentMode.DebitCard;
    }

}
