package com.book.my.show.Payment;

import com.book.my.show.enums.PaymentMode;

public class Cash extends BasePayment implements Payment {

    public Cash(float amnt) {
        this.amountPayed = amnt;
    }

    @Override
    public PaymentMode paymentMode() {
        return PaymentMode.Cash;
    }

    @Override
    public String cardNo() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String nameOnCard() {
        throw new UnsupportedOperationException();
    }
}
