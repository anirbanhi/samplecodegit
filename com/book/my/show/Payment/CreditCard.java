package com.book.my.show.Payment;

import com.book.my.show.enums.PaymentMode;

public class CreditCard extends BasePayment implements Payment {

    public CreditCard(String name, String cardno, float amnt) {
        this.nameOnCard = name;
        this.cardNo = cardno;
        this.amountPayed = amnt;
    }

    @Override
    public PaymentMode paymentMode() {
        return PaymentMode.CreditCard;
    }

}
