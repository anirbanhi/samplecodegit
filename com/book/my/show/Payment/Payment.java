package com.book.my.show.Payment;

import com.book.my.show.enums.PaymentMode;

public interface Payment {
    PaymentMode paymentMode();
    float amountPayed();
    String cardNo();
    String nameOnCard();
}
