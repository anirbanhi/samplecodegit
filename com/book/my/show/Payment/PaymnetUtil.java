package com.book.my.show.Payment;

import com.book.my.show.enums.PaymentMode;

public class PaymnetUtil {
    public static Payment getPaymentObject(PaymentMode pm,
                                    String name,
                                    String cardno,
                                    float amnt)   {
        switch(pm)  {
            case CreditCard:    {
                CreditCard cc;
                cc = new CreditCard(name,cardno,amnt);
                return cc;
            }
            case DebitCard: {
                DebitCard dc = new DebitCard(name,cardno,amnt);
                return dc;
            }
            case Cash:  {
                Cash c = new Cash(amnt);
                return c;
            }
        }
        throw new RuntimeException("PaymentMode not found");
    }
}
