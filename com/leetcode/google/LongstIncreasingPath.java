package com.leetcode.google;
/*
https://leetcode.com/explore/interview/card/google/61/trees-and-graphs/3072/
 */
public class LongstIncreasingPath {

    public static void main(String[] args) {
        new LongstIncreasingPath().driver();
    }

    public void driver()    {
        int[][] m = {   {9,9,4},
                        {6,6,8},
                        {2,1,1}
                    };
        System.out.println(" 3x3 " + longestIncreasingPath(m));
    }



    public int longestIncreasingPath(int[][] matrix) {

        return dfsdriver(matrix);
    }

    int[][] d = {{-1,0},{1,0},{0,1},{0,-1}};
    int maxl = -1;
    public int dfsdriver(int[][] m) {
        maxl = -1;
        int rowmax = m.length;
        int colmax = m[0].length;

        int[][] v = new int[rowmax][colmax];
        int[][] cache = new int[rowmax][colmax];
        for(int i=0;i<rowmax;i++)   {
            for(int j=0;j<colmax;j++)   {
                if(v[i][j] == 0) {
                    int cnt = dfs(m, i, j, 1, v, cache);
                    maxl = Math.max(maxl, cnt);
                }
            }
        }
        return maxl;
    }

    public int dfs(int[][] m, int x, int y, int l,int[][] v, int[][] cache)  {
        if(cache[x][y] > 0)
            return cache[x][y];
        int x1;
        int y1;
        int cnt = 0;
        for(int i=0;i<4;i++)    {
            x1 = x + d[i][0];
            y1 = y + d[i][1];
            int ret = 0;
            if(isvalid(m,x1,y1,v) && m[x1][y1] > m[x][y]) {
                ret = dfs(m, x1, y1, l + 1, v,cache);
                cnt = Math.max(cnt, ret);
            }
        }
        cache[x][y] = cnt+1;
        return cache[x][y];
    }

    public boolean isvalid(int[][]m, int x, int y, int[][] v)   {
        int rowmax = m.length;
        int colmax = m[0].length;

        if(x <0 || x >= rowmax || y < 0 || y >= colmax
                || v[x][y] == 1)
            return false;

        return true;
    }

}