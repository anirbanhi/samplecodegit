package com.leetcode.google;

/*
https://leetcode.com/explore/interview/card/google/64/dynamic-programming-4/3088/
 */

public class CoinChangeFewest {

    public static void main(String[] args) {
        new CoinChangeFewest().driver();
    }

    public void driver()    {
        System.out.println("3 == " +coinChange(new int[]{1,2,3},11,
                0,0));
    }

    public int coinChange(int[] coins, int amount)  {
        return coinChange(coins, amount, 0, 0);
    }

    public int coinChange(int[] coins, int amount, int cnt, int cur) {
        if(amount == 0)
            return cnt-1;
        if(cur >= coins.length)
            return -1;
        int p1 = -1;
        if(amount >= coins[cur])
            p1 = coinChange(coins, amount - coins[cur],
                    cnt+1, cur);
        int p2 = -1;
        p2 = coinChange(coins, amount, cnt, cur+1);
        if(p1 == -1)    return p2;
        if(p2 == -1)    return p1;
        return Math.min(p1,p2);
    }

}