package com.leetcode.google;

import java.util.Comparator;
import java.util.PriorityQueue;

/*
https://leetcode.com/explore/interview/card/google/59/array-and-strings/3062/
 */
public class KNearestPointToOrigin {
    public static void main(String[] args) {
        new KNearestPointToOrigin().driver();
    }

    public void driver()    {
        int[][] points = {{3,3},{5,-1},{-2,4}};
        int k = 2;
        int[][] res = kClosest(points, k);
        for(int i=0;i<k;i++)
            System.out.println(res[i][0] + " # " + res[i][1]);
    }

    public int[][] kClosest(int[][] points, int k) {
        Comparator<Point> comp = new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                int disto1 = o1.x*o1.x + o1.y*o1.y;
                int disto2 = o2.x*o2.x + o2.y*o2.y;

                return disto2 - disto1;
            }
        };
        PriorityQueue<Point> pq = new PriorityQueue<>(k, comp);
        for(int i=0;i<points.length;i++){
            pq.offer(new Point(points[i][0], points[i][1]));
            if(pq.size() > k)
                pq.remove();
        }
        int[][] res = new int[k][2];
        int i = 0;
        while(pq.size() > 0)    {
            Point p = pq.remove();
            res[i][0] = p.x;
            res[i][1] = p.y;
            i++;
        }
        return res;
    }

    class Point {
        int x;
        int y;
        public Point(int x1, int y1){
            x = x1;
            y = y1;
        }
    }
}
