package com.leetcode.google;

/*
https://leetcode.com/explore/interview/card/google/64/dynamic-programming-4/451/
 */

import java.util.Arrays;

public class LongestPalindromicSubstring {
    public static void main(String[] args) {
        new  LongestPalindromicSubstring().driver();
    }

    public void driver()    {
        //String s = "babad";
        String s = "babaddtattarrattatddetartrateedredividerb";
        System.out.println(s + "  => " + longestPalindrome(s));
        System.out.println("substring = " + sub);
    }

    private String longestPalindrome(String s) {
        sub = null;
        sublen = -1;
        int[][] dp = new int[s.length()][s.length()];
        int n = s.length();
        for(int i=0;i<n;i++)
            Arrays.fill(dp[i], -1);
        lps(s,0,s.length()-1,dp);
        return sub;
    }

    String sub;
    int sublen = -1;

    private int lps(String s, int st, int end, int[][] dp)   {
        if(end < st )
            return 0;
        if(st == end) {
            if(sublen == -1) {
                sub = s.substring(st, end+1);
            }
            return 1;
        }
        if(dp[st][end] != -1)
            return dp[st][end];
        else {
            if (s.charAt(st) == s.charAt(end)) {
                int len = end - st - 1;
                if (len == lps(s, st + 1, end - 1, dp)) {
                    if (sublen < (len + 2)) {
                        sublen = len + 2;
                        sub = s.substring(st, end + 1);
                    }
                    return len + 2;
                }
            }
            int t1 = lps(s, st + 1, end, dp);
            int t2 = lps(s, st, end - 1, dp);
            dp[st][end] = Math.max(t1, t2);
        }
        return dp[st][end];
    }
}
