package com.tic.tac.toe;

import java.util.Random;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) {
        new TicTacToe().driver();
    }

    Integer[][] mat = new Integer[3][3];
    int count=0;
    int userInput = 1;
    Scanner scanner = new Scanner(System.in);
    String name = "";
    boolean isComputerIntelligent = true;

    class Result {
        boolean isGameFinished = false;
        String winnerName;
    }

    class Position {
        int row;
        int col;

        int getRow()    {
            return row;
        }

        int getCol()    {
            return col;
        }

        public Position(int pos)    {
            switch (pos) {
                case 1:row=0;col=0;break;
                case 2:row=0;col=1;break;
                case 3:row=0;col=2;break;

                case 4:row=1;col=0;break;
                case 5:row=1;col=1;break;
                case 6:row=1;col=2;break;

                case 7:row=2;col=0;break;
                case 8:row=2;col=1;break;
                case 9:row=2;col=2;break;

                default:break;
            }
        }

    }

    private void driver()    {
        startGame();
        print();
        for(int i=0;i<5;i++)    {
            getUserInput();
            computerChance();
        }
    }

    private void setMat(int r, int c, int val)   {
        mat[r][c] = val;
        Result res = isGameFinished(r,c);
        if(res.isGameFinished)  {
            print();
            if(res.winnerName.equalsIgnoreCase(name))
                System.out.println("Congratulations !!! "+res.winnerName+" has won.");
            else
                System.out.println("You Lost !!! "+res.winnerName+" has won.");
            System.exit(1);
        }
        count++;
        if(count >= 9)  {
            print();
            System.out.println("Sorry !!! match has Drawn");
            System.exit(1);
        }
    }

    private Result isGameFinished(int r, int c) {
        int winner = -1;
        if( mat[r][0] != null && mat[r][0] == 1 && mat[r][1] != null && mat[r][1] == 1 && mat[r][2] != null && mat[r][2] == 1)
            winner = 1;
        if(mat[0][c] != null && mat[0][c]==1 && mat[1][c] != null && mat[1][c]==1 && mat[2][c] != null && mat[2][c]==1)
            winner = 1;
        if(mat[0][0] != null && mat[0][0]==1 && mat[1][1] != null && mat[1][1]==1 && mat[2][2] != null && mat[2][2]==1)
            winner = 1;

        if(mat[r][0] != null && mat[r][0]==0 && mat[r][1] != null && mat[r][1]==0 && mat[r][2] != null && mat[r][2]==0)
            winner = 0;
        if(mat[0][c] != null && mat[0][c]==0 && mat[1][c] != null && mat[1][c]==0 && mat[2][c] != null && mat[2][c]==0)
            winner = 0;
        if(mat[0][0] != null && mat[0][0]==0 && mat[1][1] != null && mat[1][1]==0 && mat[2][2] != null && mat[2][2]==0)
            winner = 0;

        Result res = new Result();
        res.isGameFinished = winner == -1 ? false : true;
        if(res.isGameFinished)  {
            if(winner == userInput)
                res.winnerName = name;
            else
                res.winnerName = "Computer";
            return res;
        } else  {
            return res;
        }
    }

    private void startGame()    {
        System.out.println("Welcome to Adrija's Tic-Tac-Toe");
        String line = null;
        do {
            System.out.println("What is your name ?");
            line = scanner.nextLine();
        } while (line==null || line.trim().equals(""));
        name = line;
        System.out.println("What you want? X or O ?");
        line = scanner.nextLine();
        if(line!=null)  {
            if(line.trim().equalsIgnoreCase("x"))
                userInput = 1;
            else
                userInput = 0;
        }
        System.out.println("Thank you "+name+" you have chosen "+(userInput==1?"X":"O"));
        System.out.println("Computer will play as "+(userInput==1?"O":"X"));
        System.out.println("Lets begin ......... ");
    }

    private void getUserInput() {
        System.out.println("-------------------------- " + name + " ----------------------------------");
        String line = null;
        int num = -1;
        do {
            System.out.println("Enter box no ");
            line = scanner.nextLine();
            num = Integer.parseInt(line);
        } while (line == null || !isValidNo(num));

        int r = -1;
        int j = -1;
        Position pos = new Position(num);
        r = pos.getRow();
        j = pos.getCol();
        setMat(r,j,userInput);
        print();
    }

    private void computerChance()   {
        System.out.println("-------------------------- Computer ----------------------------------");
        int r = -1;
        int c = -1;

        RetRowCol retRowCol = findNextMove();
        if(retRowCol.r != -1 && retRowCol.c != -1)  {
            r = retRowCol.r;
            c = retRowCol.c;
        }   else {
            Random rand = new Random();
            do{
                r = rand.nextInt(3);
                c = rand.nextInt(3);
            } while(mat[r][c] != null );
        }
        int computerInput = userInput == 1? 0:1;
        setMat(r,c,computerInput);
        print();
    }

    class RetRowCol {
        int r;
        int c;
        public RetRowCol(int r, int c)  {
            this.r = r;
            this.c = c;
        }
    }

    private RetRowCol findNextMove()    {
        if(userInput == 1)  {
            RetRowCol winningMove = findEmptySpace(-2);
            if(winningMove.r == -1 && winningMove.c == -1)  {
                return findEmptySpace(2);
            }
        } else {
            RetRowCol winningMove = findEmptySpace(2);
            if(winningMove.r == 1 && winningMove.c == 1)  {
                return findEmptySpace(-2);
            }
        }
        return new RetRowCol(-1,-1);
    }

    private RetRowCol findEmptySpace(int sumEquals) {
        if (!isComputerIntelligent)  {
            return new RetRowCol(-1,-1);
        }
            //find empty in row
            for (int r = 0; r < 3; r++) {
                int sum = 0;
                int emptyColNo = -1;
                for (int i = 0; i < 3; i++) {
                    sum += mat[r][i] != null ? mat[r][i] == 1 ? 1 : -1 : 0;
                    if(mat[r][i] == null)
                        emptyColNo = i;
                    if(sum == sumEquals)    {
                        //find missing 1 from that row.
                            if(emptyColNo >=0 && mat[r][emptyColNo] == null)  {
                                return new RetRowCol(r,emptyColNo);
                            }
                    }
                }
            }
            //find empty in column
            for (int c = 0; c < 3; c++) {
                int sum = 0;
                int emptyRowNo = -1;
                for (int i = 0; i < 3; i++) {
                    sum += mat[i][c] != null ? mat[i][c] == 1 ? 1 : -1 : 0;
                    if(mat[i][c] == null)
                        emptyRowNo = i;
                    if(sum == sumEquals)    {
                        //find missing 1 from that row.
                            if(emptyRowNo >=0 && mat[emptyRowNo][c] == null )  {
                                return new RetRowCol(emptyRowNo,c);
                            }
                    }
                }
            }
            //find empty in diagonal
            int sum =0;
            int emptyDiagnoalIndex = -1;
            for(int i=0;i<3;i++)    {
                sum += mat[i][i] != null ? mat[i][i] == 1 ? 1 : -1 : 0;
                if(mat[i][i] == null)
                    emptyDiagnoalIndex = i;
            }
            if(sum == sumEquals)    {
                if(emptyDiagnoalIndex >= 0 && mat[emptyDiagnoalIndex][emptyDiagnoalIndex] == null)
                        return new RetRowCol(emptyDiagnoalIndex,emptyDiagnoalIndex);
            }
            //find empty.
            sum = 0;
            emptyDiagnoalIndex = -1;
            for(int i=0;i<3;i++)    {
                sum += mat[2-i][i] != null ? mat[2-i][i] == 1 ? 1 : -1 : 0;
                if(mat[2-i][i] == null)
                    emptyDiagnoalIndex = i;
            }
            if(sum == sumEquals)    {
                if(emptyDiagnoalIndex >= 0 && mat[2-emptyDiagnoalIndex][emptyDiagnoalIndex] == null)
                    return new RetRowCol(2-emptyDiagnoalIndex,emptyDiagnoalIndex);
            }
        return new RetRowCol(-1,-1);
    }

    private boolean isValidNo(int no) {
        int r=-1,j=-1;
        if(no > 0 && no < 10 )  {
            Position pos = new Position(no);
            r = pos.getRow();
            j = pos.getCol();
            if(mat[r][j] == null)   return true;
            else return false;
        }   else
            return false;
    }

    private void print()    {
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(mat[i][j] == null)
                    System.out.printf("    ");
                else if(mat[i][j] == 1)
                    System.out.printf("%3c ",'X');
                else
                    System.out.printf("%3c ",'O');
                if(j<2)
                    System.out.printf(" |");
            }
            System.out.println();
            System.out.println("-----------------");
        }
    }

}