package com.microsoft.questions;

    /*
        https://leetcode.com/problems/rotate-image/solution/

        Rotate an image to right by 90 degree.
    */

import java.util.Arrays;

public class RotateImageToRightBy90 {

    public static void main(String[] args) {
        new RotateImageToRightBy90().driver();
    }

    public void driver()    {
        int[][] mat = new int[][]   {
                {1,2,3},
                {4,5,6},
                {7,8,9}
            };
        rotate(mat);
        for(int i=0;i<mat.length;i++)
            System.out.println(Arrays.toString(mat[i]));
    }

    public void rotate(int[][] mat) {
        int n = mat.length;

        for(int i = 0; i < n; i++)    {
            for(int j = i; j < n; j++)    {
                int temp = mat[i][j];
                mat[i][j] = mat[j][i];
                mat[j][i] = temp;
            }
        }

        for(int r = 0; r < n; r++)    {
            for(int cs=0,ce=n-1; cs <= ce ;cs++,ce--)    {
                int temp = mat[r][cs];
                mat[r][cs] = mat[r][ce];
                mat[r][ce] = temp;
            }
        }
    }

}