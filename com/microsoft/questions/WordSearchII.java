package com.microsoft.questions;

/*
https://leetcode.com/problems/word-search-ii/
*/

import java.util.*;

public class WordSearchII {

    public static void main(String[] args) {
        char[][] a = {
                {1,2,3},
                {4,5,6}
        };
        //m x n 2 x 3
        //a.length = row count
        //a[0].length = column count
        System.out.println(a.length);
        System.out.println(a[0].length);

        new WordSearchII().driver();
    }

    public void driver()    {
        //char[][] board = getBoard();
        //String[] words = new String[]{"oath","dig","dog","dogs"};
        char[][] board = getBoard1();
        String[] words = new String[]{"oath","pea","eat","rain","hklf", "hf"};
        findWords(board,words);
    }

    public List<String> findWords(char[][] board, String[] words) {
        Set<String> opSet = new HashSet<>();
        int rc = board.length;
        int cc = board[0].length;
        boolean[][] visited = new boolean[rc][cc];
        Trie trie = formTrieFromDictionary(words);

        for(int r=0;r<rc;r++)   {
            for(int c=0;c<cc;c++)   {
                //StringBuilder sb = new StringBuilder();

                findWordFromTrie(board,r,c,visited,trie,"",opSet);
            }
        }
        return new ArrayList<>(opSet);
    }

    public void findWordFromTrie(char[][] board, int r, int c, boolean[][] visited, Trie trie, String sb, Set<String> op)  {
        if(!trie.child.containsKey(board[r][c]))
            return;

        char cur = board[r][c];
        trie = trie.child.get(cur);
        //sb.append(cur);
        sb=sb+cur;
        if(trie.isWord) {
            System.out.println(sb.toString());
            op.add(sb.toString());
        }

        visited[r][c] = true;

        if(isValid(board,visited,r+1,c))    {
            findWordFromTrie(board,r+1,c,visited,trie,sb,op);
        }
        if(isValid(board,visited,r,c+1))    {
            findWordFromTrie(board,r,c+1,visited,trie,sb,op);
        }
        if(isValid(board,visited,r-1,c))    {
            findWordFromTrie(board,r-1,c,visited,trie,sb,op);
        }
        if(isValid(board,visited,r,c-1))    {
            findWordFromTrie(board,r,c-1,visited,trie,sb,op);
        }
        visited[r][c] = false;
    }

    public boolean isValid(char[][] board, boolean[][] visited, int r, int c)    {
        int rowCount = board.length;
        int colCount = board[0].length;
        if(r < 0 || r >= rowCount || c < 0 || c >= colCount)    {
            return false;
        }
        if(visited[r][c])
            return false;
        return true;
    }

    public Trie formTrieFromDictionary(String[] words)    {
        Trie root = new Trie();
        for(int i=0;i<words.length;i++) {
            addWordToTrie(root,words[i]);
        }
        //traverseTrie(root,"");
        return root;
    }

    public void traverseTrie(Trie root,String s) {
        if(root.isWord) {
            System.out.println(s);
        }
        for(char ch : root.child.keySet())  {
            traverseTrie(root.child.get(ch),s+ch);
        }
    }

    public void addWordToTrie(Trie root, String word) {
        int i = 0;
        int n = word.length();
        Trie trie = root;
        while(i<n) {
            char ch = word.charAt(i);
            if(trie.child.containsKey(ch))  {
                trie = trie.child.get(ch);
            } else {
                Trie newTrie = new Trie();
                trie.child.put(ch, newTrie);
                trie = trie.child.get(ch);
            }
            if(i==n-1)    {
                trie.isWord = true;
            }
            i++;
        }
    }
    public char[][] getBoard()  {
        char[][] board =
                {
                        {'o','a','a','n'},
                        {'e','t','a','e'},
                        {'t','h','d','o'},
                        {'i','f','l','g'}
                };
        return board;
    }

    public char[][] getBoard1() {
        char[][] board1 =
                {       {'o','a','a','n'},
                        {'e','t','a','e'},
                        {'i','h','k','r'},
                        {'i','f','l','v' }
                };
        return board1;
    }

    class Trie {
        Map<Character, Trie> child;
        boolean isWord;

        public Trie()   {
            isWord = false;
            child = new HashMap<>();
        }
    }

}