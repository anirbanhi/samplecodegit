package com.microsoft.questions;

import java.util.PriorityQueue;

/*
https://leetcode.com/problems/merge-k-sorted-lists/
 */
public class MergeKSortedLists {
    public static void main(String[] args) {
        new MergeKSortedLists().driver();
    }

    class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }

    }

    public void driver()  {
        ListNode l1 = new ListNode(1);
        l1.next = new ListNode(4);
        l1.next.next = new ListNode(5);
        l1.next.next.next = null;

        ListNode l2 = new ListNode(1);
        l2.next = new ListNode(3);
        l2.next.next = new ListNode(4);
        l2.next.next.next = null;

        ListNode l3 = new ListNode(2);
        l3.next = new ListNode(6);
        l3.next.next = null;

        ListNode[] list = new ListNode[3];
        list[0] = l1;
        list[1] = l2;
        list[2] = l3;

        ListNode ans = mergeKLists(list);
        while(ans!=null) {
            System.out.print(ans.val+" -> ");
            ans = ans.next;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {
        ListNode head = null ;
        ListNode tail = null ;

        //create a minheap with ListNode elements. We will store k elements at any one time in
        //this minheap. where k = lists.length
        PriorityQueue<ListNode> pq = new PriorityQueue<>((l1,l2) -> l1.val-l2.val);

        //Add first element of every list in this heap, if the element is not null.
        //this will handle [] case.
        for(int i=0;i<lists.length;i++) {
            if(lists[i] != null)
            pq.add(lists[i]);
        }

        //iterate untill minheap or pq is empty
        while(pq.size() > 0) {

            //remove minimum element and add next element from the list where this top of min heap
            //belongs to. This top element is now next inline sorted element.
            ListNode pqnode = pq.remove();
            if(pqnode.next != null) {
                pq.add(pqnode.next);
            }

            //head points to final answer list and tail points to last element.
            //for adding to answer just add element to next of tail and do tail.next.
            if(head == null)    {
                head = pqnode;
                tail = pqnode;
            } else {
                tail.next = pqnode;
                tail = tail.next;
            }
        }
        return head;
    }

}
