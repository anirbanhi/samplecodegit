package com.microsoft.questions;
/*
https://leetcode.com/problems/count-good-nodes-in-binary-tree/
 */

public class FindAllPathsInBT {

    public static void main(String[] args) {
        new FindAllPathsInBT().driver();
    }
    int count;
    public int driver()    {
        Node root = setup1();
        count = 0;
        findAllPaths(root,Integer.MIN_VALUE);
        System.out.println(count);
        return count;
    }

    public void findAllPaths(Node root, int maxTillNow)  {
        if(root == null)    return;
        if(maxTillNow <= root.val) {
            count++;
        }
        maxTillNow = Math.max(maxTillNow, root.val);

        findAllPaths(root.left,maxTillNow);
        findAllPaths(root.right,maxTillNow);
    }

    public Node setup() {
        Node root = new Node(4);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(1);
        root.left.right = new Node(5);
        root.right.left = new Node(6);
        root.right.right = new Node(7);
        return root;
    }

    public Node setup1()    {
        Node root = new Node(3);
        root.left = new Node(1);
        root.left.left = new Node(3);
        root.right = new Node(4);
        root.right.left = new Node(1);
        root.right.right = new Node(5);
        return root;
    }

    class Node{
        int val;
        Node left;
        Node right;

        public Node(int v)  {
            this.val = v;
            this.right = null;
            this.left = null;
        }
    }
}