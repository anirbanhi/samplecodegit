package com.microsoft.questions;

import java.util.Arrays;
/*
https://leetcode.com/problems/word-search/
 */
public class WordSearch {

    class Solution {
        public boolean pattern(int row,int col,char[][] board,String word,int index,int[][] vis,int n,int m){
            if(index==word.length())
                return true;
            if(row<0||col<0||row>=n||col>=m)
                return false;
            if(board[row][col]!=word.charAt(index))
                return false;
            if(vis[row][col]==1)
                return false;
            vis[row][col]=1;

            boolean left=pattern(row,col-1,board,word,index+1,vis,n,m);
            boolean right=pattern(row,col+1,board,word,index+1,vis,n,m);
            boolean top=pattern(row-1,col,board,word,index+1,vis,n,m);
            boolean bottom=pattern(row+1,col,board,word,index+1,vis,n,m);

            vis[row][col]=0;

            return left||right||top||bottom;
        }

        public boolean exist(char[][] board, String word) {
            int n=board.length;
            int m=board[0].length;
            int[][] vis=new int[n][m];
            for(int[] a:vis)Arrays.fill(a,0);
            for(int i=0;i<n;i++){
                for(int j=0;j<m;j++){
                    for(int[] a:vis) Arrays.fill(a,0);
                    if(pattern(i,j,board,word,0,vis,n,m)){
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
